<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");
class Currency extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Currency_model', '', TRUE);
    }

    public function index() {
        if (empty($this->session->userdata('log_sess_id_user')))
            redirect('Login');    

        $this->data['title']        = 'Currency Settings';
        $this->data['content']      = 'currency/index';
        $this->data['currency']     = $this->Currency_model->fetch();
        $this->load->view('template',$this->data);
    }

    public function api_table()
    {
        if(empty($this->session->userdata('log_sess_id_user')))
            redirect('Login');

        $list = $this->Currency_model->get_datatables();
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $cur) {
            $no++;
            $row = array();
            $row['code']        = '<center>'.$cur->code.'</center>';
            $row['label']       = '<center>'.$cur->label.'</center>';
            $row['value']       = '<div class="text-right"><span class="pull-left">Rp.</span>' . number_format($cur->value,2,',','.') . '</div>';
            $row['last_update'] = date('d F Y', strtotime($cur->last_update));
            $row['action']      = '<center><a href="javascript:void(0);" data-code="'.$cur->code.'" class="btn btn-xs btn-danger btn-delete">Hapus</a></center>';
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Currency_model->count_all(),
            "recordsFiltered" => $this->Currency_model->count_filtered(),
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function create()
    {
        if(empty($this->session->userdata('log_sess_id_user')))
            redirect('Login');

        try 
        {
            $label = $this->input->post('label');
            $code = $this->input->post('code');

            $result = $this->Currency_model->create($label, $code);
            if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Data has been created</div>");
        } 
        catch (Exception $e) 
        {
            $this->session->set_flashdata('alert',"<div class='alert alert-warning'>".$e->getMessage()."</div>");
        }
        
        redirect('currency');
    }    

    public function delete($code)
    {
        if(empty($this->session->userdata('log_sess_id_user')))
            redirect('Login');

        try 
        {            
            $result = $this->Currency_model->delete($code);
            if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Data has been deleted</div>");
        } 
        catch (Exception $e) 
        {
            $this->session->set_flashdata('alert',"<div class='alert alert-warning'>".$e->getMessage()."</div>");
        }
        
        redirect('currency');        
    }

    public function recalculate()
    {
        if(empty($this->session->userdata('log_sess_id_user')))
            redirect('Login');

        try 
        {
            $result = $this->Currency_model->recalculate(TRUE);
            if($result['codestatus'] != 'S') throw new Exception($result['message'], 1);

            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Data has been deleted</div>");
        } 
        catch (Exception $e) 
        {
            $this->session->set_flashdata('alert',"<div class='alert alert-warning'>".$e->getMessage()."</div>");
        }
        
        redirect('currency');        
    }
}

?>