<?php
date_default_timezone_set("Asia/Jakarta");
class Contract extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('M_Contract', '', TRUE);
	}

	public $data = array(
        'ldt1'              => 'CONTRACT',
        'ldl1'              => 'index.php/Contract',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'Contract',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

	function index()
	{
		$this->data['title']	= 'Create Contract';
		$this->data['icon']		= 'fa fa-list';
		$this->data['content']	= 'Contract/index';
		$this->data['script'] 	= 'Contract/script_index';
		if($this->session->userdata('log_sess_level_user_role') == 4){
			$this->data['customer'] = $this->M_Contract->get_customer();
		}else{
			$this->data['customer'] = [];
		}

		$this->load->view('template', $this->data);
	}

	function DetailContract($id_contract=null)
	{
		$sql = "
		SELECT 
			CUSTOMER.COMPANY_NAME 
		FROM 
			TC_CONTRACT
		JOIN CUSTOMER ON TC_CONTRACT.ID_CUSTOMER = CUSTOMER.ID_CUSTOMER
		WHERE 
			TC_CONTRACT.ID = $id_contract
		";
		$customer = $this->M_Contract->query($sql);
		$customer = $customer[0]['COMPANY_NAME'];

		$detail_contract = $this->M_Contract->get_detail_contract($id_contract);
		foreach ($detail_contract as $val) {
			$desc = array(
				'contract_number' 	=> $val['CONTRACT_NUMBER'],
				'contract_desc'		=> $val['DESCRIPTION'],
				'customer'			=> $customer,
				'delivery_point'	=> $val['DELIVERY_POINT'],
				'delivery_address'	=> $val['DELIVERY_ADDRESS']
			);
		}

		$this->data['title'] = "Detail Contract";
		$this->data['icon']	= 'fa fa-list';
		$this->data['content'] = 'Contract/detail_contract';
		$this->data['script'] = 'Contract/script_detail_contract';
		$this->data['desc'] = $desc;
		
		$this->load->view('template', $this->data);
	}

	function ListContract()
	{
		$this->data['title']	= 'List Contract';
		$this->data['icon']		= 'fa fa-list';
		$this->data['content']	= 'Contract/list_contract';
		$this->data['script'] 	= 'Contract/script_list_contract';

		if($this->session->userdata('log_sess_level_user_role') == 4){
			$this->data['customer'] = $this->M_Contract->get_customer();
		}else{
			$this->data['customer'] = $this->M_Contract->get_all_customer();
		}

		$this->load->view('template', $this->data);
	}
}
?>