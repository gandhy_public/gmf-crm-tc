<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Customers extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('Customers_m', '', TRUE);
	}

    // ./Construct

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Customers',
        'ldl1'              => 'index.php/Customers',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'LIST CUSTOMER',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data
    // View

    function index()
    {
        redirect('customers/customerlist');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
				$this->data['content']                      = 'master/dashboard';
        $this->load->view('template',$this->data);
		}

    function create()
    {

        $this->data['title']                        = 'Create Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'master/create';
        $this->load->view('template', $this->data);
    }

    function customerlist()
    {
        $this->data['title']                        = 'List Customers';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'customers/list';
        $this->load->view('template', $this->data);
    }

    function update($NO_ACCOUNT)
    {
        $this->data['ldl2']                         = '<?php echo base_url(); ?>index.php/master/list';
        $this->data['ldt2']                         = 'List Order';
        $this->data['ldi2']                         = 'fa fa-list';

        $this->data['title']                        = 'Update Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'master/update';
        $this->load->view('template', $this->data);
    }



}
