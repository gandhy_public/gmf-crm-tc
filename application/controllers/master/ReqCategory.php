<?php defined('BASEPATH') OR exit('No direct script access allowed');
class ReqCategory extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('/master/ReqCategory_m', '', TRUE);
	}

    // ./Construct

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Request Category',
        'ldl1'              => 'index.php/Master/ReqCategory',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'POOLING PBTH',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data
    // View

    function index()
    {
        redirect('master/reqcategory/datalist');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
				$this->data['content']                      = 'master/req_category';
        $this->load->view('template',$this->data);
		}

    function create()
    {
			$param = $this->input->post();
			// print_r($param);
			// die;

			if($this->ReqCategory_m->insert_request_category($param)){
				redirect('master/ReqCategory/datalist');
			}else{
				redirect('master/ReqCategory/datalist');
			}
    }

    function datalist()
    {
        $this->data['title']                        = 'List Requirement Category';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'master/req_category';
        $this->load->view('template', $this->data);
    }

		function update($id)
		{
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->ReqCategory_m->update_request_category($param)){
				redirect('master/reqcategory/datalist');
			}else{
				redirect('master/reqcategory/datalist');
			}
		}

		function delete($id)
		{
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->ReqCategory_m->delete_request_category($param)){
				redirect('master/reqcategory/datalist');
			}else{
				redirect('master/reqcategory/datalist');
			}
		}
	}
