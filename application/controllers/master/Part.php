<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Part extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->model('/master/PartNumber_m', '', TRUE);
	}

    // ./Construct

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'PART NUMBER',
        'ldl1'              => 'index.php/Master/Part',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'POOLING PBTH',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'Pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data
    // View

    function index()
    {
        redirect('master/part/datalist');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
				$this->data['content']                      = 'master/dashboard';
        $this->load->view('template',$this->data);
		}

    function create()
    {
      	$param = $this->input->post();
				// print_r($param);
				// die;

				if($this->PartNumber_m->insert_part_number($param)){
					redirect('master/part/datalist');
				}else{
					redirect('master/part/datalist');
				}
    }

    function datalist()
    {
        $this->data['title']                        = 'List Part Number';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'master/part_number';
        $this->load->view('template', $this->data);
    }

    function update($id)
    {
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->PartNumber_m->update_part_number($param)){
				redirect('master/part/datalist');
			}else{
				redirect('master/part/datalist');
			}
		}
    function delete($id)
    {
			$param = $this->input->post();
			$param['ID'] = $id;
			// print_r($param);
			// die;

			if($this->PartNumber_m->delete_part_number($param)){
				redirect('master/part/datalist');
			}else{
				redirect('master/part/datalist');
			}
    }



}
