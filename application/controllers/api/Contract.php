<?php
date_default_timezone_set("Asia/Jakarta");
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
class Contract extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('M_Contract', '', TRUE);
	}

	function SelectCustomer()
	{
		$response["body"] = NULL;
        try{
            $sql = "
            SELECT
				COMPANY_NAME,
				ID_CUSTOMER
			FROM
				CUSTOMER
			WHERE 
				COMPANY_NAME LIKE '%".$_GET['q']."%'
			";

        	$result = $this->db->query($sql)->result_array();
        	$response = [];
            if(!$result) throw new Exception("No Data", 1);
            

            foreach ($result as $key) {
            	$response[] = [
            		'id' => $key['ID_CUSTOMER'],
            		'text' => $key['COMPANY_NAME'],
            	];
            }
        }catch(Exception $e){
        	$response[] = [
        		'id' => NULL,
        		'text' => $e->getMessage(),
        	];
        }
        echo json_encode($response);
	}

	function CreateContract()
	{
		$param = $this->input->post();

		$contract['CONTRACT_NUMBER'] = $param['CONTRACT_NUMBER'];
		$contract['ID_CUSTOMER']	= $param['CUSTOMER'];
		$contract['DELIVERY_POINT']	= $param['DELIVERY_POINT'];
		$contract['DELIVERY_ADDRESS']	= $param['DELIVERY_ADDRESS'];
		$contract['DESCRIPTION']	= $param['DESC_CONTRACT'];

		// $subparam['fleet']		= (isset($param['fleet']))?$param['fleet']:[];
		// $subparam['pn']			= (isset($param['pn']))?$param['pn']:[];

		$id = $this->M_Contract->create_contract($contract);
		if($id){
			/*$count_fleet 	= count($subparam['fleet']['TYPE']);
			$count_pn 		= count($subparam['pn']['ATA']);

			for($i=0; $i < count($subparam['fleet']['TYPE']) ; $i++){
				$paramFleet['REGISTRATION'] = $subparam['fleet']['AIRCRAFT_REGISTRATION'][$i];
				$paramFleet['TYPE'] = $subparam['fleet']['TYPE'][$i];
				$paramFleet['MSN'] = $subparam['fleet']['MSN'][$i];
				$paramFleet['MFG_DATE'] = $subparam['fleet']['MFN_GATE'][$i];
				$paramFleet['ID_CONTRACT'] = $id;
				$paramFleet['NAMA_FILE'] = "by form";

				$this->M_Contract->create_contract_fleet($paramFleet);
			}

			for($i=0; $i < $count_pn ; $i++){
				$paramPn['ID_CONTRACT'] = $id;
				$paramPn['ATA'] = $subparam['pn']['ATA'][$i];
				$paramPn['PART_NUMBER'] = $subparam['pn']['ID_PN'][$i];
				$paramPn['DESCRIPTION'] = $subparam['pn']['DESCRIPTION'][$i];
				$paramPn['ESS'] = $subparam['pn']['ESS'][$i];
				$paramPn['MEL'] = $subparam['pn']['MEL'][$i];
				$paramPn['TARGET_SLA'] = $subparam['pn']['TARGET_SLA'][$i];
				$sla = explode(" ", $subparam['pn']['TARGET_SLA'][$i]);
				$paramPn['WAKTU_SLA'] = $sla[0];
				$paramPn['SATUAN_SLA'] = $sla[1];
				$paramPn['NAMA_FILE'] = 'by form';
				$paramPn['TARGET_UNSERVICEABLE_RETURN'] = $subparam['pn']['TUR'][$i];
				$paramPn['SERVICE_LEVEL_CATEGORY'] = $subparam['pn']['SLC'][$i];
				$paramPn['TARGET_SERVICE_LEVEL'] = $subparam['pn']['TSL'][$i];

				$this->M_Contract->create_contract_pn($paramPn);
			}

			if (($count_fleet > 0) && ($count_pn > 0)) {
				$message = "Succesfully Create Contract";
			}else{
				$message = "Succesfully Create Contract, Please Upload Data Detail Contract";
			}*/
			$message = "Succesfully Create Contract, Please Upload Data Detail Contract";
			$con_num = $param['CONTRACT_NUMBER'];
			$this->session->set_flashdata('alert_success',$message);
			header('Location: '.base_url().'index.php/Contract/DetailContract/'.$id);
			//redirect("Contract/ListContract");
		}else{
			$this->session->set_flashdata('alert_failed','Create contract failed, please try again');
			header('Location: '.base_url().'index.php/Contract');
			//redirect('Contract');
		}
	}

	function ListPn($id_ct)
	{
		$dataPn = $this->M_Contract->get_list_pn($id_ct,FALSE,FALSE);

		$listPn = $dataPn['result'];		
		$total = $this->M_Contract->get_list_pn($id_ct,TRUE,FALSE);
		$filter = $this->M_Contract->get_list_pn($id_ct,FALSE,TRUE);
		
		$query = $dataPn['query'];

		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listPn as $item) {
	    	$no++;
            $row = array();
            $row['ata']		= "<center>".$item->ATA."</center>";
            $row['pn']		= $item->PART_NUMBER;
            $row['desc']	= $item->DESCRIPTION;
            $row['ess']		= "<center>".$item->ESS."</center>";
            $row['mel']		= "<center>".$item->MEL."</center>";
            $row['sla']		= "<center>".$item->TARGET_SLA."</center>";
						$row['id']		= $item->ID;
						$row['waktu_sla'] = $item->WAKTU_SLA;
						$row['satuan_sla'] = $item->SATUAN_SLA;
						$row['unserviceable_return'] = "<center>".$item->TARGET_UNSERVICEABLE_RETURN."</center>";
						$row['service_level_category'] = "<center>".$item->SERVICE_LEVEL_CATEGORY."</center>";
						$row['target_service_level'] = "<center>".($item->TARGET_SERVICE_LEVEL*100)."%"."</center>";
 
            $data[] = $row;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data,
			"query" => $query
	    );

	    echo json_encode($results);
	}

	function ListFlt($id_ct)
	{
		$dataFleet = $this->M_Contract->get_list_fleet($id_ct,FALSE,FALSE);

		$listFleet = $dataFleet['result'];		
		$total = $this->M_Contract->get_list_fleet($id_ct,TRUE,FALSE);
		$filter = $this->M_Contract->get_list_fleet($id_ct,FALSE,TRUE);
		
		$query = $dataFleet['query'];

		
		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listFleet as $item) {
	    	$no++;
            $row = array();
            $row['regis']	= $item->REGISTRATION;
            $row['type']	= $item->TYPE;
            $row['msn']		= $item->MSN;
            $row['mfg']		= $item->MFG_DATE;
			$row['id']		= $item->ID;
 
            $data[] = $row;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data,
			"query" => $query
	    );

	    echo json_encode($results);
	}

	function CekUpload()
	{
		$ct_id = $_POST['ct_id'];

		$sql_pn = "SELECT COUNT(*) as PN FROM TC_CONTRACT_PN WHERE ID_CONTRACT = '$ct_id'";
		$cek_pn = $this->M_Contract->query($sql_pn);

		$sql_fleet = "SELECT COUNT(*) as PN FROM TC_CONTRACT_FLEET WHERE ID_CONTRACT = '$ct_id'";
		$cek_fleet = $this->M_Contract->query($sql_fleet);

		/*$sql_deliv = "SELECT COUNT(*) as PN FROM MS_DELIVERY_POINT WHERE ID_CONTRACT = '$ct_id'";
		$cek_deliv = $this->M_Contract->query($sql_fleet);*/

		if (($cek_pn[0]['PN'] >= 1) || ($cek_fleet[0]['PN'] >= 1) /*|| ($cek_deliv[0]['PN'] >= 1)*/) {
			echo TRUE;
		}else{
			echo FALSE;
		}
	}

	function Updel()
	{
		$url_redir = $_POST['url'];
		if(empty($url_redir)) $url_redir = base_url();
		$ct_num = $_POST['ct_num'];
		$ct_id = $_POST['ct_id'];
		$file_type = explode(".", $_FILES['files']['name']);
		$file_type = strtolower($file_type[1]);
		if($file_type != "xlsx"){
			echo "<script language='JavaScript'>
					swal({
						title: 'Erorr',
						text: 'Type file is not .xlsx',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
		}else{
			$file_path = $_FILES['files']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);

			$list = $fleet = $deliv = [];

			foreach ($reader->getSheetIterator() as $sheet) {
				if (strtolower($sheet->getName()) == "pn list") {
					foreach ($sheet->getRowIterator() as $row) {
						$list[] = $row;
					}
				}elseif (strtolower($sheet->getName()) == "fleet") {
					foreach ($sheet->getRowIterator() as $row) {
						$fleet[] = $row;
					}
				} else{
					foreach ($sheet->getRowIterator() as $row) {
						$deliv[] = $row;
					}
				}
			}

			$delete_list 	= $this->M_Contract->delete_pn($ct_id);
			$delete_fleet 	= $this->M_Contract->delete_fleet($ct_id);
			// $delete_deliv 	= $this->contract_m->delete_deliv($ct_id);

			$reader->close();
			//die();
			if($delete_list && $delete_fleet) {
				$insert_list 	= $this->M_Contract->insert_pn($list,$ct_id,$_FILES['files']['name']);
				$insert_fleet 	= $this->M_Contract->insert_fleet($fleet,$ct_id,$_FILES['files']['name']);
				// $insert_deliv 	= $this->contract_m->insert_deliv($deliv,$ct_id,$_FILES['files']['name']);
				if($insert_list && $insert_fleet){
					echo "<script language='JavaScript'>
						swal({
							title: 'Upload Succesfully',
							text: 'Upload File is Succesfully',
							icon: 'success',
						}).then((value) => {
							window.location.href='".$url_redir."'
						});				
					</script>";
				}else{
					echo "<script language='JavaScript'>
						swal({
							title: 'Upload Failed',
							text: 'Upload File is Failed',
							icon: 'error',
						}).then((value) => {
							window.location.href='".$url_redir."'
						});				
					</script>";
				}
			}else{
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Failed',
						text: 'Delete previous data failed!',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}
		}
	}

	function Upload()
	{
		$url_redir = $_POST['url'];
		if(empty($url_redir)) $url_redir = base_url();
		$ct_num = $_POST['ct_num'];
		$ct_id = $_POST['ct_id'];
		$file_type = explode(".", $_FILES['files']['name']);
		$file_type = strtolower($file_type[1]);
		if($file_type != "xlsx"){
			echo "<script language='JavaScript'>
					swal({
						title: 'Erorr',
						text: 'Type file is not .xlsx',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
		}else{
			$file_path = $_FILES['files']['tmp_name'];
			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($file_path);

			foreach ($reader->getSheetIterator() as $sheet) {
				if (strtolower($sheet->getName()) == "pn list") {
					foreach ($sheet->getRowIterator() as $row) {
				        $list[] = $row;
				    }
				}elseif (strtolower($sheet->getName()) == "fleet") {
					foreach ($sheet->getRowIterator() as $row) {
				        $fleet[] = $row;
				    }
				}else{
					foreach ($sheet->getRowIterator() as $row) {
				        $deliv[] = $row;
				    }
				}
			}

			$insert_list 	= $this->M_Contract->insert_pn($list,$ct_id,$_FILES['files']['name']);
			$insert_fleet 	= $this->M_Contract->insert_fleet($fleet,$ct_id,$_FILES['files']['name']);
			//print("<pre>".print_r($insert,true)."</pre>");

			$reader->close();
			//die();
			if($insert_list && $insert_fleet) {
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Succesfully',
						text: 'Upload File is Succesfully',
						icon: 'success',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}else{
				echo "<script language='JavaScript'>
					swal({
						title: 'Upload Failed',
						text: 'Upload File is Failed',
						icon: 'error',
					}).then((value) => {
						window.location.href='".$url_redir."'
					});				
				</script>";
			}
		}
	}

	function ListCt()
	{
		if($this->session->userdata('log_sess_level_user_role') == 4){
			$id_customer = $this->session->userdata('log_sess_id_customer');
		}else{
			$id_customer = null;
		}

		$dataContract = $this->M_Contract->get_list_contract($id_customer,FALSE,FALSE);

		$listContract = $dataContract['result'];
		$total = $this->M_Contract->get_list_contract($id_customer,TRUE,FALSE);
		$filter = $this->M_Contract->get_list_contract($id_customer,FALSE,TRUE);
		
		$query = $dataContract['query'];
		
		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listContract as $item) {
	    	$no++;
            $row = array();
            $row['no'] 					= "<center>".$no."</center>";
            $row['contract_number']		= "<center><a href='".site_url('Contract/DetailContract/'.$item->ID)."'>".$item->CONTRACT_NUMBER."</a></center>";
            $row['customer']			= $item->COMPANY_NAME;
            $row['delivery_point']		= $item->DELIVERY_POINT;
            $row['delivery_address']	= str_replace('\r\n', "<br \>", json_encode($item->DELIVERY_ADDRESS));
            $row['description']			= $item->DESCRIPTION;
            $row['act']					= "<center>
            <button class='btn btn-success btn-xs btn_edit' data-x='".$item->ID."' data-y='".$item->CONTRACT_NUMBER."'><i class='fa fa-edit'></i></button>
            &nbsp&nbsp&nbsp
            <button class='btn btn-danger btn-xs btn_del' data-x='".$item->ID."' data-y='".$item->CONTRACT_NUMBER."'><i class='fa fa-trash'></i></button>
			</center>";
 
            $data[] = $row;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data,
			"query"	=> $query
	    );

	    echo json_encode($results);
	}

	function DetailCt()
	{
		$id = $this->input->post('id');
		$ct_number = $this->input->post('ct_number');

		$sql = "
			SELECT 
				ID_CUSTOMER,
				DESCRIPTION,
				DELIVERY_POINT,
				DELIVERY_ADDRESS,
				CONTRACT_NUMBER
			FROM 
				TC_CONTRACT
			WHERE 
				ID=$id AND 
				CONTRACT_NUMBER='$ct_number'
		";
		$data = $this->M_Contract->query($sql);
		$data_edit['description'] = $data[0]['DESCRIPTION'];
		$data_edit['customer'] = $data[0]['ID_CUSTOMER'];
		$data_edit['delivery_point'] = $data[0]['DELIVERY_POINT'];
		$data_edit['delivery_address'] = $data[0]['DELIVERY_ADDRESS'];
		$data_edit['contract_number'] = $data[0]['CONTRACT_NUMBER'];
		echo json_encode($data_edit);
	}

	function UpdateCt()
	{
		try {
			$id = $this->input->post('id');
			$ct_number = $this->input->post('CONTRACT_NUMBER');
			$delivery_point = $this->input->post('DELIVERY_POINT');
			$delivery_address = $this->input->post('DELIVERY_ADDRESS');
			$description = $this->input->post('DESC_CONTRACT');
			$customer = $this->input->post('CUSTOMER');
			$date = date('Y-m-d');

			if(empty($id)) throw new Exception("Update Failed", 1);

			$sql = "
				UPDATE TC_CONTRACT 
				SET 
					CONTRACT_NUMBER='$ct_number',
					DELIVERY_POINT='$delivery_point',
					DELIVERY_ADDRESS='$delivery_address',
					DESCRIPTION='$description',
					ID_CUSTOMER='$customer',
					UPDATE_AT='$date' 
				WHERE 
					ID='$id'
			";
			$update = $this->db->query($sql);
			if($update){
				$this->session->set_flashdata('alert_success','Update Contract Succesfully');	
			}else{
				throw new Exception("Update Failed", 1);
			}
		} catch (Exception $e) {
			$this->session->set_flashdata('alert_failed',$e->getMessage());
		}
		redirect('Contract/ListContract');
	}

	function DeleteCt()
	{
		$id = $this->input->post('id');
		$contract_number = $this->input->post('contract_number');
		$date = date('Y-m-d');

		$sql = "
			UPDATE TC_CONTRACT 
			SET 
				IS_DELETE=1, 
				UPDATE_AT='$date' 
			WHERE 
				ID=$id
		";
		$delete = $this->db->query($sql);
		if($delete){
			echo TRUE;
		}else{
			echo FALSE;
		}
	}
}
?>