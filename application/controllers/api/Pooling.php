<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pooling extends CI_Controller{


	private $column_show = ['REFERENCE', 'REQUIREMENT_CATEGORY_NAME', 'PN_NAME', 'PN_DESC', 'DELIVERY_POINT_CODE', 'AIRCRAFT_REGISTRATION', 'CUSTOMER_PO', 'CUSTOMER_RO', 'CUSTOMER_PO', 'CUSTOMER_RO', 'REQUEST_ORDER_DATE', 'REQUESTED_DELIVERY_DATE', 'STATUS_ORDER_NAME', 'STATUS_ORDER'];

    // Construct

	function __construct() {
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Pooling_model', '', TRUE);
	}



		function list_table(){
	        if($this->input->post('STATUS_ORDER'))
	        {	$param['st'] = $this->input->post('STATUS_ORDER');
	        }
	        if($this->input->post('UNSERVICE_STATUS'))
	        {
	            $param['us_st'] = $this->input->post('UNSERVICE_STATUS');
	        }
	        if($this->input->post('SERVICE_STATUS'))
	        {
	            $param['s_st'] = $this->input->post('SERVICE_STATUS');
	        }
			$param['draw'] = $this->input->post('draw');
			$param['order'] = $this->input->post('order');
			if ($this->input->post('columns')) {
				$param['column'] = $this->input->post('columns');
			}
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			// print_r($param['s_st']);

			$res = $this->Pooling_model->get_data_pooling($param);
			// echo $this->db->last_query();
			// $param['search']['value'] = '';
			
			// $extra_param = $this->find_search_column($this->input->post('column'));

			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$result = $this->Pooling_model->get_data_pooling($param);
			
			$totalRow = count($res);
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = count($res);

			$n = (int)$param['start'];
			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $result;
			// // $data['extra']						= $extra_param;

			echo json_encode($data);
		}

		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}

	function get_part_number(){
        $response["body"] = NULL;
        $contract = $_GET['contract'];

        try{
            $sql = "
            SELECT 
            	PART_NUMBER, 
            	DESCRIPTION
			FROM
				TC_CONTRACT_PN
			WHERE
				ID_CONTRACT = $contract AND
				PART_NUMBER LIKE '%".strtolower($_GET['q'])."%' AND 
				IS_DELETE = 0
			";

			$result  = $this->db->query($sql)->result_array();
			$response = [];
            if(!$result) throw new Exception("No Data", 1);

            $response[] = array(
            	'id' => 'OTHER',
            	'text' => 'OTHER PART NUMBER'
            );
            foreach ($result as $key) {
            	$response[] = [
            		'id' => $key['PART_NUMBER'],
            		'text' => $key['PART_NUMBER'],
            	];
            }
        }catch(Exception $e){
            $response[] = [
        		'id' => 'OTHER',
        		'text' => 'OTHER PART NUMBER'
        	];
        }
        echo json_encode($response);
    }

    function get_existing_email(){
        $response["body"] = NULL;
        $cust = $this->session->userdata('log_sess_id_user');

        try{
            $sql = "
            SELECT 
            	DISTINCT DESTINATION
            FROM
				TC_SEND_EMAIL
			WHERE
				CREATE_BY = '$cust' AND
				DESTINATION LIKE '%".strtolower($_GET['q'])."%'
			";

			$result  = $this->db->query($sql)->result_array();
			$response = [];
            if(!$result) throw new Exception("No Data", 1);

            foreach ($result as $key) {
            	$response[] = [
            		'id' => $key['DESTINATION'],
            		'text' => $key['DESTINATION'],
            	];
            }
        }catch(Exception $e){
            $response[] = [
        		'id' => '',
        		'text' => $e->getMessage()
        	];
        }
        echo json_encode($response);
    }

     public function get_fleet_list()
    {
        $response["body"] = NULL;
        $keyword = "'%".strtolower($_GET['q'])."%'";
        $contract = $_GET['contract'];

        try{
            $sql = "
            	SELECT 
            		REGISTRATION AS AIRCRAFT_REGISTRATION_NAME, 
            		ID
				FROM
					TC_CONTRACT_FLEET
				WHERE
					ID_CONTRACT = $contract AND
					REGISTRATION LIKE {$keyword} AND
					IS_DELETE = 0
			";

			$query  = $this->db->query($sql);
        	$result = $query->result_array();
            if($result){
                $response["status"] = 'success';
                $response["body"] = $result;
            }

            $result  = $this->db->query($sql)->result_array();
			$response = [];
            if(!$result) throw new Exception("No Data", 1);

            $response[] = array(
            	'id' => 'OTHER',
            	'text' => 'OTHER AIRCRAFT REGISTRATION'
            );
            foreach ($result as $key) {
            	$response[] = [
            		'id' => $key['ID'],
            		'text' => $key['AIRCRAFT_REGISTRATION_NAME'],
            	];
            }
        }catch(Exception $e){
            $response[] = array(
            	'id' => 'OTHER',
            	'text' => 'OTHER AIRCRAFT REGISTRATION'
            );
        }

        // header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function get_contract_list()
    {
        $response["body"] = NULL;
        if(isset($_GET['q'])){
        	$keyword = "'%".strtolower($_GET['q'])."%'";
        	$script = "AND DESCRIPTION LIKE $keyword";
        	$select = "SELECT ID, DESCRIPTION";
        }else{
        	$script = "";
        	$select = "SELECT ID as id, DESCRIPTION as text";
        }
        $customer = $_GET['customer'];

        try{
            $sql = "$select
            		FROM
						TC_CONTRACT
					WHERE
							ID_CUSTOMER = '$customer'
							$script
							AND IS_DELETE = 0
					";

			$query  = $this->db->query($sql);
        	$result = $query->result_array();
            if($result){
                $response["status"] = 'success';
                $response["body"] = $result;
            }
        }catch(Exception $e){
            error_log($e);
        }

        // header('Content-Type: application/json');
        echo json_encode($response);
    }

    function GetListOrder(){
		$st_order = $this->input->post('st_order');
		$st_service = $this->input->post('st_service');
		$st_unservice = $this->input->post('st_unservice');
		$contract = $this->input->post('contract');

		$dataProject = $this->Pooling_model->get_list_order($st_order,$st_service,$st_unservice,$contract,FALSE,FALSE);

		$listProject = $dataProject['result'];
		//print('<pre>'.print_r($listProject,TRUE).'</pre>');die();
		$total = $this->Pooling_model->get_list_order($st_order,$st_service,$st_unservice,$contract,TRUE,FALSE);
		$filter = $this->Pooling_model->get_list_order($st_order,$st_service,$st_unservice,$contract,FALSE,TRUE);
		
		$query = $dataProject['query'];

		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listProject as $item) {
			$no++;
			$list = array();

			if($item->STATUS_ORDER == "1"){
				$action = '
				<button title="View Order" data-x="'.$item->ID.'" class="btn-view btn btn-primary btn-xs" type="button">
					<i class="fa fa-edit"></i>View
				</button>
				<br><br>
				<button title="Confirm Order" data-x="'.$item->ID.'" class="btn-confirm btn btn-success btn-xs" type="button">
					<i class="fa fa-check"></i>Confirm
				</button>
				<br><br>
				<button title="Cancel Order" data-x="'.$item->ID.'" class="btn-cancel btn btn-danger btn-xs" type="button">
					<i class="fa fa-close"></i>Cancel
				</button>
				';
			}elseif($item->STATUS_ORDER == "2"){
				$action = '
				<button title="View Order" data-x="'.$item->ID.'" class="btn-view btn btn-primary btn-xs" type="button">
					<i class="fa fa-edit"></i>View
				</button>
				<br><br>
				<button title="Complete Order" data-x="'.$item->ID.'" class="btn-complete btn btn-warning btn-xs" type="button">
					<i class="fa fa-check"></i>Complete
				</button>
				';
			}elseif($item->STATUS_ORDER == "3" || $item->STATUS_ORDER == "4"){
				$action = '
				<button title="View Order" data-x="'.$item->ID.'" class="btn-view btn btn-primary btn-xs" type="button">
					<i class="fa fa-edit"></i>View
				</button>
				';
			}else{
				$action = "No Action";
			}

			$level_user = $this->session->userdata('log_sess_level_user_role');
			if($level_user == "4"){
				$action = '
				<button title="View Order" data-x="'.$item->ID.'" class="btn-view btn btn-primary btn-xs" type="button">
					<i class="fa fa-edit"></i>View
				</button>
				';
			}

			$list['action'] = $action;

			$st_order = $item->STATUS_ORDER;
			if($st_order == "1"){
				$st_order = "Open Order & Waiting Approve";
			}elseif($st_order == "2"){
				$st_order = "In Progress";
			}elseif($st_order == "3"){
				$st_order = "Close";
			}elseif($st_order == "4"){
				$st_order = "Cancel Order";
			}
			$list['st_order'] = $st_order;

			$st_service = $item->STATUS_SERVICE;
			if($st_service == "1"){
				$st_service = "Waiting Delivery";
			}elseif($st_service == "2"){
				$st_service = "Delivered";
			}
			$list['st_service'] = $st_service;

			$st_unservice = $item->STATUS_UNSERVICE;
			if($st_unservice == "1"){
				$st_unservice = "Waiting Delivery";
			}elseif($st_unservice == "2"){
				$st_unservice = "Delivered";
			}
			$list['st_unservice'] = $st_unservice;

			$list['reff'] = $item->REFERENCE;
			$list['category'] = $item->REQUIREMENT_CATEGORY_NAME;
			$list['pn'] = $item->PART_NUMBER;
			$list['pn_desc'] = $item->PART_DESCRIPTION;
			$list['destination'] = $item->DESTINATION;
			$list['ac'] = $item->AC_REGISTRATION;
			$list['customer'] = $item->CUSTOMER_ORDER;
			$list['request'] = $item->REQUEST_BY;
			$request_date = date_create($item->CREATE_AT);
      		$request_date = date_format($request_date,'d F Y H:i');
			$list['request_date'] = $request_date;
			$request_date_deliv = date_create($item->DELIVERY_DATE);
      		$request_date_deliv = date_format($request_date_deliv,'d F Y H:i');
			$list['request_date_deliv'] = $request_date_deliv;
			
		    $target_deliv = date_create($item->DELIVERY_TARGET);
		    if(date_format($target_deliv,'d-m-Y') == "01-01-1900"){
		        $target_deliv = "";
		    }else{
		        $target_deliv = date_format($target_deliv,'d F Y H:i');
		    }
			$list['target_date'] = $target_deliv;

			$date_service = date_create($item->DATE_SERVICE);
			if(date_format($date_service,'d-m-Y') == "05-08-1998"){
		        $date_service = "";
		    }else{
		    	$date_service = date_format($date_service,'d F Y H:i');
		    }
			$list['date_service'] = $date_service;

			$date_unservice = date_create($item->DATE_UNSERVICE);
			if(date_format($date_unservice,'d-m-Y') == "05-08-1998"){
		        $date_unservice = "";
		    }else{
		    	$date_unservice = date_format($date_unservice,'d F Y H:i');
		    }
			$list['date_unservice'] = $date_unservice;

			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data,
			"query" => $query
	    );

	    echo json_encode($results);
	}

	function GetDetailOrder(){
		try {
			$order_id = $this->input->post('order_id');
			$data_order = $this->Pooling_model->get_detail_order($order_id);
			if(!count($data_order)) throw new Exception("No data order", 1);
			
			$resp = [
				'status' => TRUE,
				'data' => $data_order
			];
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'message' => $e->getMessage()
			];
		}
		echo json_encode($resp);
	}

	function GetDetailService(){
		try {
			$order_id = $this->input->post('order_id');
			$contract_id = $this->input->post('contract_id');
			$data_service = $this->Pooling_model->get_detail_service($order_id,$contract_id);
			if(!count($data_service)) throw new Exception("No data order", 1);
			$data_service[0]['TARGET_UNSERVICE_RETURN'] = date('Y-m-d H:i:s', strtotime("+".$data_service[0]['TUR'], strtotime($data_service[0]['DELIVERY_DATE'])));
			
			$resp = [
				'status' => TRUE,
				'data' => $data_service
			];
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'message' => $e->getMessage(),
			];
		}
		echo json_encode($resp);
	}

	function GetDetailUnservice(){
		try {
			$order_id = $this->input->post('order_id');
			$data_unservice = $this->Pooling_model->get_detail_unservice($order_id);
			if(!count($data_unservice)) throw new Exception("No data order", 1);
			
			$resp = [
				'status' => TRUE,
				'data' => $data_unservice
			];
		} catch (Exception $e) {
			$resp = [
				'status' => FALSE,
				'message' => $e->getMessage(),
			];
		}
		echo json_encode($resp);
	}

	function GetListEmail()
	{
		$st_email = $this->input->post('st_email');

		$listEmail = $this->Pooling_model->get_list_email($st_email,FALSE,FALSE);
		$total = $this->Pooling_model->get_list_email($st_email,TRUE,FALSE);
    	$filter = $this->Pooling_model->get_list_email($st_email,FALSE,TRUE);
		
		$data = array();
		$no = $_REQUEST['start'];
	    foreach ($listEmail as $item) {
			$no++;
			$list = array();

			if($item->STATUS){
				$status = "<center><span class='label label-success'>Success Send Email</span></center>";
			}else{
				$status = "<center><span class='label label-danger'>Failed Send Email</span></center>";
			}
			$list['status'] = $status;
			$list['reference'] = "<center>".$item->REFERENCE."</center>";
			$list['type'] = "<center>".$item->POOLING."</center>";
			$list['send'] = "<center>".$item->DESTINATION."</center>";

			$data[] = $list;
	    }

	    $results = array(
			"draw" => $_REQUEST["draw"],
			"recordsTotal" => $total,
			"recordsFiltered" => $filter,
			"data" => $data
	    );

	    echo json_encode($results);
	}

	function GetReportServiceableLevel() {
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$customer_id = $this->input->post('id_customer');
		$contract_id = $this->input->post('id_contract');

		$sql = "
		WITH RESULT AS (
			SELECT
				A.SERVICE_LEVEL_CATEGORY,
				A.TARGET_SERVICE_LEVEL,
				A.TARGET_SLA,
				CASE
					WHEN C.DELIVERY_DATE < B.DELIVERY_TARGET THEN 1
					ELSE 0
				END AS ACTUAL
				-- DATEDIFF( SECOND, B.CREATE_AT, B.DELIVERY_DATe ) / 3600.0 AS DELIV_DIFF,
				-- CASE A.SATUAN_SLA
					-- WHEN 'days' THEN ( A.WAKTU_SLA * 24 )
					-- ELSE A.WAKTU_SLA
				-- END AS SLA
			FROM
				TC_CONTRACT_PN A
				JOIN TC_POOLING_ORDER B ON ( A.PART_NUMBER = B.PN_ID AND B.SERVICE_ID IS NOT NULL AND A.IS_DELETE = 0 AND B.IS_DELETE = 0 )
				JOIN TC_POOLING_ORDER_SERVICE C ON ( B.PN_ID = C.PN_REQUEST)
			WHERE
				B.CREATE_AT >= "."'".str_replace("/", "-", $start_date)." 00:00:00'"."
				AND B.CREATE_AT <= "."'".str_replace("/", "-", $end_date)." 23:59:59'"."
				AND A.ID_CONTRACT = '$contract_id'
				AND B.CUSTOMER_ID = '$customer_id'
		),
		RESULT_TOTAL AS (
			SELECT 
				SERVICE_LEVEL_CATEGORY,
				TARGET_SERVICE_LEVEL,
				-- COUNT(ACTUAL) AS TOTAL,
				COUNT(ACTUAL) AS TOTAL,
				SUM(ACTUAL) AS ONTIME,
				(COUNT(ACTUAL) - SUM(ACTUAL)) AS DELAYED,
				TARGET_SLA
			FROM
				RESULT
			GROUP BY
				SERVICE_LEVEL_CATEGORY, TARGET_SERVICE_LEVEL, TARGET_SLA
		),
		RESULT_ONTIME AS (
			SELECT 
				SERVICE_LEVEL_CATEGORY,
				TARGET_SERVICE_LEVEL,
				COUNT(ACTUAL) AS ONTIME,
				TARGET_SLA
			FROM
				RESULT
			WHERE
				ACTUAL = 1
			GROUP BY
				SERVICE_LEVEL_CATEGORY, TARGET_SERVICE_LEVEL, TARGET_SLA
		),
		RESULT_DELAYED AS (
			SELECT 
				SERVICE_LEVEL_CATEGORY,
				TARGET_SERVICE_LEVEL,
				COUNT(ACTUAL) AS DELAYED,
				TARGET_SLA
			FROM
				RESULT
			WHERE
				ACTUAL = 0
			GROUP BY
				SERVICE_LEVEL_CATEGORY, TARGET_SERVICE_LEVEL, TARGET_SLA
		),
		RESULT_FINAL AS (
			SELECT
				A.SERVICE_LEVEL_CATEGORY,
				A.TARGET_SERVICE_LEVEL,
				A.TOTAL,
				A.TARGET_SLA,
				CASE 
					WHEN B.ONTIME IS NULL THEN 0
					ELSE B.ONTIME
				END AS ONTIME,
				CASE 
					WHEN C.DELAYED IS NULL THEN 0
					ELSE C.DELAYED
				END AS DELAYED
			FROM
				RESULT_TOTAL A
				LEFT JOIN RESULT_ONTIME B ON A.SERVICE_LEVEL_CATEGORY = B.SERVICE_LEVEL_CATEGORY
				LEFT JOIN RESULT_DELAYED C ON A.SERVICE_LEVEL_CATEGORY = C.SERVICE_LEVEL_CATEGORY
		)
		SELECT 
			*,
			( CONVERT(varchar(4), ( ONTIME / TOTAL ) * 100 ) + '%') AS ACHIEVEMENT
		FROM
			-- RESULT_FINAL 
			RESULT_TOTAL
		ORDER BY
			SERVICE_LEVEL_CATEGORY ASC";
		$servicable_chart_data = $this->Pooling_model->query($sql);

		$chart = array(); 
		foreach ($servicable_chart_data as $val) {
			$chart['labels'][] = $val->SERVICE_LEVEL_CATEGORY;
		}

		foreach ($servicable_chart_data as $val) {
			$chart['values']['target'][] = floatval($val->TARGET_SERVICE_LEVEL) * 100;
			$chart['values']['actual'][] = ($val->ONTIME / $val->TOTAL) * 100;
		}

		$result = [
			'list'  => $servicable_chart_data,  
			'chart' => $chart
		];
		echo json_encode($result);
	}

	function GetReportUnserviceableLevel() {
	$start_date = $this->input->post('start_date');
	$end_date = $this->input->post('end_date');
	$id_customer = $this->input->post('id_customer');
	$id_contract = $this->input->post('id_contract');
	
	$sql = "WITH RESULT AS (
			SELECT
				A.SERVICE_LEVEL_CATEGORY as KATEGORI,
				CASE
				WHEN B.DELIVERY_DATE < B.TARGET_UNSERVICE_RETURN THEN 1
				ELSE 0
				END AS ACTUAL
			FROM
				TC_CONTRACT_PN A
				JOIN TC_POOLING_ORDER_UNSERVICE B ON ( A.PART_NUMBER = B.PN_REMOVED AND A.IS_DELETE = 0 )
				JOIN TC_POOLING_ORDER C ON A.PART_NUMBER = C.PN_ID
			WHERE
					B.DELIVERY_DATE >= "."'".str_replace("/", "-", $start_date)." 00:00:00'"."
					AND B.DELIVERY_DATE <= "."'".str_replace("/", "-", $end_date)." 23:59:59'"."
					AND A.ID_CONTRACT = '$id_contract'
					AND C.CUSTOMER_ID = '$id_customer'
			),
			RESULT_TOTAL AS (
			SELECT 
				KATEGORI,
				COUNT(ACTUAL) AS TOTAL
			FROM
			RESULT
			GROUP BY
				KATEGORI
			),
			RESULT_ONTIME AS (
			SELECT 
				KATEGORI,
				COUNT(ACTUAL) AS ONTIME
			FROM
				RESULT
			WHERE
				ACTUAL = 1
			GROUP BY
				KATEGORI
			),
			RESULT_DELAYED AS (
			SELECT 
				KATEGORI,
				COUNT(ACTUAL) AS DELAYED
			FROM
				RESULT
			WHERE
				ACTUAL = 0
			GROUP BY
				KATEGORI
			),
			RESULT_FINAL AS (
			SELECT
				A.KATEGORI,
				A.TOTAL,
				CASE 
				WHEN C.DELAYED IS NULL THEN 0
				ELSE C.DELAYED
				END AS JUMLAH_DELAY,
				CASE 
				WHEN B.ONTIME IS NULL THEN 0
				ELSE B.ONTIME
				END AS JUMLAH_ONTIME
			FROM
				RESULT_TOTAL A
				LEFT JOIN RESULT_ONTIME B ON A.KATEGORI = B.KATEGORI
				LEFT JOIN RESULT_DELAYED C ON A.KATEGORI = C.KATEGORI
			)
			SELECT 
			*,
			( CONVERT(varchar(4), ( JUMLAH_ONTIME / TOTAL ) * 100 ) + '%') AS ACHIEVEMENT
			FROM
			RESULT_FINAL 
			ORDER BY
			KATEGORI ASC";

		$data_grafik = $this->Pooling_model->query($sql);

			$chart = array(); 
			foreach ($data_grafik as $val) {
			$chart['labels'][] = $val->KATEGORI;
			}

			foreach ($data_grafik as $val) {
			$chart['values']['actual'][] = ($val->JUMLAH_ONTIME / $val->TOTAL) * 100;
			}

			$result = [
			'list'  => $data_grafik,  
			'chart' => $chart
			];
	echo json_encode($result);
	}

	function check_customer_order(){
		$customer_order = $this->input->post('customer_order');
		$contract_id = $this->input->post('contract_id');

		$cek = $this->Pooling_model->cek_customer_order($customer_order,$contract_id);
		if($cek){
			echo "<p style='color:green'>Customer Order Available</p>";
		}else{
			echo "<p style='color:red'>Costumer Order Has Been Used</p>";
		}
	}

}
