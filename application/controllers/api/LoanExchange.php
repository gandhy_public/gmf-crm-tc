<?php defined('BASEPATH') OR exit('No direct script access allowed');
class LoanExchange extends CI_Controller{


	private $column_show = ['VBELN', 'MATNR', 'ARKTX', 'AUART'];

    // Construct

	function __construct() {
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Loan_exchange_model', '', TRUE);
	}


		function set_curl($number){

	        $curl = curl_init();
	        // $curl_obj = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"personnel_number\"\r\n\r\n".$number."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--" ;
	        curl_setopt_array($curl, array(
	          CURLOPT_URL => "http://172.16.232.60/hcis-dev/public/api/v1/external/employee",
	          CURLOPT_RETURNTRANSFER => true,
	          CURLOPT_ENCODING => "",
	          CURLOPT_MAXREDIRS => 10,
	          CURLOPT_TIMEOUT => 30,
	          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	          CURLOPT_CUSTOMREQUEST => "POST",
	          CURLOPT_POSTFIELDS => "personnel_number=".$number,
	          CURLOPT_HTTPHEADER => array(
	            "Authorization: Bearer 9dd3990778dc4ced0d668efbe8292a67",
	            "Cache-Control: no-cache",
	            "Content-Type: application/x-www-form-urlencoded",
	            "Postman-Token: 061d19d0-8b01-41a8-af88-96c9787fa9ee",
	            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
	          ),
	        ));

	        $response = curl_exec($curl);
	        $err = curl_error($curl);

	        curl_close($curl);

	        // print_r($response);
	        // print_r($err);
	        // die;

	        if ($err) {
	          return array();
	        } else {
	          $result = json_decode($response, 1);
	          return $result;
	        }
	    }

	    function select_user(){
	    	$salesperson = $this->Loan_exchange_model->getlistsalesperson();
			$s_result = array();
			$not_result = array();
			foreach ($salesperson as $key => $value) {
	            $tmp = $value;
	            $ernam = substr($value['ERNAM'], 1);
	            $kode = substr($value['ERNAM'],0,1);
	            if ($kode=='G'){
	            	if ($value['UNIT_CODE'] == 'TC') {
	            		$s_result[] = $value['ERNAM'];
	            	}
	            	else if ($value['UNIT_CODE'] == '') {
	            		$response = $this->set_curl($ernam);
		            // print_r($response);
			            if (!in_array($value['ERNAM'], $s_result)){
			            	if (!in_array($value['ERNAM'], $not_result)){
			            		if (!array_key_exists('status_code', $response)) {
					            	if (!empty($response)) {
					            	foreach ($response as $key => $value2) {
						                if (array_key_exists('unit_code', $value2)) {
					                        if (strpos($value2['unit_code'], 'TC') !== FALSE) {
												  	$s_result[] = $value['ERNAM'];
					                        	}
					                        else {
					                        	$not_result[] = $value['ERNAM'];
					                        	}
					                 $ins = $this->Loan_exchange_model->update_code_user($value['ERNAM'], $value2['unit_code']);	
					                        // var_dump($ins);
					                        // die;
						                 	}
					            		}
					            	}
					        	}
			            	}
			            }
	            	}  
		        }  
	    	}
	    	return $s_result;
	    }

		function list_table(){
	    	// $param['salesperson'] = $this->select_user();
	    	$param['draw'] = $this->input->post('draw');
	    	$param['order'] = $this->input->post('order');	
	    	if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}		
	    	// echo $this->db->last_query();
	    	$res = $this->Loan_exchange_model->getListDataOrder_count($param);
	    	$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;


			// $param['search']['value'] = '';
			
			// $extra_param = $this->find_search_column($this->input->post('column'));
			$n_result = [];
			$result = $this->Loan_exchange_model->getListDataOrder($param);
			foreach ($result as $key => $value) {
                $tmp = $value;
                if ($tmp['VBEGDAT']!='00000000') {
                    $tmp['START_DATE'] = date('d-m-Y', strtotime($tmp['VBEGDAT']));
                    }
                   else {
                	$tmp['START_DATE'] = " ";
                }
                if ($tmp['VENDDAT']!='00000000') {
                    $tmp['END_DATE'] = date('d-m-Y', strtotime($tmp['VENDDAT']));
                    }
                	else {
                	$tmp['END_DATE'] = " ";
                }
                if ($tmp['DELIVERY_DATE'] != null) {
                    $tmp['DELIVERY_DATE'] = date('d-m-Y', strtotime($tmp['DELIVERY_DATE']));
                    }
                   else {
                	$tmp['DELIVERY_DATE'] = " ";
                }
                if ($tmp['RETURN_DATE'] != null) {
                    $tmp['RETURN_DATE'] = date('d-m-Y', strtotime($tmp['RETURN_DATE']));
                    }
                	else {
                	$tmp['RETURN_DATE'] = " ";
                }
                $n_result[] = $tmp;
            }
		
			$totalRow = $res[0]['JUMLAH'];
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = $res[0]['JUMLAH'];

			$n = (int)$param['start'];

			foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;


			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $n_result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		function list_table_exchange(){
			// $param['salesperson'] = $this->select_user();
	    	$param['draw'] = $this->input->post('draw');
	    	$param['order'] = $this->input->post('order');	
	    	if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}
			$res = $this->Loan_exchange_model->getListDataOrderExchange_count($param);
			// echo $this->db->last_query();
			//echo $res;die();

			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;
			
			// $param['search']['value'] = '';
			
			$result = $this->Loan_exchange_model->getListDataOrderExchange($param);
		
			$totalRow = $res[0]['JUMLAH'];
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = $res[0]['JUMLAH'];

			$n = (int)$param['start'];

			foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;


			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  		= $result;
			// $data['extra']						= $extra_param;

			echo json_encode($data);
		}

		function list_request_exchange(){
			$param['draw'] = $this->input->post('draw');
			$param['order'] = $this->input->post('order');
			// $param['search']['value'] = '';
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}

			$res = $this->Loan_exchange_model->select_exchange_request($param);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;
			
			// echo $this->db->last_query();

			// $extra_param = $this->find_search_column($this->input->post('column'));

			$result = $this->Loan_exchange_model->select_exchange_request($param);
		
			$totalRow = count($res);
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = count($res);

			$n = (int)$param['start'];

			foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;


			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  		= $result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		function list_request_loan(){

			$param['draw'] = $this->input->post('draw');
	    	$param['order'] = $this->input->post('order');	
			// $param['search']['value'] = '';
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');

			}

			$res = $this->Loan_exchange_model->select_loan_request($param);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			// echo $this->db->last_query();
			
			// $extra_param = $this->find_search_column($this->input->post('column'));



			$result = $this->Loan_exchange_model->select_loan_request($param);
		
			$totalRow = count($res);
			// echo $this->db->last_query();

			// $totalRow = $this->Pooling_model->get_counter_tr();
			$totalFIltered = count($res);

			$n = (int)$param['start'];

			foreach ($result as $i => $v) $result[$i]['NO'] = ++$n;


			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  		= $result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}



}
