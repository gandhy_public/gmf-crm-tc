<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
class Retail extends CI_Controller{

	private $column_show = ['VBELN', 'MATNR', 'ARKTX', 'AUART'];

    // Construct

	function __construct() {
		parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Retail_model', '', TRUE);
	}



		function list_order2(){
			$param['tahun'] = $this->input->post('tahun');
			$param['kunnr'] = $this->input->post('kunnr');
			$param['bulan'] = $this->input->post('bulan');
			$param['filterby'] = $this->input->post('filterby');
			if ($param['kunnr'] == 'all') {
				$param['kunnr'] = '';
			}
			$param['draw'] = $this->input->post('draw');			
			$param['order'] = $this->input->post('order');
			

			if ($this->input->post('columns')) {
				$param['column'] = $this->input->post('columns');
			}
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			$res = $this->Retail_model->select_retail2_count($param);
			// echo $this->db->last_query();
		    $totalRow = $res[0]['JUMLAH'];
		    $totalFIltered = $res[0]['JUMLAH'];
			// $res2 = $this->Retail_model->select_retail2_count($param);
		    // $totalRow = count($res);
			

			// print_r($param['column']);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$n = (int)$param['start'];
			$tmp1 = [];
	        $tmp11 = [];
	        $tmp22 = [];
	  
	   //      $listStatus = $this->Retail_model->getStatusOrderPerTahun($param['tahun'],$param['kunnr'],$param['bulan'],$param['bulan']);
	   //      foreach ($listStatus as $key => $value) {
				// if ($value->JUMLAH != 0){
				// 	$tmp1[] = $value;
				// }
	   //      } 
	        
		    $result = $this->Retail_model->select_retail2($param);
		    // echo $this->db->last_query();
	        $txt = 50;
	        $tmp = [];
	        $n_result = []; 
	          if (isset($result)) {
	            foreach ($result as $key => $value) {
	                $tmp = $value;
	                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    // $teco_date = $tmp['TECO_DATE'];
	                    // $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
	                    // $e = date_create(date('Y'));
	                 
	                    // // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
	                    // $diff = (int) date_diff($s, $e)->format("%a");
	                    // // echo $diff;echo " diff<br>";
	                    // if ( $diff > 5) {
	                    //     $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
	                    //     // print_r($newdate);echo " newdate <br>";
	                    //     $date2 = date('Ymd', strtotime($newdate));
	                    // }else{
	                      
	                    $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
	                    // }

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }else{
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $date2 = date('Ymd');

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }
	               
	                $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
	                
	                // $tmp['STATUS'] = '';
	                if (($tmp['ERDAT_VBFA']!='00000000') and (($tmp['ERDAT_VBFA'])!='')) {
	                  $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['ERDAT_VBFA']));
	                } else {
	                  $tmp['DELIVERY_DATE'] = '';
	                }
	                // $tmp['REMARKS'] = '';
	                $tmp['ID_RETAIL'] = '';
	                $tmp['QUOTATION_DATE'] = '';
	                $tmp['APPROVAL_DATE'] = '';
	                $day_waiting = 0;

	                for ($i=1; $i <= $txt ; $i++) { 
	                  if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
	                      break;
	                    } 
	                  // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
	                  if (strpos($value["TXT".$i], 'SROA') !== false) {

	                      $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
	                      $nexti = $i+1;
	                      for ($ii=$i+1; $ii <= $txt ; $ii++)
	                          if ((strpos($value["TXT".$ii], 'WR') !== false) OR (strpos($value["TXT".$ii], 'SROA') !== false)){
	                             
	                              $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
	                              $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
	                          break 1;  
	                          }
	                      // $tmp[][$i] = $value["UDATE".$i];
	               
	                  }
	                }
	              // if ($tmp['ERDAT_VBFA'] != '') {
	                if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
	                  $tmp22[]= $value;
	                  $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
	                }else if ((strpos($tmp['STAT'], 'WR') !== false)){
	                // or (strpos($tmp['STAT'], 'UR') !== false)){
	                  $tmp11[] = $value;
	                  $tmp['TXT_STAT'] = 'Repair Started';
	                }
	              // }
	               if ($day_waiting != 0){
	              	$tmp['TAT_APPROVAL'] =  $day_waiting;
	              } else {
	              	$tmp['TAT_APPROVAL'] =  '';
	              }
	              $tmp['TAT'] = (date_diff($start, $end)->format("%a"))-$day_waiting ;
	              $n_result[] = $tmp;

	            }
	            // die;
	        }
	        $sum_wr1 = count($tmp11);
	        if ($sum_wr1 != 0){
	          array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
	        }
	        $sum_wr2 = count($tmp22);
	        if ($sum_wr2 != 0)
	        array_push($tmp1, ((object) array('STATUS'=> 'Approval Received/Repair Started', 'JUMLAH' => $sum_wr2)) );  	

		$data['draw']             = $param['draw'];
		$data['recordsTotal']     = $totalRow;
		$data['recordsFiltered']  = $totalFIltered;
		$data['data'] 			  = $n_result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		function order_status2(){
			$param['status'] = $this->input->post('status');
			$param['tahun'] = $this->input->post('tahun');
			$param['kunnr'] = $this->input->post('kunnr');
			$param['bulan'] = $this->input->post('bulan');
			$param['filterby'] = $this->input->post('filterby');
			if ($param['kunnr'] == 'all') {
				$param['kunnr'] = '';
			}
			// $param['kunnr'] = $this->input->post('kunnr');
			$param['draw'] = $this->input->post('draw');			
			$param['order'] = $this->input->post('order');
			
			
			if ($this->input->post('search')) {
				$param['search'] = $this->input->post('search');
			}
			$res = $this->Retail_model->select_retail_where_status2_count($param);
			// echo $this->db->last_query();
		    $totalRow = $res[0]['JUMLAH'];
			$totalFIltered = $res[0]['JUMLAH'];

			// $res2 = $this->Retail_model->select_retail_where_status2_count($param);
			// $totalFIltered = $res2[0]['JUMLAH'];
			// print_r($param['order']);
			$param['start'] = $this->input->post('start');
			$param['end'] = ($param['start']) + ($this->input->post('length')) + 1;

			$n = (int)$param['start'];
			$result   = $this->Retail_model->select_retail_where_status2($param);
        	// echo $this->db->last_query();
	        $txt = 50;
	        $tmp = [];
	        $tmp1 = [];
	        $n_result = []; 
	        if (isset($result)) {
	            foreach ($result as $key => $value) {
	                $tmp = $value;
	                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    // $teco_date = $tmp['TECO_DATE'];
	                    // $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
	                    // $e = date_create(date('Y'));
	                 
	                    // // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
	                    // $diff = (int) date_diff($s, $e)->format("%a");
	                    // // echo $diff;echo " diff<br>";
	                    // if ( $diff > 5) {
	                    //     $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
	                    //     // print_r($newdate);echo " newdate <br>";
	                    //     $date2 = date('Ymd', strtotime($newdate));
	                    // }else{
	                      
	                     $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
	                    // }

	                    // print_r($date1);echo "<br>";
	                    // print_r($date2);

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }else{
	                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
	                    $date2 = date('Ymd');

	                    $start = date_create($date1);
	                    $end = date_create($date2);

	                }

	                $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
	                
	                // $tmp['STATUS'] = '';
	                if (($tmp['ERDAT_VBFA']!='00000000') and (($tmp['ERDAT_VBFA'])!='')){
	                  $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['ERDAT_VBFA']));
	                } else {
	                  $tmp['DELIVERY_DATE'] = '';
	                }
	                // $tmp['REMARKS'] = '';
	                $tmp['ID_RETAIL'] = '';
	                $tmp['QUOTATION_DATE'] = '';
	                $tmp['APPROVAL_DATE'] = '';
	                $day_waiting = 0;

	                for ($i=1; $i <= $txt ; $i++) { 
	                  if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
	                      break;
	                    } 
	                  // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
	                  if (strpos($value["TXT".$i], 'SROA') !== false) {

	                      $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
	                      $nexti = $i+1;
	                      for ($ii=$i+1; $ii <= $txt ; $ii++)
	                          if ((strpos($value["TXT".$ii], 'WR') !== false) OR (strpos($value["TXT".$ii], 'SROA') !== false)){
	                             
	                              $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
	                              $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
	                          break 1;  
	                          }
	                      // $tmp[][$i] = $value["UDATE".$i];
	               
	                  }
	                }
	              // if ($tmp['ERDAT_VBFA'] != '') {
	                if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
	                  // $tmp22[]= $value;
	                  $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
	                }else if ((strpos($tmp['STAT'], 'WR') !== false)){
	                // or (strpos($tmp['STAT'], 'UR') !== false)){
	                  // $tmp11[] = $value;
	                  $tmp['TXT_STAT'] = 'Repair Started';
	                }
	              // }
	              if ($day_waiting != 0){
	              	$tmp['TAT_APPROVAL'] =  $day_waiting;
	              } else {
	              	$tmp['TAT_APPROVAL'] =  '';
	              }
	              $tmp['TAT'] = (date_diff($start, $end)->format("%a")) - $day_waiting;
	              // if ($tmp['TXT_STAT'] == $status ) {
	              //     $n_result[] = $tmp;
	              // }
	              $n_result[] = $tmp;
	            }
	          }

			$data['draw']             = $param['draw'];
			$data['recordsTotal']     = $totalRow;
			$data['recordsFiltered']  = $totalFIltered;
			$data['data'] 			  = $n_result;
			// $data['extra']						= $extra_param;


			echo json_encode($data);
		}

		private function find_search_column($arr){

				$val = [];

				foreach ($arr as $key => $value) {
					if ($value['search']['value']) {
							array_push($val, ['colname' => $column_show[$key], 'val' => $value['search']['value']]);
					}
				}

				return $val;
		}

	function select_customer(){
        // $response["tahun"] = NULL;
        $response["body"] = NULL;
        $keyword = "'%".strtolower($_GET['q'])."%'";
        $tahun = $_GET['tahun'];

        try{
            $sql = "SELECT DISTINCT
							S.KUNNR,
							S.NAME1
					FROM
							M_SALESORDER S
							-- M_STATUSPM B
							-- LEFT JOIN M_PMORDER L ON L.AUFNR = B.AUFNR
							-- LEFT JOIN M_SALESORDER S ON (1*S.VBELN) = L.KDAUF
					WHERE
							-- L.AUART = 'GA03' 
							-- AND B.PRCTR LIKE 'GMFTC%' 
							S.NAME1 LIKE {$keyword} 
							-- AND ( L.IDAT2 = '00000000' OR ( SUBSTRING ( L.IDAT2, 5, 4 ) ) = $tahun )";

			$query  = $this->db->query($sql);
        	$result = $query->result_array();
            if($result){
                $response["status"] = 'success';
                $response["body"] = $result;
            }
        }catch(Exception $e){
            error_log($e);
        }

        // header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function order_edit()
	{
		$aufnr = $this->input->post('AUFNR');

		$sql = "SELECT M.AUFNR, N.REMARKS FROM M_PMORDERH M LEFT JOIN TC_M_PMORDER N ON M.AUFNR = N.AUFNR
				WHERE M.AUFNR = $aufnr";
		// $data = $this->Contract_m->query($sql);
		$query  = $this->db->query($sql);
        $data = $query->result_object();
		$data_edit['AUFNR'] = $data[0]->AUFNR;
		$data_edit['REMARKS'] = $data[0]->REMARKS;
		echo json_encode($data_edit);
	}

	public function get_tat_aging() {
		$cur_customer = rtrim($this->input->post('cur_customer'));
		$cur_code = rtrim($this->input->post('cur_code'));
		$cur_type = rtrim($this->input->post('cur_type'));

		$tat_aging = $this->Retail_model->finance_tat_aging($cur_customer, $cur_code, $cur_type);

		$result = [
			[
				"code_range" => 1,
				"aging" => "0 - 30 Days",
				"total" => $tat_aging[0],
			],
			[
				"code_range" => 2,
				"aging" => "31 - 60 Days",
				"total" => $tat_aging[1],
			],
			[
				"code_range" => 3,
				"aging" => "61 - 90 Days",
				"total" => $tat_aging[2],
			],
			[
				"code_range" => 4,
				"aging" => "> 90 Days",
				"total" => $tat_aging[3],
			]
		];

		echo json_encode($result);
	}

	public function write_outstanding($type,$customer){
		$list_data = $this->Retail_model->finance_outstanding($type,$customer);

        $data_isi = array();
        $no = 1;
        foreach ($list_data as $key) {
        	$doc_date = DateTime::createFromFormat('Ymd', $key['FKDAT']);
        	array_push($data_isi, 
        		array(
        			$no++,
        			$key['VBELN_BILL'], //Billing Document
        			($doc_date ? $doc_date->format('Y-m-d') : '0000-00-00'), //Document Date
        			$key['BSTNK'], //RO Number
        			$key['VBELN'], //SO Number
        			$key['AUFNR'], //MO Number
        			$key['PART_NUMBER'], //Part Number
        			$key['PART_NAME'], //Part Name
        			$key['SERIAL_NUMBER'], //Serial Number
        			(str_replace(" ", "", $key['WAERK']) == 'IDR' ? $key['NETWR']*100 : $key['NETWR']), //Amount
        			$key['WAERK'], //Currency
        			$key['COMPANY_NAME'] //Customer Name
        		)
        	);
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
        $writer->setDefaultRowStyle($default_style);

        $file_name = date('Ymd') . "_" . "Invoice_Outstanding" . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "NO";
        $data_header[] = "Billing Document";
        $data_header[] = "Document Date";
        $data_header[] = "RO Number";
        $data_header[] = "SO Number";
        $data_header[] = "MO Number";
        $data_header[] = "Part Number";
        $data_header[] = "Part Name";
        $data_header[] = "Serial Number";
        $data_header[] = "Amount";
        $data_header[] = "Currency";
        $data_header[] = "Customer Name";

        $writer->addRow($data_header);
        $writer->addRows($data_isi);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
    }

    public function write_historical($start_date,$end_date,$customer){
		$list_data = $this->Retail_model->finance_historical($customer,$start_date,$end_date);

        $data_isi = array();
        $no = 1;
        foreach ($list_data as $key) {
        	$doc_date = DateTime::createFromFormat('Ymd', $key['FKDAT']);
        	array_push($data_isi, 
        		array(
        			$no++,
        			$key['VBELN_BILL'], //Billing Document
        			($doc_date ? $doc_date->format('Y-m-d') : '0000-00-00'), //Document Date
        			$key['BSTNK'], //RO Number
        			$key['VBELN'], //SO Number
        			$key['AUFNR'], //MO Number
        			$key['PART_NUMBER'], //Part Number
        			$key['PART_NAME'], //Part Name
        			$key['SERIAL_NUMBER'], //Serial Number
        			(str_replace(" ", "", $key['WAERK']) == 'IDR' ? $key['NETWR']*100 : $key['NETWR']), //Amount
        			$key['WAERK'], //Currency
        			$key['COMPANY_NAME'] //Customer Name
        		)
        	);
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
        $writer->setDefaultRowStyle($default_style);

        $file_name = date('Ymd') . "_" . "History_Transaction" . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "NO";
        $data_header[] = "Billing Document";
        $data_header[] = "Document Date";
        $data_header[] = "RO Number";
        $data_header[] = "SO Number";
        $data_header[] = "MO Number";
        $data_header[] = "Part Number";
        $data_header[] = "Part Name";
        $data_header[] = "Serial Number";
        $data_header[] = "Amount";
        $data_header[] = "Currency";
        $data_header[] = "Customer Name";

        $writer->addRow($data_header);
        $writer->addRows($data_isi);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
    }

}