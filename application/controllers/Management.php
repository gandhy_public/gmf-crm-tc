<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");
class Management extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }

        $this->load->model('Management_model', '', TRUE);
    }    

    function index() {
        redirect('Management/user');
    }

    public function user()
    {
        $this->data['role']             = $this->Management_model->getRole();
        $this->data['customer']         = $this->Management_model->getCustomer();
        $this->data['group']            = $this->Management_model->getGroup();
        $this->data['title']            = 'Management User';
        $this->data['content']          = 'management/user';
        $this->load->view('template',$this->data);
    }

    public function menu()
    {
        $this->data['title']            = 'Management Menu';
        $this->data['content']          = 'management/menu';
        $data_menu = $this->Management_model->getMenu();
        foreach ($data_menu as $key) {
            
            $menu_arr[$key->modul][] = [
                'menu' => $key->menu,
                'id' => $key->menu_id,
                'modul_id' => $key->modul_id,
            ];

        }
        $this->data['data_menu']             = $menu_arr;
        $this->load->view('template',$this->data);
    }

    public function list_user()
    {
        $query = $this->Management_model->get_datatables();
        
        $query = explode("FROM", $query);

        $table = explode("JOIN", $query[1]);
        $table_fix = $table[0];

        $order = explode("ORDER", $table[2]);
        $order_fix = $order[1];

        $where = explode("WHERE", $order[0]);
        if (count($where) == 2) {
            $where_fix = "WHERE (".$where[1].")";
        }else{
            $where_fix = "";
        }


        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length']+$_REQUEST['start'];

        $sql = "WITH TblDatabases AS
                (
                SELECT 
                TC_M_ROLE.name,
                TC_M_GROUP.name AS user_group,
                TC_M_USER.*,
                ROW_NUMBER() OVER (ORDER $order_fix) as Row 
                FROM TC_M_USER
                JOIN TC_M_ROLE ON TC_M_ROLE.id = TC_M_USER.role_id
                JOIN TC_M_GROUP ON TC_M_GROUP.id = TC_M_USER.group_id
                $where_fix
                )
                SELECT * FROM TblDatabases WHERE Row > $start and Row <= $end";
        
        $list = $this->Management_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {   
            if($detail->is_active){
                $status = "<span class='label label-success'>Active</span>";
            }else{
                $status = "<span class='label label-danger'>Non-Aktive</span>";
            }    

            if($detail->password == "using ldap"){
                $act = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='tooltip' title='Edit Data' data-x='".$detail->id."'><i class='fa fa-edit'></i></button>
            </center>";
            }else{
                $act = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='tooltip' title='Edit Data' data-x='".$detail->id."'><i class='fa fa-edit'></i></button>
            &nbsp&nbsp&nbsp
            <button class='btn btn-warning btn-xs btn_reset' data-toggle='tooltip' title='Reset Password' data-x='".$detail->id."'><i class='fa fa-lock'></i></button>
            </center>";
            }

            $no++;
            $row = array();
            $row['no']          = "<center>".$no."</center>";
            $row['name']        = "<center>".$detail->nama."</center>";
            $row['username']    = "<center>".$detail->username."</center>";
            $row['email']       = "<center>".$detail->email."</center>";
            $row['role']        = "<center>".$detail->name."</center>";
            $row['group']        = "<center>".$detail->user_group."</center>";
            $row['status']      = "<center>".$status."</center>";
            $row['act']         = "$act";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Management_model->count_all(),
            "recordsFiltered" => $this->Management_model->count_filtered(),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        //output to json format
        echo json_encode($output);
    }

    public function list_group()
    {
        $query = $this->Management_model->get_datatables_group();
        $query = explode("FROM", $query);

        $order = explode("ORDER", $query[1]);
        $order_fix = $order[1];

        $table = explode("WHERE", $order[0]);
        $table_fix = $table[0];
        if (count($table) == 2) {
            $where_fix = "WHERE (".$table[1].")";
        }else{
            $where_fix = "";
        }


        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length']+$_REQUEST['start'];

        $sql = "WITH TblDatabases AS
        (
        SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
        $where_fix
        )
        SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";
        
        $list = $this->Management_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {   
            if($detail->is_active){
                $status = "<span class='label label-success'>Active</span>";
            }else{
                $status = "<span class='label label-danger'>Non-Aktive</span>";
            }      
            $no++;
            $row = array();
            $row['no']          = "<center>".$no."</center>";
            $row['group']        = "<center>".$detail->name."</center>";
            $row['status']      = "<center>".$status."</center>";
            $row['act']         = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='tooltip' title='Edit Group' data-x='".$detail->id."'><i class='fa fa-edit'></i></button>
            &nbsp&nbsp&nbsp
            <button class='btn btn-warning btn-xs btn_detail' data-toggle='tooltip' title='Detail Group' data-x='".$detail->id."'><i class='fa fa-eye'></i></button>
            </center>";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Management_model->count_all_group(),
            "recordsFiltered" => $this->Management_model->count_filtered_group(),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        //output to json format
        echo json_encode($output);
    }

    public function create_user()
    {
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $role = $this->input->post('role');
        $customer = $this->input->post('customer');
        $group = $this->input->post('group');
        $ldap = $this->input->post('ldap');


        if (($ldap == 'on') && ($role != 4)) {
            $cek_ldap = $this->checkLdap($username,$password);
            if($cek_ldap){
                $param = [
                    'username' => $username,
                    'password' => "using ldap",
                    'nama' => $nama,
                    'email' => $email,
                    'role_id' => $role,
                    'group_id' => $group,
                    'is_active' => 1,
                    'create_at' => date('Y-m-d H:i:s'),
                    'customer_id' => $customer
                ];
                $cek_user = $this->Management_model->cekUser($username,$param['password']);
                if(count($cek_user) > 0){
                    $this->session->set_flashdata('alert',"<div class='alert alert-warning'>Data user sudah ada sebelumnya</div>");
                    redirect('Management/user');
                }else{
                    $insert = $this->Management_model->createUser($param);
                    if($insert){
                        $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create new user</div>");
                    }else{
                        $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed create new user</div>");
                    }
                }
            }else{
                $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Data LDAP tidak ada!</div>");
            }
        }else{
            $param = [
                'username' => $username,
                'password' => $password,
                'nama' => $nama,
                'email' => $email,
                'role_id' => $role,
                'group_id' => $group,
                'is_active' => 1,
                'create_at' => date('Y-m-d H:i:s'),
                'customer_id' => $customer
            ];
            $cek_user = $this->Management_model->cekUser($username,$password);
            if(count($cek_user) > 0){
                $this->session->set_flashdata('alert',"<div class='alert alert-warning'>Data user sudah ada sebelumnya</div>");
                redirect('Management/user');
            }else{
                $insert = $this->Management_model->createUser($param);
                if($insert){
                    $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create new user</div>");
                }else{
                    $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed create new user</div>");
                }
            }
        }
        
        redirect('Management/user');
    }

    public function create_group()
    {
        $group = $this->input->post('group');
        $param = [
            'name' => $group,
            'is_active' => "1",
            'create_at' => date('Y-m-d H:i:s'),
        ];
        $group_id = $this->Management_model->creatGroup($param);

        $modul = array_keys($this->input->post());
        for($a=1;$a<count($modul);$a++){
            $modul_name = $modul[$a];
            for($b=0;$b<count($_POST[$modul_name]);$b++){
                $menu_modul = $_POST[$modul_name][$b];
                $menu_modul = explode("_", $menu_modul);
                $menu_id = $menu_modul[0];
                $modul_id = $menu_modul[1];
                $data_insert[] = [
                    'group_id' => $group_id,
                    'modul_id' => $modul_id,
                    'menu_id' => $menu_id
                ];
            }
        }
        //print("<pre>".print_r($data_insert,true)."</pre>");die();
        $create_management = $this->Management_model->creatManagement($data_insert);
        if($create_management){
            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create management menu</div>");
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed create management menu</div>");
        }
        redirect('Management/menu');
    }
    
    public function detail_edit_user()
    {
        $id = $this->input->post('id');
        $sql = "SELECT * FROM TC_M_USER WHERE id=$id";
        $detail = $this->Management_model->query($sql);
        echo json_encode($detail);
    }

    public function detail_edit_group($id_group)
    {
        $view = $this->input->get('v');
        if($view == null){ $view = true; }
        $this->data['view'] = $view;
        if($view == true){
            $this->data['title'] = 'View Group Menu';
            $this->data['title_tab'] = 'View Group';
        }else{
            $this->data['title'] = 'Edit Group Menu';
            $this->data['title_tab'] = 'Edit Group';
        }
        $this->data['content'] = 'management/edit_group';
        $data_menu = $this->Management_model->getMenu();
        foreach ($data_menu as $key) {
            
            $menu_arr[$key->modul][] = [
                'menu' => $key->menu,
                'id' => $key->menu_id,
                'modul_id' => $key->modul_id,
            ];

        }
        $this->data['data_menu'] = $menu_arr;
        $master = $this->Management_model->getDetailGroup($id_group);
        /*print("<pre>".print_r($master,true)."</pre>");die();
        foreach ($master as $key) {
            $modul_id = $key->modul_id;
            $menu_id = $key->menu_id;
            $master_all[] = [
                'value' => $menu_id."_".$modul_id
            ];
        }*/
        $this->data['master'] = $master;
        $this->load->view('template',$this->data);
    }

    public function update_user()
    {
        $nama = $this->input->post('nama');
        //$username = $this->input->post('username');
        $email = $this->input->post('email');
        //$role = $this->input->post('role');
        $customer = $this->input->post('customer');
        $group = $this->input->post('group');
        $status = $this->input->post('status');
        $id = $this->input->post('id');

        $param = [
            //'username' => $username,
            'nama' => $nama,
            'email' => $email,
            //'role_id' => $role,
            'group_id' => $group,
            'is_active' => $status,
            'update_at' => date('Y-m-d H:i:s'),
            'customer_id' => $customer
        ];
        $update = $this->Management_model->updateUser($param,$id);
        if($update){
            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses update data user</div>");
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed update data user</div>");
        }
        redirect('Management/user');
    }

    public function update_group()
    {
        $group_id = $this->input->post('group_id');
        $group = $this->input->post('group');
        $status = $this->input->post('status');
        $param = [
            'name' => $group,
            'is_active' => $status,
            'update_at' => date('Y-m-d H:i:s'),
        ];
        $this->Management_model->updateGroup($param,$group_id);

        $modul = array_keys($this->input->post());
        //print("<pre>".print_r($modul,true)."</pre>");die();
        for($a=3;$a<count($modul);$a++){
            $modul_name = $modul[$a];
            for($b=0;$b<count($_POST[$modul_name]);$b++){
                $menu_modul = $_POST[$modul_name][$b];
                $menu_modul = explode("_", $menu_modul);
                $menu_id = $menu_modul[0];
                $modul_id = $menu_modul[1];
                $data_insert[] = [
                    'modul_id' => $modul_id,
                    'menu_id' => $menu_id
                ];
            }
        }
        //print("<pre>".print_r($data_insert,true)."</pre>");die();
        $create_management = $this->Management_model->updateManagement($data_insert,$group_id);
        if($create_management){
            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses update management menu</div>");
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed update management menu</div>");
        }
        redirect('Management/menu');
    }

    public function update_password()
    {
        $password = $this->input->post('password');
        $repassword = $this->input->post('repassword');
        $id = $this->input->post('id');

        if($password == $repassword){
            $param = [
                'password' => $password,
                'update_at' => date('Y-m-d H:i:s')
            ];
            $update = $this->Management_model->updateUser($param,$id);
            if($update){
                $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses reset password user</div>");
            }else{
                $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed reset password user</div>");
            }
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Password dan Confirm Password harus sama</div>");
        }
        redirect('Management/user');
    }

    public function checkLdap($username, $password)
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            /*print("<pre>".print_r(@$info,true)."</pre>");
            print("<pre>".print_r(@$infomail,true)."</pre>");
            print("<pre>".print_r(@$usermail,true)."</pre>");
            print("<pre>".print_r(@$bind,true)."</pre>");
            die();*/
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) OR (@$usermail == $username AND $bind)) {
                return true;
            } else {
                return false;
            }
        } else {
            echo "LDAP Connection trouble,, please try again 2/3 time";
        }
    }
}

?>