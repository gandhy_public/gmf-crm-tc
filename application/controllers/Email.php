<?php
date_default_timezone_set("Asia/Jakarta");
class Email extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        //$this->load->model('M_Contract', '', TRUE);
	}

	public $data = array(
        'ldt1'              => 'Pooling',
        'ldl1'              => 'index.php/Pooling',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'EMAIL',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

	function index()
	{
		if(isset($_GET['status_email'])){
          $st_email = $_GET['status_email'];
        }else{
          $st_email = "";
        }
        $this->data['st_email'] = $st_email;

        $this->data['title']                        = 'List Email';
        $this->data['icon']                         = 'fa fa-envelope';
        $this->data['content']                      = 'pooling/list_email';
        $this->load->view('template', $this->data);
	}
}
?>