<?php
date_default_timezone_set("Asia/Jakarta");

require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Contract extends CI_Controller
{
  function __construct()
	{
    parent::__construct();
    
    if (empty($this->session->userdata('log_sess_id_user'))) redirect('Login');

    $this->load->model('M_Contract', '', TRUE);

    $this->writer = WriterFactory::create(Type::XLSX);
    $this->headerStyle = (new StyleBuilder())->setFontBold()->setFontName('Arial')->setFontSize(10)->build();
    $this->contentStyle = (new StyleBuilder())->setFontName('Arial')->setFontSize(10)->setShouldWrapText(false)->build();
  }
  
  function ExportListContractToExcel()
  {
    $header = explode(",", $_POST['header']);
    $query = $_POST['query'];

    $file_name = date('Ymd')."_List_Contract.xlsx";

    $this->writer->openToBrowser($file_name);

    $listContract = $this->M_Contract->query($query);

    $content = [];
    $no = 0;
    foreach ($listContract as $item) {
      $no++;
      $row = [];
      $row[] = $no;
      $row[] = $item['CONTRACT_NUMBER'];
      $row[] = $item['COMPANY_NAME'];
      $row[] = $item['DELIVERY_POINT'];
      $row[] = str_replace('\r\n', "<br \>", json_encode($item['DELIVERY_ADDRESS']));
      $row[] = $item['DESCRIPTION'];

      $content[] = $row;
    }

    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }

  function ExportPartNumberToExcel()
  {

    $header = explode(",", $_POST['header']);
    $query = $_POST['query'];

    $file_name = date('Ymd')."_List_Part_Number.xlsx";

    $this->writer->openToBrowser($file_name);

    $listPartNumber = $this->M_Contract->query($query);

    $content = [];
    foreach ($listPartNumber as $item) {
      $row = [];
      $row[] = $item['ATA'];
      $row[] = $item['PART_NUMBER'];
      $row[] = $item['DESCRIPTION'];
      $row[] = $item['ESS'];
      $row[] = $item['MEL'];
      $row[] = $item['TARGET_SLA'];
      $row[] = $item['TARGET_UNSERVICEABLE_RETURN'];
      $row[] = $item['SERVICE_LEVEL_CATEGORY'];
      $row[] = ($item['SERVICE_LEVEL_CATEGORY'] * 100) . "%";

      $content[] = $row;
    }

    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }

  function ExportFleetToExcel()
  {
    $header = explode(",", $_POST['header']);
    $query = $_POST['query'];

    $file_name = date('Ymd')."_List_Fleet.xlsx";

    $this->writer->openToBrowser($file_name);

    $listFleet = $this->M_Contract->query($query);

    $content = [];
    foreach ($listFleet as $item) {
      $row = [];
      $row[] = $item['REGISTRATION'];
      $row[] = $item['TYPE'];
      $row[] = $item['MSN'];
      $row[] = $item['MFG_DATE'];
      
      $content[] = $row;
    }

    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }
}