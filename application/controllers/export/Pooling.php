<?php
date_default_timezone_set("Asia/Jakarta");

require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Pooling extends CI_Controller
{
  function __construct()
	{
    parent::__construct();
    
    if (empty($this->session->userdata('log_sess_id_user'))) redirect('Login');

    $this->load->model('Pooling_model', '', TRUE);

    $this->writer = WriterFactory::create(Type::XLSX);
    $this->headerStyle = (new StyleBuilder())->setFontBold()->setFontName('Arial')->setFontSize(10)->build();
    $this->contentStyle = (new StyleBuilder())->setFontName('Arial')->setFontSize(10)->setShouldWrapText(false)->build();
  }

  function ExportListOrderToExcel()
  {
    $header = explode(",", $_POST['header']);
    $query = $_POST['query'];

    $file_name = date('Ymd')."_List_Order.xlsx";

    $this->writer->openToBrowser($file_name);

    $listContract = $this->Pooling_model->query($query);

    $content = [];

    foreach ($listContract as $item) {
      $row = [];

      $st_order = $item->STATUS_ORDER;
      if($st_order == "1"){
				$st_order = "Open Order & Waiting Approve";
			}elseif($st_order == "2"){
				$st_order = "In Progress";
			}elseif($st_order == "3"){
				$st_order = "Close";
			}elseif($st_order == "4"){
				$st_order = "Cancel Order";
			}
      $row[] = $st_order;

      $st_service = $item->STATUS_SERVICE;
			if($st_service == "1"){
				$st_service = "Waiting Delivery";
			}elseif($st_service == "2"){
				$st_service = "Delivered";
			}
			$row[] = $st_service;

      $st_unservice = $item->STATUS_UNSERVICE;
			if($st_unservice == "1"){
				$st_unservice = "Waiting Delivery";
			}elseif($st_unservice == "2"){
				$st_unservice = "Delivered";
			}
      $row[] = $st_unservice;

      $row[] = $item->REFERENCE;
			$row[] = $item->REQUIREMENT_CATEGORY_NAME;
			$row[] = $item->PART_NUMBER;
			$row[] = $item->PART_DESCRIPTION;
		  $row[] = $item->DESTINATION;
			$row[] = $item->AC_REGISTRATION;
			$row[] = $item->CUSTOMER_ORDER;
      $row[] = $item->REQUEST_BY;
      
      $request_date = date_create($item->CREATE_AT);
      $request_date = date_format($request_date,'d F Y H:i');
      $row[] = $request_date;

      $request_date_deliv = date_create($item->DELIVERY_DATE);
      $request_date_deliv = date_format($request_date_deliv,'d F Y H:i');
      $row[] = $request_date_deliv;
      
      $target_deliv = date_create($item->DELIVERY_TARGET);
      if(date_format($target_deliv,'d-m-Y') == "01-01-1900"){
          $target_deliv = "";
      }else{
          $target_deliv = date_format($target_deliv,'d F Y H:i');
      }
      $row[] = $target_deliv;

      $date_service = date_create($item->DATE_SERVICE);
      if(date_format($date_service,'d-m-Y') == "05-08-1998"){
          $date_service = "";
      }else{
        $date_service = date_format($date_service,'d F Y H:i');
      }
      $row[] = $date_service;

      $date_unservice = date_create($item->DATE_UNSERVICE);
      if(date_format($date_unservice,'d-m-Y') == "05-08-1998"){
          $date_unservice = "";
      }else{
        $date_unservice = date_format($date_unservice,'d F Y H:i');
      }
      $row[] = $date_unservice;

      $content[] = $row;
    }

    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }

}