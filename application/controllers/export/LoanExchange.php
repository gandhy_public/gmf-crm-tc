<?php
date_default_timezone_set("Asia/Jakarta");

require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class LoanExchange extends CI_Controller
{
  function __construct()
	{
    parent::__construct();
    
    if (empty($this->session->userdata('log_sess_id_user'))) redirect('Login');

    $this->load->model('Loan_exchange_model', '', TRUE);

    $this->writer = WriterFactory::create(Type::XLSX);
     $this->writerCSV = WriterFactory::create(Type:: CSV);
    $this->headerStyle = (new StyleBuilder())->setFontBold()->setFontName('Arial')->setFontSize(10)->build();
    $this->contentStyle = (new StyleBuilder())->setFontName('Arial')->setFontSize(10)->setShouldWrapText(false)->build();
  }
  
  function ExportExchangeToExcel()
  {
    // $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
    // print_r($listExchange);die;
    $header = explode(",", $_POST['header']);
    // $query = $_POST['query'];

    $file_name = date('Ymd')."_Exchange_Request.xlsx";

    $this->writer->openToBrowser($file_name);

    $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
 
    $content = [];
    $no = 0;
    foreach ($listExchange as $item) {
      $no++;
      $row = [];
      $row[] = $no;
      $row[] = $item['PART_NUMBER'];
      $row[] = $item['EXCHANGE_TYPE'];
      $row[] = $item['EXCHANGE_RATE'];
      $row[] = $item['CONTACT_INFO'];
      $row[] = $item['KETERANGAN'];
      $row[] = $item['STATUS'];
      // $row[] = $item['CREATED_DATE'];
     
     
      // $row[] = str_replace('\r\n', "<br \>", json_encode($item['DELIVERY_ADDRESS']));
      // $row[] = $item['DESCRIPTION'];

      $content[] = $row;
    }
       // print_r($content);die;
    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }

   function ExportExchangeToCSV()
  {
    // $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
    // print_r($listExchange);die;
    $header = explode(",", $_POST['header']);
    // $query = $_POST['query'];

    $file_name = date('Ymd')."_Exchange_Request.csv";

    $this->writerCSV->openToBrowser($file_name);

    $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
 
    $content = [];
    $no = 0;
    foreach ($listExchange as $item) {
      $no++;
      $row = [];
      $row[] = $no;
      $row[] = $item['PART_NUMBER'];
      $row[] = $item['EXCHANGE_TYPE'];
      $row[] = $item['EXCHANGE_RATE'];
      $row[] = $item['CONTACT_INFO'];
      $row[] = $item['KETERANGAN'];
      $row[] = $item['STATUS'];
      // $row[] = $item['CREATED_DATE'];
     
     
      // $row[] = str_replace('\r\n', "<br \>", json_encode($item['DELIVERY_ADDRESS']));
      // $row[] = $item['DESCRIPTION'];

      $content[] = $row;
    }
       // print_r($content);die;
    $this->writerCSV->addRowWithStyle($header, $this->headerStyle);
    $this->writerCSV->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writerCSV->close();
  }

  function ExportLoanToExcel()
  {
    // $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
    // print_r($listExchange);die;
    $header = explode(",", $_POST['header']);
    // $query = $_POST['query'];

    $file_name = date('Ymd')."_Loan_Request.xlsx";

    $this->writer->openToBrowser($file_name);

    $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('loan',FALSE);
 
    $content = [];
    $no = 0;
    foreach ($listExchange as $item) {
      $no++;
      $row = [];
      $row[] = $no;
      $row[] = $item['PART_NUMBER'];
      $row[] = $item['PERIOD_FROM'];
      $row[] = $item['PERIOD_TO'];
      $row[] = $item['CONDITION'];
      $row[] = $item['CONTACT_INFO'];
      $row[] = $item['KETERANGAN'];
      $row[] = $item['STATUS'];
      // $row[] = $item['CREATED_DATE'];
     
     
      // $row[] = str_replace('\r\n', "<br \>", json_encode($item['DELIVERY_ADDRESS']));
      // $row[] = $item['DESCRIPTION'];

      $content[] = $row;
    }
       // print_r($content);die;
    $this->writer->addRowWithStyle($header, $this->headerStyle);
    $this->writer->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writer->close();
  }

   function ExportLoanToCSV()
  {
    // $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('Exchange',FALSE);
    // print_r($listExchange);die;
    $header = explode(",", $_POST['header']);
    // $query = $_POST['query'];

    $file_name = date('Ymd')."_Loan_Request.csv";

    $this->writerCSV->openToBrowser($file_name);

    $listExchange = $this->Loan_exchange_model->get_list_loan_exchange_export('loan',FALSE);
 
    $content = [];
    $no = 0;
    foreach ($listExchange as $item) {
      $no++;
      $row = [];
      $row[] = $no;
      $row[] = $item['PART_NUMBER'];
      $row[] = $item['PERIOD_FROM'];
      $row[] = $item['PERIOD_TO'];
      $row[] = $item['CONDITION'];
      $row[] = $item['CONTACT_INFO'];
      $row[] = $item['KETERANGAN'];
      $row[] = $item['STATUS'];
      // $row[] = $item['CREATED_DATE'];
     
     
      // $row[] = str_replace('\r\n', "<br \>", json_encode($item['DELIVERY_ADDRESS']));
      // $row[] = $item['DESCRIPTION'];

      $content[] = $row;
    }
       // print_r($content);die;
    $this->writerCSV->addRowWithStyle($header, $this->headerStyle);
    $this->writerCSV->addRowsWithStyle($content, $this->contentStyle);
    
    $this->writerCSV->close();
  }

}