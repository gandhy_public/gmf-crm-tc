<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_exchange extends CI_Controller {

    // Construct

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }

        //$this->load->library('email');
        $this->load->database();
        $this->load->model('Loan_exchange_model', '', TRUE);
        // $this->load->model('Pooling_model', '', TRUE);
        $this->load->helper('url');
        $this->load->library('pagination');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.sisi.id',
            'smtp_port' => 587,
            'smtp_user' => 'okky.permana@sisi.id',
            'smtp_pass' => 'password',
            'smtp_crypto' => 'tls',
            'mailtype' => 'html'
        );
    }    

    // ./Construct



    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Loan Exchange',
        'ldl1'              => 'index.php/Loan_exchange',
        'ldi1'              => 'fa fa-usd',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'loan-exchange/nav_tabs',
        'nav_tabs2'         => 'loan-exchange/nav_tabs2',
        'link_directory'    => 'layout/link-directory'
    );

    // fa fa-refresh fa-spin fa-fw
    // ./Parsing Public Data

    function se_mysession(){
        echo "string";
        echo $this->session->userdata('log_sess_id_customer');
        
    }

    // View

    function index() {
        redirect('Loan_exchange/exchange_order');
    }

    function loan_order() {
        // $this->data['listOrder']        = $this->Loan_exchange_model->getListDataOrderDT($s_result);

        // $this->data['listOrder']        = $temp;
        $this->data['title']            = 'Loan Order List';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'loan-exchange/loan_order';
        $this->load->view('template',$this->data);
    }

    function set_curl($number){

        $curl = curl_init();
        // $curl_obj = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"personnel_number\"\r\n\r\n".$number."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--" ;
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://172.16.232.60/hcis-dev/public/api/v1/external/employee",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "personnel_number=".$number,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer 9dd3990778dc4ced0d668efbe8292a67",
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 061d19d0-8b01-41a8-af88-96c9787fa9ee",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        // print_r($response);
        // print_r($err);
        // die;

        if ($err) {
          return array();
        } else {
          $result = json_decode($response, 1);
          return $result;
        }
    }



    function loan_request() {
        $this->data['title']            = 'Loan Request List';
        $this->data['icon']             = 'fa fa-inbox';
        $this->data['content']          = 'loan-exchange/loan_request';
        $this->load->view('template',$this->data);
    }

    function form_request_loan($id_loan = null) {
        $this->data['title']            = 'Form Request Loan';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'loan-exchange/form_request_loan';
        if($id_loan == null){
            $this->data['status_form']      = 'request';
        }else{
            $this->data['status_form']      = 'available';
        }
        $this->load->view('template',$this->data);
    }

    function exchange_order() {
        $this->data['title']            = 'Exchange Order List';
        $this->data['icon']             = 'fa fa-exchange';
        $this->data['content']          = 'loan-exchange/exchange_order';
        $this->load->view('template',$this->data);
    }

    function exchange_request() {
        $this->data['title']            = 'Exchange Request List';
        $this->data['icon']             = 'fa fa-inbox';
        $this->data['content']          = 'loan-exchange/exchange_request';
        $this->load->view('template',$this->data);
    }

    function form_request_exchange($id_exchange = null) {
        $this->data['title']            = 'Form Request Exchange';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'loan-exchange/form_request_exchange';
        if($id_exchange == null){
            $this->data['status_form']      = 'request';
        }else{
            $this->data['status_form']      = 'available';
        }
        $this->load->view('template',$this->data);
    }

    // ./ View



    // Actions

    public function insert_request_loan() 
    {
        if(isset($_POST['insert'])) 
        {
            $query_insert_loan_request                       = $this->Loan_exchange_model->insert_loan_request();
            if($query_insert_loan_request)
            {
                $this->session->set_flashdata('alert_success','Add Data Request Loan Success');
                redirect('Loan_exchange/loan_request');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Add Data Request Loan Failed');
            redirect('Loan_exchange/form_request_loan');
        }
    }

    public function insert_request_exchange() 
    {
        if(isset($_POST['insert'])) 
        {
            $query_insert_exchange_request                       = $this->Loan_exchange_model->insert_exchange_request();
            if($query_insert_exchange_request)
            {
                $this->session->set_flashdata('alert_success','Add Data Request Exchange Success');
                redirect('Loan_exchange/exchange_request');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Add Data Request Exchange Failed');
            redirect('Loan_exchange/form_request_exchange');
        }
    }

    function get_loan_exchange($jenis){
        $listData = $this->Loan_exchange_model->get_list_loan_exchange($jenis,FALSE,FALSE);
        $total = $this->Loan_exchange_model->get_list_loan_exchange($jenis,TRUE,FALSE);
        $filter = $this->Loan_exchange_model->get_list_loan_exchange($jenis,FALSE,TRUE);

        $data = array();
        $no = $_REQUEST['start'];
        foreach ($listData as $item) {
            $no++;
            $list = array();
            $list['no'] = "<center>".$no."</center>";
            $list['part_number']    = $item->PART_NUMBER;

            if($jenis == "loan"){
                $list['start_date']     = $item->PERIOD_FROM;
                $list['end_date']       = $item->PERIOD_TO;
                $list['condition']      = $item->CONDITION;
            }else{
                $list['type']       = $item->EXCHANGE_TYPE;
                $list['rate']       = $item->EXCHANGE_RATE;
            }
            
            $list['contact_info']   = $item->CONTACT_INFO;
            $list['information']    = $item->KETERANGAN;

            $action = "";
            if($item->STATUS == "request"){
                $status = '<center><span class="label label-warning">REQUEST</span></center>';
                if($this->session->userdata('log_sess_level_user_role') != "4"){
                    $action = "<center><button id='btn_edit' class='btn btn-xs btn-primary' data-x='{$item->ID}'>ACTION</button></center>";
                }
            }elseif($item->STATUS == "available"){
                $status = '<center><span class="label label-success">AVAILABLE</span></center>';
                if($this->session->userdata('log_sess_level_user_role') != "4"){
                    $action = "<center><button id='btn_edit' class='btn btn-xs btn-primary' data-x='{$item->ID}'>ACTION</button></center>";
                }else{
                    $action = "<center><button id='btn_form' class='btn btn-xs btn-primary' data-x='{$item->ID}'>Form Request</button></center>";
                }
            }elseif($item->STATUS == "not available"){
                $status = '<center><span class="label label-danger">NOT AVAILABLE</span></center>';
            }elseif($item->STATUS == "process"){
                $status = '<center><span class="label label-info">PROCESS</span></center>';
                if($this->session->userdata('log_sess_level_user_role') != "4"){
                    $action = "<center><button id='btn_done' class='btn btn-xs btn-primary' data-x='{$item->ID}'>DONE</button></center>";
                }
            }else{
                $status = '<center><span class="label label-primary">DONE</span></center>';
            }
            $list['status']         = $status;

            $list['action']         = $action;

            $data[] = $list;
        }

        $results = array(
            "draw" => $_REQUEST["draw"],
            "recordsTotal" => $total,
            "recordsFiltered" => $filter,
            "data" => $data
        );

        echo json_encode($results);
    }

    public function detail_loan()
    {
        $id_data = $this->input->post('id');

        $data = $this->Loan_exchange_model->get_detail_loan($id_data);
        echo json_encode($data);
    }

    public function detail_exchange()
    {
        $id_data = $this->input->post('id');

        $data = $this->Loan_exchange_model->get_detail_exchange($id_data);
        echo json_encode($data);
    }

    function update_loan(){
        $id_data = $this->input->post('id');
        $status = $this->input->post('status');
        $keterangan = $this->input->post('keterangan');

        $update_data = $this->Loan_exchange_model->update_loan($id_data,$status,$keterangan);
        echo $update_data;
    }

    function update_exchange(){
        $id_data = $this->input->post('id');
        $status = $this->input->post('status');
        $keterangan = $this->input->post('keterangan');

        $update_data = $this->Loan_exchange_model->update_exchange($id_data,$status,$keterangan);
        echo $update_data;
    }

    public function finish_loan($id_data) {
        $part_number = $this->input->post('PART_NUMBER');
        $start_date = $this->input->post('PERIOD_FROM');
        $end_date = $this->input->post('PERIOD_TO');
        $condition = $this->input->post('CONDITION');
        $contact = $this->input->post('CONTACT_INFO');

        $finish_data = $this->Loan_exchange_model->finish_loan($id_data,$part_number,$start_date,$end_date,$condition,$contact);
        return $finish_data;
    }

    public function finish_exchange($id_data) {
        $part_number = $this->input->post('PART_NUMBER');
        $type = $this->input->post('EXCHANGE_TYPE');
        $rate = $this->input->post('EXCHANGE_RATE');
        $contact = $this->input->post('CONTACT_INFO');

        $finish_data = $this->Loan_exchange_model->finish_exchange($id_data,$part_number,$type,$rate,$contact);
        return $finish_data;
    }

    public function done_loan($id_data) {
        $done_data = $this->Loan_exchange_model->done_loan($id_data);
        return $done_data;
    }

    public function done_exchange($id_data) {
        $done_data = $this->Loan_exchange_model->done_exchange($id_data);
        return $done_data;
    }

    public function summary_loan_exchange(){
        $data_loan = $this->Loan_exchange_model->summary_loan();
        $data_exchange = $this->Loan_exchange_model->summary_exchange();
        $data = [];
        foreach ($data_loan as $key) {
            $data[] = [
                'JENIS' => "loan",
                'STATUS' => $key['STATUS'],
                'JUMLAH' => $key['JUMLAH'],
            ];
        }

        foreach ($data_exchange as $key) {
            $data[] = [
                'JENIS' => "exchange",
                'STATUS' => $key['STATUS'],
                'JUMLAH' => $key['JUMLAH'],
            ];
        }
        echo json_encode($data);
    }
}

?>