<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");
class Pooling extends CI_Controller{

    // Construct

	function __construct() {
		parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->model('Pooling_model', '', TRUE);
        
        $this->load->library('email');
        $config['useragent'] = 'CodeIgniter';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'mail.gmf-aeroasia.co.id';
        $config['smtp_user'] = 'app.notif';
        $config['smtp_pass'] = 'app.notif';
        $config['smtp_port'] = 25; 
        $config['smtp_timeout'] = 5;
        $config['wordwrap'] = TRUE;
        $config['wrapchars'] = 76;
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['validate'] = FALSE;
        $config['priority'] = 3;
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['bcc_batch_mode'] = FALSE;
        $config['bcc_batch_size'] = 200;    
        $config['smtp_keepalive'] = 'TRUE';
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
	}

    // ./Construct

		public function check_session(){
			print_r($this->session->userdata('log_sess_id_user_role'));

		}

    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Pooling',
        'ldl1'              => 'index.php/Pooling',
        'ldi1'              => 'fa fa-object-ungroup',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'title_controller'  => 'POOLING',
        'icon_controller'   => 'fa fa-object-ungroup',
        'nav_tabs'          => 'pooling/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data

    function tes_email(){
      $content = $this->Pooling_model->get_content_email_order(77);
      $request_deliv = date_create($content[0]['DELIVERY_DATE']);
      $request_deliv = date_format($request_deliv,'d F Y H:i');
      if($content[0]['DELIVERY_TARGET'] == "" || $content[0]['DELIVERY_TARGET'] == null){
        $target_deliv = 0;
      }else{
        $target_deliv = date_create($content[0]['DELIVERY_TARGET']);
        $target_deliv = date_format($target_deliv,'d F Y H:i');
      }
      $destination = str_replace('\r\n', "<br>", json_encode($content[0]['DESTINATION']));
      $this->email->to("gandhybagoesc@gmail.com");
      $this->email->from('app.notif@gmf-aeroasia.co.id','GMF AeroAsia Pooling Order');
      $this->email->subject('Congratulation, Your Request Order Success');
      $isi_email = "
      <center><h1>Your order has been created</h1></center>
      <center>
        <table border='1'>
          <tr>
            <td><b>Customer Order</b></td>
            <td>{$content[0]['CUSTOMER_ORDER']}</td>
          </tr>
          <tr>
            <td><b>Request Part Number</b></td>
            <td>{$content[0]['PART_NUMBER']}</td>
          </tr>
          <tr>
            <td><b>Component Description</b></td>
            <td>{$content[0]['COMPONENT_DESCRIPTION']}</td>
          </tr>
          <tr>
            <td><b>Requirement Category</b></td>
            <td>{$content[0]['REQUIREMENT_CATEGORY_NAME']}</td>
          </tr>
          <tr>
            <td><b>Request Delivery Date</b></td>
            <td>{$request_deliv}</td>
          </tr>
          <tr>
            <td><b>Aircraft Registration</b></td>
            <td>{$content[0]['AC_REGISTRATION']}</td>
          </tr>
          <tr>
            <td><b>Target Delivery Date</b></td>
            <td>{$target_deliv}</td>
          </tr>
          <tr>
            <td><b>Destionation Address</b></td>
            <td>{$destination}</td>
          </tr>
          <tr>
            <td><b>Requested By</b></td>
            <td>{$content[0]['NAMA']}</td>
          </tr>
        </table>  
      </center>
      ";
      $this->email->message($isi_email);

      //Send email
      $send = $this->email->send();
      if($send){
        echo "berhasil";
        print('<pre>'.print_r($send,TRUE).'</pre>');die();
      }else {
        echo "gagal";
        print('<pre>'.print_r($send,TRUE).'</pre>');die();
      }
    }

    // View
    function email_order($destination,$content){
      $this->email->to($destination);
      $this->email->from('app.notif@gmf-aeroasia.co.id','GMF AeroAsia Pooling Order');
      $this->email->subject('Congratulation, Your Request Order Success');
      $this->email->message($content);

      //Send email
      $send = $this->email->send();
      if($send){
        return TRUE;
      }else {
        return FALSE;
      }
    }

    function email_service($destination,$content){
      $this->email->to($destination);
      $this->email->from('app.notif@gmf-aeroasia.co.id','GMF AeroAsia Pooling Order');
      $this->email->subject('Congratulation, Your Submit Serviceable Success');
      $this->email->message($content);

      //Send email
      $send = $this->email->send();
      if($send){
        return TRUE;
      }else {
        return FALSE;
      }
    }

    function email_unservice($destination,$content){
      $this->email->to($destination);
      $this->email->from('app.notif@gmf-aeroasia.co.id','GMF AeroAsia Pooling Order');
      $this->email->subject('Congratulation, Your Submit Unserviceable Success');
      $this->email->message($content);

      //Send email
      $send = $this->email->send();
      if($send){
        return TRUE;
      }else {
        return FALSE;
      }
    }


    function index()
    {   
        redirect('Pooling/list_order');
    }

		function dashboard()
    {
        $this->data['title']                        = 'Dashboard';
        $this->data['icon']                         = 'fa fa-dashboard';
		    $this->data['content']                      = 'pooling/dashboard';
        $this->load->view('template',$this->data);
		}

    function create_order()
    {   
        $id_customer = $this->session->userdata('log_sess_id_customer');
 
        // /*** create the form token ***/
        $form_token = uniqid();
 
        // /*** add the form token to the session ***/
        // $_SESSION['form_token'] = $form_token;
        $this->data['form_token'] = $form_token;
        $this->data['customer'] = $id_customer;
        $this->data['contract_list'] = $this->Pooling_model->get_contract_list($id_customer);
        // $this->data['part_number_list']             = $this->Pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->Pooling_model->get_requirement_category_list();
        // $this->data['aircraft_registration_list']   = $this->Pooling_model->get_aircraft_registration_list();
        // $this->data['delivery_point_list']          = $this->Pooling_model->get_delivery_point_list();

        $this->data['title']                        = 'Create Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'pooling/create_order';
        $this->load->view('template', $this->data);
    }

    function list_order()
    {   
        if(isset($_GET['status_order'])){
          $st_order = $_GET['status_order'];
        }else{
          $st_order = "";
        }
        if(isset($_GET['status_service'])){
          $st_service = $_GET['status_service'];
        }else{
          $st_service = "";
        }
        if(isset($_GET['status_unservice'])){
          $st_unservice = $_GET['status_unservice'];
        }else{
          $st_unservice = "";
        }
        if(isset($_GET['contract'])){
          $contract = $_GET['contract'];
        }else{
          $contract = "";
        }

        $this->data['st_order'] = $st_order;
        $this->data['st_service'] = $st_service;
        $this->data['st_unservice'] = $st_unservice;
        $this->data['contract'] = $contract;
        $id_customer = $this->session->userdata('log_sess_id_customer');
        $this->data['contract_list'] = $this->Pooling_model->get_contract_list($id_customer);
        $this->data['title']                        = 'List Order';
        $this->data['icon']                         = 'fa fa-list';
        $this->data['content']                      = 'pooling/list_order2';
        $this->load->view('template', $this->data);
    }

    function update_order($id_order,$status){
        try {
          if($status != "0"){
            $update_status = $this->Pooling_model->update_status_order($id_order,$status);
            if(!$update_status) throw new Exception("Failed update status order, please try again", 1);

            if($status == "4" || $status == "3"){
              redirect('Pooling/list_order');
            }
          }

          $this->data['order'] = 'disabled';
          $this->data['order_id'] = $id_order;
          $data_order = $this->Pooling_model->get_detail_order($id_order);
          $this->data['order_status'] = $data_order[0]['STATUS_ORDER'];
          $this->data['contract_id'] = $data_order[0]['CONTRACT_ID'];
          
          $this->data['title'] = 'Update Order';
          $this->data['icon'] = 'fa fa-edit';
          $this->data['content'] = 'pooling/update_order2';
          $this->load->view('template', $this->data);
        } catch (Exception $e) {
          $this->session->set_flashdata('alert_failed',$e->getMessage());
          redirect('Pooling/list_order');
        }
    }

    function update_order2($NO_ACCOUNT)
    {
        $this->data['ldl2']                         = 'index.php/Pooling/list_order';
        $this->data['ldt2']                         = 'List Order';
        $this->data['ldi2']                         = 'fa fa-list';

        // $this->data['part_number_list']             = $this->Pooling_model->get_part_number_list();
        $this->data['requirement_category_list']    = $this->Pooling_model->get_requirement_category_list();
        // $this->data['aircraft_registration_list']   = $this->Pooling_model->get_aircraft_registration_list();
        // $this->data['delivery_point_list']          = $this->Pooling_model->get_delivery_point_list();

        $this->data['order']                        = $this->Pooling_model->select_request_where_no_account($NO_ACCOUNT);
        $order = $this->data['order'];
        $this->data['SERVICE_EDT'] = explode(" ", $order->SERVICE_EDT);
        $this->data['UNSERVICE_EDT'] = explode(" ", $order->UNSERVICE_EDT);
        // echo $this->data['SERVICE_EDT'], $this->data['UNSERVICE_EDT'];
        // die;

				$section_role['main'] = [
					'SERVICE_LEVEL' => '',
					'ID_PART_NUMBER' => '',
					'COMPONENT_DESCRIPTION' => '',
					'ID_REQ_CAT' => '',
					'REQUESTED_DELIVERY_DATE' => '',
					'MAINTENANCE' => '',
					'REASON_OF_REQUESTED_SERVICE_LEVEL' => '',
					'ATA_CHAPTER' => '',
					'RELEASE_TYPE' => '',
					'AIRCRAFT_REGISTRATION' => '',
					'3_DIGIT_AIRPORT_CODE' => '',
					'DESTINATION_ADDRESS' => '',
					'CUSTOMER_PO' => '',
					'REMARKS_OF_CUSTOMER' => '',
					'THY_REMARKS' => '',
					'3_DIGIT_AIRPORT_CODE' => '',
				];
				$section_role['serviceable'] = [
                    'SERVICE_SHIPMENT_TYPE' => '',
					'ID_PART_NUMBER' => '',
					'COMPONENT_DESCRIPTION_V' => '',
					'AWB_NO' => '',
					'FLIGHT_NO' => '',
					'EST_DELIVERY_DATE' => '',
					'AIR_DEST' => '',
					'ATTENTION_EMAIL' => '',
				];
				$section_role['unserviceable'] = [
					'SEND_TYPE' => '',
					'SHIPMENT_TYPE' => '',
					'PART_NUMBER' => '',
					'SERIAL_NUMBER' => '',
					'REMOVAL_DATE' => '',
					'AC_REGISTRATION' => '',
					'REMOVAL_REASON' => '',
                    'ATTENTION_EMAIL1' => '',
                    'AWN_NO' => '',
                    'FLIGHT_NO1' => '',
                    'EST_DELIVERY_DATE1' => '',
                    'REMARKS' => '',
                    'REMOVAL_REASON_TYPE' => '',
                    // 'REMOVAL_REASON2' => '',
                    // 'REMOVAL_REASON3' => '',
				];

				$this->data['main'] = 'disabled';
				$this->data['serviceable'] = 'disabled';
				$this->data['unserviceable'] = 'disabled';

				$this->data['main_btn'] = 'disabled';
				$this->data['serviceable_btn'] = 'disabled';
				$this->data['unserviceable_btn'] = 'disabled';
        $this->data['serviceable_btn_update'] = 'disabled';
        $this->data['unserviceable_btn_update'] = 'disabled';

          if ($order->STATUS_ORDER != 1 and $order->STATUS_ORDER != 4) {
    				if ($this->session->userdata('log_sess_id_customer')) {
    					// code...
    					$this->data['main'] = 'disabled';
    					$this->data['serviceable'] = 'disabled';
                        if ( $order->REVISI_UNSERVICE == 0){
    					   $this->data['unserviceable'] = '';
                           $this->data['unserviceable_btn'] = '';
                        } else {
                           $this->data['unserviceable'] = 'disabled'; 
                           $this->data['unserviceable_btn'] = 'disabled';
                        }

    					$this->data['main_btn'] = 'disabled';
    					$this->data['serviceable_btn'] = 'disabled';
    					
                        $this->data['serviceable_btn_update'] = 'disabled';
                        $this->data['unserviceable_btn_update'] = '';
    				}
					else {
    					// code...
    					$this->data['main'] = 'disabled';
                        // $this->data['serviceable'] = '';
                        $this->data['unserviceable'] = 'disabled';
                         if ( $order->REVISI_SERVICE == 0){
                           $this->data['serviceable'] = '';
                           $this->data['serviceable_btn'] = '';
                        } else {
                           $this->data['serviceable'] = 'disabled'; 
                           $this->data['serviceable_btn'] = 'disabled';
                        }

                        $this->data['main_btn'] = 'disabled';
                        // $this->data['serviceable_btn'] = '';
                        $this->data['serviceable_btn_update'] = '';
                        $this->data['unserviceable_btn'] = 'disabled';
                        $this->data['unserviceable_btn_update'] = 'disabled';

					}
                }

        $this->data['title']                        = 'Update Order';
        $this->data['icon']                         = 'fa fa-edit';
        $this->data['content']                      = 'pooling/update_order';
        $this->load->view('template', $this->data);
    }

      // Report

      function report_serviceable_level()
      {
          $start_date = date('Y/m/01');
          $end_date = date('Y/m/d');
          $id_customer = $this->session->userdata('log_sess_id_customer');

          $this->data['title']                        = 'Report Serviceable Level';
          $this->data['icon']                         = 'fa fa-bar-chart';
          $this->data['content']                      = 'pooling/report_serviceable_level';
          $this->data['start_date']                   = $start_date;
          $this->data['end_date']                     = $end_date;
          //$this->data['contract_list']                = $this->Pooling_model->get_contract_list($id_customer);
          if($id_customer){
            $this->data['customer_list'] = $this->Pooling_model->get_customer($id_customer);
          }else{
            $this->data['customer_list'] = [1,2,3,4,5];
          }
          //$this->data['customer_list']                = $this->Pooling_model->get_customer($id_customer);
          $this->load->view('template', $this->data);
      }

      function report_unserviceable_level()
      {   
          $start_date = date('Y/m/01');
          $end_date = date('Y/m/d');
          $id_customer = $this->session->userdata('log_sess_id_customer');

          $this->data['title']                        = 'Report Unserviceable Level';
          $this->data['icon']                         = 'fa fa-bar-chart';
          $this->data['content']                      = 'pooling/report_unserviceable_level';
          $this->data['start_date']                   = $start_date;
          $this->data['end_date']                     = $end_date;
          //$this->data['contract_list']                = $this->Pooling_model->get_contract_list($id_customer);
          if($id_customer){
            $this->data['customer_list'] = $this->Pooling_model->get_customer($id_customer);
          }else{
            $this->data['customer_list'] = [1,2,3,4,5];
          }
          //$this->data['customer_list']                = $this->Pooling_model->get_customer($id_customer);
          $this->load->view('template', $this->data);
      }

        function part_number()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/part_number';
            $this->load->view('template',$this->data);
        }

        function contract()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/contract';
            $this->load->view('template',$this->data);
        }

        function requirement_category()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/requirement_category';
            $this->load->view('template',$this->data);
        }

        function user()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/user';
            $this->load->view('template',$this->data);
        }

        function customer()
        {
            $this->data['title']                        = 'Dashboard';
            $this->data['content']                      = 'pooling/customer';
            $this->load->view('template',$this->data);
        }

    public function calculate_target_delivery($ID_PN,$ID_CONTRACT,$DATE_DELIVERY)
    {
        /*$ID_PN          = $_GET['part'];
        $ID_CONTRACT    = $_GET['contract'];
        $DATE_DELIV     = $_GET['date'];*/

        //$DATE_DELIVERY  = date('Y-m-d H:i:s'); //By Submit user
        $DATE_DELIVERY = $DATE_DELIVERY.":".date("s"); //By form

        if($ID_PN != "OTHER"){
          $sql = "
            SELECT 
              TARGET_SLA,
              WAKTU_SLA,
              SATUAN_SLA
            FROM
              TC_CONTRACT_PN
            WHERE
              ID_CONTRACT = $ID_CONTRACT AND
              PART_NUMBER = '$ID_PN' AND
              IS_DELETE = 0
          ";

          $result = $this->db->query($sql)->result_array();
          if ($result) {
            if ($result[0]['WAKTU_SLA'] and $result[0]['SATUAN_SLA']){
              $sla = '+'.$result[0]['WAKTU_SLA'].' '.$result[0]['SATUAN_SLA'];
            } else {
              $sla = '+0 days';
            }
          }
        }else{
          $sla = '+0 days';
        }

        if($sla != '+0 days'){
          $target_date = date('Y-m-d H:i:s', strtotime($sla, strtotime($DATE_DELIVERY)));
        }else{
          $target_date = "";
        }
        // print('<pre>'.print_r($target_date,TRUE).'</pre>');die();
        // $start = strtotime($DATE_DELIVERY);
        // $end = strtotime($target_date);
        // $target_jam = $end-$start;
        // $target_jam   = floor($target_jam / (60 * 60));
      
        return $target_date;
      }

    public function get_part_number_description()
    {
        $ID_PN                          = $this->input->post('ID_PN');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $part_number_description        = $this->Pooling_model->get_part_number_description($ID_PN);                      //mengambil data dari database melalui model mcombox
        echo json_encode($part_number_description); // Encode to json data
    }
    public function get_part_number_contract($id)
    {                             //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $ID_PN       = $this->input->post('ID_PN');  
        $data        = $this->Pooling_model->get_part_number_contract_description($ID_PN);
                            //mengambil data dari database melalui model mcombox
        echo json_encode($data); // Encode to json data
    }
   
    public function get_part_number_list($id_c=null)
    {
        $data        = $this->Pooling_model->get_pn_list_by_contract($id_c);   
        // print_r($data)         ;          //mengambil data dari database melalui model mcombox
        echo json_encode($data); // Encode to json data
    }

    public function get_delivery_point_address()
    {
        $ID_DP                          = $this->input->post('ID_DP');                                   //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
        $delivery_point_address         = $this->Pooling_model->get_delivery_point_address($ID_DP);                      //mengambil data dari database melalui model mcombox
        echo json_encode($delivery_point_address); // Encode to json data
    }

     public function get_delivery_point($id_c)
    {
        $data        = $this->Pooling_model->get_delivery_point($id_c);  
        // echo $this->db->last_query(); 
        // print_r($data)         ;          //mengambil data dari database melalui model mcombox
        echo json_encode($data);
    }

    // ./ Get Data



    // Actions

    public function insert_create_order()
    {
      try {
        if($this->session->userdata('log_sess_id_customer') == "0" || $this->session->userdata('log_sess_id_customer') == "" || $this->session->userdata('log_sess_id_customer') == null) throw new Exception("Sory, you can't create order because your role is not customer!", 1);
        
        $param = $this->input->post();

        if($param['ID_PART_NUMBER'] == "OTHER"){
          $pn = $this->Pooling_model->create_pn($param['OTHER_PN'],$param['COMPONENT_DESCRIPTION_V'],$param['ID_CONTRACT']);
          if(!$pn) throw new Exception("Failed create other Part Number!", 1);
          $pn = 'x_'.$pn;
        }else{
          $pn = $param['ID_PART_NUMBER'];
        }

        if($param['ID_AIRCRAFT_REGISTRATION'] == "OTHER"){
          $ac = $this->Pooling_model->create_ac($param['OTHER_AC'],$param['ID_CONTRACT']);
          if(!$ac) throw new Exception("Failed create other Aircraft Registration", 1);
          $ac = 'x_'.$ac;
        }else{
          $ac = $param['ID_AIRCRAFT_REGISTRATION'];
        }

        if($param['ID_DELIVERY_POINT'] == "OTHER"){
          $desti = $this->Pooling_model->create_desti($param['OTHER_DP'],$param['DESTINATION_ADDRESS'],$param['ID_CONTRACT']);
          if(!$desti) throw new Exception("Failed create other Destination", 1);
          $desti = 'x_'.$desti;
        }else{
          $desti = $param['ID_CONTRACT'];
        }

        //$deliv_date = DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d", strtotime($param['REQUESTED_DELIVERY_DATE']))." ".date('H:i:s'));

        $target_deliv = $this->calculate_target_delivery($param['ID_PART_NUMBER'],$param['ID_CONTRACT'],$param['REQUESTED_DELIVERY_DATE']);
        $data_insert = [
          'CONTRACT_ID' => $param['ID_CONTRACT'],
          'CUSTOMER_ID' => $this->session->userdata('log_sess_id_customer'),
          'PN_ID' => $pn,
          'COMPONENT_DESCRIPTION' => $param['COMPONENT_DESCRIPTION'],
          'CATEGORY_ID' => $param['ID_REQUIREMENT_CATEGORY'],
          'DELIVERY_DATE' => $param['REQUESTED_DELIVERY_DATE'].":".date('s'),
          'DELIVERY_TARGET' => $target_deliv,
          'DELIVERY_SLA' => $param['sla_time']." ".$param['sla_type'],
          'ATA_CHAPTER' => $param['ATA_CHAPTER_V'],
          'AC_ID' => $ac,
          'DESTINATION_ID' => $desti,
          'CUSTOMER_ORDER' => $param['CUSTOMER_PO'],
          'REMARK' => $param['REMARKS_OF_CUSTOMER'],
          'CREATE_AT' => date('Y-m-d H:i:s'),
          'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
          'IS_DELETE' => 0,
          'STATUS_ORDER' => 1,
          'STATUS_SERVICE' => 1,
          'STATUS_UNSERVICE' => 1
        ];

        $create_order = $this->Pooling_model->create_order($data_insert);
        if(!$create_order) throw new Exception("Failed create request order, please try again", 1);

        $reference = date('Y')."-".sprintf("%05d",$create_order);
        $this->Pooling_model->create_reference($create_order,$reference);
        $this->order_mail($create_order,$param);
        
        $resp = [
          'status' => TRUE,
          'alert' => 'success',
          'message' => 'Success create request order'
        ];
      } catch (Exception $e) {
        $resp = [
          'status' => FALSE,
          'alert' => 'warning',
          'message' => $e->getMessage()
        ];
      }
      echo json_encode($resp);
    }

    function order_mail($order_id,$param){
      try {
        if(isset($param['EXISTING_EMAIL'])){
          $mail_exis = [];
          for($a=0;$a<count($param['EXISTING_EMAIL']);$a++){
            $mail_exis = [
              'POOLING_ORDER_ID' => $order_id,
              'DESTINATION' => $param['EXISTING_EMAIL'][$a],
              'SENDER' => 'app.notif@gmf-aeroasia.co.id',
              'STATUS' => 0,
              'CREATE_AT' => date('Y-m-d H:i:s'),
              'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
              'POOLING' => "order"
            ];

            $content = $this->Pooling_model->get_content_email_order($order_id);
            $request_deliv = date_create($content[0]['DELIVERY_DATE']);
            $request_deliv = date_format($request_deliv,'d F Y H:i');
            if($content[0]['DELIVERY_TARGET'] == "" || $content[0]['DELIVERY_TARGET'] == null){
              $target_deliv = 0;
            }else{
              $target_deliv = date_create($content[0]['DELIVERY_TARGET']);
              $target_deliv = date_format($target_deliv,'d F Y H:i');
            }
            $destination = str_replace('\r\n', "<br>", json_encode($content[0]['DESTINATION']));
            $isi_email = "
            <center><h1>Your order has been created</h1></center>
            <center>
              <table border='1'>
                <tr>
                  <td><b>Customer Order</b></td>
                  <td>{$content[0]['CUSTOMER_ORDER']}</td>
                </tr>
                <tr>
                  <td><b>Request Part Number</b></td>
                  <td>{$content[0]['PART_NUMBER']}</td>
                </tr>
                <tr>
                  <td><b>Component Description</b></td>
                  <td>{$content[0]['COMPONENT_DESCRIPTION']}</td>
                </tr>
                <tr>
                  <td><b>Requirement Category</b></td>
                  <td>{$content[0]['REQUIREMENT_CATEGORY_NAME']}</td>
                </tr>
                <tr>
                  <td><b>Request Delivery Date</b></td>
                  <td>{$request_deliv}</td>
                </tr>
                <tr>
                  <td><b>Aircraft Registration</b></td>
                  <td>{$content[0]['AC_REGISTRATION']}</td>
                </tr>
                <tr>
                  <td><b>Target Delivery Date</b></td>
                  <td>{$target_deliv}</td>
                </tr>
                <tr>
                  <td><b>Destination Address</b></td>
                  <td>{$destination}</td>
                </tr>
                <tr>
                  <td><b>Requested By</b></td>
                  <td>{$content[0]['NAMA']}</td>
                </tr>
              </table>  
            </center>
            ";
            $send_email = $this->email_order($param['EXISTING_EMAIL'][$a],$isi_email);
            if($send_email){
              $mail_exis['STATUS'] = 1;
            }else{
              $mail_exis['STATUS'] = 0;
            }

            $this->Pooling_model->insert_email($mail_exis);
            $mail_exis = [];
          }
        }

          $mail_new = [];
          $new_mail = json_decode($param['new_email'],true);
          for($a=0;$a<count($new_mail);$a++){

              $mail_new = [
                'POOLING_ORDER_ID' => $order_id,
                'DESTINATION' => $new_mail[$a],
                'SENDER' => 'app.notif@gmf-aeroasia.co.id',
                'STATUS' => 0,
                'CREATE_AT' => date('Y-m-d H:i:s'),
                'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
                'POOLING' => 'order'
              ];

              $content = $this->Pooling_model->get_content_email_order($order_id);
              $request_deliv = date_create($content[0]['DELIVERY_DATE']);
              $request_deliv = date_format($request_deliv,'d F Y H:i');
              if($content[0]['DELIVERY_TARGET'] == "" || $content[0]['DELIVERY_TARGET'] == null){
                $target_deliv = 0;
              }else{
                $target_deliv = date_create($content[0]['DELIVERY_TARGET']);
                $target_deliv = date_format($target_deliv,'d F Y H:i');
              }
              $destination = str_replace('\r\n', "<br>", json_encode($content[0]['DESTINATION']));
              $isi_email = "
              <center><h1>Your order has been created</h1></center>
              <center>
                <table border='1'>
                  <tr>
                    <td><b>Customer Order</b></td>
                    <td>{$content[0]['CUSTOMER_ORDER']}</td>
                  </tr>
                  <tr>
                    <td><b>Request Part Number</b></td>
                    <td>{$content[0]['PART_NUMBER']}</td>
                  </tr>
                  <tr>
                    <td><b>Component Description</b></td>
                    <td>{$content[0]['COMPONENT_DESCRIPTION']}</td>
                  </tr>
                  <tr>
                    <td><b>Requirement Category</b></td>
                    <td>{$content[0]['REQUIREMENT_CATEGORY_NAME']}</td>
                  </tr>
                  <tr>
                    <td><b>Request Delivery Date</b></td>
                    <td>{$request_deliv}</td>
                  </tr>
                  <tr>
                    <td><b>Aircraft Registration</b></td>
                    <td>{$content[0]['AC_REGISTRATION']}</td>
                  </tr>
                  <tr>
                    <td><b>Target Delivery Date</b></td>
                    <td>{$target_deliv}</td>
                  </tr>
                  <tr>
                    <td><b>Destination Address</b></td>
                    <td>{$destination}</td>
                  </tr>
                  <tr>
                    <td><b>Requested By</b></td>
                    <td>{$content[0]['NAMA']}</td>
                  </tr>
                </table>  
              </center>
              ";
              $send_email = $this->email_order($new_mail[$a],$isi_email);
              if($send_email){
                $mail_new['STATUS'] = 1;
              }else{
                $mail_new['STATUS'] = 0;
              }

              $this->Pooling_model->insert_email($mail_new);
              $mail_new = [];
            
          }
        
      } catch (Exception $e) {
        
      }
    }

    public function update_status_order()
    {   $id = $this->input->post('ID');
        $status = $this->input->post('status');
        if ($id != '') 
        {   if ($status == 4) {
            $data = array(
                'STATUS_ORDER' => $status,
                'CANCELLED_BY' => $this->session->userdata('log_sess_name'),
                'CANCELLED_DATE' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );
          } else if ($status == 2) {
            $data = array(
                'STATUS_ORDER' => $status,
                'ORDER_CONFIRMED_BY' => $this->session->userdata('log_sess_name'),
                'ORDER_CONFIRMED_DATE' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );
          } else if ($status == 3) {
            $data = array(
                'STATUS_ORDER' => $status,
                'ORDER_COMPLETE_BY' => $this->session->userdata('log_sess_name'),
                'ORDER_COMPLETE_DATE' => date('Y-m-d H:i:s'),
                'CHANGED_ON' => date('Y-m-d H:i:s'),
                'CHANGED_BY' => $this->session->userdata('log_sess_name')
            );

          }
            $query_update_order             = $this->Pooling_model->update_status($id, $data);
            if ($query_update_order) {
                if ($status == 2){
                     $data = array(
                        "msg" => 'Good Job! This Order has been confirmed!'
                    );  
                }
                 else if ($status == 3){
                     $data = array(
                        "msg" => 'Good Job! This Order has been completed!'
                    );  
                }
                else if ($status == 4){
                     $data = array(
                        "msg" => ' This Order has been cancelled!'
                    );  
                }

            }
            else {
                $data = array(
                        "msg" => 'Operation Failed'
                    ); 
            }
        echo json_encode($data);
        }
    }

    public function submit_unserviceable()
    {   $id = $this->input->post('id');
        if ($id != '') 
        {   $edt = $this->input->post('EST_DELIVERY_DATE1');
            $waktu = date('H:i:00');
            $edt = $edt." ".$waktu;
            $data = array(
                'UNSERVICE_SEND_TYPE' => $this->input->post('SEND_TYPE'),
                'UNSERVICE_SHIPMENT_TYPE' => $this->input->post('SHIPMENT_TYPE'),
                'UNSERVICE_PART_NUMBER' => $this->input->post('PART_NUMBER'),
                'UNSERVICE_SERNUM' => $this->input->post('SERIAL_NUMBER'),
                'UNSERVICE_REMOVE_DATE' => $this->input->post('REMOVAL_DATE'),
                'UNSERVICE_AC_REG' => $this->input->post('AC_REGISTRATION'),
                'UNSERVICE_REMOVAL_REASON' => $this->input->post('REMOVAL_REASON'),
                'UNSERVICE_EMAIL' => $this->input->post('ATTENTION_EMAIL1'),
                'UNSERVICE_AWB_NO' => $this->input->post('AWN_NO'),
                'UNSERVICE_FLIGHT_NO' => $this->input->post('FLIGHT_NO1'),
                'UNSERVICE_EDT' => $edt,
                'UNSERVICE_REMARKS' => $this->input->post('REMARKS'),
                'UNSERVICE_REMOVAL_REASON_TYPE' => $this->input->post('REMOVAL_REASON_TYPE'),
                'REVISI_UNSERVICE' => (int)$this->input->post('revisi_unserv') + 1,
                'UNSERVICE_SUBMITTER' => $this->session->userdata('log_sess_name'),
                'CHANGED_UNSERVICE_ON' => date('Y-m-d H:i:s'),
                'CHANGED_UNSERVICE_BY' => $this->session->userdata('log_sess_name'),
                'UNSERVICE_STATUS' => 'Delivered'
            );
            // if ((int)$this->input->post('revisi_unserv') == 0 ){
            //   array_push($data,"blue","yellow");
            // }
            
            $query_update_unserviceable = $this->Pooling_model->update_unserviceable($id, $data);
        }
        // else
        // {
        //     redirect('Pooling');
        // }
    }

    public function submit_serviceable()
    {   
        $id = $this->input->post('id');
        if($id != '') 
        {   $edt = $this->input->post('EST_DELIVERY_DATE');
            $waktu = date('H:i:00');
            $edt = $edt." ".$waktu;
            if ($this->input->post('EST_DELIVERY_DATE')){
              // $target_date = date($this->input->post('EST_DELIVERY_DATE')+ 'H:i:00', strtotime(+12 days));
              $target_date = date('Y-m-d H:i:s',strtotime('+12 days',strtotime($edt)));
            }
            $data = array(
                'SERVICE_SHIPMENT_TYPE' => $this->input->post('SERVICE_SHIPMENT_TYPE'),
                'SERVICE_PART_NUMBER' => $this->input->post('ID_PART_NUMBER'),
                'SERVICE_MFR_SERNUM' => $this->input->post('SERVICE_MFR_SERNUM'),
                'SERVICE_AWB_NO' => $this->input->post('AWB_NO'),
                'SERVICE_FLIGHT_NO' => $this->input->post('FLIGHT_NO'),
                'SERVICE_EDT' => $edt,
                'TARGET_UNSERV_RETURN' => $target_date,
                'SERVICE_DESTINATION' => $this->input->post('AIR_DEST'),
                'SERVICE_EMAIL' => $this->input->post('ATTENTION_EMAIL'),
                'REVISI_SERVICE' => (int)$this->input->post('revisi_serv') + 1,
                'SERVICE_SUBMITTER' => $this->session->userdata('log_sess_name'),
                'CHANGED_SERVICE_ON' => date('Y-m-d H:i:s'),
                'CHANGED_SERVICE_BY' => $this->session->userdata('log_sess_name'),
                'SERVICE_STATUS' => 'Delivered'
            );
            $query_update_serviceable = $this->Pooling_model->update_serviceable($id, $data);
            // redirect('Pooling/update_order/'+$id);
            // redirect('Pooling');
            // header("Location: $current_url");
        }
        // else
        // {
        //     redirect('Pooling');
        // }
    }
    // ./Actions

    function create_service(){
      try {
        $order_id = $this->input->post('order_id');
        $get_order = $this->Pooling_model->get_detail_order($order_id);
        if($get_order[0]['SERVICE_ID'] != "" || $get_order[0]['SERVICE_ID'] != null){
            $data_update = [
              'SHIPMENT_TYPE' => $this->input->post('SERVICE_SHIPMENT_TYPE'),
              'PN_REQUEST' => $this->input->post('SERVICE_PN_REQUEST'),
              'PN_DELIVERED' => $this->input->post('SERVICE_PN_DELIVERED'),
              'SN_DELIVERED' => $this->input->post('SERVICE_SN_DELIVERED'),
              'AWB_NO' => $this->input->post('SERVICE_AWB'),
              'FLIGHT_NO' => $this->input->post('SERVICE_FLIGHT'),
              'DELIVERY_DATE' => $this->input->post('SERVICE_DELIVERY_DATE').":".date('s'),
              'DELIVERY_ADDRESS' => $this->input->post('SERVICE_DESTINATION_ADDRESS'),
            ];
            $this->Pooling_model->update_service($data_update,$get_order[0]['SERVICE_ID']);
        }else{
          $data_insert = [
            'CREATE_AT' => date('Y-m-d H:i:s'),
            'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
            'SHIPMENT_TYPE' => $this->input->post('SERVICE_SHIPMENT_TYPE'),
            'PN_REQUEST' => $this->input->post('SERVICE_PN_REQUEST'),
            'PN_DELIVERED' => $this->input->post('SERVICE_PN_DELIVERED'),
            'SN_DELIVERED' => $this->input->post('SERVICE_SN_DELIVERED'),
            'AWB_NO' => $this->input->post('SERVICE_AWB'),
            'FLIGHT_NO' => $this->input->post('SERVICE_FLIGHT'),
            'DELIVERY_DATE' => $this->input->post('SERVICE_DELIVERY_DATE').":".date('s'),
            'DELIVERY_ADDRESS' => $this->input->post('SERVICE_DESTINATION_ADDRESS'),
          ];
          $create_service = $this->Pooling_model->create_service($data_insert);
          if(!$create_service) throw new Exception("Failed update serviceable", 1);
          $update_status_service = $this->Pooling_model->update_status_service($order_id,$create_service,"2");
          if(!$update_status_service) throw new Exception("Failed update serviceable", 1);
        }
        
        //SEND EMAIL
        if(isset($_POST['service_email'])){
          for($a=0;$a<count($_POST['service_email']);$a++){
            $data_email = [
              'POOLING_ORDER_ID' => $order_id,
              'DESTINATION' => $_POST['service_email'][$a],
              'SENDER' => 'app.notif@gmf-aeroasia.co.id',
              'STATUS' => 0,
              'CREATE_AT' => date('Y-m-d H:i:s'),
              'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
              'POOLING' => "service"
            ];

            $content = $this->Pooling_model->get_content_email_service($order_id);
            $destination = str_replace('\r\n', "<br>", json_encode($content[0]['DESTINATION']));
            $deliv_date = substr($content[0]['DELIVERY_DATE'],0,16);
            $isi_email = "
            <center><h1>We inform you that your order has been shipped</h1></center>
            <center>
              <table border='1'>
                <tr>
                  <td><b>Customer Order</b></td>
                  <td>{$content[0]['CUSTOMER_ORDER']}</td>
                </tr>
                <tr>
                  <td><b>Aircraft Registration</b></td>
                  <td>{$content[0]['AC_REGISTRATION']}</td>
                </tr>
                <tr>
                  <td><b>Part Number Delivered</b></td>
                  <td>{$content[0]['PN_DELIVERED']}</td>
                </tr>
                <tr>
                  <td><b>Serial Number Delivered</b></td>
                  <td>{$content[0]['SN_DELIVERED']}</td>
                </tr>
                <tr>
                  <td><b>AWB Number</b></td>
                  <td>{$content[0]['AWB_NO']}</td>
                </tr>
                <tr>
                  <td><b>Flight Number</b></td>
                  <td>{$content[0]['FLIGHT_NO']}</td>
                </tr>
                <tr>
                  <td><b>Delivery Date</b></td>
                  <td>{$deliv_date}</td>
                </tr>
                <tr>
                  <td><b>Destination Address</b></td>
                  <td>{$destination}</td>
                </tr>
              </table>  
            </center>
            ";
            $send_email = $this->email_service($_POST['service_email'][$a],$isi_email);
            if($send_email){
              $data_email['STATUS'] = 1;
            }else{
              $data_email['STATUS'] = 0;
            }

            $this->Pooling_model->insert_email($data_email);
            $data_email = [];
          }
        }

        $this->session->set_flashdata('alert_success', 'Success update serviceable');
      } catch (Exception $e) {
        $this->session->set_flashdata('alert_failed', $e->getMessage());
      }
      redirect("Pooling/update_order/$order_id/2");
    }

    function create_unservice(){
      try {
        $order_id = $this->input->post('order_id');
        $get_order = $this->Pooling_model->get_detail_order($order_id);
        if($get_order[0]['UNSERVICE_ID'] != "" || $get_order[0]['UNSERVICE_ID'] != null){
          $data_update = [
            'SEND_TYPE' => $this->input->post('unservice_send'),
            'SHIPMENT_TYPE' => $this->input->post('unservice_shipment'),
            'PN_REQUEST' => $this->input->post('unservice_pn_request'),
            'PN_REMOVED' => $this->input->post('unservice_pn_removed'),
            'SN_REMOVED' => $this->input->post('unservice_sn_removed'),
            'AWB_NO' => $this->input->post('unservice_awb'),
            'FLIGHT_NO' => $this->input->post('unservice_flight'),
            'DELIVERY_DATE' => $this->input->post('unservice_delivery_date').":".date('s'),
            'REMARK' => $this->input->post('unservice_remark'),
            'REMOVAL_REASON_TYPE' => $this->input->post('unservice_removal_reason_type'),
            'TARGET_UNSERVICE_RETURN' => $this->input->post('unservice_target_return').":".date('s'),
            'TSN' => $this->input->post('unservice_tsn'),
            'CSN' => $this->input->post('unservice_csn'),
          ];

          if($data_update['SEND_TYPE'] == "1"){
            $data_update['REMOVAL_DATE'] = null;
            $data_update['AC_REGISTRATION'] = "";
            $data_update['REMOVAL_REASON'] = "";
          }else{
            $data_update['REMOVAL_DATE'] = $this->input->post('unservice_removal_date');
            $data_update['AC_REGISTRATION'] = ($this->input->post('unservice_ac_val') == "OTHER AIRCRAFT REGISTRATION" ? strtoupper($this->input->post('unservice_ac_other')) : $this->input->post('unservice_ac_val'));
            $data_update['REMOVAL_REASON'] = $this->input->post('unservice_removal_reason');
          }
          $this->Pooling_model->update_unservice($data_update,$get_order[0]['UNSERVICE_ID']);
        }else{
          $data_insert = [
            'CREATE_AT' => date('Y-m-d H:i:s'),
            'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
            'SEND_TYPE' => $this->input->post('unservice_send'),
            'SHIPMENT_TYPE' => $this->input->post('unservice_shipment'),
            'PN_REQUEST' => $this->input->post('unservice_pn_request'),
            'PN_REMOVED' => $this->input->post('unservice_pn_removed'),
            'SN_REMOVED' => $this->input->post('unservice_sn_removed'),
            'AWB_NO' => $this->input->post('unservice_awb'),
            'FLIGHT_NO' => $this->input->post('unservice_flight'),
            'DELIVERY_DATE' => $this->input->post('unservice_delivery_date').":".date('s'),
            'REMARK' => $this->input->post('unservice_remark'),
            'REMOVAL_REASON_TYPE' => $this->input->post('unservice_removal_reason_type'),
            'TARGET_UNSERVICE_RETURN' => $this->input->post('unservice_target_return').":".date('s'),
            'TSN' => $this->input->post('unservice_tsn'),
            'CSN' => $this->input->post('unservice_csn'),
          ];

          if($data_insert['SEND_TYPE'] == "1"){
            $data_insert['REMOVAL_DATE'] = null;
            $data_insert['AC_REGISTRATION'] = "";
            $data_insert['REMOVAL_REASON'] = "";
          }else{
            $data_insert['REMOVAL_DATE'] = $this->input->post('unservice_removal_date');
            $data_insert['AC_REGISTRATION'] = ($this->input->post('unservice_ac_val') == "OTHER AIRCRAFT REGISTRATION" ? strtoupper($this->input->post('unservice_ac_other')) : $this->input->post('unservice_ac_val'));
            $data_insert['REMOVAL_REASON'] = $this->input->post('unservice_removal_reason');
          }

          $create_unservice = $this->Pooling_model->create_unservice($data_insert);
          if(!$create_unservice) throw new Exception("Failed update unserviceable", 1);
          
          $update_status_unservice = $this->Pooling_model->update_status_unservice($order_id,$create_unservice,"2");
          if(!$update_status_unservice) throw new Exception("Failed update unserviceable", 1);
        }

        //SEND EMAIL
        if(isset($_POST['unservice_email'])){
          for($a=0;$a<count($_POST['unservice_email']);$a++){
            $data_email = [
              'POOLING_ORDER_ID' => $order_id,
              'DESTINATION' => $_POST['unservice_email'][$a],
              'SENDER' => 'app.notif@gmf-aeroasia.co.id',
              'STATUS' => 0,
              'CREATE_AT' => date('Y-m-d H:i:s'),
              'CREATE_BY' => $this->session->userdata('log_sess_id_user'),
              'POOLING' => "unservice"
            ];

            $content = $this->Pooling_model->get_content_email_unservice($order_id);
            $deliv_date = substr($content[0]['DELIVERY_DATE'],0,16);
            $destination = str_replace('\r\n', "<br>", json_encode($content[0]['DESTINATION']));
            $isi_email = "
            <center><h1>Your Unserviceable part has been returned</h1></center>
            <center>
              <table border='1'>
                <tr>
                  <td><b>Customer Order</b></td>
                  <td>{$content[0]['CUSTOMER_ORDER']}</td>
                </tr>
                <tr>
                  <td><b>Aircraft Registration</b></td>
                  <td>{$content[0]['AC_REGISTRATION']}</td>
                </tr>
                <tr>
                  <td><b>Part Number Delivered</b></td>
                  <td>{$content[0]['PN_DELIVERED']}</td>
                </tr>
                <tr>
                  <td><b>Serial Number Delivered</b></td>
                  <td>{$content[0]['SN_DELIVERED']}</td>
                </tr>
                <tr>
                  <td><b>AWB Number</b></td>
                  <td>{$content[0]['AWB_NO']}</td>
                </tr>
                <tr>
                  <td><b>Flight Number</b></td>
                  <td>{$content[0]['FLIGHT_NO']}</td>
                </tr>
                <tr>
                  <td><b>Delivery Date</b></td>
                  <td>{$deliv_date}</td>
                </tr>
                <tr>
                  <td><b>Destionation Address</b></td>
                  <td>{$destination}</td>
                </tr>
              </table>  
            </center>
            ";
            $send_email = $this->email_unservice($_POST['unservice_email'][$a],$isi_email);
            if($send_email){
              $data_email['STATUS'] = 1;
            }else{
              $data_email['STATUS'] = 0;
            }

            $this->Pooling_model->insert_email($data_email);
            $data_email = [];
          }
        }

        $this->session->set_flashdata('alert_success', 'Success update unserviceable');
      } catch (Exception $e) {
         $this->session->set_flashdata('alert_failed', $e->getMessage());
      }
      redirect("Pooling/update_order/$order_id/2");
    }
}