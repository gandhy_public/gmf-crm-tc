<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Services_control extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('services_model', '', TRUE);
        //$this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
        $this->data["open_menu"] = TRUE; 
    }

    function pooling() {
        $data["session"]      = $this->session->userdata('logged_in');
        $data['content']      = 'administrator/services/pooling/index';
        $data['pooling']      = TRUE;
        $this->load->view('template', $data);
    }
    function loan() {
        $data["session"]      = $this->session->userdata('logged_in');
        $data['content']      = 'administrator/services/loan/index';
        $data['loan']         = TRUE;
        $this->load->view('template', $data);
    }
    function retail() {
        $data["session"]      = $this->session->userdata('logged_in');
        $data['content']      = 'administrator/services/retail/index';
        $data['retail']       = TRUE;
        $this->load->view('template', $data);
    }
    function landinggear() {
        $data["session"]      = $this->session->userdata('logged_in');
        $data['content']      = 'administrator/services/landinggear/index';
        $data['landinggear']  = TRUE;
        $this->load->view('template', $data);
    }
}