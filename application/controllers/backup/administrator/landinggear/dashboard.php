<?php

class Dashboard extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model('services/landinggear/dashboard_model', '', TRUE);
        // $this->load->model('notification_model', '', TRUE);
        // $this->data["new_order"] = $this->notification_model->get_new_order();

        $this->data["session"] = $this->session->userdata('logged_in');

        if ($this->data["session"]["group_id"] != "1") {

            redirect('index.php/login/logout', 'refresh');
        }
    }

    function index() {

        $data["session"] 		= $this->session->userdata('logged_in');
        $data['content'] 		= 'administrator/services/landinggear/dashboard/index';
        $data['listProject'] 	= $this->dashboard_model->getlistProject();
        $this->load->view('template', $data);
    }	
	
	function overview($docno) {
		$data["docno"] 			= $docno;
		$data["session"] 		= $this->session->userdata('logged_in');
		$data['header_menu'] 	= 'administrator/services/landinggear/header_menu';
		$data['title_menu']		= 'administrator/services/landinggear/title_menu';
		$data['title'] 			= "OVERVIEW";
        $data['content']		= 'administrator/services/landinggear/dashboard/overview';
        $data['listProject'] 	= $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('template', $data);
	}
	
	function profit_analisyst($docno) {
		$data["docno"] 			= $docno;
		$data["session"]		= $this->session->userdata('logged_in');
		$data['header_menu'] 	= 'administrator/services/landinggear/header_menu';
		$data['title_menu'] 	= 'administrator/services/landinggear/title_menu';
		$data['title'] 			= "PROFIT ANALYSIST";
        $data['content'] 		= 'administrator/services/landinggear/dashboard/profit_analisyst';
        /*$data['listProject'] = $this->dashboard_model->getOverviewProject($docno);*/
        $this->load->view('template', $data);
	}
	
	function lo_report($docno) {
		$data["docno"] 			= $docno;
		$data["session"] 		= $this->session->userdata('logged_in');
		$data['header_menu']	= 'administrator/services/landinggear/header_menu';
		$data['title_menu'] 	= 'administrator/services/landinggear/title_menu';
		$data['title'] 			= "EO-REPORT";
        $data['content'] 		= 'administrator/services/landinggear/dashboard/lo_report';
        /*$data['listProject'] = $this->dashboard_model->getOverviewProject($docno);*/
        $this->load->view('template', $data);
	}
	
	
	 function header_menu($docno) {
        $data["docno"]			= $docno;
        $data['listProject'] 	= $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('administrator/services/landinggear/header_menu', $data);
    }
	
	

}