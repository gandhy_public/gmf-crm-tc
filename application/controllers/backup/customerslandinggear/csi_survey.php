<?php

class Csi_survey extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('dashboard_model', '', TRUE);
        //$this->load->model('notification_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "4") {
            redirect('index.php/login/logout', 'refresh');
        }
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'oustomerslandinggear/project_list/csi_survey/index';
        $this->load->view('template_landinggear', $data);
    }
    

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

