<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

class Csi_survey extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }
        $this->load->model('Survey_model', '', TRUE);
        $this->load->model('Retail_model', '', TRUE);
        $this->load->model('Landinggear_model', '', TRUE);
        $this->load->model('pooling_model', '', TRUE);
    }

    function index() {
        $id_customer = $this->session->userdata('log_sess_id_customer');
        if ($id_customer){
            $data['customer_list']      = $this->Survey_model->get_customer_list($id_customer);
            $data['content']            = 'csi_survey/form';
        }else {
            $data['customer_list']      = $this->Survey_model->get_customer_list(False);
            $data['year_list']          = $this->Survey_model->get_year_list();
            $data['content']            = 'csi_survey/summary';
        }

        $this->load->view('template', $data);
    }

    public function summary()
    {
        $data['customer_list']      = $this->Survey_model->get_customer_list(False);
        $data['year_list']          = $this->Survey_model->get_year_list();
        $data['content']            = 'csi_survey/summary_customer';
        $data['customer']           = $this->session->userdata('log_sess_id_customer');
        $this->load->view('template', $data);
    }

    public function submit_survey()
    {
        if(isset($_POST['insert']))
        {
            $query_insert_survey           = $this->Survey_model->insert_survey();
            if($query_insert_survey)
            {
                $this->session->set_flashdata('alert_success','Submit Survey success');
                redirect('csi_survey');
            } 
        }
        else
        {   $this->session->set_flashdata('alert_failed','Submit Survey Failed');
            redirect('csi_survey');
        }
    }

    public function create_survey()
    {
        try {
            if((!isset($_POST['insert'])) && ($_POST['insert'] != 1)){
                throw new Exception("Submit Survey Failed", 1);
            }

            $type = $this->input->post('type');
            $period = $this->input->post('period');

            $quality = $this->input->post('quality');
            $cost = $this->input->post('cost');
            $delivery = $this->input->post('delivery');
            $services = $this->input->post('services');
            $recomendation = $this->input->post('recomendation');

            $id_customer = $this->session->userdata('log_sess_id_customer');
            $id_user = $this->session->userdata('log_sess_id_user');

            $cek_survey = $this->Survey_model->cek_survey($type,$period,date('Y'),$id_customer);
            if($cek_survey){
                throw new Exception("Survey already exists", 1);
            }else{
                $data = [
                    'USER_ID' => $id_user,
                    'PERIOD' => $period,
                    'TYPE_OF_SERVICE' => $type,
                    'YEAR' => date('Y'),
                    'QUALITY_RATE' => $quality,
                    'DELIVERY_RATE' => $delivery,
                    'COST_RATE' => $cost,
                    'SERVICE_RATE' => $services,
                    'CREATED_DATE' => date('Y-m-d H:i:s'),
                    'RECOMENDATION' => $recomendation,
                    'CUSTOMER_ID' => $id_customer
                ];
                $create_survey = $this->Survey_model->create_survey($data);
                if(!$create_survey){
                    throw new Exception("Submit Survey Failed", 1);
                }
            }

            $this->session->set_flashdata('alert_success','Submit Survey success');
            redirect('csi_survey');
        } catch (Exception $e) {
            $this->session->set_flashdata('alert_failed',$e->getMessage());
            redirect('csi_survey');
        }
    }

    public function data($type){
        $data = $this->Survey_model->getDocumentNumber($type);
        echo json_encode($data); 
    }

    public function detail()
    {
        $type = $this->input->post('type');
        $customer = $this->input->post('customer');
        $tahun = $this->input->post('tahun');

        $detail = $this->Survey_model->detail_survey($type,$customer,$tahun);
        if(count($detail)){
            $resp[1] = [
                'status' => "null",
                'nilai_angka' => "0",
                'nilai_huruf' => "X",
                'up_down' => "up",
                'recomendation' => "",
                'follow_up' => "",
                'id' => "0",
            ];
            $resp[2] = [
                'status' => "null",
                'nilai_angka' => "0",
                'nilai_huruf' => "X",
                'up_down' => "down",
                'recomendation' => "",
                'follow_up' => "",
                'id' => "0",
            ];

            $a = 0;
            foreach ($detail as $key) {
                $average = ($key['QUALITY_RATE'] + $key['DELIVERY_RATE'] + $key['COST_RATE'] + $key['SERVICE_RATE'])/4;

                if($average <= 0.9){
                    $resp[$key['PERIOD']]['status'] = "Very Bad";
                    $resp[$key['PERIOD']]['nilai_angka'] = $average;
                    $resp[$key['PERIOD']]['nilai_huruf'] = "E";
                }elseif($average <= 1.9){
                    $resp[$key['PERIOD']]['status'] = "Bad";
                    $resp[$key['PERIOD']]['nilai_angka'] = $average;
                    $resp[$key['PERIOD']]['nilai_huruf'] = "D";
                }elseif($average <= 2.9){
                    $resp[$key['PERIOD']]['status'] = "Fair";
                    $resp[$key['PERIOD']]['nilai_angka'] = $average;
                    $resp[$key['PERIOD']]['nilai_huruf'] = "C";
                }elseif($average <= 3.9){
                    $resp[$key['PERIOD']]['status'] = "Good";
                    $resp[$key['PERIOD']]['nilai_angka'] = $average;
                    $resp[$key['PERIOD']]['nilai_huruf'] = "B";
                }elseif($average <= 5){
                    $resp[$key['PERIOD']]['status'] = "Very Good";
                    $resp[$key['PERIOD']]['nilai_angka'] = $average;
                    $resp[$key['PERIOD']]['nilai_huruf'] = "A";
                }

                $resp[$key['PERIOD']]['recomendation'] = $key['RECOMENDATION'];
                $resp[$key['PERIOD']]['follow_up'] = $key['FOLLOW_UP'];
                $resp[$key['PERIOD']]['id'] = $key['ID_SURVEY'];

                $a++;
            }

            $resp[2]['up_down'] = ($resp[2]['nilai_angka'] > $resp[1]['nilai_angka']) ? "up" : "down";

            echo json_encode($resp);
        }else{
            echo false;
        }
    }

    public function follow_up()
    {
        $val_follow_1 = $this->input->post('follow_1');
        $val_follow_2 = $this->input->post('follow_2');

        $id_follow_1 = $this->input->post('id_follow_1');
        $id_follow_2 = $this->input->post('id_follow_2');

        $resp[0] = [
            'semester1' => "",
            'semester2' => ""
        ];
        
        if($id_follow_2 != "0"){
            $update_semester2 = $this->Survey_model->follow_up($id_follow_2,$val_follow_2);
            if($update_semester2){
                $resp[0]['semester2'] = "sukses";
            }else{
                $resp[0]['semester2'] = "gagal";
            }

            $update_semester1 = $this->Survey_model->follow_up($id_follow_1,$val_follow_1);
            if($update_semester1){
                $resp[0]['semester1'] = "sukses";
            }else{
                $resp[0]['semester1'] = "gagal";
            }
        }else{
            $update_semester1 = $this->Survey_model->follow_up($id_follow_1,$val_follow_1);
            if($update_semester1){
                $resp[0]['semester1'] = "sukses";
            }else{
                $resp[0]['semester1'] = "gagal";
            }
        }

        echo json_encode($resp);
    }
    
}

