<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Welcome');
        }

        $this->load->model('Login_model', '', TRUE);
    }

    function index() {
        $this->load->view('login');
    }

    function action_login()
    {
        if(isset($_POST['login']))
        {
            $cheking = $this->Login_model->cek_login();
            if($cheking)
            {
                $this->session->set_flashdata('login_success', 'Login Success ! Welcome to App CRM - TC :)');
                // if ($this->session->userdata('log_sess_id_user_role')==3) {
                //   if ($this->session->userdata('log_sess_id_user_group')==1) {
                //     // code...
                //     redirect('pooling');
                //   } else if ($this->session->userdata('log_sess_id_user_group')==2) {
                //     // code...
                //     redirect('Loan_exchange');
                //   } else if ($this->session->userdata('log_sess_id_user_group')==3) {
                //     // code...
                //     redirect('retail');
                //   } else if ($this->session->userdata('log_sess_id_user_group')==4) {
                //     redirect('landing_gear');
                //   }
                // }else{
                //   redirect('Dashboard');
                // }

                redirect('Welcome');
            }
            else
            {
                $this->session->set_flashdata('login_failed', 'Login Failed, Check your Username and Password!!!');
                redirect('Login');
            }
        }
        else
        {
            redirect('Login');
        }
    }

}
?>
