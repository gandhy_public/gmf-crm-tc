<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    // Construct

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user'))) 
        {
            redirect('Login');
        }

        $this->load->model('User_model', '', TRUE);
        $this->load->library('pagination');
    }    

    // ./Construct



    // Parsing Public Data

    public $data = array(
        'ldt1'              => 'Users',
        'ldl1'              => 'index.php/Users',
        'ldi1'              => 'fa fa-users',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'users/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // fa fa-refresh fa-spin fa-fw
    // ./Parsing Public Data



    // View

    function index() {
        redirect('Users/list_user');
    }

    function list_user() {
        $this->data['data_table']       = $this->User_model->select_user();

        $this->data['title']            = 'User List';
        $this->data['icon']             = 'fa fa-inbox';
        $this->data['content']          = 'users/list_user';
        $this->load->view('template',$this->data);
    }

    function create_user() {
        $this->data['data_user_role']   = $this->User_model->get_user_role();
        $this->data['data_user_group']  = $this->User_model->get_user_group();
        $this->data['data_customer']    = $this->User_model->get_customer();

        $this->data['title']            = 'Create User';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'users/create_user';
        $this->load->view('template',$this->data);
    }

    function update_user($ID_USER) {
        $this->data['data_user_role']   = $this->User_model->get_user_role();
        $this->data['data_user_group']  = $this->User_model->get_user_group();
        $this->data['data_customer']    = $this->User_model->get_customer();

        $this->data['data_user']        = $this->User_model->select_user_where_id($ID_USER);
        // echo '<pre>';
        // print_r($this->User_model->select_user_where_id($ID_USER));
        // die;

        $this->data['title']            = 'Update User';
        $this->data['icon']             = 'fa fa-edit';
        $this->data['content']          = 'users/update_user';
        $this->load->view('template',$this->data);
    }

    // ./ View



    // Actions

    public function action_insert() 
    {
        if(isset($_POST['insert'])) 
        {
            $query_insert_user                       = $this->User_model->insert_user();
            if($query_insert_user)
            {
                $this->session->set_flashdata('alert_success','Add Data User Success');
                redirect('Users/list_user');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Add Data User Failed');
            redirect('Users/create_user');
        }
    }

    public function action_update() 
    {
        if(isset($_POST['insert'])) 
        {
            $ID_USER            = $this->input->post('ID_USER');
            $query_update_user  = $this->User_model->update_user($ID_USER);
            if($query_update_user)
            {
                $this->session->set_flashdata('alert_success','Update Data User Success');
                redirect('Users/list_user');
            }
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Update Data User Failed');
            redirect('Users/update_user'.'/'.$ID_USER);
        }
    }

    public function action_delete($ID_USER)
    {
        $query_delete = $this->User_model->delete_user($ID_USER);
        if($query_delete)
        {   
            $this->session->set_flashdata('alert_success','Delete Data User Success');
            redirect('Users/list_user');
        }
        else
        {
            $this->session->set_flashdata('alert_failed','Delete Data User Failed');
            redirect('Users/list_user');
        }
    }

    // ./Actions
}

?>