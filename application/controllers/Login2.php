<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login2 extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Welcome');
        }

        $this->load->model('Login2_model', '', TRUE);
        $this->load->model('Currency_model', '', TRUE);
        $this->Currency_model->recalculate(FALSE, TRUE);
    }

    function index() {
        $this->load->view('login');
    }

    function action_login() {
        try {
            $username = $this->input->post('username');
            if(!isset($username)) throw new Exception("Username tidak boleh kosong!", 1);
            if(empty($username)) throw new Exception("Username tidak boleh kosong!", 1);
            $password = $this->input->post('password');
            if(!isset($password)) throw new Exception("Password tidak boleh kosong!", 1);
            if(empty($password)) throw new Exception("Password tidak boleh kosong!", 1);

            $cek_user = $this->Login2_model->cekUser($username,$password);
            //print("<pre>".print_r($cek_user,true)."</pre>");
            if($cek_user['codestatus'] != 'S') throw new Exception($cek_user['message'], 1);
            $update_last_login = $this->Login2_model->updateLastLogin($cek_user['resultdata'][0]->user_id);
            $log = array(
                'log_sess_id_user'                  => $cek_user['resultdata'][0]->user_id,
                'log_sess_name'                     => $cek_user['resultdata'][0]->user_nama,
                'log_sess_username'                 => $cek_user['resultdata'][0]->user_username,
                'log_sess_email'                    => $cek_user['resultdata'][0]->user_email,
                'log_sess_id_user_role'             => $cek_user['resultdata'][0]->user_role_id,
                'log_sess_user_role'                => $cek_user['resultdata'][0]->user_role,
                'log_sess_level_user_role'          => $cek_user['resultdata'][0]->user_role_level,
                'log_sess_id_user_group'            => $cek_user['resultdata'][0]->user_group_id,
                'log_sess_id_customer'              => $cek_user['resultdata'][0]->user_customer_id,
            );

            $get_menu = $this->Login2_model->getMenu($cek_user['resultdata'][0]->user_group_id);
            $cookie_name = "menu";
            $cookie_value = json_encode($get_menu);
            setcookie($cookie_name, $cookie_value, 2147483647, "/");
            //$log['menu'] = $get_menu;
            
            $this->session->set_userdata($log);
            redirect('Welcome');
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->session->set_flashdata('login',"$message");
            redirect('Login2');
        }
    }

}
?>
