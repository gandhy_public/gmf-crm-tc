<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retail extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('Retail_model', '', TRUE);
        $this->load->model('Currency_model', '', TRUE);
        $this->load->model('Customers_m', '', TRUE);
        $this->load->library('pagination');
    }

    // Parsing Public Data
    public $data = array(
        'ldt1'              => 'Retail',
        'ldl1'              => 'index.php/Retail/dashboard',
        'ldi1'              => 'fa fa-tags',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'retail/nav_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    // ./Parsing Public Data

    function index()
    {
        redirect('Retail/dashboard');
    }

    function dashboard() {
        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data["tahun"]      = date('Y');
        $this->data["bulan"]      = date('m');
        $this->data["filter"]     = "close_date";
        $this->data["customer"]   = $this->Retail_model->getAllCustomer($this->session->userdata('log_sess_id_customer'));
        //print('<pre>'.print_r($this->data["customer"],TRUE).'</pre>');die();
        $kunnr = '';
        // $this->data["period_years"] = $this->Retail_model->get_date_retail();
        $this->data["session"]    = $this->session->userdata('logged_in');
        // $this->data["tahun"]      = $tahun;
        // $this->data["period_years"] = $this->Retail_model->get_date_retail()
        // $listStatus = $this->Retail_model->getStatusOrderPerTahun($this->data["tahun"], $kunnr);
        // $result = $this->Retail_model->select_retailPerTahun($this->data["tahun"] , $kunnr);
        // $listStatus = $this->Retail_model->getStatusOrderPerTahun($this->data["tahun"], $kunnr, $this->data["bulan"], $this->data["filter"] );
        // echo $this->db->last_query();
        // // print_r($listStatus);
        // die;
        // $result = $this->Retail_model->select_retailPerTahun($this->data["tahun"], $kunnr, $this->data["bulan"], $this->data["filter"]);
        // $tmp1 = [];
        //$tmp2 = [];
        // $tmp11 = [];
        // $tmp22 = [];
        // foreach ($listStatus as $key => $value) {
        //     $tmp1[] = $value;
        //   }

        // $chart = [0,0,0, 0];
        // // $result = $this->Retail_model->select_retailPerTahun($tahun);
        // $txt = 50;
        // $tmp = [];
        // $n_result = []; 
        //   if (isset($result)) {
        //     foreach ($result as $key => $value) {
        //         $tmp = $value;
        //         if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
        //             $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
        //             // $teco_date = $tmp['TECO_DATE'];
        //             // $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
        //             // $e = date_create(date('Y'));
                 
        //             // // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
        //             // $diff = (int) date_diff($s, $e)->format("%a");
        //             // // echo $diff;echo " diff<br>";
        //             // if ( $diff > 5) {
        //             //     $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
        //             //     // print_r($newdate);echo " newdate <br>";
        //             //     $date2 = date('Ymd', strtotime($newdate));
        //             // }else{  
        //               $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
        //             // }

        //             $start = date_create($date1);
        //             $end = date_create($date2);

        //         }else{
        //             $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
        //             $date2 = date('Ymd');

        //             $start = date_create($date1);
        //             $end = date_create($date2);

        //         }

        //         $tmp['RECEIVED_DATE'] = date('d-m-Y', strtotime($tmp['RECEIVED_DATE']));
                
        //         // $tmp['STATUS'] = '';
        //         $tmp['DELIVERY_DATE'] = '';
        //         $tmp['REMARKS'] = '';
        //         $tmp['ID_RETAIL'] = '';
        //         $tmp['QUOTATION_DATE'] = '';
        //         $tmp['APPROVAL_DATE'] = '';
        //         $day_waiting = 0;

        //         for ($i=1; $i <= $txt ; $i++) { 
        //             if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
        //               break;

        //             } 
        //             // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
        //             if (strpos($value["TXT".$i], 'SROA') !== false) {

        //                 $tmp['QUOTATION_DATE'] = date('d-m-Y', strtotime($value["UDATE".$i]));
        //                 $nexti = $i+1;
        //                 for ($ii=$i+1; $ii <= $txt ; $ii++)
        //                     if ((strpos($value["TXT".$ii], 'WR') !== false) or (strpos($value["TXT".$ii], 'SROA') !== false)){
                               
        //                         $tmp['APPROVAL_DATE'] = date('d-m-Y', strtotime($value["UDATE".$ii]));
        //                         $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
        //                     break 1;  
        //                     }
        //                 // $tmp[][$i] = $value["UDATE".$i];
                 
        //             }
        //         }
        //         // if ($tmp['ERDAT_VBFA'] != '') {
        //           if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
        //             $tmp22[] = $value;
        //           }else if ((strpos($tmp['STAT'], 'WR') !== false)){
        //             //or (strpos($tmp['STAT'], 'UR') !== false)){
        //             $tmp11[] = $value;
        //           }
        //         // }
        //         $tat = (date_diff($start, $end)->format("%a")) - $day_waiting;
        //         $chart[3]++;
        //         if ($tat<10) {
        //             $chart[2]++;
        //         }else if ($tat>9 && $tat<21) {
        //             $chart[1]++;
        //         }else if ($tat>20) {
        //             $chart[0]++;
        //         }
        //     }
        //     // print_r($chart);
        //     // die;
        //     $sum_wr1 = count($tmp11);
        //     $sum_wr2 = count($tmp22);
        //     if ($sum_wr2 != 0) {
        //         array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)) );
        //     }
        //     if ($sum_wr1 != 0){
        //         array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
        //     }
        
        // }
        // $this->data['listStatus'] = ($tmp1);
        // $total = 0 ;
        // $provider = ($tmp1);
        // foreach ($tmp1 as $key => $value) {
        //   $total += $value->JUMLAH;
        // }
        
        // //listStatus =========================================

        // // $this->data['chart_label']     = $str_labels;
        // // $this->data['chart_data']     = $str_data;

        // $this->data['provider']     = json_encode($provider);

        // // $this->data['grand_total']     = $total;
        // $this->data['sum_grand_total'] = $total;
        // $this->data['chart']        = $chart;
        $this->data['title']            = 'Dashboard';
        $this->data['icon']             = 'fa fa-dashboard';
        $this->data['content']          = 'retail/dashboard'; //view dashboard
        $this->load->view('template', $this->data);
    }

    function dashboard2(){
        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data["tahun"]      = date('Y');
        $this->data["bulan"]      = date('m');
        $this->data["filter"]     = "close_date";
        $this->data["customer"]   = $this->Retail_model->getAllCustomer($this->session->userdata('log_sess_id_customer'));
        
        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data['title']            = 'Dashboard';
        $this->data['icon']             = 'fa fa-dashboard';
        $this->data['content']          = 'retail/dashboard_new'; //view dashboard
        $this->load->view('template', $this->data);
    }

    function search_dashboard(){
        $key = $this->input->post('search');
        if($key){
            $this->data['key'] = $key;
        }else{
            $this->data['key'] = "";
        }
        $customer = "all";
        if($this->session->userdata('log_sess_id_customer')){
            $customer = $this->session->userdata('log_sess_id_customer');
        }

        $list_data = $this->Retail_model->search_ro($key,$customer);
        $this->data['data'] = $list_data;
        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data['title']            = 'Search by RO (Dashboard)';
        $this->data['icon']             = '';
        $this->data['content']          = 'retail/search_dashboard';
        $this->load->view('template', $this->data);
    }

    function search_finance(){
        $key = $this->input->post('search');
        if($key){
            $this->data['key'] = $key;
        }else{
            $this->data['key'] = "";
        }
        $customer = "all";
        if($this->session->userdata('log_sess_id_customer')){
            $customer = $this->session->userdata('log_sess_id_customer');
        }

        $list_data = $this->Retail_model->search_ro($key,$customer);
        $this->data['data'] = $list_data;
        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data['title']            = 'Search by RO (Finance)';
        $this->data['icon']             = '';
        $this->data['content']          = 'retail/search_finance';
        $this->load->view('template', $this->data);
    }

    function change_dashboard2(){
        error_reporting(0);
        $type = $_REQUEST['date_type'];
        $customer  = $_REQUEST['customer'];
        $month  = $_REQUEST['month'];
        $year  = $_REQUEST['year'];

        $listStatus = $this->Retail_model->getStatusOrderPerTahun2($type,$customer,$month,$year);
        $data_status = array();

        foreach ($listStatus as $key) {
            $data_status[$key['TXT_STAT']]['STATUS'] = $key['TXT_STAT'];
            $data_status[$key['TXT_STAT']]['TOTAL_ALL'] = count($listStatus);
            $data_status[$key['TXT_STAT']]['TOTAL']++;
        }
       
        $return['data_status'] = $data_status;
        echo json_encode($return);
    }

    function change_dashboard() {
        $date_type = $_REQUEST['date_type'];
        $customer  = $_REQUEST['customer'];
        $month  = $_REQUEST['month'];
        $year  = $_REQUEST['year'];

        if ($customer == 'all') {
          $customer = '';
        }

        $this->data["session"]    = $this->session->userdata('logged_in');
        $this->data["tahun"]      = $year;
        //$this->data["period_years"] = $this->Retail_model->get_date_retail();
        if ($this->session->userdata('log_sess_id_customer')) {
          $kunnr = $this->session->userdata('log_sess_id_customer');
        }
        $listStatus = $this->Retail_model->getStatusOrderPerTahun($year, $customer, $month, $date_type);
        $result = $this->Retail_model->select_retailPerTahun($year, $customer, $month, $date_type);

        $tmp1 = [];
        $tmp11 = [];
        $tmp22 = [];
        foreach ($listStatus as $key => $value) {
            $tmp1[] = $value;
        }

        $chart = [0,0,0,0];
        //$result = $this->Retail_model->select_retailPerTahun($tahun);
        $txt = 50;
        $tmp = [];
        $n_result = []; 
        if (isset($result)) {
            foreach ($result as $key => $value) {
                $tmp = $value;
                if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    // $teco_date = $tmp['TECO_DATE'];
                    // $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
                    // $e = date_create(date('Y'));
                 
                    // // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
                    // $diff = (int) date_diff($s, $e)->format("%a");
                    // // echo $diff;echo " diff<br>";
                    // if ( $diff > 5) {
                    //     $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
                    //     // print_r($newdate);echo " newdate <br>";
                    //     $date2 = date('Ymd', strtotime($newdate));
                    // }else{
                      
                    $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
                    // }

                    $start = date_create($date1);
                    $end = date_create($date2);
                }else{
                    $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
                    $date2 = date('Ymd');

                    $start = date_create($date1);
                    $end = date_create($date2);
                }

                $tmp['RECEIVED_DATE'] = date('d-m-Y', strtotime($tmp['RECEIVED_DATE']));
                
                // $tmp['STATUS'] = '';
                $tmp['DELIVERY_DATE'] = '';
                $tmp['REMARKS'] = '';
                $tmp['ID_RETAIL'] = '';
                $tmp['QUOTATION_DATE'] = '';
                $tmp['APPROVAL_DATE'] = '';
                $day_waiting = 0;

                for ($i=1; $i <= $txt ; $i++) { 
                    if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
                      break;
                    } 
                    // echo $i."->".$value["MAINTENANCE_ORDER"]."->".$value["TXT".$i]."->".$value["UDATE".$i]."<br>";
                    if (strpos($value["TXT".$i], 'SROA') !== false) {
                        $tmp['QUOTATION_DATE'] = date('d-m-Y', strtotime($value["UDATE".$i]));
                        $nexti = $i+1;
                        for ($ii=$i+1; $ii <= $txt ; $ii++)
                            if ((strpos($value["TXT".$ii], 'WR') !== false) or (strpos($value["TXT".$ii], 'SROA') !== false)){
                                $tmp['APPROVAL_DATE'] = date('d-m-Y', strtotime($value["UDATE".$ii]));
                                $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
                            break 1;  
                            }
                        // $tmp[][$i] = $value["UDATE".$i];
                    }
                }

                // if ($tmp['ERDAT_VBFA'] != '') {
                  if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
                    $tmp22[] = $value;
                  }else if ((strpos($tmp['STAT'], 'WR') !== false)){
                    //or (strpos($tmp['STAT'], 'UR') !== false)){
                    $tmp11[] = $value;
                  }
                // }
                $tat = (date_diff($start, $end)->format("%a")) - $day_waiting;
                $chart[3]++;
                if ($tat<10) {
                    $chart[2]++;
                }else if ($tat>9 && $tat<21) {
                    $chart[1]++;
                }else if ($tat>20) {
                    $chart[0]++;
                }
            }
            // print_r($chart);
            // die;
            $sum_wr1 = count($tmp11);
            $sum_wr2 = count($tmp22);
            if ($sum_wr2 != 0) {
                array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)) );
            }
            if ($sum_wr1 != 0){
                array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
            }
        }

        $this->data['listStatus'] = $tmp1;
        $total = 0 ;
        $provider = ($tmp1);
        foreach ($tmp1 as $key => $value) {
          $total += $value->JUMLAH;
        }
            
        //listStatus =========================================

        // $this->data['chart_label']     = $str_labels;
        // $this->data['chart_data']     = $str_data;

        $this->data['provider']     = $provider;

        // $this->data['grand_total']     = $total;
        $this->data['sum_grand_total'] = $total;
        $this->data['chart']        = $chart;
        echo json_encode($this->data);
    }

    function finance()
    {
        $sess_id_cust = $this->session->userdata('log_sess_id_customer');
        $customer_list = (!empty($sess_id_cust) ? $this->Customers_m->get_customer($sess_id_cust) : $this->Customers_m->get_customer_list());

        $cur_customer = $this->input->post('cur_customer');
        if(!empty($sess_id_cust)) $cur_customer = $sess_id_cust;

        $this->data['cur_customer']     = $cur_customer;
        $this->data['customer_list']    = $customer_list;

        // --------------------------------------------------

        $cur_conf = $this->Currency_model->fetch_by_code();
        $cur_code = $this->input->post('cur_code');
        $cur_code = (empty($cur_code) || empty($cur_conf[$cur_code]) ? 'IDR' : $cur_code);
        $this->data['cur_code']         = $cur_code;
        $this->data['cur_conf']         = $cur_conf;
        $this->data['session']          = $this->session->userdata('logged_in');
        $this->data['listOutstanding']  = $this->Retail_model->getlistOutstanding($cur_customer);
        $category = [0,0,0,0];
        foreach ($this->data['listOutstanding'] as $key => $value) 
        {
            $item_code = preg_replace('/\s+/', '', $value->WAERK);
            if(empty($cur_conf[$item_code]['value']))
                $exchange = 0;
            else
                $exchange = $cur_conf[$item_code]['value'];

            if(empty($cur_conf[$cur_code]['value']))
                $divider = 1;
            else
                $divider = $cur_conf[$cur_code]['value'];
            $nominal = (float) $value->NETWR * $exchange;     
            $nominal = round(($nominal / $divider), 2);

            if ($value->CATEGORY == 1)
                { $category[($value->CATEGORY-1)] += ($nominal); }
            else if ($value->CATEGORY == 2)
                { $category[($value->CATEGORY-1)] += ($nominal); }
            else if ($value->CATEGORY == 3)
                { $category[($value->CATEGORY-1)] += ($nominal); }
            else if ($value->CATEGORY == 4)
                { $category[($value->CATEGORY-1)] += ($nominal); }
        }
        $ii = 0; 
        $provider = "[
                        {
                            code_range: 1,
                            'aging': '0 - 30 Days',
                            'total': $category[0]
                        },
                        {
                            code_range: 2,
                            'aging': '31 - 60 Days',
                            'total': $category[1]
                        },
                        {
                            code_range: 3,
                            'aging': '61 - 90 Days',
                            'total': $category[2]
                        },
                        {
                            code_range: 4,
                            'aging': '> 90 Days',
                            'total': $category[3]
                        }
                    ]"; 
          
        $this->data['category']         = $provider;
        $this->data['tat_aging']        = $category;

        // --------------------------------------------------

        $this->data['title']            = 'Finance';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'retail/finance';
        $this->load->view('template', $this->data);
    }

    function finance2(){
        $sess_id_cust = $this->session->userdata('log_sess_id_customer');
        $customer_list = (!empty($sess_id_cust) ? $this->Customers_m->get_customer($sess_id_cust) : $this->Retail_model->finance_customer());

        $cur_customer = $this->input->get('cur_customer');
        if(empty($cur_customer)) $cur_customer = "all";
        if(!empty($sess_id_cust)) $cur_customer = $sess_id_cust;

        $this->data['cur_customer']     = $cur_customer;
        $this->data['customer_list']    = $customer_list;

        $cur_type = $this->input->get('cur_type');
        $cur_type = (empty($cur_type) ? 'sum_data' : $cur_type);

        $this->data['cur_type'] = $cur_type;

        // --------------------------------------------------

        $cur_conf = $this->Retail_model->finance_cur($cur_customer);
        if (count($cur_conf)) {
            $cur_code = $this->input->get('cur_code');
            $cur_code = (empty($cur_code) ? $cur_conf[0]['WAERK'] : $cur_code);
        } else {
            $cur_conf = null;
            $cur_code = null;
        }
        $this->data['cur_code']         = $cur_code;
        $this->data['cur_conf']         = $cur_conf;
        $this->data['session']          = $this->session->userdata('logged_in');
        $this->data['listOutstanding']  = $this->Retail_model->getlistOutstanding($cur_customer);

        // --------------------------------------------------

        $this->data['title']            = 'Finance';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'retail/finance';
        $this->load->view('template', $this->data);
    }

    function finance_range($cur_customer, $code_range, $cur_code = "all")
    {
        $sess_id_cust = $this->session->userdata('log_sess_id_customer');
        if(!empty($sess_id_cust)) $cur_customer = $sess_id_cust;

        $customer_name = 'ALL CUSTOMER';
        if(!empty($cur_customer)) {
            $raw_cust = $this->Customers_m->get_customer($cur_customer);
            if(!empty($raw_cust)) $customer_name = 'CUSTOMER: ' . $raw_cust[0]->COMPANY_NAME;
        }

        $this->data['customer_name']    = $customer_name;
        $this->data['code_range']       = $code_range;
        $this->data['cur_customer']     = $cur_customer;


        // $list_data = $this->Retail_model->getlistOutstanding($cur_customer, $code_range);
        $finance_data = $this->Retail_model->finance_tat_aging_detail($cur_customer, $code_range, $cur_code);
        $this->data['finance_data']     = $finance_data;

        $aging = "";
        if($code_range == 1)
            $aging = '<= 30 Days';
        elseif($code_range == 2)
            $aging = '31 - 60 Days';
        elseif($code_range ==3)
            $aging = '61 - 90 Days';
        else
            $aging = '> 90 Days';
        
        $this->data['finance_range']    = $aging;

        // --------------------------------------------------

        $this->data['title']            = 'Finance';
        $this->data['icon']             = 'fa fa-money';
        $this->data['content']          = 'retail/finance_range';
        $this->load->view('template', $this->data);
    }    

    function finance_historical($start1, $end1){

      $filename = "HISTORICAL TRANSACTION [{$start1} - {$end1}]";

      $start = date('Ymd', strtotime($start1));
      $end = date('Ymd', strtotime($end1));

      $this->load->library('Libexcel');

      $excel = new PHPExcel();

      // Settingan awal fil excel
      $excel->getProperties()->setCreator('CRM-TC')
                   ->setLastModifiedBy('CRM-TC')
                   ->setTitle("RETAIL - HISTORICAL TRANSACTION")
                   ->setSubject("HISTORICAL TRANSACTION");
                   // ->setDescription("Laporan Semua Data Siswa")
                   // ->setKeywords("Data Siswa");
      // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
      $style_col = array(
        'font' => array('bold' => true), // Set font nya jadi bold
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
      $style_row = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      ); 
      $style_row_right = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      $excel->setActiveSheetIndex(0)->setCellValue('A1', "HISTORICAL TRANSACTION [{$start1} - {$end1}]"); // Set kolom A1 dengan tulisan "DATA SISWA"
      $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

      // Buat header tabel nya pada baris ke 3
      $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
      $excel->setActiveSheetIndex(0)->setCellValue('B3', "Billing Document"); // Set kolom B3 dengan tulisan "NIS"
      $excel->setActiveSheetIndex(0)->setCellValue('C3', "Document Date"); // Set kolom C3 dengan tulisan "NAMA"
      $excel->setActiveSheetIndex(0)->setCellValue('D3', "Amount"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
      $excel->setActiveSheetIndex(0)->setCellValue('E3', "Currency"); // Set kolom E3 dengan tulisan "ALAMAT"

      // Apply style header yang telah kita buat tadi ke masing-masing kolom header
      $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
      // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya

      // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // Set width kolom E


      $result = $this->Retail_model->get_list_historical($start, $end);
      $index = 1;
      $start_row = 4;
      $row = $start_row ;
      foreach ($result as $key => $value) {

          $excel->setActiveSheetIndex(0)->setCellValue('A'.$row, $index); 
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$row, $value['BILLING']); 
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$row, $value['DOC_DATE']); 
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$row, preg_replace('/\s+/', '', $value['AMOUNT'])); 
          $excel->setActiveSheetIndex(0)->setCellValue('E'.$row, $value['CURRENCY']); 

          $excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($style_row_right);
          $excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($style_row);

          $row++;
          $index++;

      }

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      // $excel->getActiveSheet(0)->setTitle("");
      $excel->setActiveSheetIndex(0);
      // Proses file excel

      // We'll be outputting an excel file
      header('Content-type: application/vnd.ms-excel');
    

      // It will be called file.xls
      header('Content-Disposition: attachment; filename="'.$filename.'.xls"');

      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }  

    function finance_invoice_outstanding($par){

      if ($par=='1') $range = "<= 30";
      else if ($par=='2') $range = "31 - 60";
      else if ($par=='3') $range = "61 - 90";
      else if ($par=='4') $range = "> 90";
        

      $this->load->library('Libexcel');

      $excel = new PHPExcel();

      // Settingan awal fil excel
      $excel->getProperties()->setCreator('CRM-TC')
                   ->setLastModifiedBy('CRM-TC')
                   ->setTitle("RETAIL - INVOICE OUTSTANDING")
                   ->setSubject("INVOICE OUTSTANDING");
                   // ->setDescription("Laporan Semua Data Siswa")
                   // ->setKeywords("Data Siswa");
      // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
      $style_col = array(
        'font' => array('bold' => true), // Set font nya jadi bold
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
      $style_row = array(
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      ); 
      $style_row_right = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      $excel->setActiveSheetIndex(0)->setCellValue('A1', "INVOICE OUTSTANDING"); // Set kolom A1 dengan tulisan "DATA SISWA"
      $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

      // Buat header tabel nya pada baris ke 3
      $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
      $excel->setActiveSheetIndex(0)->setCellValue('B3', "Billing Document"); // Set kolom B3 dengan tulisan "NIS"
      $excel->setActiveSheetIndex(0)->setCellValue('C3', "Document Date"); // Set kolom C3 dengan tulisan "NAMA"
      $excel->setActiveSheetIndex(0)->setCellValue('D3', "Amount"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
      $excel->setActiveSheetIndex(0)->setCellValue('E3', "Currency"); // Set kolom E3 dengan tulisan "ALAMAT"
      $excel->setActiveSheetIndex(0)->setCellValue('F3', "Conversion"); // Set kolom E3 dengan tulisan "ALAMAT"

      // Apply style header yang telah kita buat tadi ke masing-masing kolom header
      $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
      // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya

      // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setWidth(15); // Set width kolom E
      $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20); // Set width kolom D

      $cur_conf = $this->Currency_model->fetch_by_code();
      $result = $this->Retail_model->get_list_invoice_outstanding($par);
      $index = 1;
      $start_row = 4;
      $row = $start_row ;
      foreach ($result as $key => $value) {
          $value['CURRENCY'] = preg_replace('/\s+/', '', $value['CURRENCY']);
          $exchange = (!empty($cur_conf[$value['CURRENCY']]['value']) ? $cur_conf[$value['CURRENCY']]['value'] : 0);
         
          $excel->setActiveSheetIndex(0)->setCellValue('A'.$row, $index); 
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$row, $value['BILLING']); 
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$row, $value['DOC_DATE']); 
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$row, preg_replace('/\s+/', '', $value['AMOUNT'])); 
          $excel->setActiveSheetIndex(0)->setCellValue('E'.$row, $value['CURRENCY']); 
          $excel->setActiveSheetIndex(0)->setCellValue('F'.$row, $exchange); 

          $excel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($style_row_right);
          $excel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($style_row_right);

          $row++;
          $index++;

      }

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      // $excel->getActiveSheet(0)->setTitle("");
      $excel->setActiveSheetIndex(0);
      // Proses file excel

      // We'll be outputting an excel file
      header('Content-type: application/vnd.ms-excel');
      $filename = 'INVOICE OUTSTANDING '.$range;

      // It will be called file.xls
      header('Content-Disposition: attachment; filename="'.$filename.'.xls"');

      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }

    function list_order()
    {
        $tmp1 = [];
        $tmp11 = [];
        $tmp22 = [];
        
        $tahun                      = $_GET['tahun'];
        $kunnr                      = $_GET['customer'];
        $bulan                      = $_GET['bulan'];
        $filter                     = $_GET['filterby'];
    
        if ($kunnr == 'all') {
          $this->data['kunnr']        = '';
          $kunnr = '';
        } else {
          $this->data['kunnr']        = $kunnr;
        }
        $this->data['tahun']        = $tahun;
        $this->data['bulan']        = $bulan;
        $this->data['filterby']     = $filter;
        // $listStatus = $this->Retail_model->getStatusOrderPerTahun($tahun, $kunnr, $bulan, $filter);
        // print_r($listStatus);
        // echo $this->db->last_query();
        // die;
        // $result = $this->Retail_model->select_retailPerTahun($tahun, $kunnr, $bulan, $filter);
        // // $listStatus = $this->Retail_model->getStatusOrder();
        // foreach ($listStatus as $key => $value) {
        //   if ($value->JUMLAH != 0){
        //     $tmp1[] = $value;
        //   }
        // }   

        // $this->data['ldt2']             = 'Dashboard';
        // $this->data['ldl2']             = 'index.php/Retail/dashboard';
        // $this->data['ldi2']             = 'fa fa-dashboard';

        // // $result = $this->Retail_model->select_retail();
        // $txt = 50;
        // $tmp = [];
        // $n_result = []; 
        //   if (isset($result)) {
        //     foreach ($result as $key => $value) {
        //         $tmp = $value;
                

        //         if (isset($tmp['TECO_DATE']) && $tmp['TECO_DATE']!='00000000') {
        //             $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
        //             $teco_date = $tmp['TECO_DATE'];
        //             $s = date_create(date('Y' , strtotime(substr($teco_date, 5,4))));
        //             $e = date_create(date('Y'));
                 
        //             // echo "<br>".date_diff($s, $e)->format("%a")."<br>";
        //             $diff = (int) date_diff($s, $e)->format("%a");
        //             // echo $diff;echo " diff<br>";
        //             if ( $diff > 5) {
        //                 $newdate = substr($teco_date, 4,4).substr($teco_date, 2,2).substr($teco_date, 0,2);
        //                 // print_r($newdate);echo " newdate <br>";
        //                 $date2 = date('Ymd', strtotime($newdate));
        //             }else{
                      
        //               $date2 = date('Ymd', strtotime($tmp['TECO_DATE']));
        //             }

        //             $start = date_create($date1);
        //             $end = date_create($date2);

        //         }else{
        //             $date1 = date('Ymd', strtotime($tmp['RECEIVED_DATE']));
        //             $date2 = date('Ymd');

        //             $start = date_create($date1);
        //             $end = date_create($date2);

        //         }
                

        //         $tmp['RECEIVED_DATE'] = date('d-M-Y', strtotime($tmp['RECEIVED_DATE']));
                
        //         // $tmp['STATUS'] = '';
        //         if (($tmp['ERDAT_VBFA']!='00000000') and (($tmp['ERDAT_VBFA'])!='')) {
        //           $tmp['DELIVERY_DATE'] = date('d-M-Y', strtotime($tmp['ERDAT_VBFA']));
        //         } else {
        //           $tmp['DELIVERY_DATE'] = '';
        //         }
        //         $tmp['REMARKS'] = '';
        //         $tmp['ID_RETAIL'] = '';
        //         $tmp['QUOTATION_DATE'] = '';
        //         $tmp['APPROVAL_DATE'] = '';
        //         $day_waiting = 0;

        //         for ($i=1; $i <= $txt ; $i++) { 
        //           if (preg_replace('/\s+/', '', $value["TXT".$i]) == '') {
        //               break;
        //             } 
        //           if (strpos($value["TXT".$i], 'SROA') !== false) {

        //               $tmp['QUOTATION_DATE'] = date('d-M-Y', strtotime($value["UDATE".$i]));
        //               $nexti = $i+1;
        //               for ($ii=$i+1; $ii <= $txt ; $ii++)
        //                   if ((strpos($value["TXT".$ii], 'WR') !== false) OR (strpos($value["TXT".$ii], 'SROA') !== false)){
                             
        //                       $tmp['APPROVAL_DATE'] = date('d-M-Y', strtotime($value["UDATE".$ii]));
        //                       $day_waiting +=  (date_diff(date_create($tmp['QUOTATION_DATE']),date_create($tmp['APPROVAL_DATE']))->format("%a"));
        //                   break 1;  
        //                   }
        //               // $tmp[][$i] = $value["UDATE".$i];
               
        //           }
        //         }
        //       if ($tmp['LFGSA'] != 'C') {
        //         if (((strpos($tmp['STAT'], 'WR') !== false)) and ($tmp['QUOTATION_DATE'] != '')){
        //           $tmp22[]= $value;
        //           $tmp['TXT_STAT'] = 'Approval Received / Repair Started';
        //         }else if ((strpos($tmp['STAT'], 'WR') !== false)){
        //         // or (strpos($tmp['STAT'], 'UR') !== false)){
        //           $tmp11[] = $value;
        //           $tmp['TXT_STAT'] = 'Repair Started';
        //         }
        //       }
        //       $tmp['TAT'] = (date_diff($start, $end)->format("%a"))-$day_waiting ;
        //       $n_result[] = $tmp;

        //     }
        //     // die;
        // }
        // $sum_wr1 = count($tmp11);
        // if ($sum_wr1 != 0){
        //   array_push($tmp1, ((object) array('STATUS'=> 'Repair Started', 'JUMLAH' => $sum_wr1)) );
        // }
        // $sum_wr2 = count($tmp22);
        // if ($sum_wr2 != 0)
        // array_push($tmp1, ((object) array('STATUS'=> 'Approval Received / Repair Started', 'JUMLAH' => $sum_wr2)) );

        // $this->data['listStatus']       = $tmp1;        
        // $this->data['data_table']       = $n_result;
        $this->data['title']            = 'List Order';
        $this->data['icon']             = 'fa fa-list';
        $this->data['content']          = 'retail/list_order';
        $this->load->view('template', $this->data);
    }

    function order_status()
    {
        $status                     = $_GET['status'];
        $tahun                      = $_GET['tahun'];
        $kunnr                      = $_GET['customer'];
        $bulan                      = $_GET['bulan'];
        $filter                     = $_GET['filterby'];
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';

        $this->data['status']       = $status;
        if ($kunnr == 'all') {
          $this->data['kunnr']        = '';
        } else {
          $this->data['kunnr']        = $kunnr;
        }
        $this->data['tahun']        = $tahun;
        
        $this->data['title']        = 'Order Status';
        $this->data['icon']         = 'fa fa-flag';
        $this->data['content']      = 'retail/order_status';
        $this->load->view('template', $this->data);
    }

    function order_detail()
    {
        $order = $this->input->get('order');
        $kunnr = $this->input->get('customer');
        $tahun = $this->input->get('tahun');
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'List Order';
        $this->data['ldl3']         = 'index.php/Retail/list_order?tahun='.$tahun.'&customer='.$kunnr;
        $this->data['ldi3']         = 'fa fa-list';
       

        $this->data['data_order_details']    = $this->Retail_model->get_retail_where_id($order);

        $this->data['title']                = 'Order Detail';
        $this->data['icon']                 = 'fa fa-file-text';
        $this->data['content']              = 'retail/order_detail';
        $this->load->view('template', $this->data);
        // echo '<pre>';
        // print_r($this->Retail_model->get_retail_where_id($ID_RETAIL));
        // die;
    }

    function order_detail2()
    {   
        $order = $this->input->get('order');
        $status = $this->input->get('status');
        $kunnr = $this->input->get('customer');
        $tahun = $this->input->get('tahun');
        $this->data['ldt2']         = 'Dashboard';
        $this->data['ldl2']         = 'index.php/Retail/dashboard';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'Order';
        $this->data['ldl3']         = 'index.php/Retail/order_status?status='.$status.'&tahun='.$tahun.'&customer='.$kunnr;
        $this->data['ldi3']         = 'fa fa-list';

        $this->data['status']       = $status; 
        $this->data['data_order_details']    = $this->Retail_model->get_retail_where_id($order);

        $this->data['title']                = 'Order Detail';
        $this->data['icon']                 = 'fa fa-file-text';
        $this->data['content']              = 'retail/order_detail';
        $this->load->view('template', $this->data);
        // echo '<pre>';
        // print_r($this->Retail_model->get_retail_where_id($ID_RETAIL));
        // die;
    }

    public function update_remarks()
    {
        $AUFNR    = $this->input->post('AUFNR');
        $REMARKS  = $this->input->post('REMARKS');

        $sql = "WITH A AS ( SELECT
                    AUFNR
                FROM
                    M_PMORDERH
                WHERE AUFNR = $AUFNR ) MERGE INTO
                TC_M_PMORDER
                USING A ON
                A.AUFNR = TC_M_PMORDER.AUFNR
                WHEN MATCHED THEN UPDATE
                SET
                    REMARKS = '$REMARKS'
                WHEN NOT MATCHED THEN INSERT
                    ( AUFNR, REMARKS)
                VALUES ( $AUFNR, '$REMARKS' );";
        $query  = $this->db->query($sql);
        // $result = $query->result_array();  
        // $query_insert_pooling_order       = $this->Pooling_model->insert_pooling_order($insert);
           if ($query) {
               $data = array(
                  "msg" => 'Good! Your changes have been saved!',
                  // "id" => $query_insert_pooling_order  
                  );
            }
            else {
                $data = array(
                        "msg" => 'Operation Failed!'
                    ); 
              }
            echo json_encode($data);
          // }
    }

    
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
