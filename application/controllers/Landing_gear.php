<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_gear extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->template->set_layout(false);
        if (empty($this->session->userdata('log_sess_id_user')))
        {
            redirect('Login');
        }

        $this->load->model('Landinggear_model', '', TRUE);
        $this->load->helper('form');
    }
    private $perPage = 9;

    public $data = array(
        'ldt1'              => 'Landing Gear',
        'ldl1'              => 'index.php/Landing_gear',
        'ldi1'              => 'fa fa-usd',
        'ldt2'              => '',
        'ldl2'              => '',
        'ldi2'              => '',
        'ldt3'              => '',
        'ldl3'              => '',
        'ldi3'              => '',
        'ldt4'              => '',
        'ldl4'              => '',
        'ldi4'              => '',
        'ldt5'              => '',
        'ldl5'              => '',
        'ldi5'              => '',
        'nav_tabs'          => 'landinggear/dashboard/category_tabs',
        'link_directory'    => 'layout/link-directory'
    );

    function index() {
        redirect('Landing_gear/aircraft_list');
    }

    public function data_aircraft()
    {
     // $this->load->database();
     // $count = $this->db->get('posts')->num_rows()

     if(!empty($this->input->post("page"))){
        $start = ($this->input->post("page")) * $this->perPage;
        $end   = $start + $this->perPage;
        $where = "WHERE IWERK = 'WSWB'";
        $where_kunnr = "";
        $where_search = "";
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where_kunnr = "AND PARNR LIKE '".$kunnr."'";
          }
        if (($this->input->post("input_data"))) {
            $keyword = "'%".strtolower($this->input->post("input_data"))."%'";
            $where_search = 'WHERE ( T.ACREG_REVTX LIKE '.$keyword.' OR T.EARTX LIKE '.$keyword.' OR T.SERGE LIKE '.$keyword.')';
        }
        // echo $start;
        $sql = "WITH TblDatabases AS
        (
        SELECT ACREG_REVTX, IWERK, EARTX, SERGE, COUNT(*) AS JUMLAH_PROGRESS FROM M_REVISION
        WHERE IWERK = 'WSWB' AND TXT04 = 'REL' $where_kunnr GROUP BY ACREG_REVTX, IWERK, EARTX, SERGE
        ),
        BB AS (SELECT ACREG_REVTX, IWERK, EARTX, SERGE, COUNT(*) AS JUMLAH FROM M_REVISION
        WHERE IWERK = 'WSWB' AND TXT04 = 'CLSD' $where_kunnr GROUP BY ACREG_REVTX, IWERK, EARTX, SERGE
                ),
        A AS (
        SELECT T.ACREG_REVTX, T.IWERK, T.EARTX, T.SERGE, T.JUMLAH_PROGRESS, B.JUMLAH,
                ROW_NUMBER() OVER (ORDER BY T.ACREG_REVTX) as Row 
                FROM TblDatabases T LEFT JOIN BB B ON T.ACREG_REVTX = B.ACREG_REVTX $where_search)
        SELECT * FROM A WHERE Row>$start and Row<=$end;";
        $data['posts'] = $this->Landinggear_model->get_datatables_query($sql);

        $result = $this->load->view('landinggear/dashboard/data', $data);
        if (!empty($data['posts'])) {
            echo $result;
         }
        else {
            echo json_encode($result);
        }

     }else{
        $start = 0;
        $end = $start + $this->perPage;
        $where_kunnr = "";
        $where_search = "";
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where_kunnr = "AND PARNR LIKE '".$kunnr."'";
          }
        if (($this->input->post("input_data"))) {
            $keyword = "'%".strtolower($this->input->post("input_data"))."%'";
            $where_search = 'WHERE ( T.ACREG_REVTX LIKE '.$keyword.' OR T.EARTX LIKE '.$keyword.' OR T.SERGE LIKE '.$keyword.')';
        }
        // echo $start;
        $sql = "WITH TblDatabases AS
        (
        SELECT ACREG_REVTX, IWERK, EARTX, SERGE, COUNT(*) AS JUMLAH_PROGRESS FROM M_REVISION
        WHERE IWERK = 'WSWB' AND TXT04 = 'REL' $where_kunnr GROUP BY ACREG_REVTX, IWERK, EARTX, SERGE
        ),
        BB AS (SELECT ACREG_REVTX, IWERK, EARTX, SERGE, COUNT(*) AS JUMLAH FROM M_REVISION
        WHERE IWERK = 'WSWB' AND TXT04 = 'CLSD' $where_kunnr GROUP BY ACREG_REVTX, IWERK, EARTX, SERGE
                ),
        A AS (
        SELECT T.ACREG_REVTX, T.IWERK, T.EARTX, T.SERGE, T.JUMLAH_PROGRESS, B.JUMLAH,
                ROW_NUMBER() OVER (ORDER BY T.ACREG_REVTX) as Row 
                FROM TblDatabases T LEFT JOIN BB B ON T.ACREG_REVTX = B.ACREG_REVTX $where_search)
        SELECT * FROM A WHERE Row>$start and Row<=$end;";
        $data['posts'] = $this->Landinggear_model->get_datatables_query($sql);
         // = $this->db->query($sql)->result();


        $result = $this->load->view('landinggear/dashboard/data', $data);
        if (!empty($data['posts'])) {
            echo json_encode($result);
         }
        else {
        	echo json_encode($result);
        }
        }
    }


    function aircraft_list() {
        $stat = 'progress';
        $this->session->set_userdata('status_lg', 'progress');
        $this->data['title']        = 'Aircraft Registration';
        $this->data['icon']         = 'fa fa-inbox';
        $this->data["session"]      = $this->session->userdata('logged_in');
        $this->data['content']      = 'landinggear/dashboard/aircraft';
        // $this->data['listProject']  = $this->Landinggear_model->getAircraft($stat);
        $this->load->view('template', $this->data);
    }  

    function project_list($docno, $type = 1) {
        // echo $type;
        // die;
        $this->data['ldt2']         = 'Aircarft Registration';
        $this->data['ldl2']         = 'index.php/Landing_gear/aircraft_list';
        $this->data['ldi2']         = 'fa fa-dashboard';

        $this->data['status']       = $this->input->post('status');
        if ($this->data['status']){
            $this->session->set_userdata('status_lg', $this->data['status']);
        } else {
            $this->data['status'] = $this->session->userdata('status_lg');
        }
         
        $this->data['docno']        = $docno;
        $this->data['aircraft_info']  = $this->Landinggear_model->getAircraftInfo($docno);
        $this->data['type'][$type]  = 'active';
        $this->data['title']        = 'Project List';
        $this->data['icon']         = 'fa fa-inbox';
        $this->data["session"]      = $this->session->userdata('logged_in');
        $this->data['content']      = 'landinggear/dashboard/component';
        // $this->data['listProject']  = $this->Landinggear_model->getlistProject($docno, $this->data['status']);
        $this->load->view('template', $this->data);
    }

	function overview($docno) {
        $this->data['listProject']  = $this->Landinggear_model->getOverviewProject($docno);
        $this->data['ldt2']         = 'Aircarft Registration';
        $this->data['ldl2']         = 'index.php/Landing_gear/aircraft_list';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'Project List';
        $this->data['ldl3']         = 'index.php/Landing_gear/project_list/'.$this->data['listProject']['ACREG_REVTX'];
        $this->data['ldi3']         = 'fa fa-list';
		$this->data["docno"] 		= $docno;

        // $this->data["type"]         = $type;
		$this->data["session"] 		= $this->session->userdata('logged_in');
		$this->data['header_menu'] 	= 'landinggear/header_menu';
		$this->data['title_menu']	= 'landinggear/title_menu';
		$this->data['title'] 		= "OVERVIEW";
        $this->data['icon']         = 'fa fa-inbox';
        $this->data['content']		= 'landinggear/dashboard/overview';
        
        //$this->data['OverviewProgress'] = $this->Landinggear_model->getOverviewProgress($docno);
        $this->data['listProgress'] = $this->Landinggear_model->getOverviewListProgress($docno);
        $this->data['listProgressMAT'] = $this->Landinggear_model->getMATProgress($docno);
        // $orders = count($this->data['listProgress']);
        // echo $orders;
        // die;
        $progress = 0;
        $complete = 0;
        foreach ($this->data['listProgress'] as $key => $value) {
            // $date1 = date('Ymd', strtotime($tmp['ISDD']));
            // $date2 = date('Ymd', strtotime($tmp['IEBD']));
            // $tat = (date_diff($start, $end)->format("%a")) ;
            // $value['TAT'] = $tat;

            if ($value['TXT_STAT'] == 'open') {
              $progress++;
            }else if ($value['TXT_STAT'] == 'close'){
              $complete++;
            }
        }
        
        $orders = $progress + $complete;
        $count_orders = $orders;
        $count_complete = $complete;
        if ($this->data['listProgress']) {
            $progress = round(($progress/$orders)*100);
            $complete = round(($complete/$orders)*100);

        }else{
          $progress = 0;
          $complete = 0;
        }
        $end = "";
        $start = "";
        if ($this->data['listProject']['UDATE_SMR'] != '00000000'){
            $revbd = date('Ymd', strtotime($this->data['listProject']['UDATE_SMR']));
            $start = date_create($revbd);
        }
        if ($this->data['listProject']['ATSDATE_SMR'] != '00000000'){
            $reved = date('Ymd', strtotime($this->data['listProject']['ATSDATE_SMR']));
            $end = date_create($reved);
        }
        if ($start and $end) {
            $this->data['actual']     = date_diff($start, $end)->format("%a");
        }
        else {
            $this->data['actual']     = '';
        }
        

        $totalRemoval = 0;
        $closedRemoval = 0;
        $totalDsy      = 0;
        $closedDsy     = 0;
        $totalInspection    = 0;
        $closedInspection   = 0;
        $totalRepair        = 0;
        $closedRepair       = 0;
        $totalAssembly      = 0;
        $closedAssembly     = 0;
        $totalInstall       = 0;
        $closedInstall      = 0;
        $totalTest          = 0;
        $closedTest         = 0;
        $totalQEC          = 0;
        $closedQEC         = 0;

        foreach ($this->data['listProgressMAT'] as $key => $value) {
            if ($value['MAT'] == 'REMOVAL') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedRemoval +=  $value['JUMLAH'];
                }
                $totalRemoval += $value['JUMLAH'];
            }
            else if ($value['MAT'] == 'DISSAMBLY') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedDsy +=  $value['JUMLAH'];
                }
                $totalDsy += $value['JUMLAH'];
            }
            else if ($value['MAT'] == 'INSPECTION') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedInspection +=  $value['JUMLAH'];
                }
                $totalInspection += $value['JUMLAH'];
            } 
            else if ($value['MAT'] == 'REPAIR') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedRepair +=  $value['JUMLAH'];
                }
                $totalRepair += $value['JUMLAH'];
            }
            else if ($value['MAT'] == 'ASSEMBLY') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedAssembly +=  $value['JUMLAH'];
                }
                $totalAssembly += $value['JUMLAH'];
            }  
            else if ($value['MAT'] == 'INSTALL') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedInstall +=  $value['JUMLAH'];
                }
                $totalInstall += $value['JUMLAH'];
            } 
            else if ($value['MAT'] == 'TEST') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedTest +=  $value['JUMLAH'];
                }
                $totalTest += $value['JUMLAH'];
            }
            else if ($value['MAT'] == 'QEC') {
                if ($value['TXT_STAT'] == 'close') {
                  $closedQEC +=  $value['JUMLAH'];
                }
                $totalQEC += $value['JUMLAH'];
            }   
        }

        // $dtotalRemoval = $this->Landinggear_model->getTotalRemoval($docno);
        // $closedRemoval = $this->Landinggear_model->getClosedRemoval($docno);
        $this->data['Removal'] = @round(($closedRemoval / $totalRemoval) * 100);

        // $totalDsy = $this->Landinggear_model->getTotalDisassembly($docno);
        // $closedDsy = $this->Landinggear_model->getClosedDisassembly($docno);
        $this->data['Disassambly'] = @round(($closedDsy / $totalDsy) * 100);

        // $totalInspection = $this->Landinggear_model->getTotalInspection($docno);
        // $closedInspection = $this->Landinggear_model->getClosedInspection($docno);
        $this->data['Inspection'] = @round(($closedInspection / $totalInspection) * 100);

        // $totalRepair = $this->Landinggear_model->getTotalRepair($docno);
        // $closedRepair = $this->Landinggear_model->getClosedRepair($docno);
        $this->data['Repair'] = @round(($closedRepair / $totalRepair) * 100);


        // $totalAssembly = $this->Landinggear_model->getTotalAssembly($docno);
        // $closedAssembly = $this->Landinggear_model->getClosedAssembly($docno);
        $this->data['Assembly'] = @round(($closedAssembly / $totalAssembly) * 100);

        // $totalInstall = $this->Landinggear_model->getTotalInstall($docno);
        // $closedInstall = $this->Landinggear_model->getClosedInstall($docno);
        $this->data['Install'] = @round(($closedInstall / $totalInstall) * 100);


        // $totalTest = $this->Landinggear_model->getTotalTest($docno);
        // $closedTest = $this->Landinggear_model->getClosedTest($docno);
        $this->data['Test'] = @round(($closedTest / $totalTest) * 100);
        $this->data['QEC'] = @round(($closedQEC / $totalQEC) * 100);

        $this->data['jobcard']      = $count_orders;
        $this->data['jobcard_complete']     = $count_complete;
        $this->data['Progress']     = $progress;
        $this->data['Complete']     = $complete;
       
        $this->load->view('template', $this->data);
	}

	function profit_analisyst($docno) {
		$this->data["docno"] 		= $docno;
		$this->data["session"]		= $this->session->userdata('logged_in');
		$this->data['header_menu'] 	= 'landinggear/header_menu';
		$this->data['title_menu'] 	= 'landinggear/title_menu';
		$this->data['title'] 		= "PROFIT ANALYSIST";
        $this->data['icon']         = 'fa fa-money';
        $this->data['content'] 		= 'landinggear/dashboard/profit_analisyst';
        $this->data['listProject']  = $this->Landinggear_model->getOverviewProject($docno);
        $this->load->view('template', $this->data);
	}

	function deviation($docno) {
        $this->data['listProject']  = $this->Landinggear_model->getOverviewProject($docno);
        $this->data['ldt2']         = 'Aircarft Registration';
        $this->data['ldl2']         = 'index.php/Landing_gear/aircraft_list';
        $this->data['ldi2']         = 'fa fa-dashboard';
        $this->data['ldt3']         = 'Project List';
        $this->data['ldl3']         = 'index.php/Landing_gear/project_list/'.$this->data['listProject']['ACREG_REVTX'];
        $this->data['ldi3']         = 'fa fa-list';
        $this->data["docno"]        = $docno;

		$this->data["session"] 		   = $this->session->userdata('logged_in');
		$this->data['header_menu']	   = 'landinggear/header_menu';
		$this->data['title_menu'] 	   = 'landinggear/title_menu';
		$this->data['title'] 		   = "DEVIATION";
        $this->data['icon']            = 'fa fa-inbox';
        $this->data['content'] 		   = 'landinggear/dashboard/deviation';
        $this->data['listProject']     = $this->Landinggear_model->getOverviewProject($docno);
        // $this->data['listProject']  = $this->Landinggear_model->getOverviewProject($docno,$revnr);
        $this->data['listDeviation']   = $this->Landinggear_model->getlistDeviation($docno);
        $count_gate = [0,0,0,0,0,0,0,0,0];
        // $count_gate = [4,2,9,12,51,3,7,1,9];
        foreach ($this->data['listDeviation'] as $key => $value) {

            $count_gate[($value->CURRENT_GATE-1)] = $count_gate[($value->CURRENT_GATE-1)] + 1;

        }
       
        // $str_count_gate = '[';
        // foreach ($count_gate as $key => $value) {
        //   $str_count_gate .= $value.',';
        // }
        // $str_count_gate = substr($str_count_gate, 0, -1);
        // $str_count_gate .= ']';
        // $this->data['count_gate'] = $str_count_gate;
        $ii = 0;
        
        $provider = '[';
        foreach ($count_gate as $key => $value) {
         $ii += 1;
          $provider .= "{
            'gate': 'Gate $ii',
            'total': $value
          },"; 
          
        }
        $provider .= ']';
        $this->data['count_gate'] = $provider;
        
        //print_r($this->data);
        //die;
        // $this->load->view('template', $data);
        $this->load->view('template', $this->data);
	}


	 function header_menu($docno) {
        $this->data["docno"]		= $docno;
        $this->data['listProject'] 	= $this->dashboard_model->getOverviewProject($docno);
        $this->load->view('landinggear/header_menu', $this->data);
    }



}
