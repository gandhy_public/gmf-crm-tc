
<?php

Class Loan_exchange_model extends CI_Model {

	private $column_order_loan = array('VBELN',
										'VBELN',
										'MATNR',
										'ARKTX',
										'TYPESERVICE',
										'SERNR',
										'PERIOD',
										'VBEGDAT',
										'VENDDAT',
										'PERIOD',
										'DELIVERY_DATE',
										'RETURN_DATE',
										'STATUS',
										// 'LFDAT'
                            );

	private $column_order_exc = array('VBELN',
										'VBELN',
										'MATNR',
										'ARKTX',
										'SERNR',
										'TYPESERVICE',
										'DELIVERY_DATE'
                            );
	private $column_req_loan = array(
								'ID_LOAN_REQUEST',
								'PART_NUMBER',
								'PERIOD_FROM',
								'PERIOD_TO',
								'CONDITION',
								'CONTACT_INFO'
                            	);
	private $column_req_exc = array(
							'PART_NUMBER',
							'EXCHANGE_TYPE',
							'EXCHANGE_RATE',
							'CONDITION',
							'CONTACT_INFO '
                            	);
	

	public function select_loan_request($param, $ext=null)
	{
	    $keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_req_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY ID_LOAN_REQUEST ASC";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				PART_NUMBER LIKE {$keyword} OR
				CONDITION LIKE {$keyword} OR
				PERIOD_FROM LIKE {$keyword} OR
				PERIOD_TO LIKE {$keyword} OR
				CONTACT_INFO LIKE {$keyword}
			)";
		}
		$sql = "SELECT
					*
					FROM
						(
							SELECT 
							ROW_NUMBER () OVER ( $order ) AS RowNum,
							*	
							FROM TC_LOAN_REQUEST
							WHERE  YEAR(CREATED_DATE) = YEAR ( getdate ( ) )  
						) tb
					WHERE
						$where
						$rownum
				";
		$query = $this->db->query($sql);

		return $query->result_array();
		$this->db->select('*');
		$this->db->from('M_LOAN_REQUEST');
		$this->db->order_by('ID_LOAN_REQUEST', 'DESC');
		
		$query = $this->db->get();	
		return $query->result_object();
	}

	public function count_loan_order()
	{
		$this->db->select('*');
		$this->db->from('M_SALESORDER');
		$this->db->where('AUART', 'ZP01');
		$query = $this->db->get();	
		return $query->num_rows();
	}

	public function update_code_user($name, $unit)
	{
		$data = array(
        'UNIT_CODE' => $unit,
        'USERNAME' => $name,
		);
		
		return  $this->db->insert('SAP_USERLIST', $data);
	}

	public function getlistsalesperson() 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where = " AND S.KUNNR LIKE '{$kunnr}' ";
			}
		$sql = "SELECT DISTINCT
					S.ERNAM,
					L.UNIT_CODE
				FROM
					M_SALESORDER S
					LEFT JOIN SAP_USERLIST L ON S.ERNAM = L.USERNAME
				WHERE
					S.AUART IN ('ZP01','ZX01') 
					$where ";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
	public function getListDataOrder($param, $ext=null) 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		$where_kunnr = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		$sp = "";
		// print_r();
		// if ($param['salesperson'])
		// {
		// 	$sps = $param['salesperson'];
		// 	$swe = "('" . implode("','",$sps) . "')";
		// 	$sp = "AND B.ERNAM IN {$swe} ";
		// 	// echo $sp;
		// }
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '%%';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} OR
				TYPESERVICE LIKE {$keyword} OR
				SERNR LIKE {$keyword} OR
				PERIOD LIKE {$keyword} OR
				VBEGDAT LIKE {$keyword} OR
				VENDDAT LIKE {$keyword} 
			)";
		}
	$sql = "WITH FF AS ( 
			SELECT 
				DISTINCT VBELN, MATNR, ARKTX 
			FROM 
				M_SALESORDER 
			WHERE 
				MATKL = 'DIEN' AND AUART IN ( 'ZP01' )
				$where_kunnr
				AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )),
			JJ AS (
			SELECT 
				DISTINCT VBELN, MATNR, ARKTX, VBEGDAT, VENDDAT, MATKL, ERNAM, SERNR, BSTNK
			FROM 
				M_SALESORDER 
			WHERE
				AUART = 'ZX01' 
				AND MATKL != 'DIEN' 
				$where_kunnr
				AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
				),
			BB AS (
			SELECT
				H.ARKTX AS 'TYPESERVICE',
				J.*,
				CASE
					WHEN ( ( J.VBEGDAT != '00000000' ) AND ( J.VENDDAT != '00000000' ) ) THEN
					DATEDIFF( DAY, CONVERT ( DATE, J.VBEGDAT, 104 ), CONVERT ( DATE, J.VENDDAT, 104 ) ) 
				END AS PERIOD
			FROM
				JJ J
				LEFT JOIN FF H ON J.VBELN = H.VBELN
				),
			CC AS (
				SELECT DISTINCT
						MS.VBELN,
						MS.VBELN_VBFA,
						MS.ERDAT_VBFA AS 'DELIVERY_DATE'
				FROM
						BB B
						LEFT JOIN 
						M_SALESORDER MS
						ON B.VBELN = MS.VBELN 
				WHERE
						MS.BWART LIKE '621' 
						AND MS.VBELN_VBFA LIKE '49%' 
						AND MS.KUNNR LIKE '%{$kunnr}%'  
				),
			DD AS (
				SELECT DISTINCT
						MS.VBELN,
						MS.VBELN_VBFA,
						MS.ERDAT_VBFA AS 'RETURN_DATE'
				FROM
						BB B
						LEFT JOIN 
						M_SALESORDER MS
						ON B.VBELN = MS.VBELN 
				WHERE
						MS.BWART LIKE '622' 
						AND MS.VBELN_VBFA LIKE '49%' 
						AND MS.KUNNR LIKE '%{$kunnr}%'  
				),
			EE AS (
						SELECT
							B1.VBELN,
							B1.MATNR,
							B1.ARKTX,
							B1.ERNAM,
							B1.VBEGDAT,
							B1.VENDDAT,
							B1.MATKL,
							B1.SERNR,
							B1.PERIOD,
							C.DELIVERY_DATE,
							D.RETURN_DATE,
							BSTNK,
							TYPESERVICE,
							CASE
								WHEN ( D.RETURN_DATE is not null ) THEN
								'RETURN DELIVERY' 
								WHEN ( C.DELIVERY_DATE is not null) THEN
								'DELIVERED' 
							END AS 'STATUS',
							CASE
								
								WHEN ( ( D.RETURN_DATE is not null ) AND ( C.DELIVERY_DATE is not null ) ) THEN
								DATEDIFF( DAY, CONVERT ( DATE, C.DELIVERY_DATE, 104 ), CONVERT ( DATE, D.RETURN_DATE, 104 ) ) 
							END AS 'TAT',
						CASE
								
								WHEN ( VBEGDAT != '00000000' ) THEN
								VBEGDAT ELSE ' ' 
							END AS START_DATE,
						CASE
								
								WHEN ( VENDDAT != '00000000' ) THEN
								VENDDAT ELSE ' ' 
							END AS END_DATE
						-- US.UNIT_CODE
						FROM
							BB B1
							LEFT JOIN CC C ON B1.VBELN = C.VBELN
							LEFT JOIN DD D ON B1.VBELN = D.VBELN
							-- LEFT JOIN SAP_USERLIST US ON B1.ERNAM = US.USERNAME
						WHERE 
							B1.MATKL != 'DIEN' 
							-- AND (US.UNIT_CODE LIKE 'TC%' OR US.UNIT_CODE IS NULL)
							 ),
						GG AS ( SELECT *, ROW_NUMBER ( ) OVER ( $order ) AS RowNum FROM EE WHERE $where)
						SELECT * FROM GG WHERE MATKL != 'DIEN' $rownum ;";

		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }

    public function getListDataOrder_count($param, $ext=null) 
	{
		$keyword = "'%%'";
		$rownum = '';
		$where = '';
		$where_kunnr = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			// $rownum = "	 AND RowNum >= {$param['start']}
			// 	AND RowNum < {$param['end']}";
		}
		$sp = "";
		// print_r();
		// if ($param['salesperson'])
		// {
		// 	$sps = $param['salesperson'];
		// 	$swe = "('" . implode("','",$sps) . "')";
		// 	$sp = "AND B.ERNAM IN {$swe} ";
		// 	// echo $sp;
		// }
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_loan[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} OR
				TYPESERVICE LIKE {$keyword} OR
				SERNR LIKE {$keyword} OR
				PERIOD LIKE {$keyword} OR
				VBEGDAT LIKE {$keyword} OR
				VENDDAT LIKE {$keyword} OR
				STATUS LIKE {$keyword}
			)";
		}
		$sql = "WITH FF AS ( 
			SELECT 
				DISTINCT VBELN, MATNR, ARKTX 
			FROM 
				M_SALESORDER 
			WHERE 
				MATKL = 'DIEN' AND AUART IN ( 'ZP01' )
				$where_kunnr
				AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )),
			JJ AS (
			SELECT 
				DISTINCT VBELN, MATNR, ARKTX, VBEGDAT, VENDDAT, MATKL, ERNAM, SERNR, BSTNK
			FROM 
				M_SALESORDER 
			WHERE
				AUART = 'ZX01' 
				AND MATKL != 'DIEN' 
				$where_kunnr
				AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
				),
			BB AS (
			SELECT
				H.ARKTX AS 'TYPESERVICE',
				J.*,
				CASE
					WHEN ( ( J.VBEGDAT != '00000000' ) AND ( J.VENDDAT != '00000000' ) ) THEN
					DATEDIFF( DAY, CONVERT ( DATE, J.VBEGDAT, 104 ), CONVERT ( DATE, J.VENDDAT, 104 ) ) 
				END AS PERIOD
			FROM
				JJ J
				LEFT JOIN FF H ON J.VBELN = H.VBELN
			),
			CC AS (
				SELECT DISTINCT
						MS.VBELN,
						MS.VBELN_VBFA,
						MS.ERDAT_VBFA AS 'DELIVERY_DATE'
				FROM
						BB B
						LEFT JOIN 
						M_SALESORDER MS
						ON B.VBELN = MS.VBELN 
				WHERE
						MS.BWART LIKE '621' 
						AND MS.VBELN_VBFA LIKE '49%' 
						AND MS.KUNNR LIKE '%{$kunnr}%'  
				),
			DD AS (
				SELECT DISTINCT
						MS.VBELN,
						MS.VBELN_VBFA,
						MS.ERDAT_VBFA AS 'RETURN_DATE'
				FROM
						BB B
						LEFT JOIN 
						M_SALESORDER MS
						ON B.VBELN = MS.VBELN 
				WHERE
						MS.BWART LIKE '622' 
						AND MS.VBELN_VBFA LIKE '49%' 
						AND MS.KUNNR LIKE '%{$kunnr}%'  
				),
			EE AS (
				SELECT
					B1.VBELN,
					B1.MATNR,
					B1.ARKTX,
					B1.ERNAM,
					B1.VBEGDAT,
					B1.VENDDAT,
					B1.MATKL,
					B1.SERNR,
					B1.PERIOD,
					C.DELIVERY_DATE,
					D.RETURN_DATE,
					BSTNK,
					TYPESERVICE,
					CASE
						WHEN ( D.RETURN_DATE is not null ) THEN
						'RETURN DELIVERY' 
						WHEN ( C.DELIVERY_DATE is not null) THEN
						'DELIVERED' 
						END AS 'STATUS',
					CASE
							
							WHEN ( ( D.RETURN_DATE is not null ) AND ( C.DELIVERY_DATE is not null ) ) THEN
							DATEDIFF( DAY, CONVERT ( DATE, C.DELIVERY_DATE, 104 ), CONVERT ( DATE, D.RETURN_DATE, 104 ) ) 
						END AS 'TAT',
				CASE
						
						WHEN ( VBEGDAT != '00000000' ) THEN
						VBEGDAT ELSE ' ' 
					END AS START_DATE,
				CASE
						
						WHEN ( VENDDAT != '00000000' ) THEN
						VENDDAT ELSE ' ' 
					END AS END_DATE
				-- US.UNIT_CODE
				FROM
					BB B1
					LEFT JOIN CC C ON B1.VBELN = C.VBELN
					LEFT JOIN DD D ON B1.VBELN = D.VBELN
					-- LEFT JOIN SAP_USERLIST US ON B1.ERNAM = US.USERNAME
				WHERE 
					B1.MATKL != 'DIEN' 
					-- AND (US.UNIT_CODE LIKE 'TC%' OR US.UNIT_CODE IS NULL)
					 ),
				GG AS ( SELECT * FROM EE WHERE $where)
				SELECT COUNT(*) AS 'JUMLAH' FROM GG WHERE MATKL != 'DIEN' ;";

		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }
	

	public function select_exchange_request($param, $ext=null)
	{
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_req_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY ID_EXCHANGE_REQUEST ASC";
        }

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				PART_NUMBER LIKE {$keyword} OR
				CONDITION LIKE {$keyword} OR
				EXCHANGE_TYPE LIKE {$keyword} OR
				EXCHANGE_RATE LIKE {$keyword} OR
				CONTACT_INFO LIKE {$keyword}
			)";
		}
		$sql = "SELECT
					*
					FROM
						(
							SELECT 
							ROW_NUMBER () OVER ( $order ) AS RowNum,
							*	
							FROM TC_EXCHANGE_REQUEST
							WHERE  YEAR(CREATED_DATE) = YEAR ( getdate ( ) ) 
						) tb
					WHERE
						$where
						$rownum
				";
		$query = $this->db->query($sql);

		return $query->result_array();
	}

	function getListDataOrderExchange($param, $ext=null) {
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum > {$param['start']}
				AND RowNum < {$param['end']}";
		}
		$where = '';
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY B1.VBELN ASC";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} 
			)";
		}
	
		$sql = "
		WITH FF AS ( 
			SELECT 
				DISTINCT VBELN, 
				MATNR, 
				ARKTX 
			FROM 
				M_SALESORDER 
			WHERE 
				MATKL = 'DIEN' AND 
				AUART IN ( 'ZX01' ) AND 
				KUNNR LIKE '%{$kunnr}%' AND 
				(SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate () )
		),
		JJ AS (
			SELECT 
				DISTINCT VBELN, 
				MATNR, 
				ARKTX, 
				SERNR, 
				AUDAT, 
				BSTNK
			FROM 
				M_SALESORDER 
			WHERE
				AUART = 'ZX01' AND 
				MATKL != 'DIEN' AND 
				KUNNR LIKE '%{$kunnr}%' AND 
				(SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate () )
		),
		BB AS (
			SELECT
				H.ARKTX AS 'TYPESERVICE',
				J.* 
			FROM
				JJ J
			LEFT JOIN FF H ON J.VBELN = H.VBELN
		),
		CC AS (
			SELECT 
				DISTINCT
				MS.VBELN,
				MS.VBELN_VBFA,
				MS.ERDAT_VBFA AS 'DELIVERY_DATE'
			FROM
				BB B
			LEFT JOIN M_SALESORDER MS ON B.VBELN = MS.VBELN 
			WHERE
				MS.BWART LIKE '601' AND 
				MS.VBELN_VBFA LIKE '49%' AND 
				MS.KUNNR LIKE '%{$kunnr}%'
		),
		DD AS ( 
			SELECT
				B1.VBELN,
				B1.MATNR,
				B1.ARKTX,
				B1.SERNR,
				C.DELIVERY_DATE,
				B1.AUDAT,
				B1.BSTNK,
				B1.TYPESERVICE
			FROM
				BB B1
			LEFT JOIN CC C ON B1.VBELN = C.VBELN
		), 
		GG AS ( 
			SELECT
					*,
					ROW_NUMBER () OVER ( $order ) AS RowNum
			FROM
				DD
			WHERE 
				$where
		)

		SELECT 
			*
		FROM 
			GG
		WHERE
			$where
			$rownum
		";

		$query = $this->db->query($sql);
		$n_result = [];
		$tmp = [];
		$result = $query->result_array();

        return $result;	
    }

    function getListDataOrderExchange_count($param, $ext=null) {
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			// $rownum = "	 AND RowNum >= {$param['start']}
			// 	AND RowNum < {$param['end']}";
		}
		$where = '';
		if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order_exc[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY B1.VBELN ASC";
        }
		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$kunnr = '';
			if ($this->session->userdata('log_sess_id_customer')) {
				$kunnr = $this->session->userdata('log_sess_id_customer');
				$where_kunnr = " AND KUNNR LIKE '{$kunnr}' ";
			}
			$where .= "(
				VBELN LIKE {$keyword} OR
				MATNR LIKE {$keyword} OR
				ARKTX LIKE {$keyword} 
			)";
		}
	
		$sql = "
				WITH FF AS ( 
					SELECT 
						DISTINCT VBELN, MATNR, ARKTX 
					FROM M_SALESORDER 
					WHERE 
						MATKL = 'DIEN' 
						AND AUART IN ( 'ZX01' ) 
						AND KUNNR LIKE '%{$kunnr}%'
						AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
						),
					JJ AS (
					SELECT 
						DISTINCT VBELN, MATNR, ARKTX, SERNR, AUDAT, BSTNK
					FROM 
						M_SALESORDER 
					WHERE
						AUART = 'ZX01' 
						AND MATKL != 'DIEN' 
						AND KUNNR LIKE '%{$kunnr}%'
						AND (SUBSTRING ( AUDAT, 1, 4 ) ) = YEAR ( getdate ( ) )
						),
					BB AS (
					SELECT
						H.ARKTX AS 'TYPESERVICE',
						J.* 
					FROM
						JJ J
						LEFT JOIN FF H ON J.VBELN = H.VBELN
						),
					CC AS (
						SELECT DISTINCT
								MS.VBELN,
								MS.VBELN_VBFA,
								MS.ERDAT_VBFA AS 'DELIVERY_DATE'
						FROM
								BB B
								LEFT JOIN 
								M_SALESORDER MS
								ON B.VBELN = MS.VBELN 
						WHERE
								MS.BWART LIKE '601' 
								AND MS.VBELN_VBFA LIKE '49%' 
								AND MS.KUNNR LIKE '%{$kunnr}%'  
						),
					DD AS ( SELECT
						B1.VBELN,
						B1.MATNR,
						B1.ARKTX,
						B1.SERNR,
						C.DELIVERY_DATE,
						B1.AUDAT,
						B1.BSTNK,
						B1.TYPESERVICE
					FROM
						BB B1
						LEFT JOIN CC C ON B1.VBELN = C.VBELN
					),
					GG AS ( SELECT
						* 
					FROM
						DD
					WHERE 
						$where
					) SELECT
						count(*) AS 'JUMLAH'
					FROM
						GG
					;";

		
		$query = $this->db->query($sql);
		$result = $query->result_array();
        return $result;	
    }


    public function insert_loan_request()
	{
		$insert = array(
			'PART_NUMBER' 			=> $this->input->post('PART_NUMBER'),
			'PERIOD_FROM' 			=> $this->input->post('PERIOD_FROM'),
			'PERIOD_TO' 			=> $this->input->post('PERIOD_TO'),
			'CONDITION' 			=> $this->input->post('CONDITION'),
			'CONTACT_INFO' 			=> $this->input->post('CONTACT_INFO'),
			'CREATED_DATE' 			=> date('Y-m-d H:i:s'),
			'STATUS' 				=> 'request',
			'CREATE_BY' 			=> $this->session->userdata('log_sess_id_user'),
			'KETERANGAN' 			=> 'Requested in GMF AeroAsia'
		);
		return $this->db->insert('TC_LOAN_REQUEST', $insert);
	}

	public function insert_exchange_request()
	{
		$insert = array(
			'PART_NUMBER' 			=> $this->input->post('PART_NUMBER'),
			'EXCHANGE_TYPE' 		=> $this->input->post('EXCHANGE_TYPE'),
			'EXCHANGE_RATE' 		=> $this->input->post('EXCHANGE_RATE'),
			//'CONDITION' 			=> $this->input->post('CONDITION'),
			'CONTACT_INFO' 			=> $this->input->post('CONTACT_INFO'),
			'CREATED_DATE' 			=> date('Y-m-d H:i:s'),
			'STATUS' 				=> 'request',
			'CREATE_BY' 			=> $this->session->userdata('log_sess_id_user'),
			'KETERANGAN' 			=> 'Requested in GMF AeroAsia'
		);
		return $this->db->insert('TC_EXCHANGE_REQUEST', $insert);
	}





	/*=======================================================================================================================*/
	private $column_order = [
		'loan' => [null,"PART_NUMBER","PERIOD_FROM","PERIOD_TO","CONDITION","CONTACT_INFO","KETERANGAN","STATUS"],
		'exchange' => [null,"PART_NUMBER", "EXCHANGE_TYPE", "EXCHANGE_RATE","CONTACT_INFO","KETERANGAN","STATUS"]
	];
	private $column_search = [
		'loan' => ["PART_NUMBER","PERIOD_FROM","PERIOD_TO","CONDITION","CONTACT_INFO","KETERANGAN","STATUS"],
		'exchange' => ["PART_NUMBER", "EXCHANGE_TYPE", "EXCHANGE_RATE","CONTACT_INFO","KETERANGAN","STATUS"]
	];
	private $order = [
		'loan' => ['ID_LOAN_REQUEST' => 'DESC'],
		'exchange' => ['ID_EXCHANGE_REQUEST' => 'DESC']
	];

	private function _get_datatables($type) {

        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
          foreach ($column_search as $item){
            if($_REQUEST['search']['value']) {  
              if($i===0) {
                $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                $this->db->like($item, $_REQUEST['search']['value']);
              } else {
                $this->db->or_like($item, $_REQUEST['search']['value']);
              }
              if(count($column_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
            }
            $i++;
          }
        }
      
        if(isset($_REQUEST['order'])) {
          if($_REQUEST['order']['0']['column'] == 0) {
            $this->db->order_by(key($order), $order[key($order)]);
          }else{
            $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
          }
        } else if(isset($order)) {
          $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
        	$pecah = explode("ORDER BY", $result);
        	$where = "";
        }else{
        	$pecah = explode("ORDER BY", $query[1]);
        	$where = "WHERE (".$pecah[0].") ";
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
          'start'           => $start,
          'end'             => $end,
          'order'           => $order,
          'where' 			=> $where
        );
    }

    function get_list_loan_exchange_export($jenis,$filter = FALSE) {
    	if($jenis == "loan"){
        	$sql = "
		        WITH X AS ( 
				  SELECT 
				  ROW_NUMBER ( ) OVER ( ORDER BY ID_LOAN_REQUEST DESC) AS BARIS,
				  ID_LOAN_REQUEST AS ID,
				  * 
				  FROM TC_LOAN_REQUEST 
				
				) 
		    	SELECT * FROM X 
		    
		    	";
        }else{
        	$sql = "
		        WITH X AS ( 
				  SELECT 
				  ROW_NUMBER ( ) OVER ( ORDER BY ID_EXCHANGE_REQUEST DESC ) AS BARIS,
				  ID_EXCHANGE_REQUEST AS ID,
				  * 
				  FROM TC_EXCHANGE_REQUEST 
				) 
		    	SELECT * FROM X 
		    	
		    	";
        }
        return $this->db->query($sql)->result_array();
    }


	function get_list_loan_exchange($jenis, $total = FALSE, $filter = FALSE) {
        $param = $this->_get_datatables($jenis);

        $kondisi = $this->session->userdata('log_sess_id_user');
        if ($this->session->userdata('log_sess_level_user_role') != "1") {
        	if($param['where'] == ""){
	        	$kondisi = "WHERE CREATE_BY=".$kondisi;
	        }else{
	        	$kondisi = " AND CREATE_BY=".$kondisi;
	        }
        }else{
        	$kondisi = "";
        }

        if($jenis == "loan"){
        	$sql = "
		        WITH X AS ( 
				  SELECT 
				  ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS BARIS,
				  ID_LOAN_REQUEST AS ID,
				  * 
				  FROM TC_LOAN_REQUEST ".$param['where']." ".$kondisi."
				) 
		    	SELECT * FROM X 
		    	WHERE BARIS > ".$param['start']." AND BARIS <= ".$param['end']."
		    	";
        }else{
        	$sql = "
		        WITH X AS ( 
				  SELECT 
				  ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS BARIS,
				  ID_EXCHANGE_REQUEST AS ID,
				  * 
				  FROM TC_EXCHANGE_REQUEST ".$param['where']." ".$kondisi."
				) 
		    	SELECT * FROM X 
		    	WHERE BARIS > ".$param['start']." AND BARIS <= ".$param['end']."
		    	";
        }

    	if($total){
    		$sql = explode("WHERE BARIS >", $sql);
    		$sql = $sql[0];
    		return $this->db->query($sql)->num_rows();
    	}elseif($filter){
    		$sql = explode("WHERE BARIS >", $sql);
    		$sql = $sql[0];
    		return $this->db->query($sql)->num_rows();
    	}else{
    		return $this->db->query($sql)->result();
    	}
    }

    public function get_detail_loan($id_data) {
		$this->db->from('TC_LOAN_REQUEST');
		$this->db->where("ID_LOAN_REQUEST",$id_data);

    	$this->db->select('*');
    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function get_detail_exchange($id_data) {
		$this->db->from('TC_EXCHANGE_REQUEST');
		$this->db->where("ID_EXCHANGE_REQUEST",$id_data);

    	$this->db->select('*');
    	$query = $this->db->get();
    	return $query->result_array();
    }

    public function update_loan($id_data,$status,$keterangan) {
    	$data = array(
		        'STATUS' => $status,
		        'KETERANGAN' => $keterangan
		);

    	$this->db->where("ID_LOAN_REQUEST",$id_data);
    	$update = $this->db->update('TC_LOAN_REQUEST',$data);
		
		return $update;
    }

    public function update_exchange($id_data,$status,$keterangan) {
    	$data = array(
		        'STATUS' => $status,
		        'KETERANGAN' => $keterangan
		);

    	$this->db->where("ID_EXCHANGE_REQUEST",$id_data);
    	$update = $this->db->update('TC_EXCHANGE_REQUEST',$data);
		
		return $update;
    }

    public function finish_loan($id_data,$part_number,$start_date,$end_date,$condition,$contact) {
    	$data = [
    		'PERIOD_FROM' => $start_date,
    		'PERIOD_TO' => $end_date,
    		'CONDITION' => $condition,
    		'CONTACT_INFO' => $contact,
    		'STATUS' => 'process'
    	];
    	$this->db->where('PART_NUMBER',$part_number);
    	$this->db->where('ID_LOAN_REQUEST',$id_data);
    	return $this->db->update('TC_LOAN_REQUEST',$data);
    }

    public function finish_exchange($id_data,$part_number,$type,$rate,$contact) {
    	$data = [
    		'EXCHANGE_TYPE' => $type,
    		'EXCHANGE_RATE' => $rate,
    		'CONTACT_INFO' => $contact,
    		'STATUS' => 'process'
    	];
    	$this->db->where('PART_NUMBER',$part_number);
    	$this->db->where('ID_EXCHANGE_REQUEST',$id_data);
    	return $this->db->update('TC_EXCHANGE_REQUEST',$data);
    }

    public function done_loan($id_data)
    {
    	$data = [
    		'STATUS' => 'done'
    	];
    	$this->db->where('ID_LOAN_REQUEST',$id_data);
    	return $this->db->update('TC_LOAN_REQUEST',$data);
    }

    public function done_exchange($id_data)
    {
    	$data = [
    		'STATUS' => 'done'
    	];
    	$this->db->where('ID_EXCHANGE_REQUEST',$id_data);
    	return $this->db->update('TC_EXCHANGE_REQUEST',$data);
    }

    public function summary_loan()
    { 
    	if($this->session->userdata('log_sess_level_user_role') == "4"){
    		$id_user = $this->session->userdata('log_sess_id_user');
    		$this->db->where('CREATE_BY',$id_user);
    		$status = ['available'];
    	}else{
    		$status = ['request','process'];
    	}
    	$select = [
    		'STATUS',
    		'COUNT(*) AS JUMLAH'
    	];
		$query = $this->db->select($select)
		        ->from('TC_LOAN_REQUEST')
				->where_in('STATUS', $status)
		        ->group_by('STATUS')
		        ->get();
		return $query->result_array();
    }

    public function summary_exchange()
    { 
    	if($this->session->userdata('log_sess_level_user_role') == "4"){
    		$id_user = $this->session->userdata('log_sess_id_user');
    		$this->db->where('CREATE_BY',$id_user);
    		$status = ['available'];
    	}else{
    		$status = ['request','process'];
    	}
    	$select = [
    		'STATUS',
    		'COUNT(*) AS JUMLAH'
    	];
		$query = $this->db->select($select)
		        ->from('TC_EXCHANGE_REQUEST')
				->where_in('STATUS', $status)
		        ->group_by('STATUS')
		        ->get();
		return $query->result_array();
    }
}

?>