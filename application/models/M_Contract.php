<?php
date_default_timezone_set("Asia/Jakarta");
class M_Contract extends CI_Model
{
	private $column_order = [
        'pn' => ['ATA','PART_NUMBER','DESCRIPTION','ESS','MEL','TARGET_SLA','TARGET_UNSERVICEABLE_RETURN','SERVICE_LEVEL_CATEGORY','TARGET_SERVICE_LEVEL'],
        'fleet' => ['REGISTRATION','TYPE','MSN','MFG_DATE'],
        'contract' => [null,'CONTRACT_NUMBER','COMPANY_NAME','DELIVERY_POINT','DELIVERY_ADDRESS','DESCRIPTION'],
    ];
    private $column_search = [
        'pn' => ['ATA','PART_NUMBER','DESCRIPTION','ESS','MEL','TARGET_SLA','TARGET_UNSERVICEABLE_RETURN','SERVICE_LEVEL_CATEGORY','TARGET_SERVICE_LEVEL'],
        'fleet' => ['REGISTRATION','TYPE','MSN','MFG_DATE'],
        'contract' => ['CONTRACT_NUMBER','COMPANY_NAME','DELIVERY_POINT','DELIVERY_ADDRESS','DESCRIPTION'],
    ];
    private $order = [
        'pn' => ['ID' => 'ASC'],
        'fleet' => ['ID' => 'ASC'],
        'contract' => ['ID' => 'DESC'],
    ];

	function get_customer()
	{
		$id_customer = $this->session->userdata('log_sess_id_customer');
		return $this->db->get('CUSTOMER')->result_array();
	}

	function get_all_customer()
	{
		return $this->db->get('CUSTOMER')->result_array();
	}

	function create_contract($param)
	{
		$data = array(
        	'CONTRACT_NUMBER' => $param['CONTRACT_NUMBER'],
        	'ID_CUSTOMER' => $param['ID_CUSTOMER'],
        	'DELIVERY_POINT' => $param['DELIVERY_POINT'],
        	'DELIVERY_ADDRESS' => $param['DELIVERY_ADDRESS'],
        	'DESCRIPTION' => $param['DESCRIPTION'],
        	'CREATE_AT' => date('Y-m-d'),
        	'IS_DELETE' => 0,
		);
		$result = $this->db->insert('TC_CONTRACT', $data);

		return $this->db->insert_id();
	}

	function create_contract_fleet($param)
	{
		$data = array(
  			'ID_CONTRACT' => $param['ID_CONTRACT'],
  			'REGISTRATION' => $param['REGISTRATION'],
  			'TYPE' => $param['TYPE'],
          	'MSN' => $param['MSN'],
          	'MFG_DATE' => $param['MFG_DATE'],
          	'NAMA_FILE' => $param['NAMA_FILE'],
          	'CREATE_AT' => date('Y-m-d'),
          	'IS_DELETE' => 0
  		);
  		$result = $this->db->insert('TC_CONTRACT_FLEET', $data);

  		return $this->db->insert_id();
	}

	function create_contract_pn($param)
	{
		$data = array(
      		'ID_CONTRACT' => $param['ID_CONTRACT'],
      		'ATA' => $param['ATA'],
      		'PART_NUMBER' => $param['PART_NUMBER'],
      		'DESCRIPTION' => $param['DESCRIPTION'],
      		'ESS' => $param['ESS'],
          	'MEL' => $param['MEL'],
          	'TARGET_SLA' => $param['TARGET_SLA'],
          	'WAKTU_SLA' => $param['WAKTU_SLA'],
          	'SATUAN_SLA' => $param['SATUAN_SLA'],
          	'NAMA_FILE' => $param['NAMA_FILE'],
          	'CREATE_AT' => date('Y-m-d'),
          	'IS_DELETE' => 0,
          	'TARGET_UNSERVICEABLE_RETURN' => $param['TARGET_UNSERVICEABLE_RETURN'],
          	'SERVICE_LEVEL_CATEGORY' => $param['SERVICE_LEVEL_CATEGORY'],
          	'TARGET_SERVICE_LEVEL' => $param['TARGET_SERVICE_LEVEL']
      	);
      	$result = $this->db->insert('TC_CONTRACT_PN', $data);

      	return $this->db->insert_id();
	}

	function query($sql)
	{
		return $this->db->query($sql)->result_array();
    }

	function get_detail_contract($id_contract)
	{
		return $this->db->get_where('TC_CONTRACT', ['ID' => $id_contract])->result_array();
	}

	function get_list_pn($id_contract, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('pn');

        $sql = "
        WITH TEMP AS (
            SELECT
                *
            FROM
                TC_CONTRACT_PN
            WHERE
            	ID_CONTRACT = $id_contract AND
            	IS_DELETE = 0
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
            $query = explode("WHERE ROWID >", $sql);
            $query = $query[0];
            $result = $this->db->query($sql)->result();
            return array(
                'query'     => $query,
                'result'    => $result
            );
        }
    }

    function get_list_fleet($id_contract, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('fleet');

        $sql = "
        WITH TEMP AS (
            SELECT
                *
            FROM
                TC_CONTRACT_FLEET
            WHERE
            	ID_CONTRACT = $id_contract AND
            	IS_DELETE = 0
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
            $query = explode("WHERE ROWID >", $sql);
            $query = $query[0];
            $result = $this->db->query($sql)->result();
            return array(
                'query'     => $query,
                'result'    => $result
            );
        }
    }

    function delete_pn($ct_id)
	{
		$date = date('Y-m-d');
		$sql = "UPDATE TC_CONTRACT_PN SET UPDATE_AT = '$date', IS_DELETE = '1' WHERE ID_CONTRACT='$ct_id'";
		return $this->db->query($sql);
	}

	function delete_fleet($ct_id)
	{
		$date = date('Y-m-d');
		$sql = "UPDATE TC_CONTRACT_FLEET SET UPDATE_AT = '$date', IS_DELETE = '1' WHERE ID_CONTRACT='$ct_id'";
		return $this->db->query($sql);
	}

	function insert_pn($data,$ct_id,$file)
	{
		$sql = "
		INSERT INTO 
			TC_CONTRACT_PN (
				ID_CONTRACT, 
				ATA, 
				PART_NUMBER, 
				DESCRIPTION, 
				ESS, 
				MEL, 
				TARGET_SLA, 
				WAKTU_SLA, 
				SATUAN_SLA, 
				TARGET_UNSERVICEABLE_RETURN, 
				SERVICE_LEVEL_CATEGORY, 
				TARGET_SERVICE_LEVEL, 
				NAMA_FILE, 
				CREATE_AT, 
				IS_DELETE,
                TUR_WAKTU,
                TUR_SATUAN
			) VALUES ";
		$date = date('Y-m-d');
		for($a=1;$a<count($data);$a++){
			if ($a != 1) $sql .= ", ";
			$sla 		= explode(" ", $data[$a][6]);
            $tur        = explode(" ", $data[$a][7]);
			if(count($sla) > 1){
				$waktu_sla 	= $sla[0];
				$satuan_sla = $sla[1];
			}else{
				$waktu_sla 	= NULL;
				$satuan_sla = NULL;	
			}

            if(count($tur) > 1){
                $waktu_tur  = $tur[0];
                $satuan_tur = $tur[1];
            }else{
                $waktu_tur  = NULL;
                $waktu_tur = NULL; 
            }

		    $sql .= "('{$ct_id}','{$data[$a][1]}','{$data[$a][2]}','{$data[$a][3]}','{$data[$a][4]}','{$data[$a][5]}','{$data[$a][6]}','{$waktu_sla}','{$satuan_sla}','{$data[$a][7]}','{$data[$a][8]}','{$data[$a][9]}','{$file}','{$date}','0','{$waktu_tur}','{$satuan_tur}')";
		}
		return $this->db->query($sql);
	}

	function insert_fleet($data,$ct_id,$file)
	{
		$sql = "INSERT INTO TC_CONTRACT_FLEET (ID_CONTRACT, REGISTRATION, TYPE, MSN, MFG_DATE, NAMA_FILE, CREATE_AT, IS_DELETE) VALUES ";
		$date = date('Y-m-d');
		for($a=1;$a<count($data);$a++){
			if ($a != 1) $sql .= ", ";
			if(!($data[$a][4] instanceof DateTime)){
				$mfg_date = '0000-00-00';
			}else{
				$mfg_date = explode(" ", $data[$a][4]->format('Y-m-d H:i:s'));
				$mfg_date = $mfg_date[0];
			} 

		    $sql .= "('{$ct_id}','{$data[$a][1]}','{$data[$a][2]}','{$data[$a][3]}','{$mfg_date}','{$file}','{$date}','0')";
		}
		return $this->db->query($sql);
	}

	function get_list_contract($id_customer, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('contract');

        if($id_customer){
        	$customer = "AND TC_CONTRACT.ID_CUSTOMER = '$id_customer'";
        }else{
        	$customer = "";
        }

        $sql = "
        WITH TEMP AS (
            SELECT 
				CUSTOMER.COMPANY_NAME,
				TC_CONTRACT.CONTRACT_NUMBER,
				TC_CONTRACT.ID_CUSTOMER,
				TC_CONTRACT.DELIVERY_POINT,
				TC_CONTRACT.DELIVERY_ADDRESS,
				TC_CONTRACT.DESCRIPTION,
				TC_CONTRACT.CREATE_AT,
				TC_CONTRACT.UPDATE_AT,
				TC_CONTRACT.ID,
				TC_CONTRACT.IS_DELETE
			FROM TC_CONTRACT
			JOIN CUSTOMER ON TC_CONTRACT.ID_CUSTOMER = CUSTOMER.ID_CUSTOMER
			WHERE 
				IS_DELETE = 0
				$customer
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
            $query = explode("WHERE ROWID >", $sql);
            $query = $query[0];
            $result = $this->db->query($sql)->result();
            return array(
                'query'     => $query,
                'result'    => $result
            );
        }
    }





    /*===============================================================================================================*/

    private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }
}
?>