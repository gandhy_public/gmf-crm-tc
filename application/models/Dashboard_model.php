<?php

Class Dashboard_model extends CI_Model {
    
    function hasil_survey() {
    $sql = "SELECT STATUS AS hasil, COUNT(*) total FROM status_order GROUP BY STATUS ";
    return $this->db->query($sql);
    }

    function getTotalWaitingApproved() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Approved")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWaitingMaterial() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Material")
                // ->where('ID_PROGRESS_STATUS', "2")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalUnderMaintenance() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Under Maintenance")
                // ->where('ID_PROGRESS_STATUS', "3")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWaitingRepair() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Repair")
                // ->where('ID_PROGRESS_STATUS', "4")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalFinished() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Finished")
                // ->where('ID_PROGRESS_STATUS', "5")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWA() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Approved")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWM() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Material")
                // ->where('ID_PROGRESS_STATUS', "2")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalUM() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Under Maintenance")
                // ->where('ID_PROGRESS_STATUS', "3")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWR() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Repair")
                // ->where('ID_PROGRESS_STATUS', "4")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalFIN() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Finished")
                // ->where('ID_PROGRESS_STATUS', "5")
                ->get();
        return $query->num_rows();
    }

}



?>