<?php

class Landinggear_model extends CI_Model{

  private $tc_project = "M_REVISION";
  private $tc_project_detail = "M_REVISIONDETAIL";
  private $tc_order_lg = "M_PMORDER";
  private $customer = "CUSTOMER";

  var $column_order_ac = array(null,'ACREG_REVTX','EARTX','SERGE');
  var $column_search_ac = array('ACREG_REVTX','EARTX','SERGE');
  var $order_ac = array('ACREG_REVTX' => 'ASC');

  var $column_order_project = array(null,'M_REVISION.REVNR','M_REVISION.ACREG_REVTX','M_REVISION.MATNR','M_REVISION.SERNR','CUSTOMER.COMPANY_NAME','M_REVISIONDETAIL.WORKSCOPE','M_REVISION.REVBD');
  var $column_search_project = array('M_REVISION.REVNR','M_REVISION.ACREG_REVTX','M_REVISION.MATNR','M_REVISION.SERNR','CUSTOMER.COMPANY_NAME','M_REVISIONDETAIL.WORKSCOPE','M_REVISION.REVBD');
  var $order = array('M_REVISION.REVNR' => 'ASC');

  private $column_order_lg = array('AUFNR','AUFNR','ILART','KTEXT','TXT_STAT');
  private $column_search_lg = array('AUFNR','ILART','KTEXT','TXT_STAT');
  private $order_lg = array('AUFNR' => 'ASC');

	function tampil_data(){
		/*return $this->db->get('tb_pooling');*/
		$query = $this->db->select('*')
                ->from('tb_pooling')
                /*where('STATUS', "Waiting Approved")*/
                /*>where('ID_PROGRESS_STATUS', "1")*/
                ->get();
        return $query->result();
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}

	function getAircraft($stat){
		if ($this->session->userdata('status_lg') == 'closed') {
			$where = " AND TXT04 = 'CLSD' ";
		} else {
			$where = " AND TXT04 != 'CLSD' ";
		}
		// $kunnr = '';
  //       if ($this->session->userdata('log_sess_id_customer')) {
  //           $kunnr = $this->session->userdata('log_sess_id_customer');
  //           $where = " CUSTOMER LIKE '{$kunnr}' ";
  //       }
  //       else {
  //          $where = "ORDER_TYPE LIKE 'GA03'"; 
  //       }
		$sql = "
				SELECT ACREG_REVTX, EQUNR_EQUZ, SERGE FROM M_REVISION WHERE IWERK = 'WSWB' $where
				GROUP BY  ACREG_REVTX,EQUNR_EQUZ, SERGE ";

		$query = $this->db->query($sql);

		return $query->result();
	}

	function getAircraftInfo($doc){
		$sql = "
				SELECT ACREG_REVTX, EQART, EARTX, EQUNR_EQUZ, SERGE FROM M_REVISION WHERE IWERK = 'WSWB' AND ACREG_REVTX LIKE '$doc'
				GROUP BY  ACREG_REVTX, EQART, EARTX, EQUNR_EQUZ, SERGE ";

		$query = $this->db->query($sql);

		return $query->result();
	}


	function getListProject($doc,$stat){
		if ($this->session->userdata('status_lg') == 'closed') {
			$where = " AND TXT04 = 'CLSD' ";
		} else {
			$where = " AND TXT04 != 'CLSD' ";
		}
		// $kunnr = '';
  //       if ($this->session->userdata('log_sess_id_customer')) {
  //           $kunnr = $this->session->userdata('log_sess_id_customer');
  //           $where = " CUSTOMER LIKE '{$kunnr}' ";
  //       }
  //       else {
  //          $where = "ORDER_TYPE LIKE 'GA03'"; 
  //       }
		$sql = "
		SELECT
			A.REVNR, -- NO REVISION
			A.EQUNR, -- ESN
			B.TEAM_EO,
      B.AGREED_TAT,
			A.MATNR,
			A.SERNR,
      A.ACREG_REVTX,
      A.PARNR,
      A.EARTX,
			-- ENGINE OWNER
			-- 	CUSTOMER NAME
			B.WORKSCOPE,
			A.REVBD, -- START PROJECT
			A.REVED,
			CASE
				WHEN ((REVBD != '00000000') AND (REVBD != '00000000')) THEN	DATEDIFF( DAY, CONVERT ( DATE, REVBD, 104 ), CONVERT ( DATE, REVED, 104 ) )
				ELSE ' '		
			END AS TAT 	
			-- 	B.AGREED_TAT
			-- 	CURRENT TAT
		FROM
			M_REVISION A
			LEFT JOIN M_REVISIONDETAIL B ON B.REVNR = A.REVNR
		WHERE
			A.IWERK = 'WSWB' AND ACREG_REVTX LIKE '$doc'
			$where

		";

		$query = $this->db->query($sql);

		return $query->result();
	}

  	function getOverviewProject($docno){
		$sql = "
		SELECT
			A.REVNR, -- NO REVISION
			A.EQUNR, -- ESN
			B.TEAM_EO, -- ENGINE OWNER
			B.AGREED_TAT,
			A.REVTY,
			A.UDATE_SMR, -- START PROJECT
			A.ATSDATE_SMR,
      A.ACREG_REVTX,
      A.PARNR, --   CUSTOMER NAME
      A.EARTX,
      C.COMPANY_NAME
			-- 	B.AGREED_TAT
			-- 	CURRENT TAT
			FROM
			M_REVISION A
			LEFT JOIN M_REVISIONDETAIL B ON B.REVNR = A.REVNR
      LEFT JOIN CUSTOMER C ON C.ID_CUSTOMER = A.PARNR
			WHERE
			A.IWERK = 'WSWB'
			AND A.REVNR = '{$docno}'
		";
        $query = $this->db->query($sql);

        return $query->row_array();
  	}

  //DEVIATION
  	function getlistDeviation($docno) {
	    $sql = "
	      SELECT
	        RD.DEV_NUMBER,
	        RD.DELAY_TAT,
	        RD.DELAY_CUSTOMER,
	        RD.CG_DELAY_PROCESS,
	        RD.CURRENT_GATE,
	        RD.NEXT_GATE_EFFCTD,
	        RD.DEVIATION_REASON,
	        RD.DEPARTEMENT,
	        RD.PROCESS_DELAYED,
	        RD.ROOT_CAUSE,
	        RD.CORECTIVE_ACTION,
	        RD.CREATED_BY,
	        RD.CREATED_DATE
	      FROM
	        M_REVISIONDEVIATION RD
	        JOIN M_REVISION R ON RD.REVNR = R.REVNR
	      WHERE R.REVNR = '{$docno}'
	    ";
	     return $this->db->query($sql)->result();
  	}

	function getOverviewListProgress($docno){
		$sql = "SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT     
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '$docno'
                AND AUART IN ('GA01', 'GA02', 'GA05') ";
		$query = $this->db->query($sql);

		return $query->result_array();
	}

  function getOverviewProgress($docno){
    $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT     
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '$docno'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH', TXT_STAT FROM BB GROUP BY  TXT_STAT ;";
    $query = $this->db->query($sql);

    return $query->result_array();
  }

  function getMATProgress($revnr){
    $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT,
                CASE 
                  WHEN ILART IN ('REM','PRI') THEN 'REMOVAL'
                  WHEN ILART = 'DSY' THEN 'DISSAMBLY'
                  WHEN ILART IN ('1','RT1','RT2','RT3') THEN 'INSPECTION'
                  WHEN ILART IN ('REP','NWT') THEN 'REPAIR'
                  WHEN ILART = 'ASY' THEN 'ASSEMBLY'
                  WHEN ILART = 'INS' THEN 'INSTALL'
                  WHEN ILART = 'TES' THEN 'TEST'
                  WHEN ILART = 'QEC' THEN 'QEC'
                END AS MAT        
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
              SELECT COUNT(*) as 'JUMLAH', MAT, TXT_STAT FROM BB GROUP BY MAT, TXT_STAT ;";

      $result = $this->db->query($sql)->result_array();
      return $result;
    }

		//REMOVAL
	function getTotalRemoval($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'Progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'Close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI');";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedRemoval($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI') AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//REMOVAL

	//DSY
	function getTotalDisassembly($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'DSY';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedDisassembly($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'DSY' AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//DSY

	//Inspection
	function getTotalInspection($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT1','RT2','RT3');";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedInspection($revnr) {
       // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT1','RT2','RT3') AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//Inspection

	//Repair
	function getTotalRepair($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NWT');";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedRepair($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NWT') AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//Repair

	//ASY
	function getTotalAssembly($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                   WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close'  
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'ASY';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedAssembly($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                   WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'ASY' AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//ASY

	//INS
	function getTotalInstall($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'INS';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedInstall($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                   WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'INS' AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//INS


	//TST
	function getTotalTest($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close'  
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TST';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedTest($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close'  
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TST' AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//TST


	//QEC
	function getTotalQEC($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

	function getClosedQEC($revnr) {
        // $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                ILART,
                KTEXT,
                CASE
                    WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDERH
              WHERE
                REVNR LIKE '%$revnr'
                AND AUART IN ('GA01', 'GA02', 'GA05')) 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC' AND TXT_STAT LIKE 'close';";
                // echo $this->db->last_query();
                // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }
	//QEC

  /*================================================Server Side================================================*/

  private function _get_datatables_query($type)
    {
      if ($type == 'ac') {
          $this->db->select('ACREG_REVTX', 'EARTX', 'SERGE');
          $this->db->from($this->tc_project);

          $cari = $this->column_search_ac;
      }elseif($type == 'project'){
        $this->db->from($this->tc_project);
        $this->db->join($this->tc_project_detail, 'M_REVISION.REVNR = M_REVISIONDETAIL.REVNR', 'left');
        $this->db->join($this->customer, 'M_REVISION.PARNR = CUSTOMER.ID_CUSTOMER', 'left');
        
        $cari = $this->column_search_project;
      }
 
        $i = 0;
      foreach ($cari as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
          if($_REQUEST['order']['0']['column'] == 0){
            if ($type == 'project') {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
              $order = $this->order_ac;
                $this->db->order_by(key($order), $order[key($order)]);
            }
          }else{
            if($type == 'ac'){
              $this->db->order_by($this->column_order_ac[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }elseif($type == 'project'){
              $this->db->order_by($this->column_order_project[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
          }
        } 
        // else if(isset($this->order) && isset($this->order_ac))
        // {
        //   if ($type == 'ct') {
        //     $order = $this->order_ct;
        //       $this->db->order_by(key($order), $order[key($order)]);
            
        //   }else{
        //     $order = $this->order;
        //       $this->db->order_by(key($order), $order[key($order)]);
        //   }
        // }
    }

  function get_datatables($ac_type, $lg, $type)
    {
        $this->_get_datatables_query($type);
        if(!empty($ac_type) and $type == 'project'){
          $this->db->where('M_REVISION.ACREG_REVTX', $ac_type);
          $this->db->where('M_REVISION.IWERK', 'WSWB');
          if ($lg == 1)
            {
              $this->db->like('M_REVISION.PNAME', 'NLG_');
            }
          else if ($lg == 2)
            {
              $this->db->like('M_REVISION.PNAME', 'MLG_LH');
            }
          else if ($lg == 3)
            {
              $this->db->like('M_REVISION.PNAME', 'MLG_RH');
            }
          if ($this->session->userdata('status_lg') == 'closed') {
            $this->db->where('M_REVISION.TXT04', 'CLSD');
          } else {
            $this->db->where('M_REVISION.TXT04 !=', 'CLSD');
          }
        }
        if($type == 'ac'){
          if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $this->db->where('PARNR', $kunnr);
          }
          $this->db->where('IWERK', 'WSWB');
          $this->db->group_by(array('ACREG_REVTX','EARTX', 'SERGE'));
        }

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }    
 
    function count_filtered($sql)
    {
        // $this->_get_datatables_query($type);
        // if(!empty($ac_type) and $type == 'project'){
        //   $this->db->where('M_REVISION.ACREG_REVTX', $ac_type);
        //   $this->db->where('M_REVISION.IWERK', 'WSWB');
        //   if ($this->session->userdata('status_lg') == 'closed') {
        //     $this->db->where('M_REVISION.TXT04', 'CLSD');
        //   } else {
        //     $this->db->where('M_REVISION.TXT04 !=', 'CLSD');
        //   }
        // }
        // if($type == 'ac'){
        //   $this->db->where('IWERK', 'WSWB');
        //   $this->db->group_by(array('ACREG_REVTX','EARTX', 'SERGE'));
        // }
        // $query = $this->db->get();
        // return $query->num_rows();
        $query = $this->db->query($sql);
        return $query->result();
    }
 
    public function count_all($ac_type, $lg, $type)
    {
      if ($type == 'ac') {
        $this->db->select('ACREG_REVTX', 'EARTX', 'SERGE');
        $this->db->from($this->tc_project);
        $this->db->where('IWERK', 'WSWB');
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $this->db->where('PARNR', $kunnr);
          }
        $this->db->group_by(array('ACREG_REVTX','EARTX', 'SERGE'));
        // $this->db->where('IS_DELETE','0');
      }elseif($type == "project"){

        $this->db->from($this->tc_project);
        $this->db->join($this->tc_project_detail, 'M_REVISION.REVNR = M_REVISIONDETAIL.REVNR', 'left');
        $this->db->join($this->customer, 'M_REVISION.PARNR = CUSTOMER.ID_CUSTOMER', 'left');
        $this->db->where('M_REVISION.ACREG_REVTX', $ac_type);
        $this->db->where('M_REVISION.IWERK', 'WSWB');
        if ($lg == 1)
            {
              $this->db->like('M_REVISION.PNAME', 'nose');
            }
          else if ($lg == 2)
            {
              $this->db->like('M_REVISION.PNAME', 'MLG_LH');
            }
          else if ($lg == 3)
            {
              $this->db->like('M_REVISION.PNAME', 'MLG_RH');
            }
        if ($this->session->userdata('status_lg') == 'closed') {
          $this->db->where('M_REVISION.TXT04', 'CLSD');
        } else {
          $this->db->where('M_REVISION.TXT04 !=', 'CLSD');
        }
      }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_datatables_order($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';
        $docno = $param['docno'];

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if (isset($param['start']) and isset($param['end'])) {
            // code...
            // print_r($param['search']);
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum <= {$param['end']}";
        }


        if (isset($param['order'])) 
          { 
          if($_REQUEST['order']['0']['column'] == 0){
            $order = "ORDER BY AUFNR ASC";
            }
          else {
            $order = "ORDER BY {$this->column_order_lg[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
          }
        }
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            // $kunnr = '';
            // if ($this->session->userdata('log_sess_id_customer')) {
            //     $kunnr = $this->session->userdata('log_sess_id_customer');
            //     $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            // }
            // else {
            //    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            // }
            $where = "AND (
                AUFNR LIKE {$keyword} OR
                KTEXT LIKE {$keyword} OR
                ILART LIKE {$keyword} 
            )";
        }

        $sql = "WITH TblDatabases AS
                    (
                    SELECT DISTINCT
                      AUFNR,
                      ILART,
                      KTEXT,
                      REVNR,
                      CASE
                          WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                          'open' 
                          WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                          'close' 
                        END AS TXT_STAT     
                    FROM
                      M_PMORDERH
                    WHERE
                      REVNR LIKE '$docno'
                      AND AUART IN ('GA01', 'GA02', 'GA05')
                      $where
                    ),
                  AA AS (
                        SELECT *, ROW_NUMBER() OVER ($order) as RowNum
                        FROM TblDatabases
                        )
                    SELECT * FROM AA WHERE REVNR LIKE '$docno' $rownum
                ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function get_datatables_order_all($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';
        $docno = $param['docno'];

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            // $kunnr = '';
            // if ($this->session->userdata('log_sess_id_customer')) {
            //     $kunnr = $this->session->userdata('log_sess_id_customer');
            //     $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            // }
            // else {
            //    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            // }
            $where = "AND (
                AUFNR LIKE {$keyword} OR
                KTEXT LIKE {$keyword} OR
                ILART LIKE {$keyword}
            )";
        }

        $sql = "WITH TblDatabases AS
                    (
                    SELECT DISTINCT
                      AUFNR,
                      ILART,
                      KTEXT,
                      REVNR,
                      CASE
                          WHEN PHAS0 LIKE 'x' OR PHAS1 LIKE 'x' THEN
                          'open' 
                          WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                          'close' 
                        END AS TXT_STAT     
                    FROM
                      M_PMORDERH
                    WHERE
                      REVNR LIKE '$docno'
                      AND AUART IN ('GA01', 'GA02', 'GA05')
                      $where
                    ),
                  AA AS (
                        SELECT *, ROW_NUMBER() OVER (ORDER BY AUFNR) as RowNum 
                        FROM TblDatabases
                        )
                    SELECT count(*) AS 'JUMLAH' FROM AA WHERE REVNR LIKE '$docno' $rownum
                ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
}
