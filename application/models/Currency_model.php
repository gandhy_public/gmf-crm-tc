<?php

Class Currency_model extends CI_Model 
{
	protected $table = 'TC_CURRENCY';

	var $column_order = array('code','label','value','last_update');
	var $column_search = array('code','label');
    var $order = array('code' => 'asc');

	public function fetch($check_last_date = FALSE)
	{		
		$sql = "SELECT * FROM TC_CURRENCY";

		if($check_last_date)
			$sql .= " WHERE last_update < Convert(date, getdate())";

		return $this->db->query($sql)->result_array();
	}

	public function fetch_by_code()
	{
		$return = [];
		$datas = $this->fetch();
		foreach($datas as $cur) 
			$return[$cur['code']] = $cur;
		
		return $return;
	}

	public function get($code)
	{
		try 
		{
			if(empty($code)) throw new Exception("Code Tidak Boleh Kosong", 1);
			
			$query = $this->db->select('*')
						->from($this->table)
						->where(['code' => $code])
						->limit(1, 0)
						->get();

			$result = $query->result_array();

			if(empty($result)) return [];
			return $result[0];	
		}
		catch (Exception $e)
		{
			return [];
		}
	}

	public function create($label, $code)
	{
		try
		{
			if(empty($label)) throw new Exception("Label Tidak Boleh Kosong", 1);
			if(empty($code)) throw new Exception("Code Tidak Boleh Kosong", 1);

			$code = strtoupper($code);

			$check = $this->get($code);
			if(!empty($check)) throw new Exception("Code Sudah Digunakan");
			
			$data = [
				'label'			=> $label,
				'code'			=> $code,
				'value'			=> 0,
				'last_update'	=> date('Y-m-d H:i:s'),
			];


			$insert = $this->db->insert($this->table, $data);
			if(!$insert) throw new Exception("Failed Insert", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function delete($code)
	{
		try
		{
			$check = $this->get($code);
			if(empty($check)) throw new Exception("Data Tidak Ditemukan", 1);

			$delete = $this->db->delete($this->table, array('code' => $code));
			if(!$delete) throw new Exception("Failed Delete", 1);
			
			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	private function get_exchange($code)
	{
		try
		{
			if(empty($code)) throw new Exception("Code Tidak Boleh Kosong", 1);
			$query = "{$code}_IDR";
			$url = "https://free.currencyconverterapi.com/api/v6/convert?q={$query}";
			
			$curl = curl_init();

	        curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(),
	        ));

	        $res = curl_exec($curl);
	        $err = curl_error($curl);

	        curl_close($curl);	        

	        if($err) throw new Exception($err, 1);

	        $resp = json_decode($res, 1);
	        if(empty($resp['results'][$query])) throw new Exception("Data Kosong");

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> $resp['results'][$query],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	public function recalculate($force = FALSE, $last_date = FALSE)
	{
		try
		{
			$today = date('Y-m-d');
			$datas = $this->fetch($last_date);

			foreach($datas as $cur)
			{
				$exchange = $this->get_exchange($cur['code']);
				if(
					$exchange['codestatus'] == 'S'
					&& (
						$force 
						|| (!$force && strtotime($today) > strtotime($cur['last_update']))
						|| empty($cur['value'])
					)
				)
				{
					$data = [				
						'value'			=> $exchange['resultdata']['val'],
						'last_update'	=> date('Y-m-d H:i:s'),
					];

					$update = $this->db->update($this->table, $data, ['code' => $cur['code']]);	
					if(!$update) throw new Exception("Failed Update", 1);
				}
			}

			return [
				'codestatus'	=> 'S',
				'message'		=> 'Berhasil',
				'resultdata'	=> [],
			];
		}
		catch (Exception $e)
		{
			return [
				'codestatus'	=> 'E',
				'message'		=> $e->getMessage(),
				'resultdata'	=> [],
			];
		}
	}

	// ========================================================

	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_REQUEST['length'] != -1)
        $this->db->limit($_REQUEST['length'], $_REQUEST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}