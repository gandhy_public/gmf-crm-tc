<?php

Class Retail_model_backup extends CI_Model {

    // Count Data

    public function count_retail()
    {
        $this->db->select('*');
        $this->db->from('status_order');
        // $this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
        // $this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
        // $this->db->join('ms_request_category', 'ms_request_category.ID_REQ_CAT = tr_pooling_order.ID_REQ_CAT','INNER');
        // $this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
                // $this->db->where('year(submited)=',date('Y'));
                // $this->db->where('month(submited)=',date('m'));

        $query = $this->db->get();  
        return $query->num_rows();
    }

    public function count_retail_where_status($status_order)
    {
        $this->db->select('*');
        $this->db->from('status_order');
        $this->db->where('status_order.STATUS', $status_order);
        
        $query = $this->db->get();  
        return $query->num_rows();
    }

    public function count_retail_where_filter($status_order,$status_paid)
    {
        $this->db->select('*');
        $this->db->from('status_order');
        if($status_paid)
        {
            $this->db->where('status_order.STATUS_PAID', $status_paid);
        }
        $this->db->where_in('status_order.STATUS', $status_order);

        $query = $this->db->get();  
        return $query->num_rows();
    }

    // ./ Count Data



    // CRUD

    public function select_retail($batas =null,$offset=null,$search=null)
    {
        if($batas != null)
        {
           $this->db->limit($batas,$offset);
        }
        
        $this->db->select('*');
        $this->db->from('status_order');
        
        $query = $this->db->get();  
        return $query->result_object();
    }

    public function select_retail_where_status($status_order,$batas =null,$offset=null,$search=null)
    {
        if($batas != null)
        {
           $this->db->limit($batas,$offset);
        }
        
        $this->db->select('*');
        $this->db->from('status_order');
        $this->db->where('status_order.STATUS', $status_order);
        
        $query = $this->db->get();  
        return $query->result_object();
    }

    public function select_retail_where_filter($status_order,$status_paid,$batas =null,$offset=null,$search=null)
    {
        if($batas != null)
        {
           $this->db->limit($batas,$offset);
        }

        
        $this->db->select('*');
        $this->db->from('status_order');
        if($status_paid)
        {
            $this->db->where('status_order.STATUS_PAID', $status_paid);
        }
        $this->db->where_in('status_order.STATUS', $status_order);
        
        $query = $this->db->get();  
        return $query->result_object();
    }

    public function get_retail_where_id($id)
    {
        $this->db->select('*');
        $this->db->from('status_order');
        $this->db->where('ID_STATUS',$id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_retail_log_where_id($id)
    {
        $this->db->select('*');
        $this->db->from('log_proses');
        $this->db->join('proses','log_proses.id_proses = proses.id_proses','INNER');
        $this->db->where('id_pelamar',$id_pelamar);
        $this->db->order_by('id_log_proses', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getlistProject() {
        $sql = "SELECT a.*,b.COMPANY_NAME as CUSTOMER_NAME from TB_PROJECT_LIST a left join customer b 
        on a.ID_CUSTOMER = b.ID_CUSTOMER
                ";
        return $this->db->query($sql)->result();
    }
    
    
    function getTotalWaitingApproved() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Approved")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWaitingMaterial() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Material")
                // ->where('ID_PROGRESS_STATUS', "2")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalUnderMaintenance() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Under Maintenance")
                // ->where('ID_PROGRESS_STATUS', "3")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWaitingRepair() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Repair")
                // ->where('ID_PROGRESS_STATUS', "4")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalFinished() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Finished")
                // ->where('ID_PROGRESS_STATUS', "5")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWA() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Approved")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWM() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Material")
                // ->where('ID_PROGRESS_STATUS', "2")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalUM() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Under Maintenance")
                // ->where('ID_PROGRESS_STATUS', "3")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalWR() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Waiting Repair")
                // ->where('ID_PROGRESS_STATUS', "4")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalFIN() {
        $query = $this->db->select('*')
                ->from('status_order')
                ->where('STATUS', "Finished")
                // ->where('ID_PROGRESS_STATUS', "5")
                ->get();
        return $query->num_rows();
    }

}



?>