<?php 
class Pooling_model extends CI_Model{

	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from('tr_pooling_order');
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();	
		return $query->num_rows();
	}



	// Listing Other

	public function get_part_number_list()
	{
		// $this->db->order_by('iduserrole', 'ASC');
		$data = $this->db->get('ms_part_number');
		return $data->result();
	}

	public function get_requirement_category_list()
	{
		$data = $this->db->get('ms_requirement_category');
		return $data->result();
	}

	public function get_aircraft_registration_list()
	{
		$data = $this->db->get('ms_aircraft_registration');
		return $data->result();
	}

	public function get_delivery_point_list()
	{
		$data = $this->db->get('ms_delivery_point');
		return $data->result();
	}

	// ./ Listing Other



	// Get Function JSON

	public function get_part_number_description($ID_PN)
	{
		$this->db->where('ID_PN',$ID_PN);
        return $this->db->get('ms_part_number')->row_object();
	}

	public function get_delivery_point_address($ID_DP)
	{
		$this->db->where('ID_DELIVERY_POINT',$ID_DP);
        return $this->db->get('ms_delivery_point')->row_object();
	}

	// Get Function JSON



	// Count Data

	public function count_request()
	{
		$this->db->select('*');
		$this->db->from('tr_pooling');
		$this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
		$this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
		$this->db->join('ms_requirement_category', 'ms_requirement_category.ID_REQUIREMENT_CATEGORY = tr_pooling_order.ID_REQUIREMENT_CATEGORY','INNER');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = tr_pooling_order.ID_AIRCRAFT_REGISTRATION','INNER');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = tr_pooling_order.ID_DELIVERY_POINT','INNER');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->get();	
		return $query->num_rows();
	}

	public function count_request_where_status($status)
	{
		$this->db->select('*');
		$this->db->from('tr_pooling');
		$this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
		$this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
		$this->db->join('ms_requirement_category', 'ms_requirement_category.ID_REQUIREMENT_CATEGORY = tr_pooling_order.ID_REQUIREMENT_CATEGORY','INNER');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = tr_pooling_order.ID_AIRCRAFT_REGISTRATION','INNER');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = tr_pooling_order.ID_DELIVERY_POINT','INNER');
		$this->db->where('tr_pooling_order.STATUS_ORDER', $status);
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->get();	
		return $query->num_rows();
	}

	// ./ Count Data



	// CRUD

	public function select_request($batas =null,$offset=null)
	{
		if($batas != null)
		{
	       $this->db->limit($batas,$offset);
	    }
	    
	    $this->db->select('*');
		$this->db->from('tr_pooling');
		$this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
		$this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
		$this->db->join('ms_requirement_category', 'ms_requirement_category.ID_REQUIREMENT_CATEGORY = tr_pooling_order.ID_REQUIREMENT_CATEGORY','INNER');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = tr_pooling_order.ID_AIRCRAFT_REGISTRATION','INNER');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = tr_pooling_order.ID_DELIVERY_POINT','INNER');
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));
		$this->db->order_by('tr_pooling.NO_ACCOUNT', 'ASC');
		
		$query = $this->db->get();	
		return $query->result_object();
	}

	public function select_request_where_status($status,$batas =null,$offset=null,$search=null)
	{
		if($batas != null)
		{
	       $this->db->limit($batas,$offset);
	    }
	    
	    $this->db->select('*');
		$this->db->from('tr_pooling');
		$this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
		$this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
		$this->db->join('ms_requirement_category', 'ms_requirement_category.ID_REQUIREMENT_CATEGORY = tr_pooling_order.ID_REQUIREMENT_CATEGORY','INNER');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = tr_pooling_order.ID_AIRCRAFT_REGISTRATION','INNER');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = tr_pooling_order.ID_DELIVERY_POINT','INNER');
		$this->db->where('tr_pooling_order.STATUS_ORDER', $status);
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->get();	
		return $query->result_object();
	}

	public function select_request_where_no_account($NO_ACCOUNT)
	{
		$this->db->select('*');
		$this->db->from('tr_pooling');
		$this->db->join('tr_pooling_order', 'tr_pooling_order.REFERENCE = tr_pooling.REFERENCE','INNER');
		$this->db->join('ms_part_number', 'ms_part_number.ID_PN = tr_pooling_order.ID_PART_NUMBER','INNER');
		$this->db->join('ms_requirement_category', 'ms_requirement_category.ID_REQUIREMENT_CATEGORY = tr_pooling_order.ID_REQUIREMENT_CATEGORY','INNER');
		$this->db->join('ms_aircraft_registration', 'ms_aircraft_registration.ID_AIRCRAFT_REGISTRATION = tr_pooling_order.ID_AIRCRAFT_REGISTRATION','INNER');
		$this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = tr_pooling_order.STATUS_ORDER','INNER');
		$this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = tr_pooling_order.ID_DELIVERY_POINT','INNER');
		$this->db->where('tr_pooling.NO_ACCOUNT', $NO_ACCOUNT);
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->get();	
		return $query->row_object();
	}

	public function insert_pooling($REFERENCE)
	{
		$insert = array(
			'REFERENCE' 			=> $REFERENCE
		);
		return $this->db->insert('tr_pooling', $insert);
	}

	public function insert_pooling_order($REFERENCE)
	{
		$insert = array(
			'REFERENCE' 				=> $REFERENCE,
			// 'ID_USER_POOL' 			=> $this->session->userdata(''),
			// 'ID_CUSTOMER' 			=> $this->session->userdata(''),
			'SERVICE_LEVEL' 			=> $this->input->post('SERVICE_LEVEL'),
			'ID_PART_NUMBER' 			=> $this->input->post('ID_PART_NUMBER'),
			'ID_REQUIREMENT_CATEGORY' 	=> $this->input->post('ID_REQUIREMENT_CATEGORY'),
			'REQUESTED_DELIVERY_DATE' 	=> $this->input->post('REQUESTED_DELIVERY_DATE'),
			'MAINTENANCE' 				=> $this->input->post('MAINTENANCE'),
			'ATA_CHAPTER' 				=> $this->input->post('ATA_CHAPTER'),
			'ID_AIRCRAFT_REGISTRATION' 	=> $this->input->post('ID_AIRCRAFT_REGISTRATION'),
			'RELEASE_TYPE' 				=> $this->input->post('RELEASE_TYPE'),
			'ID_DELIVERY_POINT' 		=> $this->input->post('ID_DELIVERY_POINT'),
			'DESTINATION_ADDRESS' 		=> $this->input->post('DESTINATION_ADDRESS'),
			'CUSTOMER_PO' 				=> $this->input->post('CUSTOMER_PO'),
			'CUSTOMER_RO' 				=> $this->input->post('CUSTOMER_RO'),
			'REMARKS_OF_CUSTOMER' 		=> $this->input->post('REMARKS_OF_CUSTOMER'),
			'EXISTING_EMAIL' 			=> $this->input->post('EXISTING_EMAIL'),
			'EMAIL1' 					=> $this->input->post('EMAIL1'),
			'EMAIL2' 					=> $this->input->post('EMAIL2'),
			'EMAIL3' 					=> $this->input->post('EMAIL3'),
			'REQUEST_ORDER_DATE' 		=> date('Y-m-d H:i:s'),
			'STATUS_ORDER' 				=> 1,
		);
		return $this->db->insert('tr_pooling_order', $insert);
	}
} 