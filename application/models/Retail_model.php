<?php

Class Retail_model extends CI_Model {

    // ======================================================================================================================================================================================
    //                                                                                  SUM AND COUNT
    // ======================================================================================================================================================================================
    private $order_sort = array('MAINTENANCE_ORDER' => 'asc');
    private $column_order = array('MAINTENANCE_ORDER', 
                            'MAINTENANCE_ORDER',
                            'SALES_ORDER',
                            'MAINTENANCE_ORDER',
                            'PURCHASE_ORDER',
                            'PART_NUMBER',
                            'PART_NAME',
                            'SERIAL_NUMBER',
                            'RECEIVED_DATE',
                            'QUOTATION_DATE',
                            'APPROVAL_DATE',
                            'TAT',
                            'TAT_APPROVAL',
                            'TXT_STAT',
                            'ERDAT_VBFA',
                            'REMARKS',
                            'BILLING_STATUS',
                            );
    private $sql_finance = "
        WITH DETAIL AS (
            SELECT 
                A.FKDAT,
                convert(varchar, getdate(), 112) AS TANGGAL,
                DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) AS TAT_AGING,
                CASE
                WHEN DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) <= 30 THEN
                    'CAT 1'
                WHEN DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) > 31 AND DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) <= 60 THEN
                    'CAT 2'
                WHEN DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) > 61 AND DATEDIFF(day,A.FKDAT,convert(varchar, getdate(), 112)) <= 90 THEN
                    'CAT 3'
                ELSE
                    'CAT 4'
                END AS TAT_CATEGORY,
                B.VBELN_BILL,
                B.VBELN,
                A.NETWR,
                A.WAERK,
                CASE
                WHEN ISDATE(A.AUGDT) = 1 THEN
                    'PAID'
                ELSE
                    'UNPAID'
                END AS STATUS_BILLING,
                A.KUNRG,
                B.AUART,
                B.BSTNK,
                A.INSERT_DATE,
                ROW_NUMBER() OVER(PARTITION BY B.VBELN_BILL ORDER BY A.INSERT_DATE DESC) ROW_INDEX
            FROM M_SALESBILLING A
            INNER JOIN M_SALESORDER B ON A.VBELN = B.VBELN_BILL
            WHERE B.PRCTR LIKE 'GMFTC%'
        ) 
        ";

    public function finance_tat_aging($cur_customer, $cur_code, $cur_type) {
        if ($cur_type == "sum_data") {
            $where_customer = ($cur_customer != 'all') ? "AND KUNRG LIKE '%$cur_customer%'" : "";
            $sql = "SELECT TAT_CATEGORY, COUNT(*) AS TOTAL FROM DETAIL WHERE ROW_INDEX='1' $where_customer AND STATUS_BILLING='UNPAID' GROUP BY TAT_CATEGORY";
        } else {
            $where_customer = ($cur_customer != 'all') ? "AND KUNRG = '$cur_customer'" : "";
            
            $is_idr = $cur_code == "IDR" ? "* 100" : "";
            
            $sql = "
            , A AS (
            SELECT
                TAT_CATEGORY,
                CASE
                    WHEN ISNUMERIC( NETWR ) = 1 THEN CAST ( REPLACE ( NETWR, ',', '' ) AS FLOAT ) $is_idr
                    ELSE NETWR $is_idr
                END AS VAL
            FROM 
                DETAIL 
            WHERE 
                WAERK = '$cur_code' AND ROW_INDEX='1' $where_customer 
                AND STATUS_BILLING='UNPAID'
            )
            SELECT
                TAT_CATEGORY,
                SUM( VAL ) AS TOTAL
            FROM
                A
            GROUP BY 
                TAT_CATEGORY
            ORDER BY
                TAT_CATEGORY ASC
            ";
        }
        $data = $this->db->query($this->sql_finance.$sql)->result();

        $result = [0,0,0,0];
        foreach ($data as $val) {
            $index = intval(substr($val->TAT_CATEGORY, -1)) - 1;
			$result[$index] = $val->TOTAL;
        }

        return $result;
    }

    public function finance_tat_aging_detail($cur_customer, $code_range, $cur_code) {
        $where_category = "AND TAT_CATEGORY = 'CAT $code_range'";
        $where_customer = ($cur_customer != 'all') ? "AND KUNRG = '$cur_customer'" : "";
        $where_currency = ($cur_code != 'all') ? "AND WAERK = '$cur_code'" : "";

        $case = "";
        if ($cur_code != 'all') {
            $is_idr = $cur_code == "IDR" ? "* 100" : "";

            $case = "
            CASE
                WHEN ISNUMERIC( NETWR ) = 1 THEN CAST ( REPLACE ( NETWR, ',', '' ) AS FLOAT ) $is_idr
                ELSE NETWR $is_idr
            END AS NETWR";
        } else {
            $case = "
            CASE
                WHEN ( ISNUMERIC( NETWR ) = 1 AND WAERK = 'IDR' ) THEN CAST ( REPLACE ( NETWR, ',', '' ) AS FLOAT ) * 100
                WHEN ( ISNUMERIC( NETWR ) = 1 AND WAERK != 'IDR' ) THEN CAST ( REPLACE ( NETWR, ',', '' ) AS FLOAT )
                WHEN ( ISNUMERIC( NETWR ) != 1 AND WAERK = 'IDR' ) THEN NETWR * 100
                ELSE NETWR
            END AS NETWR";
        }

        $sql = "
        SELECT
            COMPANY_NAME,
            VBELN_BILL AS VBELN,
            WAERK,
            $case,
            STATUS_BILLING
        FROM 
            DETAIL A
            LEFT JOIN V_Customer B ON A.KUNRG = B.ID_CUSTOMER
        WHERE 
        ROW_INDEX='1' $where_category $where_currency $where_customer AND STATUS_BILLING='UNPAID' order by VBELN_BILL";

        return $this->db->query($this->sql_finance.$sql)->result();
    }
        
    function get_date_retail(){
        $sql = "SELECT DISTINCT SUBSTRING ( GETRI, 1, 4 ) as 'tahun'
                FROM M_PMORDERH
                WHERE SUBSTRING ( GETRI, 1, 4 ) != 0000";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function finance_aging_all(){
        $sql = "
        SELECT COUNT(*) AS TOTAL,TAT_CATEGORY FROM DETAIL GROUP BY TAT_CATEGORY
        ";

        $query = $this->db->query($this->sql_finance.$sql);
        return $query->result_array();
    }

    function finance_outstanding($type,$customer){
        $where_customer = "";
        if($customer != "all"){
            $customer = trim($customer);
            $where_customer = "AND KUNRG = '$customer'";
        }

        $sql = "
        WITH DETAIL AS (
            SELECT
                A.FKDAT,
                CONVERT ( VARCHAR, getdate(), 112 ) AS TANGGAL,
                DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) AS TAT_AGING,
                CASE
                    WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) <= 30 THEN 'CAT 1' 
                    WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) > 31 AND DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) <= 60 THEN 'CAT 2' 
                    WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) > 61 AND DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate(), 112 )) <= 90 THEN 'CAT 3' 
                    ELSE 'CAT 4' 
                END AS TAT_CATEGORY,
                B.VBELN_BILL,
                B.VBELN,
                A.NETWR,
                A.WAERK,
                CASE
                    WHEN ISDATE( A.AUGDT ) = 1 THEN 'PAID' 
                    ELSE 'UNPAID' 
                END AS STATUS_BILLING,
                A.KUNRG,
                B.AUART,
                B.BSTNK,
                A.INSERT_DATE,
                ROW_NUMBER () OVER ( PARTITION BY B.VBELN_BILL ORDER BY A.INSERT_DATE DESC ) ROW_INDEX 
            FROM
                M_SALESBILLING A
            INNER JOIN M_SALESORDER B ON A.VBELN = B.VBELN_BILL 
            WHERE
                LEFT ( B.PRCTR, 5 ) = 'GMFTC' 
                $where_customer 
        ),
        FILTER AS ( 
            SELECT * FROM DETAIL WHERE ROW_INDEX = '1' 
        ),
        DETAIL_FIX AS (
            SELECT
                FILTER.*,
                CASE
                    WHEN ( M_PMORDERH.MATNR != '' ) THEN M_PMORDERH.MATNR 
                    WHEN ( M_PMORDERH.MATNR = '' AND M_PMORDER.MATNR != '' ) THEN M_PMORDER.MATNR 
                END AS PART_NUMBER,
                CASE
                    WHEN ( M_PMORDERH.MAKTX != '' ) THEN M_PMORDERH.MAKTX 
                    WHEN ( M_PMORDERH.MAKTX = '' AND M_PMORDER.MAKTX != '' ) THEN M_PMORDER.MAKTX 
                END AS PART_NAME,
                CASE 
                    WHEN ( M_PMORDERH.SERNR != '' ) THEN M_PMORDERH.SERNR 
                    WHEN ( M_PMORDERH.SERIALNR != '' ) THEN M_PMORDERH.SERIALNR 
                END AS SERIAL_NUMBER,
                M_PMORDERH.AUFNR 
            FROM
                FILTER
            LEFT JOIN (
                    SELECT
                        KDAUF,
                        AUFNR,
                        MATNR,
                        SERNR,
                        MAKTX,
                        SERIALNR,
                        ROW_NUMBER () OVER ( PARTITION BY KDAUF ORDER BY AUFNR ) AS RowNum 
                    FROM
                        M_PMORDERH 
                    WHERE
                        auart = 'GA03' 
            ) M_PMORDERH ON FILTER.VBELN = M_PMORDERH.KDAUF AND RowNum = 1
            LEFT JOIN (
                    SELECT
                        AUFNR,
                        MATNR,
                        MAKTX,
                        ROW_NUMBER () OVER ( PARTITION BY AUFNR ORDER BY RSPOS ) AS RowNum 
                    FROM
                        M_PMORDER 
                    WHERE
                        AUART = 'GA03' 
                        AND MATNR IS NOT NULL 
                        AND MATNR != '' 
            ) M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR AND M_PMORDER.RowNum = 1 
        )
        SELECT
            * 
        FROM
            DETAIL_FIX
        JOIN V_Customer ON DETAIL_FIX.KUNRG = V_Customer.ID_CUSTOMER 
        WHERE
            STATUS_BILLING = 'UNPAID' AND 
            TAT_CATEGORY = 'CAT $type'
        ORDER BY FKDAT,VBELN_BILL
        ";

        /*$sql = "
        , FILTER AS (
            SELECT * FROM DETAIL WHERE ROW_INDEX='1'
        ),
        DETAIL_FIX AS (
            SELECT
                FILTER.*,
                CASE 
                    WHEN ( M_PMORDERH.MATNR != '' ) THEN M_PMORDERH.MATNR 
                    WHEN ( M_PMORDERH.MATNR = '' AND M_PMORDER.MATNR != '' ) THEN M_PMORDER.MATNR 
                END AS PART_NUMBER,
                CASE 
                    WHEN ( M_PMORDERH.SERNR != '' ) THEN M_PMORDERH.SERNR 
                    WHEN ( M_PMORDERH.SERIALNR != '' ) THEN M_PMORDERH.SERIALNR 
                END AS SERIAL_NUMBER,
                M_PMORDERH.AUFNR,
                ROW_NUMBER() OVER(PARTITION BY FILTER.VBELN_BILL ORDER BY FILTER.FKDAT DESC) ROW_INDEX_FIX
            FROM FILTER
            LEFT JOIN M_PMORDERH ON FILTER.VBELN = M_PMORDERH.KDAUF
            LEFT JOIN M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR AND (M_PMORDER.MATNR IS NOT NULL AND M_PMORDER.MATNR !='' AND M_PMORDER.RSPOS='0001' AND M_PMORDER.AUART='GA03') 
        )
        SELECT * 
        FROM DETAIL_FIX 
        JOIN V_Customer ON DETAIL_FIX.KUNRG = V_Customer.ID_CUSTOMER
        WHERE 
            ROW_INDEX_FIX='1' AND
            STATUS_BILLING = 'UNPAID' AND 
            TAT_CATEGORY = 'CAT $type' 
            $where_customer 
        ORDER BY FKDAT DESC
        ";*/

        // $sql = "
        // SELECT * 
        // FROM DETAIL 
        // JOIN V_Customer ON DETAIL.KUNRG = V_Customer.ID_CUSTOMER
        // WHERE 
        //     STATUS_BILLING = 'UNPAID' AND 
        //     TAT_CATEGORY = 'CAT $type' AND 
        //     ROW_INDEX='1' 
        //     $where_customer 
        // ORDER BY FKDAT DESC
        // ";
        // print_r($sql);die();
        // print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function finance_historical($customer,$start,$end){
        $where_customer = "";
        if($customer != "all"){
            $customer = trim($customer);
            $where_customer = "AND KUNRG = '$customer'";
        }

        $sql = " 
        WITH DETAIL AS (
        SELECT
            A.FKDAT,
            CONVERT ( VARCHAR, getdate ( ), 112 ) AS TANGGAL,
            DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) AS TAT_AGING,
        CASE
            
            WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) <= 30 THEN 'CAT 1' WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) > 31 
            AND DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) <= 60 THEN 'CAT 2' WHEN DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) > 61 
                AND DATEDIFF( DAY, A.FKDAT, CONVERT ( VARCHAR, getdate ( ), 112 ) ) <= 90 THEN
                    'CAT 3' ELSE 'CAT 4' 
                END AS TAT_CATEGORY,
                B.VBELN_BILL,
                B.VBELN,
                A.NETWR,
                A.WAERK,
            CASE
                    
                    WHEN ISDATE ( A.AUGDT ) = 1 THEN
                    'PAID' ELSE 'UNPAID' 
                END AS STATUS_BILLING,
                A.KUNRG,
                B.AUART,
                B.BSTNK,
                A.INSERT_DATE,
                ROW_NUMBER ( ) OVER ( PARTITION BY B.VBELN_BILL ORDER BY A.INSERT_DATE DESC ) ROW_INDEX 
            FROM
                M_SALESBILLING A
                INNER JOIN M_SALESORDER B ON A.VBELN = B.VBELN_BILL 
            WHERE
                LEFT ( B.PRCTR, 5 ) = 'GMFTC' 
                $where_customer 
            ),
            AA AS (
            SELECT
                *,
                CONVERT (
                    DATE,
                    (
                        SUBSTRING ( FKDAT, 7, 2 ) + '-' + SUBSTRING ( FKDAT, 5, 2 ) + '-' + SUBSTRING ( FKDAT, 1, 4 ) 
                    ),
                    104 
                ) AS DOC_DATE 
            FROM
                DETAIL 
            ),
            DETAIL_FIX2 AS (
            SELECT
                AA.*,
            CASE
                    
                    WHEN ( M_PMORDERH.MATNR != '' ) THEN
                    M_PMORDERH.MATNR 
                    WHEN ( M_PMORDERH.MATNR = '' AND M_PMORDER.MATNR != '' ) THEN
                    M_PMORDER.MATNR 
                END AS PART_NUMBER,
            CASE            
                WHEN ( M_PMORDERH.MAKTX != '' ) THEN M_PMORDERH.MAKTX 
                WHEN ( M_PMORDERH.MAKTX = '' AND M_PMORDER.MAKTX != '' ) THEN M_PMORDER.MAKTX 
            END AS PART_NAME,
            CASE
                    
                    WHEN ( M_PMORDERH.SERNR != '' ) THEN
                    M_PMORDERH.SERNR 
                    WHEN ( M_PMORDERH.SERIALNR != '' ) THEN
                    M_PMORDERH.SERIALNR 
                END AS SERIAL_NUMBER,
                M_PMORDERH.AUFNR,
                ROW_NUMBER ( ) OVER ( PARTITION BY AA.VBELN_BILL ORDER BY AA.FKDAT DESC ) ROW_INDEX_FIX 
            FROM
                AA
                LEFT JOIN (
                SELECT
                    KDAUF,
                    AUFNR,
                    MATNR,
                    SERNR,
                    MAKTX,
                    SERIALNR,
                    ROW_NUMBER ( ) OVER ( PARTITION BY KDAUF ORDER BY AUFNR ) AS RowNum 
                FROM
                    M_PMORDERH 
                WHERE
                    auart = 'GA03' 
                ) M_PMORDERH ON AA.VBELN = M_PMORDERH.KDAUF 
                AND RowNum = 1
                LEFT JOIN (
                SELECT
                    AUFNR,
                    MATNR,
                     MAKTX,
                    ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY RSPOS ) AS RowNum 
                FROM
                    M_PMORDER 
                WHERE
                    AUART = 'GA03' 
                    AND MATNR IS NOT NULL 
                    AND MATNR != '' 
                ) M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR 
                AND M_PMORDER.RowNum = 1 
            ) SELECT
            * 
        FROM
            DETAIL_FIX2
            JOIN V_Customer ON DETAIL_FIX2.KUNRG = V_Customer.ID_CUSTOMER 
        WHERE
            STATUS_BILLING = 'PAID' 
            AND (DOC_DATE BETWEEN '{$start}' AND '{$end}') 
            AND ROW_INDEX_FIX = '1' 
        ORDER BY
        FKDAT,VBELN_BILL
        ";

        /*$sql = "
        , AA AS(
        SELECT
            *,
            CONVERT (
                DATE,
                (
                SUBSTRING ( FKDAT, 7, 2 ) + '-' + SUBSTRING ( FKDAT, 5, 2 ) + '-' + SUBSTRING ( FKDAT, 1, 4 )),
                104 
            ) AS DOC_DATE
        FROM DETAIL
        ),
        DETAIL_FIX2 AS (
            SELECT
                AA.*,
                CASE 
                    WHEN ( M_PMORDERH.MATNR != '' ) THEN M_PMORDERH.MATNR 
                    WHEN ( M_PMORDERH.MATNR = '' AND M_PMORDER.MATNR != '' ) THEN M_PMORDER.MATNR 
                END AS PART_NUMBER,
                CASE 
                    WHEN ( M_PMORDERH.SERNR != '' ) THEN M_PMORDERH.SERNR 
                    WHEN ( M_PMORDERH.SERIALNR != '' ) THEN M_PMORDERH.SERIALNR 
                END AS SERIAL_NUMBER,
                M_PMORDERH.AUFNR,
                ROW_NUMBER() OVER(PARTITION BY AA.VBELN_BILL ORDER BY AA.FKDAT DESC) ROW_INDEX_FIX
            FROM AA
            LEFT JOIN M_PMORDERH ON AA.VBELN = M_PMORDERH.KDAUF
            LEFT JOIN M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR AND (M_PMORDER.MATNR IS NOT NULL AND M_PMORDER.MATNR !='' AND M_PMORDER.RSPOS='0001' AND M_PMORDER.AUART='GA03')
        )
        SELECT * 
        FROM DETAIL_FIX2 
        JOIN V_Customer ON DETAIL_FIX2.KUNRG = V_Customer.ID_CUSTOMER
        WHERE 
            STATUS_BILLING = 'PAID' AND 
            (DOC_DATE BETWEEN '{$start}' AND '{$end}') AND
            ROW_INDEX_FIX='1' 
            $where_customer 
        ORDER BY FKDAT DESC
        ";*/

        // print_r($this->sql_finance.$sql);die();
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function finance_customer(){
        $sql = "
        ,CUSTOMER_FINANCE AS(
        SELECT DISTINCT(KUNRG) AS KUNRG FROM DETAIL
        )
        SELECT KUNRG AS ID_CUSTOMER,COMPANY_NAME 
        FROM CUSTOMER_FINANCE
        LEFT JOIN V_Customer ON CUSTOMER_FINANCE.KUNRG = V_Customer.ID_CUSTOMER
        ";
        $query = $this->db->query($this->sql_finance.$sql);
        return $query->result();
    }

    function finance_cur($customer){
        $where = "";
        if($customer != "all"){
            $where = "WHERE KUNRG LIKE '%$customer%'";
        }
        $sql = "
        SELECT DISTINCT WAERK
        FROM DETAIL
        $where
        ";
        $query = $this->db->query($this->sql_finance.$sql);
        return $query->result_array();
    }

    function search_ro($key,$customer){
        $where_customer = "";
        if($customer != "all"){
            $where_customer = "AND A.KUNRG = '$customer'";
        }

        $sql = "
        WITH DETAIL AS (
            SELECT
                A.FKDAT,
                B.VBELN_BILL,
                B.VBELN,
                CASE
                    WHEN ISDATE( A.AUGDT ) = 1 THEN 'PAID' 
                    ELSE 'UNPAID' 
                END AS STATUS_BILLING,
                A.KUNRG,
                B.BSTNK,
                B.ERDAT_VBFA,
                ROW_NUMBER () OVER ( PARTITION BY B.VBELN_BILL ORDER BY A.INSERT_DATE DESC ) ROW_INDEX 
            FROM
                M_SALESBILLING A
            INNER JOIN M_SALESORDER B ON A.VBELN = B.VBELN_BILL 
            WHERE
                LEFT ( B.PRCTR, 5 ) = 'GMFTC' AND
                B.BSTNK = '$key' 
                $where_customer
        ),
        FILTER AS ( 
            SELECT * FROM DETAIL WHERE ROW_INDEX = '1' 
        ),
        DETAIL_FIX AS (
            SELECT
                FILTER.*,
                CASE
                    WHEN ( M_PMORDERH.MATNR != '' ) THEN M_PMORDERH.MATNR 
                    WHEN ( M_PMORDERH.MATNR = '' AND M_PMORDER.MATNR != '' ) THEN M_PMORDER.MATNR 
                END AS PART_NUMBER,
                CASE 
                    WHEN ( M_PMORDERH.SERNR != '' ) THEN M_PMORDERH.SERNR 
                    WHEN ( M_PMORDERH.SERIALNR != '' ) THEN M_PMORDERH.SERIALNR 
                END AS SERIAL_NUMBER,
                M_PMORDER.GLTRP,
                CASE    
                    WHEN B.TXT50 NOT LIKE '' AND B.TXT50 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT50 LIKE 'WR' THEN CASE WHEN B.TXT49 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT50 END 
                    WHEN B.TXT49 NOT LIKE '' AND B.TXT49 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT49 LIKE 'WR' THEN CASE WHEN B.TXT48 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT49 END 
                    WHEN B.TXT48 NOT LIKE '' AND B.TXT48 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT48 LIKE 'WR' THEN CASE WHEN B.TXT47 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT48 END 
                    WHEN B.TXT47 NOT LIKE '' AND B.TXT47 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT47 LIKE 'WR' THEN CASE WHEN B.TXT46 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT47 END 
                    WHEN B.TXT46 NOT LIKE '' AND B.TXT46 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT46 LIKE 'WR' THEN CASE WHEN B.TXT45 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT46 END 
                    WHEN B.TXT45 NOT LIKE '' AND B.TXT45 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT45 LIKE 'WR' THEN CASE WHEN B.TXT44 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT45 END 
                    WHEN B.TXT44 NOT LIKE '' AND B.TXT44 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT44 LIKE 'WR' THEN CASE WHEN B.TXT43 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT44 END 
                    WHEN B.TXT43 NOT LIKE '' AND B.TXT43 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT43 LIKE 'WR' THEN CASE WHEN B.TXT42 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT43 END 
                    WHEN B.TXT42 NOT LIKE '' AND B.TXT42 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT42 LIKE 'WR' THEN CASE WHEN B.TXT41 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT42 END 
                    WHEN B.TXT41 NOT LIKE '' AND B.TXT41 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT41 LIKE 'WR' THEN CASE WHEN B.TXT40 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT41 END 
                    WHEN B.TXT40 NOT LIKE '' AND B.TXT40 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT40 LIKE 'WR' THEN CASE WHEN B.TXT39 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT40 END 
                    WHEN B.TXT39 NOT LIKE '' AND B.TXT39 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT39 LIKE 'WR' THEN CASE WHEN B.TXT38 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT39 END 
                    WHEN B.TXT38 NOT LIKE '' AND B.TXT38 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT38 LIKE 'WR' THEN CASE WHEN B.TXT37 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT38 END 
                    WHEN B.TXT37 NOT LIKE '' AND B.TXT37 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT37 LIKE 'WR' THEN CASE WHEN B.TXT36 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT37 END 
                    WHEN B.TXT36 NOT LIKE '' AND B.TXT36 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT36 LIKE 'WR' THEN CASE WHEN B.TXT35 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT36 END 
                    WHEN B.TXT35 NOT LIKE '' AND B.TXT35 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT35 LIKE 'WR' THEN CASE WHEN B.TXT34 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT35 END 
                    WHEN B.TXT34 NOT LIKE '' AND B.TXT34 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT34 LIKE 'WR' THEN CASE WHEN B.TXT33 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT34 END 
                    WHEN B.TXT33 NOT LIKE '' AND B.TXT33 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT33 LIKE 'WR' THEN CASE WHEN B.TXT32 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT33 END 
                    WHEN B.TXT32 NOT LIKE '' AND B.TXT32 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT32 LIKE 'WR' THEN CASE WHEN B.TXT31 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT32 END 
                    WHEN B.TXT31 NOT LIKE '' AND B.TXT31 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT31 LIKE 'WR' THEN CASE WHEN B.TXT30 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT31 END 
                    WHEN B.TXT30 NOT LIKE '' AND B.TXT30 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT30 LIKE 'WR' THEN CASE WHEN B.TXT29 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT30 END 
                    WHEN B.TXT29 NOT LIKE '' AND B.TXT29 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT29 LIKE 'WR' THEN CASE WHEN B.TXT28 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT29 END 
                    WHEN B.TXT28 NOT LIKE '' AND B.TXT28 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT28 LIKE 'WR' THEN CASE WHEN B.TXT27 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT28 END 
                    WHEN B.TXT27 NOT LIKE '' AND B.TXT27 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT27 LIKE 'WR' THEN CASE WHEN B.TXT26 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT27 END 
                    WHEN B.TXT26 NOT LIKE '' AND B.TXT26 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT26 LIKE 'WR' THEN CASE WHEN B.TXT25 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT26 END 
                    WHEN B.TXT25 NOT LIKE '' AND B.TXT25 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT25 LIKE 'WR' THEN CASE WHEN B.TXT24 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT25 END 
                    WHEN B.TXT24 NOT LIKE '' AND B.TXT24 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT24 LIKE 'WR' THEN CASE WHEN B.TXT23 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT24 END 
                    WHEN B.TXT23 NOT LIKE '' AND B.TXT23 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT23 LIKE 'WR' THEN CASE WHEN B.TXT22 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT23 END 
                    WHEN B.TXT22 NOT LIKE '' AND B.TXT22 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT22 LIKE 'WR' THEN CASE WHEN B.TXT21 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT22 END 
                    WHEN B.TXT21 NOT LIKE '' AND B.TXT21 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT21 LIKE 'WR' THEN CASE WHEN B.TXT20 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT21 END 
                    WHEN B.TXT20 NOT LIKE '' AND B.TXT20 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT20 LIKE 'WR' THEN CASE WHEN B.TXT19 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT20 END 
                    WHEN B.TXT19 NOT LIKE '' AND B.TXT19 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT19 LIKE 'WR' THEN CASE WHEN B.TXT18 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT19 END 
                    WHEN B.TXT18 NOT LIKE '' AND B.TXT18 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT18 LIKE 'WR' THEN CASE WHEN B.TXT17 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT18 END 
                    WHEN B.TXT17 NOT LIKE '' AND B.TXT17 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT17 LIKE 'WR' THEN CASE WHEN B.TXT16 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT17 END 
                    WHEN B.TXT16 NOT LIKE '' AND B.TXT16 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT16 LIKE 'WR' THEN CASE WHEN B.TXT15 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT16 END 
                    WHEN B.TXT15 NOT LIKE '' AND B.TXT15 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT15 LIKE 'WR' THEN CASE WHEN B.TXT14 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT15 END 
                    WHEN B.TXT14 NOT LIKE '' AND B.TXT14 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT14 LIKE 'WR' THEN CASE WHEN B.TXT13 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT14 END 
                    WHEN B.TXT13 NOT LIKE '' AND B.TXT13 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT13 LIKE 'WR' THEN CASE WHEN B.TXT12 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT13 END 
                    WHEN B.TXT12 NOT LIKE '' AND B.TXT12 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT12 LIKE 'WR' THEN CASE WHEN B.TXT11 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT12 END 
                    WHEN B.TXT11 NOT LIKE '' AND B.TXT11 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT11 LIKE 'WR' THEN CASE WHEN B.TXT10 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT11 END 
                    WHEN B.TXT10 NOT LIKE '' AND B.TXT10 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT10 LIKE 'WR' THEN CASE WHEN B.TXT9 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT10 END 
                    WHEN B.TXT9 NOT LIKE '' AND B.TXT9 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT9 LIKE 'WR' THEN CASE WHEN B.TXT8 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT9 END 
                    WHEN B.TXT8 NOT LIKE '' AND B.TXT8 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT8 LIKE 'WR' THEN CASE WHEN B.TXT7 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT8 END 
                    WHEN B.TXT7 NOT LIKE '' AND B.TXT7 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT7 LIKE 'WR' THEN CASE WHEN B.TXT6 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT7 END 
                    WHEN B.TXT6 NOT LIKE '' AND B.TXT6 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT6 LIKE 'WR' THEN CASE WHEN B.TXT5 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT6 END 
                    WHEN B.TXT5 NOT LIKE '' AND B.TXT5 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT5 LIKE 'WR' THEN CASE WHEN B.TXT4 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT5 END 
                    WHEN B.TXT4 NOT LIKE '' AND B.TXT4 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT4 LIKE 'WR' THEN CASE WHEN B.TXT3 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT4 END 
                    WHEN B.TXT3 NOT LIKE '' AND B.TXT3 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT3 LIKE 'WR' THEN CASE WHEN B.TXT2 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT3 END 
                    WHEN B.TXT2 NOT LIKE '' AND B.TXT2 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT2 LIKE 'WR' THEN CASE WHEN B.TXT1 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT2 END 
                    WHEN B.TXT1 NOT LIKE '' AND B.TXT1 NOT LIKE 'HOLD' THEN B.TXT1
                END AS STAT
            FROM
                FILTER
            LEFT JOIN (
                    SELECT
                        KDAUF,
                        AUFNR,
                        MATNR,
                        SERNR,
                        SERIALNR,
                        ROW_NUMBER () OVER ( PARTITION BY KDAUF ORDER BY AUFNR ) AS RowNum 
                    FROM
                        M_PMORDERH 
                    WHERE
                        auart = 'GA03' 
            ) M_PMORDERH ON FILTER.VBELN = M_PMORDERH.KDAUF AND RowNum = 1
            LEFT JOIN (
                    SELECT
                        AUFNR,
                        MATNR,
                                                GLTRP,
                        ROW_NUMBER () OVER ( PARTITION BY AUFNR ORDER BY RSPOS ) AS RowNum 
                    FROM
                        M_PMORDER 
                    WHERE
                        AUART = 'GA03' 
                        AND MATNR IS NOT NULL 
                        AND MATNR != '' 
            ) M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR AND M_PMORDER.RowNum = 1 
            INNER JOIN M_STATUSPM B ON M_PMORDERH.AUFNR = B.AUFNR
        )
        SELECT
            *,
            CASE
                WHEN ISDATE(ERDAT_VBFA) = 1 THEN 'Delivered' 
                WHEN STAT LIKE 'FRA' THEN 'Ready To Deliver Return As is Conditiion' 
                WHEN STAT LIKE 'FSB' THEN 'Ready To Deliver Serviceable Condition' 
                WHEN STAT LIKE 'FSC' THEN 'Ready To Deliver BER' 
                WHEN STAT LIKE 'WRX' THEN 'Approval Received / Repair Started'
                WHEN STAT LIKE 'WR' THEN 'Under Repair'
                WHEN STAT LIKE 'UR' THEN 'Under Repair'
                WHEN STAT LIKE 'SRMC' THEN 'Waiting Supply Materials From Customer'  
                WHEN STAT LIKE 'SROA' THEN 'Quotation Provided / Awaiting Approval' 
                ELSE 'Unit Under Inspection'
            END AS TXT_STAT,
            ROW_NUMBER () OVER ( PARTITION BY VBELN_BILL ORDER BY VBELN_BILL ) AS RowNumm
        FROM
            DETAIL_FIX
        JOIN V_Customer ON DETAIL_FIX.KUNRG = V_Customer.ID_CUSTOMER 
        ORDER BY RowNumm DESC
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getAllCustomer($cus_id){
        if($cus_id){
            $query = $this->db->get_where('CUSTOMER', array('ID_CUSTOMER'=>$cus_id));
        }else{
            $sql = "
            WITH A AS (
                SELECT
                    B.AUFNR,
                    CASE    
                        WHEN B.TXT50 NOT LIKE '' AND B.TXT50 NOT LIKE 'HOLD' THEN B.TXT50
                        WHEN B.TXT49 NOT LIKE '' AND B.TXT49 NOT LIKE 'HOLD' THEN B.TXT49
                        WHEN B.TXT48 NOT LIKE '' AND B.TXT48 NOT LIKE 'HOLD' THEN B.TXT48
                        WHEN B.TXT47 NOT LIKE '' AND B.TXT47 NOT LIKE 'HOLD' THEN B.TXT47
                        WHEN B.TXT46 NOT LIKE '' AND B.TXT46 NOT LIKE 'HOLD' THEN B.TXT46
                        WHEN B.TXT45 NOT LIKE '' AND B.TXT45 NOT LIKE 'HOLD' THEN B.TXT45
                        WHEN B.TXT44 NOT LIKE '' AND B.TXT44 NOT LIKE 'HOLD' THEN B.TXT44
                        WHEN B.TXT43 NOT LIKE '' AND B.TXT43 NOT LIKE 'HOLD' THEN B.TXT43
                        WHEN B.TXT42 NOT LIKE '' AND B.TXT42 NOT LIKE 'HOLD' THEN B.TXT42
                        WHEN B.TXT41 NOT LIKE '' AND B.TXT41 NOT LIKE 'HOLD' THEN B.TXT41
                        WHEN B.TXT40 NOT LIKE '' AND B.TXT40 NOT LIKE 'HOLD' THEN B.TXT40
                        WHEN B.TXT39 NOT LIKE '' AND B.TXT39 NOT LIKE 'HOLD' THEN B.TXT39
                        WHEN B.TXT38 NOT LIKE '' AND B.TXT38 NOT LIKE 'HOLD' THEN B.TXT38
                        WHEN B.TXT37 NOT LIKE '' AND B.TXT37 NOT LIKE 'HOLD' THEN B.TXT37
                        WHEN B.TXT36 NOT LIKE '' AND B.TXT36 NOT LIKE 'HOLD' THEN B.TXT36
                        WHEN B.TXT35 NOT LIKE '' AND B.TXT35 NOT LIKE 'HOLD' THEN B.TXT35
                        WHEN B.TXT34 NOT LIKE '' AND B.TXT34 NOT LIKE 'HOLD' THEN B.TXT34
                        WHEN B.TXT33 NOT LIKE '' AND B.TXT33 NOT LIKE 'HOLD' THEN B.TXT33
                        WHEN B.TXT32 NOT LIKE '' AND B.TXT32 NOT LIKE 'HOLD' THEN B.TXT32
                        WHEN B.TXT31 NOT LIKE '' AND B.TXT31 NOT LIKE 'HOLD' THEN B.TXT31
                        WHEN B.TXT30 NOT LIKE '' AND B.TXT30 NOT LIKE 'HOLD' THEN B.TXT30
                        WHEN B.TXT29 NOT LIKE '' AND B.TXT29 NOT LIKE 'HOLD' THEN B.TXT29
                        WHEN B.TXT28 NOT LIKE '' AND B.TXT28 NOT LIKE 'HOLD' THEN B.TXT28
                        WHEN B.TXT27 NOT LIKE '' AND B.TXT27 NOT LIKE 'HOLD' THEN B.TXT27
                        WHEN B.TXT26 NOT LIKE '' AND B.TXT26 NOT LIKE 'HOLD' THEN B.TXT26
                        WHEN B.TXT25 NOT LIKE '' AND B.TXT25 NOT LIKE 'HOLD' THEN B.TXT25
                        WHEN B.TXT24 NOT LIKE '' AND B.TXT24 NOT LIKE 'HOLD' THEN B.TXT24
                        WHEN B.TXT23 NOT LIKE '' AND B.TXT23 NOT LIKE 'HOLD' THEN B.TXT23
                        WHEN B.TXT22 NOT LIKE '' AND B.TXT22 NOT LIKE 'HOLD' THEN B.TXT22
                        WHEN B.TXT21 NOT LIKE '' AND B.TXT21 NOT LIKE 'HOLD' THEN B.TXT21
                        WHEN B.TXT20 NOT LIKE '' AND B.TXT20 NOT LIKE 'HOLD' THEN B.TXT20
                        WHEN B.TXT19 NOT LIKE '' AND B.TXT19 NOT LIKE 'HOLD' THEN B.TXT19
                        WHEN B.TXT18 NOT LIKE '' AND B.TXT18 NOT LIKE 'HOLD' THEN B.TXT18
                        WHEN B.TXT17 NOT LIKE '' AND B.TXT17 NOT LIKE 'HOLD' THEN B.TXT17
                        WHEN B.TXT16 NOT LIKE '' AND B.TXT16 NOT LIKE 'HOLD' THEN B.TXT16
                        WHEN B.TXT15 NOT LIKE '' AND B.TXT15 NOT LIKE 'HOLD' THEN B.TXT15
                        WHEN B.TXT14 NOT LIKE '' AND B.TXT14 NOT LIKE 'HOLD' THEN B.TXT14
                        WHEN B.TXT13 NOT LIKE '' AND B.TXT13 NOT LIKE 'HOLD' THEN B.TXT13
                        WHEN B.TXT12 NOT LIKE '' AND B.TXT12 NOT LIKE 'HOLD' THEN B.TXT12
                        WHEN B.TXT11 NOT LIKE '' AND B.TXT11 NOT LIKE 'HOLD' THEN B.TXT11
                        WHEN B.TXT10 NOT LIKE '' AND B.TXT10 NOT LIKE 'HOLD' THEN B.TXT10
                        WHEN B.TXT9 NOT LIKE '' AND B.TXT9 NOT LIKE 'HOLD' THEN B.TXT9
                        WHEN B.TXT8 NOT LIKE '' AND B.TXT8 NOT LIKE 'HOLD' THEN B.TXT8
                        WHEN B.TXT7 NOT LIKE '' AND B.TXT7 NOT LIKE 'HOLD' THEN B.TXT7
                        WHEN B.TXT6 NOT LIKE '' AND B.TXT6 NOT LIKE 'HOLD' THEN B.TXT6
                        WHEN B.TXT5 NOT LIKE '' AND B.TXT5 NOT LIKE 'HOLD' THEN B.TXT5
                        WHEN B.TXT4 NOT LIKE '' AND B.TXT4 NOT LIKE 'HOLD' THEN B.TXT4
                        WHEN B.TXT3 NOT LIKE '' AND B.TXT3 NOT LIKE 'HOLD' THEN B.TXT3
                        WHEN B.TXT2 NOT LIKE '' AND B.TXT2 NOT LIKE 'HOLD' THEN B.TXT2
                        WHEN B.TXT1 NOT LIKE '' AND B.TXT1 NOT LIKE 'HOLD' THEN B.TXT1
                    END AS STAT,
                    CASE
                        WHEN B.TXT50 NOT LIKE '' AND B.TXT50 NOT LIKE 'HOLD' THEN 'TXT50'
                        WHEN B.TXT49 NOT LIKE '' AND B.TXT49 NOT LIKE 'HOLD' THEN 'TXT49'
                        WHEN B.TXT48 NOT LIKE '' AND B.TXT48 NOT LIKE 'HOLD' THEN 'TXT48'
                        WHEN B.TXT47 NOT LIKE '' AND B.TXT47 NOT LIKE 'HOLD' THEN 'TXT47'
                        WHEN B.TXT46 NOT LIKE '' AND B.TXT46 NOT LIKE 'HOLD' THEN 'TXT46'
                        WHEN B.TXT45 NOT LIKE '' AND B.TXT45 NOT LIKE 'HOLD' THEN 'TXT45'
                        WHEN B.TXT44 NOT LIKE '' AND B.TXT44 NOT LIKE 'HOLD' THEN 'TXT44'
                        WHEN B.TXT43 NOT LIKE '' AND B.TXT43 NOT LIKE 'HOLD' THEN 'TXT43'
                        WHEN B.TXT42 NOT LIKE '' AND B.TXT42 NOT LIKE 'HOLD' THEN 'TXT42'
                        WHEN B.TXT41 NOT LIKE '' AND B.TXT41 NOT LIKE 'HOLD' THEN 'TXT41'
                        WHEN B.TXT40 NOT LIKE '' AND B.TXT40 NOT LIKE 'HOLD' THEN 'TXT40'
                        WHEN B.TXT39 NOT LIKE '' AND B.TXT39 NOT LIKE 'HOLD' THEN 'TXT39'
                        WHEN B.TXT38 NOT LIKE '' AND B.TXT38 NOT LIKE 'HOLD' THEN 'TXT38'
                        WHEN B.TXT37 NOT LIKE '' AND B.TXT37 NOT LIKE 'HOLD' THEN 'TXT37'
                        WHEN B.TXT36 NOT LIKE '' AND B.TXT36 NOT LIKE 'HOLD' THEN 'TXT36'
                        WHEN B.TXT35 NOT LIKE '' AND B.TXT35 NOT LIKE 'HOLD' THEN 'TXT35'
                        WHEN B.TXT34 NOT LIKE '' AND B.TXT34 NOT LIKE 'HOLD' THEN 'TXT34'
                        WHEN B.TXT33 NOT LIKE '' AND B.TXT33 NOT LIKE 'HOLD' THEN 'TXT33'
                        WHEN B.TXT32 NOT LIKE '' AND B.TXT32 NOT LIKE 'HOLD' THEN 'TXT32'
                        WHEN B.TXT31 NOT LIKE '' AND B.TXT31 NOT LIKE 'HOLD' THEN 'TXT31'
                        WHEN B.TXT30 NOT LIKE '' AND B.TXT30 NOT LIKE 'HOLD' THEN 'TXT30'
                        WHEN B.TXT29 NOT LIKE '' AND B.TXT29 NOT LIKE 'HOLD' THEN 'TXT29'
                        WHEN B.TXT28 NOT LIKE '' AND B.TXT28 NOT LIKE 'HOLD' THEN 'TXT28'
                        WHEN B.TXT27 NOT LIKE '' AND B.TXT27 NOT LIKE 'HOLD' THEN 'TXT27'
                        WHEN B.TXT26 NOT LIKE '' AND B.TXT26 NOT LIKE 'HOLD' THEN 'TXT26'
                        WHEN B.TXT25 NOT LIKE '' AND B.TXT25 NOT LIKE 'HOLD' THEN 'TXT25'
                        WHEN B.TXT24 NOT LIKE '' AND B.TXT24 NOT LIKE 'HOLD' THEN 'TXT24'
                        WHEN B.TXT23 NOT LIKE '' AND B.TXT23 NOT LIKE 'HOLD' THEN 'TXT23'
                        WHEN B.TXT22 NOT LIKE '' AND B.TXT22 NOT LIKE 'HOLD' THEN 'TXT22'
                        WHEN B.TXT21 NOT LIKE '' AND B.TXT21 NOT LIKE 'HOLD' THEN 'TXT21'
                        WHEN B.TXT20 NOT LIKE '' AND B.TXT20 NOT LIKE 'HOLD' THEN 'TXT20'
                        WHEN B.TXT19 NOT LIKE '' AND B.TXT19 NOT LIKE 'HOLD' THEN 'TXT19'
                        WHEN B.TXT18 NOT LIKE '' AND B.TXT18 NOT LIKE 'HOLD' THEN 'TXT18'
                        WHEN B.TXT17 NOT LIKE '' AND B.TXT17 NOT LIKE 'HOLD' THEN 'TXT17'
                        WHEN B.TXT16 NOT LIKE '' AND B.TXT16 NOT LIKE 'HOLD' THEN 'TXT16'
                        WHEN B.TXT15 NOT LIKE '' AND B.TXT15 NOT LIKE 'HOLD' THEN 'TXT15'
                        WHEN B.TXT14 NOT LIKE '' AND B.TXT14 NOT LIKE 'HOLD' THEN 'TXT14'
                        WHEN B.TXT13 NOT LIKE '' AND B.TXT13 NOT LIKE 'HOLD' THEN 'TXT13'
                        WHEN B.TXT12 NOT LIKE '' AND B.TXT12 NOT LIKE 'HOLD' THEN 'TXT12'
                        WHEN B.TXT11 NOT LIKE '' AND B.TXT11 NOT LIKE 'HOLD' THEN 'TXT11'
                        WHEN B.TXT10 NOT LIKE '' AND B.TXT10 NOT LIKE 'HOLD' THEN 'TXT10'
                        WHEN B.TXT9 NOT LIKE '' AND B.TXT9 NOT LIKE 'HOLD' THEN 'TXT9'
                        WHEN B.TXT8 NOT LIKE '' AND B.TXT8 NOT LIKE 'HOLD' THEN 'TXT8'
                        WHEN B.TXT7 NOT LIKE '' AND B.TXT7 NOT LIKE 'HOLD' THEN 'TXT7'
                        WHEN B.TXT6 NOT LIKE '' AND B.TXT6 NOT LIKE 'HOLD' THEN 'TXT6'
                        WHEN B.TXT5 NOT LIKE '' AND B.TXT5 NOT LIKE 'HOLD' THEN 'TXT5'
                        WHEN B.TXT4 NOT LIKE '' AND B.TXT4 NOT LIKE 'HOLD' THEN 'TXT4'
                        WHEN B.TXT3 NOT LIKE '' AND B.TXT3 NOT LIKE 'HOLD' THEN 'TXT3'
                        WHEN B.TXT2 NOT LIKE '' AND B.TXT2 NOT LIKE 'HOLD' THEN 'TXT2'
                        WHEN B.TXT1 NOT LIKE '' AND B.TXT1 NOT LIKE 'HOLD' THEN 'TXT1'
                    END POSITION,
                    A.KDAUF,
                    C.VBELN,
                    C.ERDAT_VBFA,
                    C.KUNNR,
                    D.COMPANY_NAME,
                    ROW_NUMBER ( ) OVER ( PARTITION BY B.AUFNR ORDER BY C.ERDAT_VBFA DESC ) AS TOP_ONE
                FROM M_PMORDERH A
                INNER JOIN M_STATUSPM B ON A.AUFNR = B.AUFNR
                LEFT JOIN M_SALESORDER C ON A.KDAUF = C.VBELN AND C.PRCTR LIKE 'GMFTC%'
                LEFT JOIN CUSTOMER D ON C.KUNNR = D.ID_CUSTOMER
            )
                SELECT 
                    DISTINCT
                    KUNNR AS ID_CUSTOMER,
                    COMPANY_NAME
                FROM A WHERE COMPANY_NAME IS NOT NULL
            ";
            $query = $this->db->query($sql);
        }
        return $query->result_array();
    }

    function getStatusOrderPerTahun2($type,$cust,$bulan,$tahun){
        if($type == "close_date"){
            if($bulan != 'all'){
                $filter = "( (SUBSTRING (A.GETRI, 1, 4) = $tahun) AND (SUBSTRING (A.GETRI,5, 2) = $bulan) )";
            }
            else {
               $filter = "(SUBSTRING (A.GETRI, 1, 4) = $tahun)" ;
            }
        }

        if($type == "create_date"){
            if($bulan != 'all'){
                $filter = "( (SUBSTRING (A.ERDAT, 1, 4) = $tahun) AND (SUBSTRING (A.ERDAT,5, 2) = $bulan) )";
            }
            else {
                $filter = "(SUBSTRING (A.ERDAT, 1, 4) = $tahun)";
            }
        }

        if($cust != "all"){
            $where_cust = " AND C.KUNNR LIKE '$cust'";
        }else{
            $where_cust = "";
        }



        $sql = "
        WITH A AS (
            SELECT
                B.AUFNR,
                CASE    
                    WHEN B.TXT50 NOT LIKE '' AND B.TXT50 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT50 LIKE 'WR' THEN CASE WHEN B.TXT49 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT50 END 
                    WHEN B.TXT49 NOT LIKE '' AND B.TXT49 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT49 LIKE 'WR' THEN CASE WHEN B.TXT48 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT49 END 
                    WHEN B.TXT48 NOT LIKE '' AND B.TXT48 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT48 LIKE 'WR' THEN CASE WHEN B.TXT47 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT48 END 
                    WHEN B.TXT47 NOT LIKE '' AND B.TXT47 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT47 LIKE 'WR' THEN CASE WHEN B.TXT46 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT47 END 
                    WHEN B.TXT46 NOT LIKE '' AND B.TXT46 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT46 LIKE 'WR' THEN CASE WHEN B.TXT45 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT46 END 
                    WHEN B.TXT45 NOT LIKE '' AND B.TXT45 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT45 LIKE 'WR' THEN CASE WHEN B.TXT44 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT45 END 
                    WHEN B.TXT44 NOT LIKE '' AND B.TXT44 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT44 LIKE 'WR' THEN CASE WHEN B.TXT43 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT44 END 
                    WHEN B.TXT43 NOT LIKE '' AND B.TXT43 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT43 LIKE 'WR' THEN CASE WHEN B.TXT42 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT43 END 
                    WHEN B.TXT42 NOT LIKE '' AND B.TXT42 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT42 LIKE 'WR' THEN CASE WHEN B.TXT41 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT42 END 
                    WHEN B.TXT41 NOT LIKE '' AND B.TXT41 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT41 LIKE 'WR' THEN CASE WHEN B.TXT40 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT41 END 
                    WHEN B.TXT40 NOT LIKE '' AND B.TXT40 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT40 LIKE 'WR' THEN CASE WHEN B.TXT39 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT40 END 
                    WHEN B.TXT39 NOT LIKE '' AND B.TXT39 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT39 LIKE 'WR' THEN CASE WHEN B.TXT38 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT39 END 
                    WHEN B.TXT38 NOT LIKE '' AND B.TXT38 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT38 LIKE 'WR' THEN CASE WHEN B.TXT37 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT38 END 
                    WHEN B.TXT37 NOT LIKE '' AND B.TXT37 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT37 LIKE 'WR' THEN CASE WHEN B.TXT36 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT37 END 
                    WHEN B.TXT36 NOT LIKE '' AND B.TXT36 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT36 LIKE 'WR' THEN CASE WHEN B.TXT35 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT36 END 
                    WHEN B.TXT35 NOT LIKE '' AND B.TXT35 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT35 LIKE 'WR' THEN CASE WHEN B.TXT34 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT35 END 
                    WHEN B.TXT34 NOT LIKE '' AND B.TXT34 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT34 LIKE 'WR' THEN CASE WHEN B.TXT33 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT34 END 
                    WHEN B.TXT33 NOT LIKE '' AND B.TXT33 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT33 LIKE 'WR' THEN CASE WHEN B.TXT32 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT33 END 
                    WHEN B.TXT32 NOT LIKE '' AND B.TXT32 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT32 LIKE 'WR' THEN CASE WHEN B.TXT31 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT32 END 
                    WHEN B.TXT31 NOT LIKE '' AND B.TXT31 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT31 LIKE 'WR' THEN CASE WHEN B.TXT30 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT31 END 
                    WHEN B.TXT30 NOT LIKE '' AND B.TXT30 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT30 LIKE 'WR' THEN CASE WHEN B.TXT29 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT30 END 
                    WHEN B.TXT29 NOT LIKE '' AND B.TXT29 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT29 LIKE 'WR' THEN CASE WHEN B.TXT28 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT29 END 
                    WHEN B.TXT28 NOT LIKE '' AND B.TXT28 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT28 LIKE 'WR' THEN CASE WHEN B.TXT27 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT28 END 
                    WHEN B.TXT27 NOT LIKE '' AND B.TXT27 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT27 LIKE 'WR' THEN CASE WHEN B.TXT26 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT27 END 
                    WHEN B.TXT26 NOT LIKE '' AND B.TXT26 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT26 LIKE 'WR' THEN CASE WHEN B.TXT25 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT26 END 
                    WHEN B.TXT25 NOT LIKE '' AND B.TXT25 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT25 LIKE 'WR' THEN CASE WHEN B.TXT24 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT25 END 
                    WHEN B.TXT24 NOT LIKE '' AND B.TXT24 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT24 LIKE 'WR' THEN CASE WHEN B.TXT23 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT24 END 
                    WHEN B.TXT23 NOT LIKE '' AND B.TXT23 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT23 LIKE 'WR' THEN CASE WHEN B.TXT22 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT23 END 
                    WHEN B.TXT22 NOT LIKE '' AND B.TXT22 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT22 LIKE 'WR' THEN CASE WHEN B.TXT21 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT22 END 
                    WHEN B.TXT21 NOT LIKE '' AND B.TXT21 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT21 LIKE 'WR' THEN CASE WHEN B.TXT20 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT21 END 
                    WHEN B.TXT20 NOT LIKE '' AND B.TXT20 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT20 LIKE 'WR' THEN CASE WHEN B.TXT19 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT20 END 
                    WHEN B.TXT19 NOT LIKE '' AND B.TXT19 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT19 LIKE 'WR' THEN CASE WHEN B.TXT18 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT19 END 
                    WHEN B.TXT18 NOT LIKE '' AND B.TXT18 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT18 LIKE 'WR' THEN CASE WHEN B.TXT17 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT18 END 
                    WHEN B.TXT17 NOT LIKE '' AND B.TXT17 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT17 LIKE 'WR' THEN CASE WHEN B.TXT16 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT17 END 
                    WHEN B.TXT16 NOT LIKE '' AND B.TXT16 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT16 LIKE 'WR' THEN CASE WHEN B.TXT15 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT16 END 
                    WHEN B.TXT15 NOT LIKE '' AND B.TXT15 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT15 LIKE 'WR' THEN CASE WHEN B.TXT14 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT15 END 
                    WHEN B.TXT14 NOT LIKE '' AND B.TXT14 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT14 LIKE 'WR' THEN CASE WHEN B.TXT13 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT14 END 
                    WHEN B.TXT13 NOT LIKE '' AND B.TXT13 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT13 LIKE 'WR' THEN CASE WHEN B.TXT12 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT13 END 
                    WHEN B.TXT12 NOT LIKE '' AND B.TXT12 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT12 LIKE 'WR' THEN CASE WHEN B.TXT11 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT12 END 
                    WHEN B.TXT11 NOT LIKE '' AND B.TXT11 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT11 LIKE 'WR' THEN CASE WHEN B.TXT10 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT11 END 
                    WHEN B.TXT10 NOT LIKE '' AND B.TXT10 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT10 LIKE 'WR' THEN CASE WHEN B.TXT9 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT10 END 
                    WHEN B.TXT9 NOT LIKE '' AND B.TXT9 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT9 LIKE 'WR' THEN CASE WHEN B.TXT8 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT9 END 
                    WHEN B.TXT8 NOT LIKE '' AND B.TXT8 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT8 LIKE 'WR' THEN CASE WHEN B.TXT7 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT8 END 
                    WHEN B.TXT7 NOT LIKE '' AND B.TXT7 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT7 LIKE 'WR' THEN CASE WHEN B.TXT6 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT7 END 
                    WHEN B.TXT6 NOT LIKE '' AND B.TXT6 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT6 LIKE 'WR' THEN CASE WHEN B.TXT5 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT6 END 
                    WHEN B.TXT5 NOT LIKE '' AND B.TXT5 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT5 LIKE 'WR' THEN CASE WHEN B.TXT4 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT5 END 
                    WHEN B.TXT4 NOT LIKE '' AND B.TXT4 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT4 LIKE 'WR' THEN CASE WHEN B.TXT3 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT4 END 
                    WHEN B.TXT3 NOT LIKE '' AND B.TXT3 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT3 LIKE 'WR' THEN CASE WHEN B.TXT2 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT3 END 
                    WHEN B.TXT2 NOT LIKE '' AND B.TXT2 NOT LIKE 'HOLD' THEN CASE WHEN B.TXT2 LIKE 'WR' THEN CASE WHEN B.TXT1 LIKE 'SROA' THEN 'WRX' ELSE 'WR' END ELSE B.TXT2 END 
                    WHEN B.TXT1 NOT LIKE '' AND B.TXT1 NOT LIKE 'HOLD' THEN B.TXT1
                END AS STAT,
                A.KDAUF,
                C.VBELN,
                C.ERDAT_VBFA,
                C.KUNNR,
                D.COMPANY_NAME,
                ROW_NUMBER ( ) OVER ( PARTITION BY B.AUFNR ORDER BY C.ERDAT_VBFA DESC ) AS TOP_ONE
            FROM M_PMORDERH A
            INNER JOIN M_STATUSPM B ON A.AUFNR = B.AUFNR
            LEFT JOIN M_SALESORDER C ON A.KDAUF = C.VBELN AND C.PRCTR LIKE 'GMFTC%'
            LEFT JOIN CUSTOMER D ON C.KUNNR = D.ID_CUSTOMER
            WHERE
                $filter
                $where_cust
        )
            SELECT 
                *,
                CASE
                    WHEN ISDATE(ERDAT_VBFA) = 1
                            THEN 'Delivered' 
                    WHEN STAT LIKE 'FRA' 
                            THEN 'Ready To Deliver Return As is Conditiion' 
                    WHEN STAT LIKE 'FSB' 
                            THEN 'Ready To Deliver Serviceable Condition' 
                    WHEN STAT LIKE 'FSC' 
                            THEN 'Ready To Deliver BER' 
                    WHEN STAT LIKE 'WRX' 
                            THEN 'Approval Received / Repair Started'
                    WHEN STAT LIKE 'WR' 
                            THEN 'Under Repair'
                    WHEN STAT LIKE 'UR' 
                            THEN 'Under Repair'
                    WHEN STAT LIKE 'SRMC' 
                            THEN 'Waiting Supply Materials From Customer'  
                    WHEN STAT LIKE 'SROA' 
                            THEN 'Quotation Provided / Awaiting Approval' 
                    ELSE 'Unit Under Inspection'
                    END AS TXT_STAT 
            FROM A WHERE TOP_ONE = '1'
        ";
        return $this->db->query($sql)->result_array();
    }

    function getStatusOrderPerTahun($tahun, $cust, $bulan, $type){
        $kunnr = '%%';
        if($cust == ''){
            $kunnr = '%%';
        }else{
            $kunnr = $cust;
        }
        $filter = '';
        $where = " C.KUNNR LIKE '{$kunnr}' AND D.ORDER_TYPE LIKE 'GA03'";

        if($type == "close_date"){
            if($bulan != 'all'){
                $filter = "AND (((SUBSTRING (H.GETRI, 1, 4)) = $tahun AND (SUBSTRING (H.GETRI,5, 2)) = $bulan ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 1, 4 ) ) = $tahun )" ;
            }
        }

        if($type == "create_date"){
            if($bulan != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = $tahun AND (SUBSTRING (B.ERDAT, 5, 2)) = $bulan ) OR ((SUBSTRING (H.GETRI, 1, 4)) = $tahun AND (SUBSTRING (H.GETRI, 5, 2)) = $bulan ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = $tahun AND (SUBSTRING (B.ERDAT, 5, 2)) = $bulan ) )" ;
            }
        }

        $sql = "
        WITH  DD AS (
            SELECT
                H.AUFNR AS 'MAINTENANCE_ORDER',
                H.MATNR AS 'PART_NUMBER',
                H.MAKTX AS 'PART_NAME',
                H.AUART AS 'ORDER_TYPE',
                H.KDAUF,
                H.GETRI,
                H.IDAT2,
                CASE 
                    WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' 
                        THEN  H.IDAT2
                    ELSE H.GETRI
                END AS 'TECO_DATE',
                CASE 
                    WHEN ( H.SERNR != '' ) THEN H.SERNR 
                    WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                END AS 'SERIAL_NUMBER',
                CASE    
                    WHEN TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN TXT50 
                    WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN TXT49 
                    WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN TXT48 
                    WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN TXT47 
                    WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN TXT46 
                    WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN TXT45 
                    WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN TXT44 
                    WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN TXT43 
                    WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN TXT42 
                    WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN TXT41 
                    WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN TXT40 
                    WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN TXT39 
                    WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN TXT38 
                    WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN TXT37 
                    WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN TXT36 
                    WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN TXT35 
                    WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN TXT34 
                    WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN TXT33 
                    WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN TXT32 
                    WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN TXT31 
                    WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN TXT30 
                    WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN TXT29 
                    WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN TXT28 
                    WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN TXT27 
                    WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN TXT26 
                    WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN TXT25 
                    WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN TXT24 
                    WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN TXT23 
                    WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN TXT22 
                    WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN TXT21 
                    WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN TXT20 
                    WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN TXT19 
                    WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN TXT18 
                    WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN TXT17 
                    WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN TXT16 
                    WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN TXT15 
                    WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN TXT14 
                    WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN TXT13 
                    WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN TXT12 
                    WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN TXT11 
                    WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN TXT10 
                    WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN TXT9 
                    WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN TXT8 
                    WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN TXT7 
                    WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN TXT6 
                    WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN TXT5 
                    WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN TXT4 
                    WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN TXT3 
                    WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN TXT2 
                    WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN TXT1 
                END AS STAT,
                B.*
            FROM
                M_STATUSPM B
            RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
            WHERE
                H.AUART = 'GA03' AND 
                B.PRCTR LIKE 'GMFTC%'
                $filter 
                -- AND ( L.GETRI = '00000000' OR ( SUBSTRING ( L.GETRI, 5, 4 ) ) = $tahun ) 
        ),
        FF AS ( 
            SELECT 
                DISTINCT
                R.VBELN AS 'SALES_ORDER',
                R.BSTNK AS 'PURCHASE_ORDER',
                R.AUDAT AS 'RECEIVED_DATE',
                R.KUNNR AS 'KUNNR',
                R.NAME1 AS 'CUSTOMER'
            FROM DD D1 
            LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
            WHERE 
                R.KUNNR LIKE '%{$kunnr}%' 
        ),
        C2 AS (
            SELECT
                MS.VBELN,
                MS.VBELN_VBFA,
                MS.ERDAT_VBFA,
                MS.BWART,
                ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
            FROM FF F1
            LEFT JOIN M_SALESORDER MS ON F1.SALES_ORDER = MS.VBELN 
            WHERE
                MS.BWART LIKE '601' AND 
                MS.VBELN_VBFA LIKE '49%' AND 
                MS.KUNNR LIKE '%{$kunnr}%' AND 
                MS.VBTYP_N = 'R'
        ),
        CF AS ( 
            SELECT * 
            FROM C2 
            WHERE RowNum1 = 1 
        ),
        CG AS (
            SELECT 
                DISTINCT
                S.VBELN,
                S.VBELN_BILL,
                BL.RFBSK,
                ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
            FROM
                M_SALESORDER S
            LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
            WHERE
                VBELN_BILL != ''  AND
                S.KUNNR LIKE '%{$kunnr}%' 
        ),
        CD AS ( 
            SELECT * 
            FROM CG 
            WHERE RowNum2 = 1 
        ),   
        CC AS (
            SELECT
                F1.*,
                C4.VBELN_VBFA,
                C4.ERDAT_VBFA,
                C4.BWART,
                C3.VBELN_BILL,
                C3.RFBSK 
            FROM 
                FF F1 
            LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
            LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
        ),
        AA AS (
            SELECT
                CASE
                    WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) 
                        THEN 'Unpaid' ELSE 'Paid' 
                END AS BILLING_STATUS,
                D.*, 
                C.*
            FROM
                DD D
            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
            WHERE
                $where
        ),
        LY AS (
            SELECT
                *,
                CASE
                    WHEN ERDAT_VBFA IS NOT NULL 
                        THEN 'Delivered' 
                    WHEN STAT LIKE 'FRA' 
                        THEN 'Ready To Deliver Return As is Conditiion' 
                    WHEN STAT LIKE 'FSB' 
                        THEN 'Ready To Deliver Serviceable Condition' 
                    WHEN STAT LIKE 'F%' 
                        THEN 'Ready To Deliver BER' 
                    WHEN STAT LIKE 'WR' 
                        THEN 'Approval Received / Repair Started'
                    WHEN STAT LIKE 'UR' 
                        THEN 'Under Repair'
                    WHEN STAT LIKE 'SRMC' 
                        THEN 'Waiting Supply Materials From Customer'  
                    WHEN STAT LIKE 'SROA' 
                        THEN 'Quotation Provided / Awaiting Approval' 
                    ELSE 'Unit Under Inspection'
                END AS TXT_STAT 
            FROM
                AA 
            WHERE
            -- ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = $tahun ) AND 
            (STAT != 'WR') 
        )

        SELECT
            COUNT( * ) AS JUMLAH,
            TXT_STAT AS STATUS
        FROM LY
        GROUP BY TXT_STAT";

        return $this->db->query($sql)->result();
    }

    public function get_list_invoice_outstanding($range){
        $id_customer = '';
        $where = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $id_customer = $this->session->userdata('log_sess_id_customer');
            $where = " AND KUNAG LIKE '{$id_customer}' ";
        }

        $sql = "WITH AA AS (
                SELECT
                    VBELN AS BILLING,
                    CONVERT (
                    DATE,
                    (
                    SUBSTRING( FKDAT, 7, 2 ) + '-' + SUBSTRING( FKDAT, 5, 2 ) + '-' + SUBSTRING( FKDAT, 1, 4 ) 
                    ),
                    104 
                    ) AS DOC_DATE,
                    NETWR AS AMOUNT,
                    WAERK AS CURRENCY,
                    DATEDIFF( DAY, CONVERT ( DATE, FKDAT, 104 ), CONVERT ( DATE, getdate ( ), 104 ) ) AS RANGE_DATE_NOW 
                FROM
                    M_SALESBILLING A 
                WHERE
                    RFBSK != 'C' $where
                    ),
                    BB AS (
                SELECT
                    *,
                CASE
                    WHEN RANGE_DATE_NOW <= 30 THEN
                    1
                    WHEN RANGE_DATE_NOW > 30 AND RANGE_DATE_NOW <= 60 THEN
                    2
                    WHEN RANGE_DATE_NOW > 60 AND RANGE_DATE_NOW <= 90 THEN
                    2
                    WHEN RANGE_DATE_NOW > 90 THEN
                    4 
                END AS CATEGORY 
                FROM
                    AA 
                    ) SELECT
                    * 
                FROM
                BB WHERE CATEGORY = {$range} ;";

        // $sql = "
        // SELECT * FROM (SELECT
        //     VBELN AS BILLING,
        //     CONVERT(DATE, (SUBSTRING(FKDAT, 7, 2)+'-'+SUBSTRING(FKDAT, 5, 2)+'-'+ SUBSTRING(FKDAT, 1, 4)), 104 ) AS DOC_DATE,
        // --  FKDAT,
        //     NETWR AS AMOUNT,
        //     WAERK AS CURRENCY,
        //     *
        // FROM
        //     TC_V_AGING
        //     WHERE RFBSK NOT LIKE 'C'
        // ) MSB
        // WHERE 
        //     {$where}
        // --DOC_DATE BETWEEN '20171212' AND '20180105'
        //     CATEGORY = {$range}
        // ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
    public function get_list_historical($start, $end){

        $id_customer = '';
        $where = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $id_customer = $this->session->userdata('log_sess_id_customer');
            $where = " AND KUNAG LIKE '{$id_customer}' ";
        }

        $sql = "WITH AA AS(
                SELECT
                    *,
                    VBELN AS BILLING,
                        CONVERT (
                            DATE,
                            (
                            SUBSTRING ( FKDAT, 7, 2 ) + '-' + SUBSTRING ( FKDAT, 5, 2 ) + '-' + SUBSTRING ( FKDAT, 1, 4 )),
                            104 
                        ) AS DOC_DATE,
                --  FKDAT,
                        NETWR AS AMOUNT,
                        WAERK AS CURRENCY,
                        C.COMPANY_NAME
                FROM
                    M_SALESBILLING A LEFT JOIN CUSTOMER C
                WHERE
                    RFBSK LIKE 'C' $where
                    )
                    SELECT
                        * 
                    FROM
                        AA
                    WHERE
                        DOC_DATE BETWEEN '{$start}' 
                    AND '{$end}' 
                ORDER BY
                KUNAG;";

        // $sql = "
        // SELECT * FROM (SELECT
        //     VBELN AS BILLING,
        //     CONVERT(DATE, (SUBSTRING(FKDAT, 7, 2)+'-'+SUBSTRING(FKDAT, 5, 2)+'-'+ SUBSTRING(FKDAT, 1, 4)), 104 ) AS DOC_DATE,
        // --  FKDAT,
        //     NETWR AS AMOUNT,
        //     WAERK AS CURRENCY,
        //     *
        // FROM
        //     TC_V_AGING
        //     WHERE RFBSK LIKE 'C'
        // ) MSB
        // WHERE 
        // {$where}
        // DOC_DATE BETWEEN '{$start}' AND '{$end}'
        // ORDER BY KUNAG
        // ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

     public function getStatusInvoice($SALES_ORDER)
    {   
        $sql = "select distinct FKSAA from M_SALESORDER WHERE VBELN LIKE '{$SALES_ORDER}' AND FKSAA not like '' order by FKSAA ASC";
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function select_retailPerTahun($tahun, $cust, $bulan, $type)
    {
        // if($offset != null)
        // {
        //    $row_awal    = $offset+1;
        //    $row_akhir   = $offset+10;
        // }
        // else
        // {
        //     $row_awal   = 1;
        //     $row_akhir  = 10;
        // }
        $kunnr = '%%';
        $filter = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where = " C.KUNNR LIKE '{$kunnr}' ";
        }
        else if ($cust) {
            $kunnr = $cust;
            if ($cust == ''){
                $where = " D.ORDER_TYPE LIKE 'GA03'";
            }
            else {
                $where = "C.KUNNR LIKE '{$cust}'"; 
            }  
        }
        else {
           $where = "D.ORDER_TYPE LIKE 'GA03'"; 
        }

        if($type == "close_date"){
            if($bulan != 'all'){
                $filter = "AND ( ((SUBSTRING (H.GETRI, 1, 4)) = $tahun AND (SUBSTRING (H.GETRI,5, 2)) = $bulan ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 1, 4 ) ) = $tahun )" ;
            }
        }

        if($type == "create_date"){
            if($bulan != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = $tahun AND (SUBSTRING (B.ERDAT, 5, 2)) = $bulan ) OR ((SUBSTRING (H.GETRI, 1, 4)) = $tahun AND (SUBSTRING (H.GETRI, 5, 2)) = $bulan ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = $tahun AND (SUBSTRING (B.ERDAT, 5, 2)) = $bulan ) )" ;
            }
        }

        $sql = "WITH DD AS (
            SELECT
                    H.AUFNR AS 'MAINTENANCE_ORDER',
                    H.MATNR AS 'PART_NUMBER',
                    H.MAKTX AS 'PART_NAME',
                    H.AUART AS 'ORDER_TYPE',
                    H.KDAUF,
                    H.GETRI,
                    H.IDAT2,
                    CASE WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' THEN  H.IDAT2
                        ELSE H.GETRI
                    END AS 'TECO_DATE',
                    CASE
                        WHEN ( H.SERNR != '' ) THEN H.SERNR 
                        WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                    END AS 'SERIAL_NUMBER',
                    CASE    
                        WHEN
                            TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN
                            TXT50 
                            WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN
                            TXT49 
                            WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN
                            TXT48 
                            WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN
                            TXT47 
                            WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN
                            TXT46 
                            WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN
                            TXT45 
                            WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN
                            TXT44 
                            WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN
                            TXT43 
                            WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN
                            TXT42 
                            WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN
                            TXT41 
                            WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN
                            TXT40 
                            WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN
                            TXT39 
                            WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN
                            TXT38 
                            WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN
                            TXT37 
                            WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN
                            TXT36 
                            WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN
                            TXT35 
                            WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN
                            TXT34 
                            WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN
                            TXT33 
                            WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN
                            TXT32 
                            WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN
                            TXT31 
                            WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN
                            TXT30 
                            WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN
                            TXT29 
                            WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN
                            TXT28 
                            WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN
                            TXT27 
                            WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN
                            TXT26 
                            WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN
                            TXT25 
                            WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN
                            TXT24 
                            WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN
                            TXT23 
                            WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN
                            TXT22 
                            WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN
                            TXT21 
                            WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN
                            TXT20 
                            WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN
                            TXT19 
                            WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN
                            TXT18 
                            WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN
                            TXT17 
                            WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN
                            TXT16 
                            WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN
                            TXT15 
                            WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN
                            TXT14 
                            WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN
                            TXT13 
                            WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN
                            TXT12 
                            WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN
                            TXT11 
                            WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN
                            TXT10 
                            WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN
                            TXT9 
                            WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN
                            TXT8 
                            WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN
                            TXT7 
                            WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN
                            TXT6 
                            WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN
                            TXT5 
                            WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN
                            TXT4 
                            WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN
                            TXT3 
                            WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN
                            TXT2 
                            WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN
                            TXT1 
                    END AS STAT,
                    B.*
            FROM
                    M_STATUSPM B
                    RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
            WHERE
                    H.AUART = 'GA03' 
                    AND B.PRCTR LIKE 'GMFTC%'
                    $filter 
                    -- AND ( L.GETRI = '00000000' OR ( SUBSTRING ( L.GETRI, 5, 4 ) ) = $tahun ) 
                    ),
        FF AS ( SELECT DISTINCT
                        R.VBELN AS 'SALES_ORDER',
                        R.BSTNK AS 'PURCHASE_ORDER',
                        R.AUDAT AS 'RECEIVED_DATE',
                        R.KUNNR AS 'KUNNR',
                        R.NAME1 AS 'CUSTOMER'
                FROM DD D1 LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
                WHERE R.KUNNR LIKE '%{$kunnr}%' ),
        C2 AS (
                SELECT
                        MS.VBELN,
                        MS.VBELN_VBFA,
                        MS.ERDAT_VBFA,
                        MS.BWART,
                        ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
                FROM
                        FF F1
                        LEFT JOIN 
                        M_SALESORDER MS
                        ON F1.SALES_ORDER = MS.VBELN 
                WHERE
                        MS.BWART LIKE '601' 
                        AND MS.VBELN_VBFA LIKE '49%' 
                        AND MS.KUNNR LIKE '%{$kunnr}%'
                        AND MS.VBTYP_N = 'R'
                ),
        CF AS ( SELECT * FROM C2 WHERE RowNum1 = 1 ),
        CG AS (
        SELECT DISTINCT
                S.VBELN,
                S.VBELN_BILL,
                BL.RFBSK,
                ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
        FROM
                M_SALESORDER S
                LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
        WHERE
                VBELN_BILL != '' 
                AND S.KUNNR LIKE '%{$kunnr}%' 
        ),
        CD AS ( SELECT * FROM CG WHERE RowNum2 = 1 ),   
        CC AS (
        SELECT
                F1.*,
                C4.VBELN_VBFA,
                C4.ERDAT_VBFA,
                C4.BWART,
                C3.VBELN_BILL,
                C3.RFBSK 
        FROM
                FF F1 
                LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
                LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
        ),
        AA AS (
            SELECT
                CASE
                    WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                    'Unpaid' ELSE 'Paid' 
                END AS BILLING_STATUS,
                D.*, C.*
                FROM
                    DD D
                    LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                WHERE
                        $where
                )
                SELECT
                        *,
                    CASE
                        WHEN ERDAT_VBFA IS NOT NULL THEN
                        'Delivered' 
                        WHEN STAT LIKE 'FRA' THEN
                        'Ready To Deliver Return As is Conditiion' 
                        WHEN STAT LIKE 'FSB' THEN
                        'Ready To Deliver Serviceable Condition' 
                        WHEN STAT LIKE 'F%' THEN
                        'Ready To Deliver BER' 
                        WHEN STAT LIKE 'WR' THEN
                        'Approval Received / Repair Started'
                        WHEN STAT LIKE 'UR' THEN
                        'Under Repair'
                        WHEN STAT LIKE 'SRMC' THEN
                        'Waiting Supply Materials From Customer'  
                        WHEN STAT LIKE 'SROA' THEN
                        'Quotation Provided / Awaiting Approval' 
                        ELSE 'Unit Under Inspection'
                        END AS TXT_STAT
                    FROM
                        AA 
                -- WHERE ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = $tahun ) 
                ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }


    public function select_retail2($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if (isset($param['start']) and isset($param['end'])) {
            // code...
            // print_r($param['search']);
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }


        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        $paid = '';
        $where_status = '';
        if (isset($param['column'])) // here order processing
        {
            if (($param['column']['16']['search']['value'])!= ''){
                $pay = substr($param['column']['16']['search']['value'], 1, -1);
                $paid = "AND BILLING_STATUS LIKE '$pay'";
            } 
            if (($param['column']['13']['search']['value'])!= ''){
                $sw = "(" . $param['column']['13']['search']['value'] . ")";
                $swe = str_replace('\\', '', $sw);
                $where_status = "AND TXT_STAT IN $swe ";
            }
        }
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else if (isset($param['kunnr'])){
                $where_kunnr = '';
                $kunnr = $param['kunnr'];
                if ($param['kunnr'] == ''){
                    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND";
                }
                else {
                    $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
                }   
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }
        if($param['filterby'] == "close_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND ( ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI,5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 1, 4 ) ) = '{$param['tahun']}' )" ;
            }
        }

        if($param['filterby'] == "create_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 5, 2)) = '{$param['bulan']}' ) OR ((SUBSTRING (H.GETRI, 3, 2)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI, 3, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 5, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 3, 2)) = '{$param['bulan']}' ) )" ;
            }
        }


        $sql = "WITH DD AS (
                SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.GETRI,
                        H.IDAT2,
                        CASE WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' THEN  H.IDAT2
                            ELSE H.GETRI
                        END AS 'TECO_DATE',
                        CASE
                            WHEN ( H.SERNR != '' ) THEN H.SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                        END AS 'SERIAL_NUMBER',
                        CASE    
                            WHEN
                                TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN
                                TXT1 
                        END AS STAT,
                        B.*
                FROM
                        M_STATUSPM B
                        RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                WHERE
                        H.AUART = 'GA03' 
                        AND B.PRCTR LIKE 'GMFTC%'
                        $filter
                        ),
                    FF AS ( SELECT DISTINCT
                            R.VBELN AS 'SALES_ORDER',
                            R.BSTNK AS 'PURCHASE_ORDER',
                            R.AUDAT AS 'RECEIVED_DATE',
                            R.KUNNR AS 'KUNNR',
                            R.NAME1 AS 'CUSTOMER'
                        FROM DD D1 LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
                        WHERE R.KUNNR LIKE '%{$kunnr}%' ),
                    C2 AS (
                        SELECT
                            MS.VBELN,
                            MS.VBELN_VBFA,
                            MS.ERDAT_VBFA,
                            MS.BWART,
                            ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
                        FROM
                            FF F1
                            LEFT JOIN 
                            M_SALESORDER MS
                            ON F1.SALES_ORDER = MS.VBELN 
                        WHERE
                            MS.BWART LIKE '601' 
                            AND MS.VBELN_VBFA LIKE '49%' 
                            AND MS.KUNNR LIKE '%{$kunnr}%' 
                            AND MS.VBTYP_N = 'R'
                        ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum1 = 1 ),
                    CG AS (
                    SELECT DISTINCT
                        S.VBELN,
                        S.VBELN_BILL,
                        BL.RFBSK,
                        ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
                    FROM
                        M_SALESORDER S
                        LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
                    WHERE
                        VBELN_BILL != '' 
                        AND S.KUNNR LIKE '%{$kunnr}%' 
                    ),
                    CD AS ( SELECT * FROM CG WHERE RowNum2 = 1 ),   
                    CC AS (
                    SELECT
                        F1.*,
                        C4.VBELN_VBFA,
                        C4.ERDAT_VBFA,
                        C4.BWART,
                        C3.VBELN_BILL,
                        C3.RFBSK 
                    FROM
                        FF F1 
                        LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
                        LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
                    ),
                    AA AS (
                    SELECT
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            D.*, C.*
                        FROM
                            DD D
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                            WHEN ERDAT_VBFA IS NOT NULL THEN
                            'Delivered' 
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started'
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair'
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer'  
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            ELSE 'Unit Under Inspection'
                        END AS TXT_STAT
                            FROM
                                AA 
                    -- WHERE
                    -- ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = '{$param['tahun']}' ) 
                    ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' $where_status $paid)
                SELECT AH.*, TC_M_PMORDER.REMARKS FROM AH LEFT JOIN TC_M_PMORDER ON AH.MAINTENANCE_ORDER = TC_M_PMORDER.AUFNR
                WHERE AUART = 'GA03' $rownum      
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

     public function select_retail2_count($param, $ext=null)
    {
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';

        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            // $rownum = " AND RowNum > {$param['start']}
            //     AND RowNum < {$param['end']}";
        }

        if (isset($param['start']) and isset($param['end'])) {
            // code...
            // print_r($param['search']);
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }


        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        $paid = '';
        $where_status = '';
        if (isset($param['column'])) // here order processing
        {
            if (($param['column']['16']['search']['value'])!= ''){
                $pay = substr($param['column']['16']['search']['value'], 1, -1);
                $paid = "AND BILLING_STATUS LIKE '$pay'";
            } 
            if (($param['column']['13']['search']['value'])!= ''){
                $sw = "(" . $param['column']['13']['search']['value'] . ")";
                $swe = str_replace('\\', '', $sw);
                $where_status = "AND TXT_STAT IN $swe ";
            }
        }
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where
                 .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else if (isset($param['kunnr'])){
                $where_kunnr = '';
                $kunnr = $param['kunnr'];
                if ($param['kunnr'] == ''){
                    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND";
                }
                else {
                    $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
                }   
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        if($param['filterby'] == "close_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND ( ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI,5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 5, 4 ) ) = '{$param['tahun']}' )" ;
            }
        }

        if($param['filterby'] == "create_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 5, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 3, 2)) = '{$param['bulan']}' ) OR ((SUBSTRING (H.GETRI, 3, 2)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI, 3, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 5, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 3, 2)) = '{$param['bulan']}' ) )" ;
            }
        }

        $sql = "WITH DD AS (
                        SELECT
                            H.AUFNR AS 'MAINTENANCE_ORDER',
                            H.MATNR AS 'PART_NUMBER',
                            H.MAKTX AS 'PART_NAME',
                            H.AUART AS 'ORDER_TYPE',
                            H.KDAUF,
                            H.GETRI,
                            H.IDAT2,
                            CASE WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' THEN  H.IDAT2
                                ELSE H.GETRI
                            END AS 'TECO_DATE',
                            CASE
                                WHEN ( H.SERNR != '' ) THEN H.SERNR 
                                WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                            END AS 'SERIAL_NUMBER',
                            CASE    
                                WHEN
                                    TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN
                                    TXT50 
                                    WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN
                                    TXT49 
                                    WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN
                                    TXT48 
                                    WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN
                                    TXT47 
                                    WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN
                                    TXT46 
                                    WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN
                                    TXT45 
                                    WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN
                                    TXT44 
                                    WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN
                                    TXT43 
                                    WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN
                                    TXT42 
                                    WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN
                                    TXT41 
                                    WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN
                                    TXT40 
                                    WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN
                                    TXT39 
                                    WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN
                                    TXT38 
                                    WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN
                                    TXT37 
                                    WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN
                                    TXT36 
                                    WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN
                                    TXT35 
                                    WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN
                                    TXT34 
                                    WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN
                                    TXT33 
                                    WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN
                                    TXT32 
                                    WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN
                                    TXT31 
                                    WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN
                                    TXT30 
                                    WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN
                                    TXT29 
                                    WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN
                                    TXT28 
                                    WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN
                                    TXT27 
                                    WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN
                                    TXT26 
                                    WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN
                                    TXT25 
                                    WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN
                                    TXT24 
                                    WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN
                                    TXT23 
                                    WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN
                                    TXT22 
                                    WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN
                                    TXT21 
                                    WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN
                                    TXT20 
                                    WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN
                                    TXT19 
                                    WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN
                                    TXT18 
                                    WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN
                                    TXT17 
                                    WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN
                                    TXT16 
                                    WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN
                                    TXT15 
                                    WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN
                                    TXT14 
                                    WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN
                                    TXT13 
                                    WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN
                                    TXT12 
                                    WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN
                                    TXT11 
                                    WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN
                                    TXT10 
                                    WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN
                                    TXT9 
                                    WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN
                                    TXT8 
                                    WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN
                                    TXT7 
                                    WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN
                                    TXT6 
                                    WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN
                                    TXT5 
                                    WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN
                                    TXT4 
                                    WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN
                                    TXT3 
                                    WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN
                                    TXT2 
                                    WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN
                                    TXT1 
                            END AS STAT,
                            B.*
                        FROM
                            M_STATUSPM B
                            RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                        WHERE
                            H.AUART = 'GA03' 
                            AND B.PRCTR LIKE 'GMFTC%'
                        $filter
                        ),
                    FF AS ( SELECT DISTINCT
                            R.VBELN AS 'SALES_ORDER',
                            R.BSTNK AS 'PURCHASE_ORDER',
                            R.AUDAT AS 'RECEIVED_DATE',
                            R.KUNNR AS 'KUNNR',
                            R.NAME1 AS 'CUSTOMER'
                        FROM DD D1 LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
                        WHERE R.KUNNR LIKE '%{$kunnr}%' ),
                    C2 AS (
                        SELECT
                            MS.VBELN,
                            MS.VBELN_VBFA,
                            MS.ERDAT_VBFA,
                            MS.BWART,
                            ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
                        FROM
                            FF F1
                            LEFT JOIN 
                            M_SALESORDER MS
                            ON F1.SALES_ORDER = MS.VBELN 
                        WHERE
                            MS.BWART LIKE '601' 
                            AND MS.VBELN_VBFA LIKE '49%' 
                            AND MS.KUNNR LIKE '%{$kunnr}%' 
                            AND MS.VBTYP_N = 'R'
                        ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum1 = 1 ),
                    CG AS (
                    SELECT DISTINCT
                        S.VBELN,
                        S.VBELN_BILL,
                        BL.RFBSK,
                        ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
                    FROM
                        M_SALESORDER S
                        LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
                    WHERE
                        VBELN_BILL != '' 
                        AND S.KUNNR LIKE '%{$kunnr}%' 
                    ),
                    CD AS ( SELECT * FROM CG WHERE RowNum2 = 1 ),   
                    CC AS (
                    SELECT
                        F1.*,
                        C4.VBELN_VBFA,
                        C4.ERDAT_VBFA,
                        C4.BWART,
                        C3.VBELN_BILL,
                        C3.RFBSK 
                    FROM
                        FF F1 
                        LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
                        LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
                    ),
                    AA AS (
                        SELECT
                            CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            D.*, C.*
                        FROM
                            DD D
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                            WHEN ERDAT_VBFA IS NOT NULL THEN
                            'Delivered' 
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started'
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair'
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer'  
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            ELSE 'Unit Under Inspection'
                        END AS TXT_STAT
                            FROM
                                AA 
                    -- WHERE
                    -- ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = '{$param['tahun']}' )
                    ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' $where_status $paid)
                SELECT count(*) as 'JUMLAH' FROM LY WHERE AUART = 'GA03' $where_status $paid
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
     public function select_retail_where_status2($param, $ext=null)
    {
        
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';
         $whereWR="";
         $wherenoWR="";
           if (isset($param['status'])) {
            if($param['status']=="Repair Started")
                $whereWR=" WHERE STAT='WR'";
             else{
                $wherenoWR=" AND TXT_STAT LIKE '{$param['status']}%' ";
                $whereWR=" WHERE (STAT!='WR')";
            }
           }    
        if (isset($param['search'])) {
            // code...
            // print_r($param['search']);
            $keyword = "'%".strtolower($param['search']['value'])."%'";

            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }

        if (isset($param['order'])) // here order processing
        {
           
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else if (isset($param['kunnr'])){
                $where_kunnr = '';
                $kunnr = $param['kunnr'];
                if ($param['kunnr'] == ''){
                    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND";
                }
                else {
                    $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
                }   
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        if($param['filterby'] == "close_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND ( ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI,5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 1, 4 ) ) = '{$param['tahun']}' )" ;
            }
        }

        if($param['filterby'] == "create_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 5, 2)) = '{$param['bulan']}' ) OR ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI, 5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 5, 2)) = '{$param['bulan']}' ) )" ;
            }
        }

        $sql = "WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        CASE
                         WHEN (H.MATNR !='') THEN H.MATNR 
                         WHEN (H.MATNR ='' AND HI.MATNR!='') THEN HI.MATNR
                         END AS 'PART_NUMBER',
                       -- H.MATNR AS 'PART_NUMBER',
                        CASE
                         WHEN (H.MAKTX !='') THEN H.MAKTX 
                         WHEN (H.MAKTX ='' AND HI.MAKTX!='') THEN HI.MAKTX
                         END AS 'PART_NAME',
                      --  H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.GETRI,
                        H.IDAT2,
                        CASE WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' THEN  H.IDAT2
                            ELSE H.GETRI
                        END AS 'TECO_DATE',
                        CASE
                            WHEN ( H.SERNR != '' ) THEN H.SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                        END AS 'SERIAL_NUMBER',
                        CASE    
                            WHEN
                                TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN
                                TXT1 
                        END AS STAT,
                        B.*
                    FROM
                        M_STATUSPM B
                        RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                        LEFT JOIN (SELECT AUFNR,MATNR,MAKTX,RSPOS,ROW_NUMBER () OVER (
                                    PARTITION BY AUFNR ORDER BY RSPOS 
                                ) AS RowNum FROM M_PMORDER WHERE AUART = 'GA03' 
                            AND MATNR IS NOT NULL
                            AND MATNR != '') HI ON H.AUFNR = HI.AUFNR AND HI.RowNum='1'
                    WHERE
                        H.AUART = 'GA03' 
                        AND B.PRCTR LIKE 'GMFTC%' 
                        $filter
                        -- AND ( L.GETRI = '00000000' OR ( SUBSTRING ( L.GETRI, 5, 4 ) ) = '{$param['tahun']}' ) 
                        ),
                    FF AS ( SELECT DISTINCT
                            R.VBELN AS 'SALES_ORDER',
                            R.BSTNK AS 'PURCHASE_ORDER',
                            R.AUDAT AS 'RECEIVED_DATE',
                            R.KUNNR AS 'KUNNR',
                            R.NAME1 AS 'CUSTOMER'
                        FROM DD D1 LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
                        WHERE R.KUNNR LIKE '%{$kunnr}%' ),
                    C2 AS (
                        SELECT
                            MS.VBELN,
                            MS.VBELN_VBFA,
                            MS.ERDAT_VBFA,
                            MS.BWART,
                            ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
                        FROM
                            FF F1
                            LEFT JOIN 
                            M_SALESORDER MS
                            ON F1.SALES_ORDER = MS.VBELN 
                        WHERE
                            MS.BWART LIKE '601' 
                            AND MS.VBELN_VBFA LIKE '49%' 
                            AND MS.KUNNR LIKE '%{$kunnr}%' 
                            AND MS.VBTYP_N = 'R'
                        ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum1 = 1 ),
                    CG AS (
                    SELECT DISTINCT
                        S.VBELN,
                        S.VBELN_BILL,
                        BL.RFBSK,
                        ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
                    FROM
                        M_SALESORDER S
                        LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
                    WHERE
                        VBELN_BILL != '' 
                        AND S.KUNNR LIKE '%{$kunnr}%' 
                    ),
                    CD AS ( SELECT * FROM CG WHERE RowNum2 = 1 ),   
                    CC AS (
                    SELECT
                        F1.*,
                        C4.VBELN_VBFA,
                        C4.ERDAT_VBFA,
                        C4.BWART,
                        C3.VBELN_BILL,
                        C3.RFBSK 
                    FROM
                        FF F1 
                        LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
                        LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
                    ),
                    AA AS (
                    SELECT
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            D.*, C.*
                        FROM
                            DD D
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                            WHEN ERDAT_VBFA IS NOT NULL AND STAT <> 'WR' THEN
                            'Delivered' 
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started'
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair'
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer'  
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            ELSE 'Unit Under Inspection'
                        END AS TXT_STAT 
                            FROM
                                AA 
                        $whereWR
                    -- WHERE
                    -- ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = '{$param['tahun']}' ) 
                ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' $wherenoWR)
                SELECT AH.*, TC_M_PMORDER.REMARKS FROM AH LEFT JOIN TC_M_PMORDER ON AH.MAINTENANCE_ORDER = TC_M_PMORDER.AUFNR
                WHERE AUART = 'GA03' $rownum        
                            ;";
                            // print_r($sql);die;
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
        // $sql =" 
        //         SELECT 
        //             RowNum, *
        //         FROM
        //                 (
        //                     SELECT *, ROW_NUMBER() OVER (ORDER BY STAT) as RowNum
        //                     FROM  TC_V_RETAIL
        //                     WHERE 
        //                         $where
        //                         TXT_STAT LIKE '{$param['status']}'        
        //                 ) AS PAGE_TB_RETAIL
        //         WHERE
        //             $where
        //             $rownum
        //     ";
        // $query  = $this->db->query($sql);
        
        // $result = $query->result_array();
        // return $result;
    }

    public function select_retail_where_status2_count($param, $ext=null)
    {
        
        $keyword = "'%%'";
        $rownum = '';
        $where = '';
        $order = '';
         $whereWR="";
         $wherenoWR="";
           if (isset($param['status'])) {
            if($param['status']=="Repair Started")
                $whereWR=" WHERE STAT='WR'";
             else{
                $wherenoWR=" AND TXT_STAT LIKE '{$param['status']}%' ";
                $whereWR=" WHERE (STAT!='WR')";
            }
           } 
        if (isset($param['search'])) {
            $keyword = "'%".strtolower($param['search']['value'])."%'";
        }

        if (isset($param['start']) and isset($param['end'])) {
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }

        if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->column_order[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY {key($order_sort)}, {$order[key($order_sort)]}";
        }
        
        if ($ext) {
            $where = " ";
            foreach ($ext as $key => $value) {
                $where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
            }
        }else{
            $kunnr = '';
            if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
            }
            else if (isset($param['kunnr'])){
               $kunnr = $param['kunnr'];
               $where_kunnr = '';
                if ($param['kunnr'] == ''){
                    $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND";
                }
                else {
                    $where_kunnr = " C.KUNNR LIKE '{$kunnr}' AND";
                }  
            }
            else {
               $where_kunnr = "D.ORDER_TYPE LIKE 'GA03' AND"; 
            }
            $where = " (
                C.SALES_ORDER LIKE {$keyword} OR
                D.MAINTENANCE_ORDER LIKE {$keyword} OR
                C.PURCHASE_ORDER LIKE {$keyword} 
            )";
        }

        if($param['filterby'] == "close_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND ( ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI, 5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND ( ( SUBSTRING ( H.GETRI, 1, 4 ) ) = '{$param['tahun']}' )" ;
            }
        }

        if($param['filterby'] == "create_date"){
            if($param['bulan'] != 'all'){
                $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 5, 2)) = '{$param['bulan']}' ) OR ((SUBSTRING (H.GETRI, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (H.GETRI, 5, 2)) = '{$param['bulan']}' ))" ;
                }
            else {
               $filter = "AND (((SUBSTRING (B.ERDAT, 1, 4)) = '{$param['tahun']}' AND (SUBSTRING (B.ERDAT, 5, 2)) = '{$param['bulan']}' ) )" ;
            }
        }

        $sql = "WITH DD AS (
                    SELECT
                        H.AUFNR AS 'MAINTENANCE_ORDER',
                        H.MATNR AS 'PART_NUMBER',
                        H.MAKTX AS 'PART_NAME',
                        H.AUART AS 'ORDER_TYPE',
                        H.KDAUF,
                        H.GETRI,
                        H.IDAT2,
                        CASE WHEN H.GETRI = '00000000' AND H.IDAT2 != '00000000' THEN  H.IDAT2
                            ELSE H.GETRI
                        END AS 'TECO_DATE',
                        CASE
                            WHEN ( H.SERNR != '' ) THEN H.SERNR 
                            WHEN ( H.SERIALNR != '' ) THEN  H.SERIALNR 
                        END AS 'SERIAL_NUMBER',
                        CASE    
                            WHEN
                                TXT50 NOT LIKE '' AND TXT50 NOT LIKE 'HOLD' THEN
                                TXT50 
                                WHEN TXT49 NOT LIKE '' AND TXT49 NOT LIKE 'HOLD' THEN
                                TXT49 
                                WHEN TXT48 NOT LIKE '' AND TXT48 NOT LIKE 'HOLD' THEN
                                TXT48 
                                WHEN TXT47 NOT LIKE '' AND TXT47 NOT LIKE 'HOLD' THEN
                                TXT47 
                                WHEN TXT46 NOT LIKE '' AND TXT46 NOT LIKE 'HOLD' THEN
                                TXT46 
                                WHEN TXT45 NOT LIKE '' AND TXT45 NOT LIKE 'HOLD' THEN
                                TXT45 
                                WHEN TXT44 NOT LIKE '' AND TXT44 NOT LIKE 'HOLD' THEN
                                TXT44 
                                WHEN TXT43 NOT LIKE '' AND TXT43 NOT LIKE 'HOLD' THEN
                                TXT43 
                                WHEN TXT42 NOT LIKE '' AND TXT42 NOT LIKE 'HOLD' THEN
                                TXT42 
                                WHEN TXT41 NOT LIKE '' AND TXT41 NOT LIKE 'HOLD' THEN
                                TXT41 
                                WHEN TXT40 NOT LIKE '' AND TXT40 NOT LIKE 'HOLD' THEN
                                TXT40 
                                WHEN TXT39 NOT LIKE '' AND TXT39 NOT LIKE 'HOLD' THEN
                                TXT39 
                                WHEN TXT38 NOT LIKE '' AND TXT38 NOT LIKE 'HOLD' THEN
                                TXT38 
                                WHEN TXT37 NOT LIKE '' AND TXT37 NOT LIKE 'HOLD' THEN
                                TXT37 
                                WHEN TXT36 NOT LIKE '' AND TXT36 NOT LIKE 'HOLD' THEN
                                TXT36 
                                WHEN TXT35 NOT LIKE '' AND TXT35 NOT LIKE 'HOLD' THEN
                                TXT35 
                                WHEN TXT34 NOT LIKE '' AND TXT34 NOT LIKE 'HOLD' THEN
                                TXT34 
                                WHEN TXT33 NOT LIKE '' AND TXT33 NOT LIKE 'HOLD' THEN
                                TXT33 
                                WHEN TXT32 NOT LIKE '' AND TXT32 NOT LIKE 'HOLD' THEN
                                TXT32 
                                WHEN TXT31 NOT LIKE '' AND TXT31 NOT LIKE 'HOLD' THEN
                                TXT31 
                                WHEN TXT30 NOT LIKE '' AND TXT30 NOT LIKE 'HOLD' THEN
                                TXT30 
                                WHEN TXT29 NOT LIKE '' AND TXT29 NOT LIKE 'HOLD' THEN
                                TXT29 
                                WHEN TXT28 NOT LIKE '' AND TXT28 NOT LIKE 'HOLD' THEN
                                TXT28 
                                WHEN TXT27 NOT LIKE '' AND TXT27 NOT LIKE 'HOLD' THEN
                                TXT27 
                                WHEN TXT26 NOT LIKE '' AND TXT26 NOT LIKE 'HOLD' THEN
                                TXT26 
                                WHEN TXT25 NOT LIKE '' AND TXT25 NOT LIKE 'HOLD' THEN
                                TXT25 
                                WHEN TXT24 NOT LIKE '' AND TXT24 NOT LIKE 'HOLD' THEN
                                TXT24 
                                WHEN TXT23 NOT LIKE '' AND TXT23 NOT LIKE 'HOLD' THEN
                                TXT23 
                                WHEN TXT22 NOT LIKE '' AND TXT22 NOT LIKE 'HOLD' THEN
                                TXT22 
                                WHEN TXT21 NOT LIKE '' AND TXT21 NOT LIKE 'HOLD' THEN
                                TXT21 
                                WHEN TXT20 NOT LIKE '' AND TXT20 NOT LIKE 'HOLD' THEN
                                TXT20 
                                WHEN TXT19 NOT LIKE '' AND TXT19 NOT LIKE 'HOLD' THEN
                                TXT19 
                                WHEN TXT18 NOT LIKE '' AND TXT18 NOT LIKE 'HOLD' THEN
                                TXT18 
                                WHEN TXT17 NOT LIKE '' AND TXT17 NOT LIKE 'HOLD' THEN
                                TXT17 
                                WHEN TXT16 NOT LIKE '' AND TXT16 NOT LIKE 'HOLD' THEN
                                TXT16 
                                WHEN TXT15 NOT LIKE '' AND TXT15 NOT LIKE 'HOLD' THEN
                                TXT15 
                                WHEN TXT14 NOT LIKE '' AND TXT14 NOT LIKE 'HOLD' THEN
                                TXT14 
                                WHEN TXT13 NOT LIKE '' AND TXT13 NOT LIKE 'HOLD' THEN
                                TXT13 
                                WHEN TXT12 NOT LIKE '' AND TXT12 NOT LIKE 'HOLD' THEN
                                TXT12 
                                WHEN TXT11 NOT LIKE '' AND TXT11 NOT LIKE 'HOLD' THEN
                                TXT11 
                                WHEN TXT10 NOT LIKE '%   %' AND TXT10 NOT LIKE 'HOLD' THEN
                                TXT10 
                                WHEN TXT9 NOT LIKE '%   %' AND TXT9 NOT LIKE 'HOLD' THEN
                                TXT9 
                                WHEN TXT8 NOT LIKE '%   %' AND TXT8 NOT LIKE 'HOLD' THEN
                                TXT8 
                                WHEN TXT7 NOT LIKE '%   %' AND TXT7 NOT LIKE 'HOLD' THEN
                                TXT7 
                                WHEN TXT6 NOT LIKE '%   %' AND TXT6 NOT LIKE 'HOLD' THEN
                                TXT6 
                                WHEN TXT5 NOT LIKE '%   %' AND TXT5 NOT LIKE 'HOLD' THEN
                                TXT5 
                                WHEN TXT4 NOT LIKE '%   %' AND TXT4 NOT LIKE 'HOLD' THEN
                                TXT4 
                                WHEN TXT3 NOT LIKE '%   %' AND TXT3 NOT LIKE 'HOLD' THEN
                                TXT3 
                                WHEN TXT2 NOT LIKE '%   %' AND TXT2 NOT LIKE 'HOLD' THEN
                                TXT2 
                                WHEN TXT1 NOT LIKE '%   %' AND TXT1 NOT LIKE 'HOLD' THEN
                                TXT1 
                        END AS STAT,
                        B.*
                    FROM
                        M_STATUSPM B
                        RIGHT JOIN M_PMORDERH H ON B.AUFNR = H.AUFNR
                    WHERE
                        H.AUART = 'GA03' 
                        AND B.PRCTR LIKE 'GMFTC%' 
                        $filter
                        -- AND ( L.GETRI = '00000000' OR ( SUBSTRING ( L.GETRI, 5, 4 ) ) = '{$param['tahun']}' ) 
                        ),
                    FF AS ( SELECT DISTINCT
                            R.VBELN AS 'SALES_ORDER',
                            R.BSTNK AS 'PURCHASE_ORDER',
                            R.AUDAT AS 'RECEIVED_DATE',
                            R.KUNNR AS 'KUNNR',
                            R.NAME1 AS 'CUSTOMER'
                        FROM DD D1 LEFT JOIN M_SALESORDER R ON D1.KDAUF = R.VBELN
                        WHERE R.KUNNR LIKE '%{$kunnr}%' ),
                    C2 AS (
                        SELECT
                            MS.VBELN,
                            MS.VBELN_VBFA,
                            MS.ERDAT_VBFA,
                            MS.BWART,
                            ROW_NUMBER ( ) OVER ( PARTITION BY MS.VBELN ORDER BY MS.VBELN, MS.ERDAT_VBFA DESC ) AS RowNum1 
                        FROM
                            FF F1
                            LEFT JOIN 
                            M_SALESORDER MS
                            ON F1.SALES_ORDER = MS.VBELN 
                        WHERE
                            MS.BWART LIKE '601' 
                            AND MS.VBELN_VBFA LIKE '49%' 
                            AND MS.KUNNR LIKE '%{$kunnr}%' 
                            AND MS.VBTYP_N = 'R'
                        ),
                    CF AS ( SELECT * FROM C2 WHERE RowNum1 = 1 ),
                    CG AS (
                    SELECT DISTINCT
                        S.VBELN,
                        S.VBELN_BILL,
                        BL.RFBSK,
                        ROW_NUMBER ( ) OVER ( PARTITION BY S.VBELN_BILL ORDER BY S.VBELN_BILL, BL.RFBSK DESC ) AS RowNum2 
                    FROM
                        M_SALESORDER S
                        LEFT JOIN M_SALESBILLING BL ON S.VBELN_BILL = BL.VBELN 
                    WHERE
                        VBELN_BILL != '' 
                        AND S.KUNNR LIKE '%{$kunnr}%' 
                    ),
                    CD AS ( SELECT * FROM CG WHERE RowNum2 = 1 ),   
                    CC AS (
                    SELECT
                        F1.*,
                        C4.VBELN_VBFA,
                        C4.ERDAT_VBFA,
                        C4.BWART,
                        C3.VBELN_BILL,
                        C3.RFBSK 
                    FROM
                        FF F1 
                        LEFT JOIN CF C4 ON F1.SALES_ORDER = C4.VBELN
                        LEFT JOIN CD C3 ON C3.VBELN = C4.VBELN 
                    ),
                    AA AS (
                    SELECT
                        CASE
                                WHEN ( C.RFBSK IS NULL OR C.RFBSK != 'C' ) THEN
                                'Unpaid' ELSE 'Paid' 
                            END AS BILLING_STATUS,
                            D.*, C.*
                        FROM
                            DD D
                            LEFT JOIN CC C ON D.KDAUF = C.SALES_ORDER 
                        WHERE
                            $where_kunnr $where
                        ),
                    LY AS (
                        SELECT
                            *,
                        CASE
                            WHEN ERDAT_VBFA IS NOT NULL  THEN
                            'Delivered' 
                            WHEN STAT LIKE 'FRA' THEN
                            'Ready To Deliver Return As is Conditiion' 
                            WHEN STAT LIKE 'FSB' THEN
                            'Ready To Deliver Serviceable Condition' 
                            WHEN STAT LIKE 'F%' THEN
                            'Ready To Deliver BER' 
                            WHEN STAT LIKE 'WR' THEN
                            'Approval Received / Repair Started'
                            WHEN STAT LIKE 'UR' THEN
                            'Under Repair'
                            WHEN STAT LIKE 'SRMC' THEN
                            'Waiting Supply Materials From Customer'  
                            WHEN STAT LIKE 'SROA' THEN
                            'Quotation Provided / Awaiting Approval' 
                            ELSE 'Unit Under Inspection'
                        END AS TXT_STAT 
                            FROM
                                AA 
                        $whereWR
                     -- WHERE
                    -- ( TECO_DATE = '00000000' OR ( SUBSTRING ( TECO_DATE, 5, 4 ) ) = '{$param['tahun']}' ) 
                   -- (STAT!='WR')
                ),
                AH AS (SELECT *,  ROW_NUMBER() OVER ($order) as RowNum FROM LY WHERE AUART = 'GA03' $wherenoWR )
                SELECT count(*) as 'JUMLAH' FROM AH LEFT JOIN TC_M_PMORDER ON AH.MAINTENANCE_ORDER = TC_M_PMORDER.AUFNR
                WHERE AUART = 'GA03'  
                            ;";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_retail_where_id($ID_RETAIL)
    {
        $sql = "

            WITH BB AS (
                    SELECT DISTINCT
                        AUFNR,
                        KDAUF,
                        AUART,
                        ILART,
                        ERNAM,
                        ERDAT,
                        PHAS0,
                        PHAS1,
                        PHAS2,
                        PHAS3 
                    FROM
                        M_PMORDER 
                    WHERE
                        AUART = 'GA03' 
                        AND AUFNR LIKE $ID_RETAIL 
                    ),
                CC AS ( SELECT DISTINCT VBELN, VBELN_BILL, AUDAT, KUNNR, NAME1, BOLNR FROM BB B RIGHT JOIN M_SALESORDER S ON B.KDAUF = S.VBELN),
                DD AS (
                    SELECT DISTINCT
                            M_SALESORDER.KUNNR,
                            M_SALESORDER.NAME1
                    FROM
                        M_SALESORDER 
                        LEFT JOIN CC ON M_SALESORDER.VBELN = CC.VBELN
                    ),
                AA AS (
                    SELECT
                        V.NAME1 AS 'SHIPTOPARTY',
                        E.MATNR,
                        E.MAKTX,
                        CONVERT ( FLOAT, D.NETWR ) AS 'NETWR',
                        CONVERT ( FLOAT, D.MWSBK ) AS 'MWSBK',
                        D.WAERK,
                        (
                        CONVERT ( FLOAT, D.NETWR ) + CONVERT ( FLOAT, D.MWSBK )) AS 'TOTAL',
                    CASE
                        
                        WHEN ( D.RFBSK IS NULL OR D.RFBSK != 'C' ) THEN
                        'UNPAID' ELSE 'PAID' 
                        END AS BILLING_STATUS,
                        A.*,
                        C.*,
                        ROW_NUMBER () OVER ( ORDER BY D.RFBSK DESC ) AS ROWS 
                    FROM
                        M_PMORDERH E
                        LEFT JOIN BB A ON E.AUFNR = A.AUFNR
                        LEFT JOIN CC C ON E.KDAUF = C.VBELN
                        LEFT JOIN M_SALESBILLING D ON C.VBELN_BILL = D.VBELN
                        LEFT JOIN DD V ON D.KUNAG = V.KUNNR
                    WHERE
                        A.AUART = 'GA03' 
                        -- AND D.NETWR NOT LIKE '0.00'
                        AND A.AUFNR LIKE $ID_RETAIL

                    ) SELECT TOP 1
                    * 
                FROM
                AA ORDER BY VBELN_BILL DESC,ERNAM DESC;";
                // print_r($sql);die;
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getlistOutstanding($customer_id = "", $code_range = 0) {
        $kunnr = '';
        if ($this->session->userdata('log_sess_id_customer')) {
            $kunnr = $this->session->userdata('log_sess_id_customer');
            $where = " KUNAG LIKE '{$kunnr}' AND RFBSK = 'C' AND M_SALESORDER.AUART IN ('ZS01','ZC01','ZC03','ZC0W','ZCDS','ZP01','ZX01') AND M_SALESORDER.PRCTR LIKE 'GMFTC%' AND M_SALESORDER.WERKS IN ('WSAV','WSEM','WSWB','WSNC','GADC')";
        } elseif (!empty($customer_id)) {
            $where = " KUNAG LIKE '{$customer_id}' AND RFBSK = 'C' AND M_SALESORDER.AUART IN ('ZS01','ZC01','ZC03','ZC0W','ZCDS','ZP01','ZX01') AND M_SALESORDER.PRCTR LIKE 'GMFTC%' AND M_SALESORDER.WERKS IN ('WSAV','WSEM','WSWB','WSNC','GADC')";   
        }
        else 
        {
            $where = " RFBSK = 'C' AND M_SALESORDER.AUART IN ('ZS01','ZC01','ZC03','ZC0W','ZCDS','ZP01','ZX01') AND M_SALESORDER.PRCTR LIKE 'GMFTC%' AND M_SALESORDER.WERKS IN ('WSAV','WSEM','WSWB','WSNC','GADC')";        
        }

        // -----------------------------------------------------------

        $where_category = '';
        if(!empty($code_range)) {
            if($code_range == 1)
                $where_category = 'WHERE RANGE_DATE_NOW <= 30';
            elseif($code_range == 2)
                $where_category = 'WHERE RANGE_DATE_NOW > 30 AND RANGE_DATE_NOW <= 60';
            elseif($code_range ==3)
                $where_category = 'WHERE RANGE_DATE_NOW > 60 AND RANGE_DATE_NOW <= 90';
            else
                $where_category = 'WHERE RANGE_DATE_NOW > 90';
        }

        $sql = "
            SELECT
                *,
            CASE    
                WHEN RANGE_DATE_NOW <= 30 THEN
                1
                WHEN RANGE_DATE_NOW > 30 AND RANGE_DATE_NOW <= 60 THEN
                2
                WHEN RANGE_DATE_NOW > 60 AND RANGE_DATE_NOW <= 90 THEN
                2
                WHEN RANGE_DATE_NOW > 90 THEN
                4 
            END AS CATEGORY  
            FROM
                (
                SELECT
                    A.VBELN,
                    A.FKART,
                    A.KUNAG,
                    A.KUNRG,
                    A.FKDAT,
                    A.VKORG,
                    A.VTWEG,
                    A.FKTYP,
                    A.VBTYP,
                    A.RFBSK,
                    A.ERNAM,
                    A.NETWR,
                    A.WAERK,
                    A.MWSBK,
                    A.AUGBL,
                    A.AUGDT,
                    A.AEDAT,
                    DATEDIFF( DAY, CONVERT ( DATE, A.FKDAT, 104 ), CONVERT ( DATE, getdate( ), 104 ) ) AS RANGE_DATE_NOW,
                    CUSTOMER.COMPANY_NAME
                FROM
                    M_SALESBILLING A 
                    LEFT JOIN CUSTOMER ON CUSTOMER.ID_CUSTOMER = A.KUNAG
                    JOIN M_SALESORDER ON A.VBELN = M_SALESORDER.VBELN_BILL
                WHERE
                    $where
                ) ag
            {$where_category}
        ";
        return $this->db->query($sql)->result();
    }
}
