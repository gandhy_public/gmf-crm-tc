<?php
date_default_timezone_set("Asia/Jakarta");
class Contract_m extends CI_Model{

	private $tb_name = "TC_CONTRACT";
	private $tb_fleet = "TC_CONTRACT_FLEET";
	private $tb_pn = "TC_CONTRACT_PN";
	private $tb_deliv = "MS_DELIVERY_POINT";

	private $tb_master_aircraft = "MS_AIRCRAFT_REGISTRATION";
	private $tb_master_pn = "MS_PART_NUMBER";

	private $tb_customer = "CUSTOMER";

	var $column_order_pn = array(null,'ATA','PART_NUMBER','DESCRIPTION','ESS','MEL','TARGET_SLA');
    var $column_search_pn = array('ATA','PART_NUMBER','DESCRIPTION','ESS','MEL','TARGET_SLA');

    var $column_order_fleet = array(null,'REGISTRATION','TYPE','MSN','MFG_DATE');
    var $column_search_fleet = array('REGISTRATION','TYPE','MSN','MFG_DATE');
    var $order = array('ID' => 'ASC');

    var $column_order_deliv = array(null,'DELIVERY_POINT_CODE','DELIVERY_POINT_ADDRESS');
    var $column_search_deliv = array('DELIVERY_POINT_CODE','DELIVERY_POINT_ADDRESS');
    var $order_deliv = array('ID_DELIVERY_POINT' => 'ASC');

	var $column_order_ct = array(null,'TC_CONTRACT.CONTRACT_NUMBER','CUSTOMER.COMPANY_NAME','TC_CONTRACT.DELIVERY_POINT','TC_CONTRACT.DELIVERY_ADDRESS','TC_CONTRACT.DESCRIPTION');
    var $column_search_ct = array('TC_CONTRACT.CONTRACT_NUMBER','CUSTOMER.COMPANY_NAME','TC_CONTRACT.DELIVERY_POINT','TC_CONTRACT.DELIVERY_ADDRESS','TC_CONTRACT.DESCRIPTION');    
    var $order_ct = array('CUSTOMER.ID_CUSTOMER' => 'ASC');


	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function query($sql)
	{
		$data = $this->db->query($sql);
		return $data->result();
	}

	public function query_update($sql)
	{
		return $this->db->query($sql);
	}

	// Listing Other


	public function get_data_list($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_PN LIKE {$keyword} OR
				PN_NAME LIKE {$keyword} OR
				PN_DESC LIKE {$keyword}
			)";
		}
		$sql = " SELECT
					*
					FROM (
						SELECT ROW_NUMBER () OVER ( ORDER BY ID_PN ) AS RowNum, * FROM
						{$this->tb_name}
					) tb
					WHERE
						$where
						$rownum
				";
		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function get_data_list_table($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				CONTRACT_NUMBER LIKE {$keyword} OR
				ID_CUSTOMER LIKE {$keyword} OR
				DELIVERY_POINT LIKE {$keyword} OR
				DELIVERY_ADDRESS LIKE {$keyword} OR
				DESCRIPTION LIKE {$keyword}
			)";
		}
		$sql = " SELECT

					*
					FROM (
						SELECT ROW_NUMBER () OVER ( ORDER BY ID ) AS RowNum,
						a.ID AS ID_CONTRACT,
						a.CONTRACT_NUMBER,
						a.ID_CUSTOMER,
						a.ID_DELIVERY_POINT,
						a.DELIVERY_POINT,
						a.DELIVERY_ADDRESS,
						a.DESCRIPTION,
						b.COMPANY_NAME
						FROM
						{$this->tb_name} a JOIN {$this->tb_customer} b ON a.ID_CUSTOMER = b.ID_CUSTOMER
					) tb
					WHERE
						$where
						$rownum
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	public function get_detail_fleet($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				AIRCRAFT_REGISTRATION LIKE {$keyword} OR
				TYPE LIKE {$keyword} OR
				MSN LIKE {$keyword} OR
				MFN_GATE LIKE {$keyword}
			)";
		}
		$sql = " SELECT

								*
							FROM (
								SELECT ROW_NUMBER () OVER ( ORDER BY ID ) AS RowNum,
								a.*
								b.AIRCRAFT_REGISTRATION,
								FROM
								{$this->tb_fleet} a JOIN {$this->tb_master_aircraft} b ON a.AIRCRAFT_REGISTRATION = b.ID_AIRCRAFT_REGISTRATION
								WHERE
									ID_CONTRACT = {$param['ID_CONTRACT']}
							) tb
							WHERE
								$where
								$rownum
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	public function get_detail_part_number($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				PN_NAME LIKE {$keyword} OR
				ESS LIKE {$keyword} OR
				MEL LIKE {$keyword} OR
				TARGET_SLA LIKE {$keyword} OR
				ATA LIKE {$keyword}
			)";
		}
		$sql = " SELECT

								*
							FROM (
								SELECT ROW_NUMBER () OVER ( ORDER BY ID ) AS RowNum,
								a.*,
								b.PN_NAME,
								b.PN_DESC
								FROM
								{$this->tb_pn} a JOIN {$this->tb_master_pn} b ON a.ID_PN = b.ID_PN
								WHERE
									ID_CONTRACT = {$param['ID_CONTRACT']}
							) tb
							WHERE
								$where
								$rownum
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	// ./ Listing Other



	// Get Function JSON



	// Get Function JSON



	// Count Data


	// ./ Count Data



	// CRUD

	public function getDetailContract($id_contract){
		$sql = "
			SELECT 
				*
			FROM 
				{$this->tb_name} 
			WHERE
				ID = {$id_contract}
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	public function insert_contract($param){
		$data = array(
        	'CONTRACT_NUMBER' => $param['CONTRACT_NUMBER'],
        	'ID_CUSTOMER' => $param['ID_CUSTOMER'],
        	'DELIVERY_POINT' => $param['DELIVERY_POINT'],
        	'DELIVERY_ADDRESS' => $param['DELIVERY_ADDRESS'],
        	'DESCRIPTION' => $param['DESCRIPTION'],
        	'CREATE_AT' => date('Y-m-d'),
        	'IS_DELETE' => 0,
		);
		$result = $this->db->insert($this->tb_name, $data);

		return $this->db->insert_id();
	}

	function delete_list($ct_id)
	{
		$date = date('Y-m-d');
		$sql = "UPDATE TC_CONTRACT_PN SET UPDATE_AT = '$date', IS_DELETE = '1' WHERE ID_CONTRACT='$ct_id'";
		return $this->db->query($sql);
	}

	function delete_fleet($ct_id)
	{
		$date = date('Y-m-d');
		$sql = "UPDATE TC_CONTRACT_FLEET SET UPDATE_AT = '$date', IS_DELETE = '1' WHERE ID_CONTRACT='$ct_id'";
		return $this->db->query($sql);
	}

	function delete_deliv($ct_id)
	{
		$date = date('Y-m-d');
		$sql = "UPDATE MS_DELIVERY_POINT SET UPDATE_AT = '$date', IS_DELETE = '1' WHERE ID_CONTRACT='$ct_id'";
		return $this->db->query($sql);
	}

	public function insert_list($data,$ct_id,$file)
	{
		$sql = "INSERT INTO TC_CONTRACT_PN (ID_CONTRACT, ATA, PART_NUMBER, DESCRIPTION, ESS, MEL, TARGET_SLA, WAKTU_SLA, SATUAN_SLA, TARGET_UNSERVICEABLE_RETURN, SERVICE_LEVEL_CATEGORY, TARGET_SERVICE_LEVEL, NAMA_FILE, CREATE_AT, IS_DELETE) VALUES ";
		$date = date('Y-m-d');
		for($a=1;$a<count($data);$a++){
			if ($a != 1) $sql .= ", ";
			$sla 		= explode(" ", $data[$a][6]);
			if(count($sla) > 1){
				$waktu_sla 	= $sla[0];
				$satuan_sla = $sla[1];
			}else{
				$waktu_sla 	= NULL;
				$satuan_sla = NULL;	
			}

		    $sql .= "('{$ct_id}','{$data[$a][1]}','{$data[$a][2]}','{$data[$a][3]}','{$data[$a][4]}','{$data[$a][5]}','{$data[$a][6]}','{$waktu_sla}','{$satuan_sla}','{$data[$a][7]}','{$data[$a][8]}','{$data[$a][9]}','{$file}','{$date}','0')";
		}
		return $this->db->query($sql);
	}

	public function insert_fleet($data,$ct_id,$file)
	{
		$sql = "INSERT INTO TC_CONTRACT_FLEET (ID_CONTRACT, REGISTRATION, TYPE, MSN, MFG_DATE, NAMA_FILE, CREATE_AT, IS_DELETE) VALUES ";
		$date = date('Y-m-d');
		for($a=1;$a<count($data);$a++){
			if ($a != 1) $sql .= ", ";
			if(!($data[$a][4] instanceof DateTime)){
				$mfg_date = '0000-00-00';
			}else{
				$mfg_date = explode(" ", $data[$a][4]->format('Y-m-d H:i:s'));
				$mfg_date = $mfg_date[0];
			} 

		    $sql .= "('{$ct_id}','{$data[$a][1]}','{$data[$a][2]}','{$data[$a][3]}','{$mfg_date}','{$file}','{$date}','0')";
		}
		return $this->db->query($sql);
	}

	public function insert_deliv($data,$ct_id,$file)
	{
		$sql = "INSERT INTO MS_DELIVERY_POINT (ID_CONTRACT, DELIVERY_POINT_CODE, DELIVERY_POINT_ADDRESS, NAMA_FILE, CREATE_AT, IS_DELETE) VALUES ";
		$date = date('Y-m-d');
		for($a=1;$a<count($data);$a++){
			if ($a != 1) $sql .= ", "; 

		    $sql .= "('{$ct_id}','{$data[$a][1]}','{$data[$a][2]}','{$file}','{$date}','0')";
		}
		return $this->db->query($sql);
	}

	public function update_contract($param){


		$data = array(
        'PN_NAME' => $param['PN_NAME'],
        'PN_DESC' => $param['PN_DESC']
		);
		$this->db->where('ID_PN', $param['ID']);
		unset($param['ID']);
		$this->db->set($data);
		$result = $this->db->update($this->tb_name);

		return $result;
	}
	public function delete_contract($param){

		$this->db->where('ID_PN', $param['ID']);

		$result = $this->db->delete($this->tb_name);

		return $result;
	}


  // CRUD FLEET


  	public function insert_contract_fleet($param){
  		$data = array(
  			'ID_CONTRACT' => $param['ID_CONTRACT'],
  			'REGISTRATION' => $param['REGISTRATION'],
  			'TYPE' => $param['TYPE'],
          	'MSN' => $param['MSN'],
          	'MFG_DATE' => $param['MFG_DATE'],
          	'NAMA_FILE' => $param['NAMA_FILE'],
          	'CREATE_AT' => date('Y-m-d'),
          	'IS_DELETE' => 0
  		);
  		$result = $this->db->insert($this->tb_fleet, $data);

  		return $this->db->insert_id();
  	}

  // CRUD PART NUMBER


    public function insert_contract_pn($param){
      	$data = array(
      		'ID_CONTRACT' => $param['ID_CONTRACT'],
      		'ATA' => $param['ATA'],
      		'PART_NUMBER' => $param['PART_NUMBER'],
      		'DESCRIPTION' => $param['DESCRIPTION'],
      		'ESS' => $param['ESS'],
          	'MEL' => $param['MEL'],
          	'TARGET_SLA' => $param['TARGET_SLA'],
          	'WAKTU_SLA' => $param['WAKTU_SLA'],
          	'SATUAN_SLA' => $param['SATUAN_SLA'],
          	'NAMA_FILE' => $param['NAMA_FILE'],
          	'CREATE_AT' => date('Y-m-d'),
          	'IS_DELETE' => 0,
          	'TARGET_UNSERVICEABLE_RETURN' => $param['TARGET_UNSERVICEABLE_RETURN'],
          	'SERVICE_LEVEL_CATEGORY' => $param['SERVICE_LEVEL_CATEGORY'],
          	'TARGET_SERVICE_LEVEL' => $param['TARGET_SERVICE_LEVEL']
      	);
      	$result = $this->db->insert($this->tb_pn, $data);

      	return $this->db->insert_id();
    }

// CRUD DELIVERY POIN


  	public function insert_contract_deliv($param){
  		$data = array(
  			'ID_CONTRACT' => $param['ID_CONTRACT'],
  			'NAMA_FILE' => $param['NAMA_FILE'],
          	'CREATE_AT' => date('Y-m-d'),
          	'IS_DELETE' => 0,
          	'DELIVERY_POINT_CODE' => $param['DELIVERY_POINT_CODE'],
          	'DELIVERY_POINT_ADDRESS' => $param['DELIVERY_POINT_ADDRESS'],
  		);
  		$result = $this->db->insert($this->tb_deliv, $data);

  		return $this->db->insert_id();
  	}


// ========================================================

	private function _get_datatables_query($type)
    {
    	if ($type == 'pn') {
        	$this->db->from($this->tb_pn);

        	$cari = $this->column_search_pn;
    	}elseif($type == 'fleet'){
        	$this->db->from($this->tb_fleet);

        	$cari = $this->column_search_fleet;
    	}elseif($type == 'ct'){
    		$this->db->from($this->tb_name);
    		$this->db->join($this->tb_customer, 'TC_CONTRACT.ID_CUSTOMER = CUSTOMER.ID_CUSTOMER');
    		
    		$cari = $this->column_search_ct;
    	}elseif($type == 'deliv'){
    		$this->db->from($this->tb_deliv);
    		
    		$cari = $this->column_search_deliv;
    	}
 
        $i = 0;
     	foreach ($cari as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		if ($type == 'ct') {
	        		$order = $this->order_ct;
	            	$this->db->order_by(key($order), $order[key($order)]);
	        	}elseif ($type == 'deliv') {
	        		$order = $this->order_deliv;
	            	$this->db->order_by(key($order), $order[key($order)]);
	        	} else{
	        		$order = $this->order;
	            	$this->db->order_by(key($order), $order[key($order)]);
	        	}
        	}else{
        		if($type == 'pn'){
        			$this->db->order_by($this->column_order_pn[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        		}elseif($type == 'fleet'){
        			$this->db->order_by($this->column_order_fleet[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        		}elseif($type == 'ct'){
        			$this->db->order_by($this->column_order_ct[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        		}elseif($type == 'deliv'){
        			$this->db->order_by($this->column_order_deliv[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        		}
        	}
        } 
        else if(isset($this->order) && isset($this->order_ct) && isset($this->order_deliv))
        {
        	if ($type == 'ct') {
        		$order = $this->order_ct;
            	$this->db->order_by(key($order), $order[key($order)]);
        	}elseif ($type == 'deliv') {
        		$order = $this->order_deliv;
            	$this->db->order_by(key($order), $order[key($order)]);
        	} else{
        		$order = $this->order;
            	$this->db->order_by(key($order), $order[key($order)]);
        	}
        }
    }

	function get_datatables($id_contract,$type,$level_user)
    {
        $this->_get_datatables_query($type);
        if(!empty($id_contract)){
        	$this->db->where('ID_CONTRACT', $id_contract);
        }

        if($level_user == "4"){
        	$this->db->where('TC_CONTRACT.ID_CUSTOMER',$this->session->userdata('log_sess_id_customer'));
        }

        if($type == 'ct'){
        	$this->db->where('TC_CONTRACT.IS_DELETE', '0');
        }else{
        	$this->db->where('IS_DELETE', '0');
        }
        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }    
 
    function count_filtered($id_contract,$type)
    {
        $this->_get_datatables_query($type);
        if(!empty($id_contract)){
        	$this->db->where('ID_CONTRACT', $id_contract);
        }

        if($type == 'ct'){
        	$this->db->where('TC_CONTRACT.IS_DELETE', '0');
        }else{
        	$this->db->where('IS_DELETE', '0');
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($id_contract,$type)
    {
    	if ($type == 'pn') {
    		$this->db->from($this->tb_pn);
    		$this->db->where('ID_CONTRACT', $id_contract);
    		$this->db->where('IS_DELETE','0');
    	}elseif($type == "fleet"){
    		$this->db->from($this->tb_fleet);
    		$this->db->where('ID_CONTRACT', $id_contract);
    		$this->db->where('IS_DELETE','0');
    	}elseif($type == "ct"){
    		$this->db->from($this->tb_name);
    		$this->db->where('IS_DELETE','0');
    	}elseif($type == "deliv"){
    		$this->db->from($this->tb_deliv);
    		$this->db->where('ID_CONTRACT', $id_contract);
    		$this->db->where('IS_DELETE','0');
    	}
        return $this->db->count_all_results();
    }
}
