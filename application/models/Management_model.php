<?php
class Management_model extends CI_Model{

	private $tb_user = "TC_M_USER";
	private $tb_role = "TC_M_ROLE";
	private $tb_group = "TC_M_GROUP";
	private $tb_customer = "CUSTOMER";

	private $tb_menu = "TC_M_MENU";
	private $tb_modul = "TC_M_MODUL";
	private $tb_management = "TC_M_MANAGE_MENU";

	var $column_order = array(null,"TC_M_USER.nama","TC_M_USER.username","TC_M_USER.email","TC_M_ROLE.name","TC_M_GROUP.name","TC_M_USER.is_active");
    var $column_search = array("TC_M_USER.nama","TC_M_USER.username","TC_M_USER.email","TC_M_ROLE.name","TC_M_GROUP.name","TC_M_USER.is_active");    
    var $order = array("TC_M_USER.id" => 'ASC');

    var $column_order_group = array(null,"name","is_active");
    var $column_search_group = array("name","is_active");    
    var $order_group = array("id" => 'ASC');

    public function query($sql)
    {
    	return $this->db->query($sql)->result_array();
    }

    public function getRole()
    {
    	return $this->db->get("$this->tb_role")->result();
    }

    public function getGroup()
    {
    	return $this->db->get("$this->tb_group")->result();
    }

    public function getCustomer()
    {
    	return $this->db->get("$this->tb_customer")->result();
    }

    public function getMenu()
    {
    	$data = $this->db->select("$this->tb_menu.name as menu,$this->tb_menu.id as menu_id,$this->tb_modul.name as modul,$this->tb_modul.id as modul_id")
    					->from($this->tb_menu)
    					->join($this->tb_modul,"$this->tb_modul.id=$this->tb_menu.modul_id")
    					->where("$this->tb_menu.is_active","1")
    					->get();
    	return $data->result();
    }


    public function createUser($param)
    {
		return $this->db->insert($this->tb_user, $param);
    }

    public function creatGroup($param)
    {
		$this->db->insert($this->tb_group, $param);

		$create_at = $param['create_at'];
		$select_id = $this->db->select('id')
							->from($this->tb_group)
							->where('create_at',"$create_at")
							->get()
							->result_array();
		$select_id = $select_id[0]['id'];
		return $select_id;
    }

    public function creatManagement($param)
    {
    	for($a=0;$a<count($param);$a++){
    		$data = [
    			'group_id' => $param[$a]['group_id'],
    			'modul_id' => $param[$a]['modul_id'],
    			'menu_id' => $param[$a]['menu_id'],
    		];
    		$this->db->insert($this->tb_management, $data);
    	}
    	return true;
    }

    public function updateManagement($param,$id)
    {
    	$this->db->where('group_id', $id);
		$this->db->delete($this->tb_management);
    	for($a=0;$a<count($param);$a++){
    		$data = [
    			'group_id' => $id,
    			'modul_id' => $param[$a]['modul_id'],
    			'menu_id' => $param[$a]['menu_id'],
    		];
    		$this->db->insert($this->tb_management, $data);
    	}
    	return true;
    }

    public function updateUser($param,$id)
    {
		$this->db->set($param);
		$this->db->where('id', $id);
		return $this->db->update($this->tb_user);
    }

    public function updateGroup($param,$id)
    {
		$this->db->set($param);
		$this->db->where('id', $id);
		return $this->db->update($this->tb_group);
    }

    public function cekUser($username,$password)
    {
        $query = $this->db->get_where($this->tb_user, array('username' => $username,'password' => $password));
        return $query->result_array();
    }

    public function getDetailGroup($id)
    {
    	$select = [
    		"$this->tb_management.menu_id as menu_id",
    		"$this->tb_management.modul_id as modul_id",
    		"$this->tb_group.name as name",
    		"$this->tb_group.is_active as status",
    	];
    	$query = $this->db->select(implode(",", $select))
    					->from($this->tb_management)
    					->join($this->tb_group,"$this->tb_group.id = $this->tb_management.group_id")
    					->where("$this->tb_management.group_id","$id")
    					->get();
    	return $query->result();
    }


// ========================================================

	private function _get_datatables_query()
    {
    	$this->db->select("$this->tb_role.name,$this->tb_group.name,$this->tb_user.*");
		$this->db->from($this->tb_user);
		$this->db->join($this->tb_role, "$this->tb_role.id = $this->tb_user.role_id");
        $this->db->join($this->tb_group, "$this->tb_group.id = $this->tb_user.group_id");
 
        $i = 0;
     	foreach ($this->column_search as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		$order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
        	}else{
        		$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        	}
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_group_query()
    {
		$this->db->from($this->tb_group);
 
        $i = 0;
     	foreach ($this->column_search_group as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		$order = $this->order_group;
	            $this->db->order_by(key($order), $order[key($order)]);
        	}else{
        		$this->db->order_by($this->column_order_group[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        	}
        } 
        else if(isset($this->order_group))
        {
        	$order = $this->order_group;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

	function get_datatables()
    {
        $this->_get_datatables_query();

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_group()
    {
        $this->_get_datatables_group_query();

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }    
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_group()
    {
        $this->_get_datatables_group_query();
        
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->from($this->tb_user);
    	
        return $this->db->count_all_results();
    }

    public function count_all_group()
    {
    	$this->db->from($this->tb_group);
    	
        return $this->db->count_all_results();
    }
}
