<?php
class Customers_m extends CI_Model{

	private $tb_name = "V_Customer";
	private $column_order = [
        'customer' => ['ID_CUSTOMER','COMPANY_NAME']
    ];
    private $column_search = [
        'customer' => ['ID_CUSTOMER','COMPANY_NAME']
    ];
    private $order = [
        'customer' => ['ID_CUSTOMER' => 'ASC'],
    ];


	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}

	// Listing Other
	function get_list_customer($total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('customer');

        $sql = "
        WITH TEMP AS (
            SELECT
                *
            FROM
                CUSTOMER
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        //print('<pre>'.print_r($sql,TRUE).'</pre>');die();
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
            $query = explode("WHERE ROWID >", $sql);
            $query = $query[0];
            $result = $this->db->query($sql)->result();
            return array(
                'query'     => $query,
                'result'    => $result
            );
        }
    }

	public function get_data_list($param, $ext=null)
	{
		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}
		// print_r($param);

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_CUSTOMER LIKE {$keyword} OR
				COMPANY_NAME LIKE {$keyword} OR
				COMPANY_EMAIL LIKE {$keyword} OR
				COMPANY_ADDRESS LIKE {$keyword}
			)";
		}
		$sql = " SELECT
						*
					FROM (
						SELECT ROW_NUMBER () OVER ( ORDER BY ID_CUSTOMER ) AS RowNum, * FROM
						{$this->tb_name}
					) tb
				WHERE
					$where
					$rownum
						";
						print('<pre>'.print_r($sql,TRUE).'</pre>');
		//$query = $this->db->query($sql);
		// echo $this->db->last_query();

		//return $query->result_array();
	}

	public function get_customer_list()
	{
		$data = $this->db->get($this->tb_name);
		return $data->result();
	}

	public function get_customer($id_customer)
	{
		$data = $this->db->where(['ID_CUSTOMER' => $id_customer])->get($this->tb_name);
		return $data->result();
	}
	// ./ Listing Other



	// Get Function JSON



	// Get Function JSON



	// Count Data


	// ./ Count Data



	// CRUD

	private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }

}
