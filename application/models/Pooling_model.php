<?php
date_default_timezone_set("Asia/Jakarta");
class Pooling_model extends CI_Model{

	private $tb_name = "TC_TR_POOLING_ORDER";
	private $tb_contract = "TC_CONTRACT";
	private $tb_contract_fleet = "TC_CONTRACT_FLEET";
	private $tb_contract_pn = "TC_CONTRACT_PN";

	private $tb_master_pn = "MS_PART_NUMBER";
	private $tb_master_fleet = "MS_AIRCRAFT_REGISTRATION";
	private $ms_requirement_category = "TC_POOL_REQUIREMENT_CATEGORY";
	private $order_sort = array('ID_POOLING_ORDER' => 'asc');
	private $kolom = array('ID_POOLING_ORDER', 
						'REFERENCE',
						'REQUIREMENT_CATEGORY_NAME',
						'ID_POOLING_ORDER',
						'ID_PART_NUMBER',
						'DESCRIPTION',
						'DELIVERY_POINT_CODE',
						'ID_AIRCRAFT_REGISTRATION',
						'CUSTOMER_PO',
						'CUSTOMER_PO',
						'ORDER_REQUESTED_BY',
						'REQUEST_ORDER_DATE',
						'REQUESTED_DELIVERY_DATE',
						'STATUS_ORDER_NAME',
						'STATUS_ORDER',
						'SERVICE_STATUS',
						'UNSERVICE_STATUS');
	private $column_order = [
        'order' => [NULL],
        'email' => [NULL],
    ];
    private $column_search = [
        'order' => ['ID','STATUS_ORDER','STATUS_SERVICE','STATUS_UNSERVICE','REFERENCE','REQUIREMENT_CATEGORY_NAME','PART_NUMBER','PART_DESCRIPTION','DESTINATION','AC_REGISTRATION','CUSTOMER_ORDER','REQUEST_BY','DELIVERY_DATE','DELIVERY_TARGET','CREATE_AT'],
        'email' => ['REFERENCE','POOLING','DESTINATION'],
    ];
    private $order = [
        'order' => ['CREATE_AT' => 'DESC'],
        'email' => ['ID' => 'DESC'],
    ];

	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from('TC_TR_POOLING_ORDER');
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return count($query->result_array());
	}
	public function get_counter_tr()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return count($query->result_array());
	}
	public function get_counter_new()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		$this->db->where('STATUS_ORDER','1');
		$query = $this->db->get();
		return count($query->result_array());
	}



	// Listing Other

	public function get_contract_list($id)
	{
		if($id == "0" || $id == "" || $id == null){

    	}else{
    		$this->db->where('ID_CUSTOMER',$id);
    	}
    	$this->db->where('IS_DELETE',"0");
    	return $this->db->select('*')->from($this->tb_contract)->get()->result();
	}
	public function get_part_number_contract_description($id)
	{
		// $this->db->order_by('iduserrole', 'ASC');

		$this->db->where('PART_NUMBER', $id);
		return $this->db->get('TC_CONTRACT_PN')->row_object();
		// $data = $this->db->get('TC_CONTRACT_PN');
		// return $data->result();
	}
	// public function get_fleet_list_by_contract($id)
	// {
	// 	// $this->db->order_by('iduserrole', 'ASC');
	// 	$this->db->select($this->tb_contract_fleet.".REGISTRATION AS AIRCRAFT_REGISTRATION_NAME", 
	// 		$this->tb_contract_fleet.".ID");
	// 	$this->db->from($this->tb_contract_fleet);
	// 	$this->db->where('ID_CONTRACT', $id);
	// 	$this->db->where('IS_DELETE', 0);

	// 	$data = $this->db->get();
	// 	return $data->result();
	// }
	public function get_pn_list_by_contract($id)
	{
		$this->db->select($this->tb_contract_pn.".PART_NUMBER AS PART_NUMBER");
		$this->db->from($this->tb_contract_pn);
		// $this->db->join($this->tb_master_pn, $this->tb_master_pn.".ID_PN = ".$this->tb_contract_pn.".ID_PN");
		$this->db->where('ID_CONTRACT', $id);
		$data = $this->db->get();
		return $data->result();
	}
	public function get_part_number_list()
	{
		// $this->db->order_by('iduserrole', 'ASC');
		$data = $this->db->get('ms_part_number');
		return $data->result();
	}

	public function get_requirement_category_list()
	{
		$data = $this->db->get('TC_POOL_REQUIREMENT_CATEGORY');
		return $data->result();
	}

	public function get_aircraft_registration_list()
	{
		$data = $this->db->get('ms_aircraft_registration');
		return $data->result();
	}

	public function get_delivery_point_list()
	{
		$data = $this->db->get('ms_delivery_point');
		return $data->result();
	}

	public function get_all_status_order()
	{
		$data = $this->db->get('ms_status_order');
		return $data->result();
	}

	public function get_data_pooling($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		$fltr = '';
		$st = '';
		$s_st = '';
		$us_st = '';
		if (isset($param['st'])) {
            $st = "AND STATUS_ORDER LIKE '{$param['st']}'";
		}
        if (isset($param['us_st']))
        {
            $s_st = "AND UNSERVICE_STATUS LIKE '{$param['us_st']}'";
        }
        if (isset($param['s_st']))
        {
            $us_st = "AND SERVICE_STATUS LIKE '{$param['s_st']}'";
        }
        if ($this->session->userdata('log_sess_id_customer')) {
                $kunnr = $this->session->userdata('log_sess_id_customer');
                $where_kunnr = " ID_CUSTOMER LIKE '{$kunnr}'";
            }
        else {
        	$where_kunnr = " ID_CUSTOMER LIKE '%%'";
        }
		if (isset($param['search'])) {
            
            $keyword = "'%".strtolower($param['search']['value'])."%'";
        }

        if (isset($param['start']) and isset($param['end'])) {
            $rownum = " AND RowNum > {$param['start']}
                AND RowNum < {$param['end']}";
        }
         if (isset($param['order'])) // here order processing
        {
            $order = "ORDER BY {$this->kolom[$param['order']['0']['column']]} {$param['order']['0']['dir']}";
        } 
        else
        {
            $order = "ORDER BY ID_POOLING_ORDER ASC";
        }
        if (isset($param['column'])) // here order processing
        {
        	// print_r($param['column']);
        	$i = 0;
        	$ii = 0;
      		foreach ($param['column'] as $item){
      			// echo $this->kolom[$i];
	            if( ($param['column'][$i]['search']['value']) != '') // if datatable send POST for search
	            {
	            	if ($ii != 0) {
      					$fltr .= " OR";
      				}
      				else if ($ii == 0){
      					$fltr .= "AND (";
      				}
	               $fltr .= " ";
	               $fltr .= $this->kolom[$i];
	               $fltr .= " like '%{$param['column']["$i"]['search']['value']}%'";
	            $ii++;
	            }
	         $i++;
        	}
        }
        if ($fltr != '') {
        	$fltr .= ")";
        }
       	 // echo $fltr;

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_PART_NUMBER LIKE {$keyword} OR
				DESCRIPTION LIKE {$keyword} OR
				DELIVERY_POINT_CODE LIKE {$keyword} OR
				REQUESTED_DELIVERY_DATE LIKE {$keyword} OR
				REQUIREMENT_CATEGORY_NAME LIKE {$keyword} OR
				ID_AIRCRAFT_REGISTRATION LIKE {$keyword} OR
				CUSTOMER_PO LIKE {$keyword} OR
				STATUS_ORDER LIKE {$keyword} OR
				STATUS_ORDER_NAME LIKE {$keyword} OR
				REQUESTED_DELIVERY_DATE LIKE {$keyword}
			)";
		}
		$sql = " SELECT
						*
					FROM
						(
							SELECT
								ROW_NUMBER () OVER ($order) AS RowNum,
								REFERENCE,
								ID_PART_NUMBER,
								ID_POOLING_ORDER,
								DESCRIPTION,
								DELIVERY_POINT_CODE,
								-- REQUESTED_DELIVERY_DATE,
								REQUIREMENT_CATEGORY_NAME,
								ID_AIRCRAFT_REGISTRATION,
								CUSTOMER_PO,
								STATUS_ORDER,
								STATUS_ORDER_NAME,
								CONVERT(VARCHAR, REQUESTED_DELIVERY_DATE, 100) as REQUESTED_DELIVERY_DATE,
								CONVERT(VARCHAR, TARGET_DELIVERY, 100) as TARGET_DELIVERY,
								ORDER_REQUESTED_BY,
								SERVICE_STATUS,
								UNSERVICE_STATUS 
							FROM
								TC_TR_POOLING_ORDER 
								LEFT JOIN TC_POOL_REQUIREMENT_CATEGORY ON TC_POOL_REQUIREMENT_CATEGORY.ID_REQUIREMENT_CATEGORY = TC_TR_POOLING_ORDER.ID_REQUIREMENT_CATEGORY
								LEFT JOIN ms_status_order ON ms_status_order.ID_STATUS_ORDER = TC_TR_POOLING_ORDER.STATUS_ORDER
								LEFT JOIN ms_delivery_point ON ms_delivery_point.ID_DELIVERY_POINT = TC_TR_POOLING_ORDER.ID_DELIVERY_POINT
							WHERE $where_kunnr $st $s_st $us_st
						) tb
					WHERE
						$where
						$rownum
						$fltr
						
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	// ./ Listing Other



	// Get Function JSON

	public function get_part_number_description($ID_PN)
	{
		$this->db->where('PART_NUMBER', $ID_PN);
		$this->db->where('IS_DELETE', 0);
		return $this->db->get('TC_CONTRACT_PN')->row_object();
	}

	public function get_delivery_point_address($ID_DP)
	{
		$this->db->where('ID',$ID_DP);
		$this->db->where('IS_DELETE', 0);
        return $this->db->get('TC_CONTRACT')->row_object();
	}

	public function get_delivery_point($ID_CONTRACT)
	{
		$this->db->where('ID',$ID_CONTRACT);
		$this->db->where('IS_DELETE', 0);
        return $this->db->get('TC_CONTRACT')->result();
	}

	// Get Function JSON


	public function select_request_where_no_account($NO_ACCOUNT)
	{	
		// $sql = " SELECT O.*, C.COMPANY_NAME, S.STATUS_ORDER_NAME
		// 		 FROM"
		$sql = "SELECT
					*,
					CONVERT(VARCHAR, REQUESTED_DELIVERY_DATE, 100) as REQUESTED_DELIV_DATE,
					CONVERT(VARCHAR, TARGET_DELIVERY, 100) as DELIVERY_TARGET,
					CONVERT(VARCHAR, TARGET_UNSERV_RETURN, 100) as RETURN_TARGET
				FROM
					TC_TR_POOLING_ORDER 
					LEFT JOIN TC_POOL_REQUIREMENT_CATEGORY ON TC_POOL_REQUIREMENT_CATEGORY.ID_REQUIREMENT_CATEGORY = TC_TR_POOLING_ORDER.ID_REQUIREMENT_CATEGORY
					LEFT JOIN ms_status_order ON ms_status_order.ID_STATUS_ORDER = TC_TR_POOLING_ORDER.STATUS_ORDER
					LEFT JOIN ms_delivery_point ON ms_delivery_point.ID_DELIVERY_POINT = TC_TR_POOLING_ORDER.ID_DELIVERY_POINT
					LEFT JOIN CUSTOMER ON CUSTOMER.ID_CUSTOMER = TC_TR_POOLING_ORDER.ID_CUSTOMER
				WHERE TC_TR_POOLING_ORDER.ID_POOLING_ORDER = $NO_ACCOUNT";
		// $this->db->select('*');
		// $this->db->from('TC_TR_POOLING_ORDER');
		// $this->db->join('TC_POOL_REQUIREMENT_CATEGORY', 'TC_POOL_REQUIREMENT_CATEGORY.ID_REQUIREMENT_CATEGORY = TC_TR_POOLING_ORDER.ID_REQUIREMENT_CATEGORY','LEFT');
		// $this->db->join('ms_status_order', 'ms_status_order.ID_STATUS_ORDER = TC_TR_POOLING_ORDER.STATUS_ORDER','LEFT');
		// $this->db->join('ms_delivery_point', 'ms_delivery_point.ID_DELIVERY_POINT = TC_TR_POOLING_ORDER.ID_DELIVERY_POINT','LEFT');
		// $this->db->join('CUSTOMER', 'CUSTOMER.ID_CUSTOMER = TC_TR_POOLING_ORDER.ID_CUSTOMER','LEFT');
		// $this->db->where('TC_TR_POOLING_ORDER.ID_POOLING_ORDER', $NO_ACCOUNT);
				// $this->db->where('year(submited)=',date('Y'));
				// $this->db->where('month(submited)=',date('m'));

		$query = $this->db->query($sql);

		return $query->row_object();
	}

	public function insert_pooling($REFERENCE)
	{
		$insert = array(
			'REFERENCE' 			=> $REFERENCE
		);
		return $this->db->insert('tr_pooling', $insert);
	}

	public function insert_pooling_order($insert)
	{
		$this->db->insert('TC_TR_POOLING_ORDER', $insert);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	public function update_status($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}
	
	public function update_unserviceable($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}

	public function update_serviceable($id, $data)
	{
		
		$this->db->where('ID_POOLING_ORDER', $id);
		return $this->db->update('TC_TR_POOLING_ORDER', $data);
	}

	public function query($sql)
	{
		$data = $this->db->query($sql);
		return $data->result();
	}

	function create_pn($pn,$desc,$contract_id){
		$data = [
			'ID_CONTRACT' => $contract_id,
			'PART_NUMBER' => $pn,
			'DESCRIPTION' => $desc,
			'NAMA_FILE' => 'by form other',
			'CREATE_AT' => date('Y-m-d'),
			'IS_DELETE' => 0
		];
		$this->db->insert('TC_CONTRACT_PN_OTHER', $data);
		return $this->db->insert_id();
	}

	function create_ac($ac,$contract_id){
		$data = [
			'ID_CONTRACT' => $contract_id,
			'REGISTRATION' => $ac,
			'NAMA_FILE' => 'by form other',
			'CREATE_AT' => date('Y-m-d'),
			'IS_DELETE' => 0
		];
		$this->db->insert('TC_CONTRACT_FLEET_OTHER', $data);
		return $this->db->insert_id();
	}

	function create_desti($point,$address,$contract_id){
		$data = [
			'DELIVERY_POINT' => $point,
			'DELIVERY_ADDRESS' => $address,
			'CONTRACT_ID' => $contract_id
		];
		$this->db->insert('TC_CONTRACT_DESTINATION', $data);
		return $this->db->insert_id();
	}

	function create_order($data){
		$this->db->insert('TC_POOLING_ORDER', $data);
		return $this->db->insert_id();
	}

	function insert_email($data){
		return $this->db->insert('TC_SEND_EMAIL',$data);
	}

	function create_reference($order_id,$reference){
		$data = [
			'REFERENCE' => $reference
		];
		$this->db->where('ID', $order_id);
		return $this->db->update('TC_POOLING_ORDER', $data);
	}

	function get_list_order($st_order, $st_service, $st_unservice, $contract, $total = FALSE, $filter = FALSE){
        $param = $this->_get_datatables('order');

        if($st_order != ''){
        	$st_order = "= $st_order";
        }else{
        	$st_order = "!= ''";
        }

        if($st_service != ''){
        	$st_service = "= $st_service";
        }else{
        	$st_service = "!= ''";
        }

        if($st_unservice != ''){
        	$st_unservice = "= $st_unservice";
        }else{
        	$st_unservice = "!= ''";
        }

        if($contract != ''){
        	$contract = "= $contract";
        }else{
        	$contract = "!= ''";
        }

        $cust_id = $this->session->userdata('log_sess_id_customer');
        if($cust_id){
        	$where_customer = "AND A.CUSTOMER_ID='$cust_id'";
        }else{
        	$where_customer = "";
        }

        $sql = "
        WITH TEMP AS (
            SELECT
            	A.ID,
				A.STATUS_ORDER,
				A.STATUS_SERVICE,
				A.STATUS_UNSERVICE,
				A.REFERENCE,
				B.REQUIREMENT_CATEGORY_NAME,
				CASE
					WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
						(
							SELECT PART_NUMBER FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
						)
					ELSE
						A.PN_ID
				END AS PART_NUMBER,
				CASE
					WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
						(
							SELECT DESCRIPTION FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
						)
					ELSE
						A.COMPONENT_DESCRIPTION
				END AS PART_DESCRIPTION,
				CASE
					WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
						(
							SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
						)
					ELSE
						(
							SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
						)
				END AS DESTINATION,
				CASE
					WHEN SUBSTRING(A.AC_ID,1,2) = 'x_' THEN 
						(
							SELECT REGISTRATION FROM TC_CONTRACT_FLEET_OTHER WHERE TC_CONTRACT_FLEET_OTHER.ID = SUBSTRING(A.AC_ID,3,LEN(A.AC_ID)-2)
						)
					ELSE
						(
							SELECT REGISTRATION FROM TC_CONTRACT_FLEET WHERE TC_CONTRACT_FLEET.ID = A.AC_ID
						)
				END AS AC_REGISTRATION,
				A.CUSTOMER_ORDER,
				C.nama AS REQUEST_BY,
				A.CREATE_AT,
				A.DELIVERY_DATE,
				A.DELIVERY_TARGET,
				CASE
					WHEN A.SERVICE_ID IS NOT NULL THEN 
						(
							SELECT DELIVERY_DATE FROM TC_POOLING_ORDER_SERVICE WHERE TC_POOLING_ORDER_SERVICE.ID = A.SERVICE_ID
						)
					ELSE
						convert(varchar, '08-05-1998', 1)
				END AS DATE_SERVICE,
				CASE
					WHEN A.UNSERVICE_ID IS NOT NULL THEN 
						(
							SELECT DELIVERY_DATE FROM TC_POOLING_ORDER_UNSERVICE WHERE TC_POOLING_ORDER_UNSERVICE.ID = A.UNSERVICE_ID
						)
					ELSE
						convert(varchar, '08-05-1998', 1)
				END AS DATE_UNSERVICE
			FROM
				TC_POOLING_ORDER A
			JOIN
				TC_POOL_REQUIREMENT_CATEGORY B ON A.CATEGORY_ID = B.ID_REQUIREMENT_CATEGORY
			JOIN
				TC_M_USER C ON A.CREATE_BY = C.id
			WHERE 
				A.STATUS_ORDER $st_order AND
				A.STATUS_SERVICE $st_service AND
				A.STATUS_UNSERVICE $st_unservice AND
				A.CONTRACT_ID $contract
				$where_customer
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
						$query = explode("WHERE ROWID >", $sql);
						$query = $query[0];
						$result = $this->db->query($sql)->result();
						return array(
								'query'     => $query,
								'result'    => $result
						);
        }
    }

    function update_status_order($id_order, $id_status){
    	$this->db->set('STATUS_ORDER', $id_status);
    	$this->db->set('UPDATE_ORDER_AT', date('Y-m-d H:i:s'));
    	$this->db->set('UPDATE_ORDER_BY', $this->session->userdata('log_sess_id_user'));
		$this->db->where('ID', $id_order);
		return $this->db->update('TC_POOLING_ORDER');
    }

    function get_detail_order($order_id){
    	$query = "
    	SELECT
        	A.ID,
			A.STATUS_ORDER,
			A.STATUS_SERVICE,
			A.STATUS_UNSERVICE,
			A.REFERENCE,
			B.REQUIREMENT_CATEGORY_DESC,
			CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
					(
						SELECT PART_NUMBER FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
					)
				ELSE
					A.PN_ID
			END AS PART_NUMBER,
			CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
					(
						SELECT DESCRIPTION FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
					)
				ELSE
					A.COMPONENT_DESCRIPTION
			END AS PART_DESCRIPTION,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_POINT FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_POINT FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION_POINT,
			CASE
				WHEN SUBSTRING(A.AC_ID,1,2) = 'x_' THEN 
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET_OTHER WHERE TC_CONTRACT_FLEET_OTHER.ID = SUBSTRING(A.AC_ID,3,LEN(A.AC_ID)-2)
					)
				ELSE
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET WHERE TC_CONTRACT_FLEET.ID = A.AC_ID
					)
			END AS AC_REGISTRATION,
			A.CUSTOMER_ORDER,
			A.CUSTOMER_ID,
			D.COMPANY_NAME AS CUSTOMER_NAME,
			C.nama AS REQUEST_BY,
			A.DELIVERY_DATE,
			A.DELIVERY_TARGET,
			A.CREATE_AT,
			C.nama AS CREATE_BY,
			A.UPDATE_ORDER_AT AS CONFIRM_AT,
			CASE
				WHEN UPDATE_ORDER_BY IS NOT NULL THEN 
					(
						SELECT nama FROM TC_M_USER WHERE A.UPDATE_ORDER_BY = TC_M_USER.id
					)
				ELSE ''
			END AS CONFIRM_BY,
			A.ATA_CHAPTER,
			A.REMARK,
			A.CONTRACT_ID,
			A.SERVICE_ID,
			A.UNSERVICE_ID
		FROM
			TC_POOLING_ORDER A
		JOIN
			TC_POOL_REQUIREMENT_CATEGORY B ON A.CATEGORY_ID = B.ID_REQUIREMENT_CATEGORY
		JOIN
			TC_M_USER C ON A.CREATE_BY = C.id
		JOIN
			CUSTOMER D ON A.CUSTOMER_ID = D.ID_CUSTOMER
		WHERE
			A.ID = $order_id
    	";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function get_detail_service($order_id,$contract_id){
    	$query = "
    	SELECT
        	A.STATUS_SERVICE,
        	CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
					(
						SELECT PART_NUMBER FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
					)
				ELSE
					A.PN_ID
			END AS PART_NUMBER,
			CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
					'0 days'
				ELSE
					(
						SELECT TARGET_UNSERVICEABLE_RETURN FROM TC_CONTRACT_PN WHERE TC_CONTRACT_PN.PART_NUMBER = A.PN_ID AND TC_CONTRACT_PN.ID_CONTRACT = $contract_id AND IS_DELETE = 0
					)
			END AS TUR,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION,
			C.nama AS CREATE_BY,
			B.CREATE_AT,
			B.SHIPMENT_TYPE,
			B.PN_DELIVERED,
			B.SN_DELIVERED,
			B.AWB_NO,
			B.FLIGHT_NO,
			B.DELIVERY_DATE
		FROM
			TC_POOLING_ORDER A
		JOIN
			TC_POOLING_ORDER_SERVICE B ON A.SERVICE_ID = B.ID
		JOIN
			TC_M_USER C ON B.CREATE_BY = C.id
		WHERE
			A.ID = $order_id
    	";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function get_detail_unservice($order_id){
    	$query = "
    	SELECT
        	A.STATUS_UNSERVICE,
        	C.nama AS CREATE_BY,
        	B.CREATE_AT,
        	B.SEND_TYPE,
        	B.SHIPMENT_TYPE,
        	B.PN_REMOVED,
        	B.SN_REMOVED,
        	B.REMOVAL_DATE,
        	B.AC_REGISTRATION,
        	B.REMOVAL_REASON,
        	B.AWB_NO,
        	B.FLIGHT_NO,
        	B.REMARK,
        	B.REMOVAL_REASON_TYPE,
        	B.TSN,
        	B.CSN,
        	B.DELIVERY_DATE
		FROM
			TC_POOLING_ORDER A
		JOIN
			TC_POOLING_ORDER_UNSERVICE B ON A.UNSERVICE_ID = B.ID
		JOIN
			TC_M_USER C ON B.CREATE_BY = C.id
		WHERE
			A.ID = $order_id
    	";
    	$query = $this->db->query($query);
    	return $query->result_array();
    }

    function create_service($data){
		$this->db->insert('TC_POOLING_ORDER_SERVICE', $data);
		return $this->db->insert_id();
	}

	function update_service($data,$service_id){
		$this->db->where('ID', $service_id);
		return $this->db->update('TC_POOLING_ORDER_SERVICE', $data);
	}

	function create_unservice($data){
		$this->db->insert('TC_POOLING_ORDER_UNSERVICE', $data);
		return $this->db->insert_id();
	}

	function update_unservice($data,$unservice_id){
		$this->db->where('ID', $unservice_id);
		return $this->db->update('TC_POOLING_ORDER_UNSERVICE', $data);
	}

	function update_status_service($order_id,$service_id,$status){
		$this->db->set('STATUS_SERVICE', $status);
    	$this->db->set('SERVICE_ID', $service_id);
		$this->db->where('ID', $order_id);
		return $this->db->update('TC_POOLING_ORDER');
	}

	function update_status_unservice($order_id,$unservice_id,$status){
		$this->db->set('STATUS_UNSERVICE', $status);
    	$this->db->set('UNSERVICE_ID', $unservice_id);
		$this->db->where('ID', $order_id);
		return $this->db->update('TC_POOLING_ORDER');
	}

	function get_content_email_order($order_id)
	{
		$sql = "
		SELECT
			A.CUSTOMER_ORDER,
			CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN 
					(
						SELECT PART_NUMBER FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
					)
				ELSE
					A.PN_ID
			END AS PART_NUMBER,
			CASE
				WHEN SUBSTRING(A.PN_ID,1,2) = 'x_' THEN
					(
						SELECT DESCRIPTION FROM TC_CONTRACT_PN_OTHER WHERE TC_CONTRACT_PN_OTHER.ID = SUBSTRING(A.PN_ID,3,LEN(A.PN_ID)-2)
					)
				ELSE
					A.COMPONENT_DESCRIPTION
			END AS COMPONENT_DESCRIPTION,
			B.REQUIREMENT_CATEGORY_NAME,
			A.DELIVERY_DATE,
			CASE
				WHEN SUBSTRING(A.AC_ID,1,2) = 'x_' THEN 
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET_OTHER WHERE TC_CONTRACT_FLEET_OTHER.ID = SUBSTRING(A.AC_ID,3,LEN(A.AC_ID)-2)
					)
				ELSE
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET WHERE TC_CONTRACT_FLEET.ID = A.AC_ID
					)
			END AS AC_REGISTRATION,
			A.DELIVERY_TARGET,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION,
			C.NAMA
		FROM
			TC_POOLING_ORDER A
		JOIN TC_POOL_REQUIREMENT_CATEGORY B ON A.CATEGORY_ID = B.ID_REQUIREMENT_CATEGORY
		JOIN TC_M_USER C ON A.CREATE_BY = C.id
		WHERE
			A.ID = $order_id
		";
		return $this->db->query($sql)->result_array();
	}

	function get_content_email_service($order_id)
	{
		$sql = "
		SELECT
			A.CUSTOMER_ORDER,
			CASE
				WHEN SUBSTRING(A.AC_ID,1,2) = 'x_' THEN 
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET_OTHER WHERE TC_CONTRACT_FLEET_OTHER.ID = SUBSTRING(A.AC_ID,3,LEN(A.AC_ID)-2)
					)
				ELSE
					(
						SELECT REGISTRATION FROM TC_CONTRACT_FLEET WHERE TC_CONTRACT_FLEET.ID = A.AC_ID
					)
			END AS AC_REGISTRATION,
			B.PN_DELIVERED,
			B.SN_DELIVERED,
			B.AWB_NO,
			B.FLIGHT_NO,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION,
			B.DELIVERY_DATE
		FROM
			TC_POOLING_ORDER A
		JOIN TC_POOLING_ORDER_SERVICE B ON A.SERVICE_ID = B.ID
		WHERE
			A.ID = $order_id
		";
		return $this->db->query($sql)->result_array();
	}

	function get_content_email_unservice($order_id)
	{
		$sql = "
		SELECT
			A.CUSTOMER_ORDER,
			B.AC_REGISTRATION,
			B.PN_REMOVED AS PN_DELIVERED,
			B.SN_REMOVED AS SN_DELIVERED,
			B.AWB_NO,
			B.FLIGHT_NO,
			CASE
				WHEN SUBSTRING(A.DESTINATION_ID,1,2) = 'x_' THEN
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT_DESTINATION WHERE TC_CONTRACT_DESTINATION.ID = SUBSTRING(A.DESTINATION_ID,3,LEN(A.DESTINATION_ID)-2)
					)
				ELSE
					(
						SELECT DELIVERY_ADDRESS FROM TC_CONTRACT WHERE TC_CONTRACT.ID = A.DESTINATION_ID
					)
			END AS DESTINATION,
			B.DELIVERY_DATE
		FROM
			TC_POOLING_ORDER A
		JOIN TC_POOLING_ORDER_UNSERVICE B ON A.UNSERVICE_ID = B.ID
		WHERE
			A.ID = $order_id
		";
		return $this->db->query($sql)->result_array();
	}

	function get_list_email($st_email,$total = FALSE, $filter = FALSE){
		if($st_email == ""){
			$where = "";
		}else{
			$where = "
			WHERE A.STATUS = '$st_email'
			";
		}
        $param = $this->_get_datatables('email');

        $sql = "
        WITH TEMP AS (
            SELECT
            	A.STATUS,
            	B.REFERENCE,
            	A.POOLING,
            	A.DESTINATION,
            	A.ID
			FROM
				TC_SEND_EMAIL A
			JOIN
				TC_POOLING_ORDER B ON A.POOLING_ORDER_ID = B.ID
			$where
        ),
        X AS ( 
            SELECT 
                ROW_NUMBER ( ) OVER ( ORDER BY  ".$param['order']." ) AS ROWID,
                *
            FROM TEMP 
            ".$param['where']."
        ) 
        SELECT * FROM X
        WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."
        ";
        
        if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }elseif($filter){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return count($this->db->query($sql)->result_array());
        }else{
            return $this->db->query($sql)->result();
        }
    }

    function get_customer($id_customer){
    	if($id_customer == "0" || $id_customer == "" || $id_customer == null){

    	}else{
    		$this->db->where('ID_CUSTOMER',$id_customer);
    	}
    	return $this->db->select('*')->from('CUSTOMER')->get()->result();
    }

    function cek_customer_order($customer_order,$contract_id){
    	$query = $this->db->get_where('TC_POOLING_ORDER', array('CONTRACT_ID'=>$contract_id,'CUSTOMER_ORDER' => $customer_order))->result_array();
    	if(count($query) > 0){
    		return FALSE;
    	}else{
    		return TRUE;
    	}
    }



    /*================================================================================================================*/

    private function _get_datatables($type){
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
    
        $i = 0;$a = 0;
        if($_REQUEST['search']['value']){
            foreach ($column_search as $item){
                if($_REQUEST['search']['value']) {  
                    if($i===0) {
                        //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_REQUEST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_REQUEST['search']['value']);
                    }
                    //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
                }
                $i++;
            }
        }else{
            $like_arr = [];
            foreach ($_REQUEST['columns'] as $key) {
                if($key['search']['value']){
                    $index = $column_search[$a-2];
                    $like_arr[$index] = $key['search']['value'];
                    /*if($a===0) {
                        $this->db->like($column_search[$a-2], $key['search']['value']);
                    } else {
                        $this->db->or_like($column_search[$a-2], $key['search']['value']);
                    }*/
                }
                $a++;
            }
            $this->db->like($like_arr);
        }
      
        if(isset($_REQUEST['order'])) {
            if($_REQUEST['order']['0']['column'] == 0) {
                $this->db->order_by(key($order), $order[key($order)]);
            }else{
                $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
            }
        } else if(isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        
        $order = $pecah[1];
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];

        return array(
            'start'       => $start,
            'end'         => $end,
            'order'       => $order,
            'where'       => $where
        );
    }
}
