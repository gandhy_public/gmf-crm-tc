<?php
date_default_timezone_set("Asia/Jakarta");
Class Login2_model extends CI_Model {

    private $tb_user = 'TC_M_USER';
    private $tb_role = 'TC_M_ROLE';
    private $tb_group = 'TC_M_GROUP';
    private $tb_modul = 'TC_M_MODUL';
    private $tb_menu = 'TC_M_MENU';
    private $tb_manage_menu = 'TC_M_MANAGE_MENU';
    private $tb_customer = 'CUSTOMER';

    public function checkLdap($username, $password)
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ip_ldap = [
            '0' => "192.168.240.57",
            '1' => "172.16.100.46"
        ];
        for($a=0;$a<count($ip_ldap);$a++){
            $ldapconn = ldap_connect($ip_ldap[$a]);
            if($ldapconn){
                break;
            }else{
                continue;
            }
        }
        
        //$ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            // print("<pre>".print_r(@$info['count'],true)."</pre>");
            // print("<pre>".print_r(@$info,true)."</pre>");
            // print("<pre>".print_r(@$infomail,true)."</pre>");
            // print("<pre>".print_r(@$usermail,true)."</pre>");
            // print("<pre>".print_r(@$bind,true)."</pre>");
            //die();


            if(@$info['count']){
                if ((@$info[0]["samaccountname"][0] == $username AND $bind)) {
                    return true;
                } else {
                    return false;
                }
            }else{
                return false;
            }
        } else {
            echo "LDAP Connection trouble, please try again 2/3 time";
        }
    }

    public function cekUser($username,$password)
    {
        try {
            $cekUserUsingLdap = $this->db->select("PASSWORD")
                                        ->from($this->tb_user)
                                        ->where("USERNAME",$username)
                                        ->get()->result_array();
            if($cekUserUsingLdap[0]['PASSWORD'] == 'using ldap'){
                $cek_ldap = $this->checkLdap($username,$password);
                if(!$cek_ldap) throw new Exception("Data User LDAP Tidak Ada!", 1);
                $password = $cekUserUsingLdap[0]['PASSWORD'];
            }

            $select = [
                "$this->tb_user.id AS user_id",
                "$this->tb_user.username AS user_username",
                "$this->tb_user.nama AS user_nama",
                "$this->tb_user.email AS user_email",
                "$this->tb_user.is_active AS user_aktif",
                "$this->tb_role.id AS user_role_id",
                "$this->tb_role.name AS user_role",
                "$this->tb_role.level AS user_role_level",
                "$this->tb_group.id AS user_group_id",
                "$this->tb_group.name AS user_group",
                "$this->tb_group.is_active AS user_group_aktif",
                "$this->tb_user.customer_id AS user_customer_id"
            ];
            $cek_user = $this->db->select(implode(",", $select))
                                ->from($this->tb_user)
                                ->join($this->tb_role,"$this->tb_role.id=$this->tb_user.role_id")
                                ->join($this->tb_group,"$this->tb_group.id=$this->tb_user.group_id")
                                ->where(array(
                                    'username'=>$username,
                                    'password'=>$password,
                                    "$this->tb_user.is_active"=>1,
                                    "$this->tb_group.is_active"=>1
                                ))
                                ->get()->result();
            if(!$cek_user)throw new Exception("Data user tidak ada!", 1);
            return [
                'codestatus' => 'S',
                'message' => 'Sukses',
                'resultdata' => [
                    $cek_user[0]
                ]
            ];
        } catch (Exception $e) {
            return [
                'codestatus' => 'E',
                'message' => $e->getMessage(),
                'resultdata' => []
            ];
        }
    }

    public function updateLastLogin($id)
    {
        $date = date('Y-m-d H:i:s');
        $this->db->set('last_login', "$date");
        $this->db->where('id', "$id");
        return $this->db->update("$this->tb_user");
    }

    public function getMenu($group_id)
    {
        $select = [
            "$this->tb_group.name AS GROUP_NAME",
            "$this->tb_modul.name AS MODUL_NAME",
            "$this->tb_modul.logo AS MODUL_LOGO",
            "$this->tb_modul.active AS ACTIVE",
            "$this->tb_menu.name AS MENU_NAME",
            "$this->tb_menu.logo AS MENU_LOGO",
            "$this->tb_menu.url AS MENU_URL",
        ];
        $menu = $this->db->select(implode(",", $select))
                        ->from($this->tb_manage_menu)
                        ->join($this->tb_group,"$this->tb_group.id=$this->tb_manage_menu.group_id")
                        ->join($this->tb_modul,"$this->tb_modul.id=$this->tb_manage_menu.modul_id")
                        ->join($this->tb_menu,"$this->tb_menu.id=$this->tb_manage_menu.menu_id")
                        ->where("$this->tb_manage_menu.group_id",$group_id)
                        ->get()->result();
        $no=0;
        foreach ($menu as $key) {
            
            $menu_arr[$key->MODUL_NAME."=".$key->MODUL_LOGO."=".$key->ACTIVE][] = [
                'm' => $key->MENU_NAME."=".$key->MENU_URL,
                'l' => $key->MENU_LOGO,
            ];

        }
        return $menu_arr;
    }

}
?>
