<?php
class PartNumber_m extends CI_Model{

	private $tb_name = "MS_PART_NUMBER";


	public function get_counter()
	{
		$this->db->select('*');
		$this->db->from($this->tb_name);
		// $this->db->where('year(submited)=',date('Y'));
		$query = $this->db->get();
		return $query->num_rows();
	}

	// Listing Other


	public function get_data_list($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				ID_PN LIKE {$keyword} OR
				PN_NAME LIKE {$keyword} OR
				PN_DESC LIKE {$keyword}
			)";
		}
		$sql = " SELECT

								*
							FROM (
								SELECT ROW_NUMBER () OVER ( ORDER BY ID_PN ) AS RowNum, * FROM
								{$this->tb_name}
							) tb
							WHERE
								$where
								$rownum
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}
	// ./ Listing Other



	// Get Function JSON



	// Get Function JSON



	// Count Data


	// ./ Count Data



	// CRUD

	public function insert_part_number($param){


		$data = array(
        'PN_NAME' => $param['PN_NAME'],
        'PN_DESC' => $param['PN_DESC']
		);

		$result = $this->db->insert($this->tb_name, $data);

		return $result;
	}
	public function update_part_number($param){


		$data = array(
        'PN_NAME' => $param['PN_NAME'],
        'PN_DESC' => $param['PN_DESC']
		);
		$this->db->where('ID_PN', $param['ID']);
		unset($param['ID']);
		$this->db->set($data);
		$result = $this->db->update($this->tb_name);

		return $result;
	}
	public function delete_part_number($param){

		$this->db->where('ID_PN', $param['ID']);

		$result = $this->db->delete($this->tb_name);

		return $result;
	}

}
