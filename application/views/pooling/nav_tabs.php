        <ul class="nav nav-tabs">
         <!--  <li <?php //if($this->uri->segment(2)=='dashboard') { echo 'class="active"'; } ?>><a class="fa fa-dashboard" href="<?php //echo base_url(); ?>index.php/Pooling/dashboard"> Dashboard</a></li> -->
          <li <?php if($this->uri->segment(2)=='list_order') { echo 'class="active"'; } ?>><a class="fa fa-list" href="<?php echo base_url(); ?>index.php/Pooling/list_order"> List Order</a></li>
          <?php if($this->uri->segment(2)=='update_order') { ?><li class="active"><a class="fa fa-edit" href="<?php echo base_url(); ?>index.php/Pooling/all_request_order"> Update Order</a></li><?php } ?>
          <?php if ($this->session->userdata('log_sess_id_user_role')==3) {
          ?>
          <li <?php if($this->uri->segment(2)=='create_order') { echo 'class="active"'; } ?>><a class="fa fa-edit" href="<?php echo base_url(); ?>index.php/Pooling/create_order"> Create Order</a></li>
        <?php }else { ?>
          <li <?php if($this->uri->segment(2)=='Contract') { echo 'class="active"'; } ?>><a class="fa fa-pencil" href="<?php echo base_url(); ?>index.php/Contract"> Contract</a></li>
        <?php } ?>
        <li <?php if($this->uri->segment(2)=='list_contract') { echo 'class="active"'; } ?>><a class="fa fa-list" href="<?php echo base_url(); ?>index.php/Contract/list_contract"> list Contract</a></li>
          <!-- <li <?php //if($this->uri->segment(2)=='report') { echo 'class="active"'; } ?>><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/Pooling/report/"> Report</a></li> -->
          <!-- <li class="dropdown">
            <a class="dropdown-toggle fa fa-bar-chart" data-toggle="dropdown" href="#tab_report"> Report <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/Pooling/report_serviceable_level/">Serviceable Level</a></li>
              <li role="presentation"><a class="fa fa-bar-chart" href="<?php //echo base_url(); ?>index.php/Pooling/report_unserviceable_level/">Unserviceable Level</a></li>
            </ul>
          </li> -->
          <li class="dropdown <?php if($this->uri->segment(2)=='report_serviceable_level' || $this->uri->segment(2)=='report_unserviceable_level') { echo 'active'; } ?>">
            <a class="dropdown-toggle fa fa-bar-chart" data-toggle="dropdown" href="#tab_configuration"> Report <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation" <?php if($this->uri->segment(2)=='report_serviceable_level') { echo 'class="active"'; } ?>><a class="fa fa-bar-chart" href="<?php echo base_url(); ?>index.php/Pooling/report_serviceable_level/"> Serviceable Level</a></li>
              <li role="presentation" <?php if($this->uri->segment(2)=='report_unserviceable_level') { echo 'class="active"'; } ?>><a class="fa fa-bar-chart" href="<?php echo base_url(); ?>index.php/Pooling/report_unserviceable_level/"> Unserviceable Level</a></li>
            </ul>
          </li>
          <?php if ($this->session->userdata('log_sess_id_user_role')==1) {
          ?>
          <li class="dropdown">
            <a class="dropdown-toggle fa fa-wrench" data-toggle="dropdown" href="#tab_configuration"> Configuration <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li role="presentation"><a class="fa fa-cubes" href="<?php echo base_url(); ?>index.php/master/Part"> Part Number</a></li>
              <li role="presentation"><a class="fa fa-plane" href="<?php echo base_url(); ?>index.php/master/Aircraft"> Aircraft Registration</a></li>
              <li role="presentation"><a class="fa fa-sitemap" href="<?php echo base_url(); ?>index.php/master/ReqCategory"> Requirement Category</a></li>
              <!-- <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/Users/list_user"> User</a></li> -->
              <li role="presentation"><a class="fa fa-users" href="<?php echo base_url(); ?>index.php/Customers"> Customers</a></li>
            </ul>
          </li>
        <?php } ?>

        </ul>
