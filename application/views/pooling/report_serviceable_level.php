<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <!-- <?php //$this->load->view($nav_tabs); ?> -->

        <div class="tab-content">
          <!-- /.tab-pane New Order-->    
          <!-- <div class="tab-pane" id="tab_new_order"> -->
            
            <legend>Filters</legend>
            <form id="filter_serviceable" class="form-horizontal">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Date From</label>
                        <div class="col-sm-7">
                          <!-- <input id="start_date" type="date" class="form-control" name="REQUESTED_DELIVERY_DATE"> -->
                          <input class="form-control" id="start_date" name="start_date" placeholder="YYYY/MM/DD" type="text"/>
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Date To</label>
                        <div class="col-sm-7">
                          <!-- <input id="end_date" type="date" class="form-control" name="REQUESTED_DELIVERY_DATE">  -->  
                          <input class="form-control" id="end_date" name="end_date" placeholder="YYYY/MM/DD" type="text"/>
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_CUSTOMER" name="ID_CUSTOMER" style="width: 100%;" required>
                            <option value="" disabled selected>Search For Customer</option>
                            <?php
                              foreach ($customer_list as $row) {
                            ?>
                            <option value="<?php echo $row->ID_CUSTOMER ?>"><?php echo $row->COMPANY_NAME; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>                            
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Contract</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_CONTRACT" name="ID_CONTRACT" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="box-footer">
                <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                <button type="submit" id="filter" name="filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Show Report</button>
              </div><!-- /.box-footer -->           
            </form>
          
          </div>
      </div>
    </div>
  </div>
  <div class="row" id="result" style="display: none">
    <div class="col-md-7">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">CHART</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>

        <div class="box-body">
          <!-- <div class="chart-"> -->
             <!-- <div id="serviceable-chart" style="height:300px; width:100%"></div> -->
          <!-- </div> -->
          <div class="card-content" style="height: 300px">
            <canvas id="serviceable-chart"></canvas>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-5">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">LIST</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-sm-12">
          <!-- style="width:670px; margin:0 auto;"          -->
            <table id="serviceable-table" class="table table-bordered table-striped" style="width: 100%;">
              <thead>
                <tr>
                  <th>Serviceable Category</th>
                  <th>Target SLA</th>
                  <th>Ontime</th>
                  <th>Delayed</th>
                  <th>Total</th>
                  <th>Achievement</th>
                  <th>Target</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>

        </div>
      </div>
    </div>
         
  </div>
  <div class="row">
    <div id="empty_result" style="font-size: 24px;text-align: center;margin: 20px 0;display:none">No data to display</div>
  </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
<!-- <script src="<?= base_url('assets/plugins/highcharts/highcharts.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/highcharts/grouped-categories.js'); ?>"></script> -->

<script type="text/javascript">
  $(document).ready(function(){
    var start_date=$('#start_date'); 
    var end_date=$('#end_date');
    var options={
      format: 'yyyy/mm/dd',
      //container: container,
      todayHighlight: true,
      autoclose: true,
    };
    start_date.datepicker(options);
    end_date.datepicker(options);

    $('#ID_CUSTOMER').select2({
      width: '100%',
      <?php if(count($customer_list) > 1){?>
        placeholder: 'Select Customer',
        minimumInputLength: 2,
        triggerChange: true,
        allowClear: true,
        ajax: {
          url: "<?=base_url()?>index.php/api/Contract/SelectCustomer",
          dataType: 'json',
          delay: 450,
          processResults: function (data) {
            return {
              results: data
            }
          },
          cache: true
        }
      <?php }?>
    });

    $('#ID_CUSTOMER').change(function(){
      var cust = $(this).val();
      $('#ID_CONTRACT').empty();
      $.ajax({
        url: '<?= base_url('index.php/api/Pooling/get_contract_list'); ?>',
        type: 'GET',
        data: {
          customer: cust
        },
        success: function(resp) {
          resp = JSON.parse(resp);
          if(resp.body){
            $('#ID_CONTRACT').select2({
              data: resp.body,
              disabled: false,
              allowClear: true
            });
          }else{
            $('#ID_CONTRACT').select2({
              placeholder: 'No data contract',
              data: [{id: '', text: 'No data contract'}],
              disabled: true,
              allowClear: true
            });
          }
        }
      });
    });

    // if ($('#ID_CONTRACT').data('select2')) {
    //    $('#ID_CONTRACT').select2('destroy');
    // }
    // $('#ID_CONTRACT').select2({
    //   data: [
    //     {id: '1', text: 'tes1'},
    //     {id: '2', text: 'tes2'}
    //   ]
    // });
    // var sampleArray = [{id:0,text:'enhancement'}, {id:1,text:'bug'}
    //                    ,{id:2,text:'duplicate'},{id:3,text:'invalid'}
    //                    ,{id:4,text:'wontfix'}];
    // $("#ID_CONTRACT").select2({ data: sampleArray });
  });
  // $(document).ready(function(){
    var date = new Date();
    // var firstDay = Date.today()
    $('#start_date').val("<?= $start_date?>");
    $('#end_date').val("<?= $end_date?>");
  // });
</script>

<!-- <script>
  $(document).ready(function() {
    var serviceableChartTarget = document.getElementById('serviceable-chart');

    var serviceableChart = new Highcharts.Chart({
      chart: {
        renderTo: serviceableChartTarget,
        type: "column"
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [
          {
            name: 'Category',
            categories: ['Target', 'Actual']
          }
        ],
        labels: {
          enabled: true,
          style: {
            fontSize: '10px',
            width: 5
          },
          groupedOptions: [{
            rotation: 0, 
            align: 'center',
            tickColor: '#eaeaea',
          }],
          rotation: 0,
          tickColor: '#eaeaea',
        },
      },
      yAxis: {
        title: {
          text: ''
        },
        tickColor: '#ddd'
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          stacking: 'normal'
        }
      },
      series: [
        {
          name: 'Target',
          type: 'column',
          color: '#0065FF',
          data: [100, 0]
        },
        {
          name: 'Actual',
          type: 'column',
          color: '#13CE66',
          data: [0, 70]
        }
      ]
    });

    // serviceableChart.addSeries({
    //   name: 'Target',
    //   type: 'column',
    //   color: '#0065FF',
    //   data: ['100', '0']
    // });

    // serviceableChart.addSeries({
    //   name: 'Actual',
    //   type: 'column',
    //   color: '#13CE66',
    //   data: ['0', '70']
    // });
  });
</script> -->

<script>
  function get_percentage(value, total) {
    return total > 0 ? Number(((value / total) * 100).toFixed(2)) + '%' : '0%';
  }

  $(document).ready(function() {
    var serviceableChartTarget = document.getElementById("serviceable-chart");
    var serviceableChartData = {
      labels: ["Category"],
      datasets: [
        // {
        //   label: "Target Serviceable",
        //   backgroundColor: "#0065FF",
        //   data: [0],
        // }, 
        {
          label: "Actual Serviceable",
          backgroundColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
          data: [0],
        }
      ]
    }

    var serviceableChart = new Chart(serviceableChartTarget, {
      type: 'bar',
      data: serviceableChartData,
      options: {
        cornerRadius: 4,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              // userCallback: function(label, index, labels) {
              //   if (Math.floor(label) === label) return label;
              // },
              max: 100,
              callback: function(value, index, values) {
                return value + "%";
              }
            },
            gridLines: {
              zeroLineColor: '#ddd',
              color: 'rgba(234, 234, 234, .5)'
            }
          }],
          xAxes: [{
            barPercentage: 0.4,
            categoryPercentage: 0.6,
            gridLines: {
              zeroLineColor: '#eaeaea',
              color: 'rgba(234, 234, 234, .5)'
            }
          }]
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var type = data.datasets[tooltipItem.datasetIndex].label + ': ';

              var label = tooltipItem.yLabel ;
              if (label) label += '%';
              // label += Math.round(tooltipItem.yLabel * 100) / 100;
              return type + label;
            }
          }
        },
        legend: {
          display: false
          //position: 'bottom'
        }
      }
    });

    $('#filter_serviceable').on('submit', function(e) {
      e.preventDefault();

      var start_date = replaceChar($('#start_date').val(), '/', '-');
      var end_date = replaceChar($('#end_date').val(), '/', '-');
      var id_customer = $('#ID_CUSTOMER').val();
      var id_contract = $('#ID_CONTRACT').val();

      $.ajax({
        url: '<?= base_url('index.php/api/Pooling/GetReportServiceableLevel'); ?>',
        type: 'POST',
        data: {
          start_date: start_date,
          end_date: end_date,
          id_customer: id_customer,
          id_contract: id_contract
        },
        beforeSend: function() {
          $('#result').hide();
          $('#empty_result').hide();
          $('#filter').prop('disabled', true);
          $('#filter').text('Processing...');
        },
        success: function(response) {
          var data = $.parseJSON(response);
          
          if (data.list.length > 0) {
            $('#result').show();

            // chart
            serviceableChart.data.labels = data.chart.labels;
            // serviceableChart.data.datasets[0].data = data.chart.values.target;
            serviceableChart.data.datasets[0].data = data.chart.values.actual;
            serviceableChart.update();

            // table
            var serviceableTableData = '';
            var ontime = delayed = total = 0;
            $.each(data.list, function(key, val) {
              serviceableTableData += '<tr><td>' + val.SERVICE_LEVEL_CATEGORY + '</td>';
              serviceableTableData += '<td>' + val.TARGET_SLA + '</td>';
              serviceableTableData += '<td>' + val.ONTIME + '</td>';
              serviceableTableData += '<td>' + val.DELAYED + '</td>';
              serviceableTableData += '<td>' + val.TOTAL + '</td>';
              serviceableTableData += '<td>' + get_percentage(val.ONTIME, val.TOTAL) + '</td>';
              serviceableTableData += '<td>' + ( parseFloat(val.TARGET_SERVICE_LEVEL) * 100 ) + '%</td></tr>';

              ontime += val.ONTIME;
              delayed += val.DELAYED;
              total += val.TOTAL;
            });

            var serviceableTableSummary = '';
            serviceableTableSummary += '<tr><th colspan="2">SUMMARY</th>';
            serviceableTableSummary += '<th>' + ontime + '</th>';
            serviceableTableSummary += '<th>' + delayed + '</th>';
            serviceableTableSummary += '<th>' + total + '</th>';
            serviceableTableSummary += '<th></th>';
            serviceableTableSummary += '<th></th></tr>';

            $('#serviceable-table tbody').html(serviceableTableData);
            $('#serviceable-table tfoot').html(serviceableTableSummary);
          } else {
            $('#empty_result').show();
          }
        },
        complete: function() {
          $('#filter').prop('disabled', false);
          $('#filter').html('<i class="fa fa-search"></i> Show Report');
        }
      });

    });

    function replaceChar(target, search, replacement) {
      return target.split(search).join(replacement);
    };

  });

    // serviceableChart.data.datasets[0].data = [100, 100];
    // serviceableChart.data.datasets[1].data = [75, 70];
    // serviceableChart.update();

</script>