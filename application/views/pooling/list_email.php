<style>

  .tfoot .input {
          width: 100%;
          padding: 3px;
          box-sizing: border-box;
      }

</style>
<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content">
    <?php 
        if($this->session->flashdata('alert_failed')){ 
            echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
        }elseif($this->session->flashdata('alert_success')){
            echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
        }
    ?>
    <div class="row">
      <div class="col-lg-12 col-xs-12 col-md-12">
        <div class="box box-solid">
        <div class="box-body">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filter : </h3>
            </div>
            <div class="panel-body">
              <form id="form-filter" action="<?= base_url('index.php')?>/Email" method="GET" class="form-horizontal">
                <div class="filter-input row" style="margin-left:-10px; margin-right:-10px">
                  <div class="col col-md-4">
                    <div class="form-line">
                      <label for="by_date">Email Status</label>
                      <select class="form-control" id="status_email" name="status_email" style="width: 100%;">
                        <option value="" <?= ($st_email == "" ? "selected" : ""); ?>>All Status</option>
                        <option value="1" <?= ($st_email == "1" ? "selected" : "")?>>Success Send Email</option>
                        <option value="0" <?= ($st_email == "0" ? "selected" : "")?>>Failed Send Email</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top:15px;margin-left:-10px;margin-right:-10px">
                  <div class="col col-md-12">
                    <button type="submit" id="btn-filter" class="btn btn-primary" style="margin-right:6px">Filter</button>
                    <a href="<?= base_url('index.php')?>/Email"><button type="button" id="btn-reset" class="btn btn-default">Reset</button></a>
                  </div>
                </div>
              </form>
            </div>
        </div>
          <table id="tbemail" class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
              <tr style="background-color: #37474f; color:#ffffff;">
                <th><center>Status</center></th>
                <th><center>No Reference</center></th>
                <th><center>Type</center></th>
                <th><center>Send To</center></th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
     </div>
   </div>
 </div>
</section>

<script>
  $(function () {
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    var table = $('#tbemail').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        paging: true,
        sScrollX: true,
        ajax: {
            url: "<?= base_url('index.php/api/Pooling/GetListEmail/') ?>",
            data:{
              st_email: "<?= $st_email?>"
            },
            type: "POST",
        },

        columns: [   
            {data: 'status', name: 'status', orderable: false},
            {data: 'reference', name: 'reference', orderable: false},
            {data: 'type', name: 'type', orderable: false},
            {data: 'send', name: 'send', orderable: false}
        ]
    }); 
  });
</script>
