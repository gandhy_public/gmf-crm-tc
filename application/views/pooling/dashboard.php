<section class="content-header">
    <h1>
      <?php echo strtoupper($title_controller) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">

          <div class="row" style="width:100%; margin:0 auto;">
            
            <div class="col-lg-1 col-xs-6">
            </div>

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>0<?php //echo $total_waiting_approved; ?></h3>

                  <p>Open Order</p>
                </div>
                <div class="icon">
                  <i class="fa fa-edit"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>            <!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3>0<?php //echo $total_waiting_material; ?><sup style="font-size: 20px"> </sup></h3>

                  <p>Waiting Approved</p>
                </div>
                <div class="icon">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>0<?php //echo $total_under_maintenance; ?></h3>

                  <p>In Progress</p>
                </div>
                <div class="icon">
                  <i class="fa fa-cog fa-spin"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>            

            <div class="col-lg-3 col-xs-6">
            </div>            
          </div>

          <div class="row" style="width:100%; margin:0 auto;">
            <div class="col-lg-3 col-xd-6">  
            </div>
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>0<?php //echo $total_waiting_repair; ?></h3>

                  <p>Closed</p>
                </div>
                <div class="icon">
                  <i class="fa fa-check"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>0<?php //echo $total_finished; ?></h3>

                  <p>Canceled</p>
                </div>
                <div class="icon">
                  <i class="fa fa-close"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-3 col-md-6">
              
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</section>