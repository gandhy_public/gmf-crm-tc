<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
		                  <h3 class="box-title"><?php echo $title; ?></h3>
		                  <div class="box-tools">
		                    <div class="input-group" style="width: 150px;">
		                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                      <div class="input-group-btn">
		                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                      </div>
		                    </div>
		                  </div>
		                </div>
		                <div class="box-body table-responsive no-padding">
						<table class="table table-bordered table-hover table-striped">
		                    <thead>
		                      <tr>
		                      	<th><center>Act</center></th>
		                        <!-- <th style="width: 40px">NO</th> -->
		                        <th style="width: 150px"><center>RefNo</center></th>
		                        <th style="width: 150px"><center>Category</center></th>
		                        <th style="width: 100px"><center>Part No</center></th>
		                        <th style="width: 200px"><center>PN Desc</center></th>
		                        <th><center>Dest</center></th>
		                        <th><center>AC Reg</center></th>
		                        <th style="width: 125px"><center>Cust PO</center></th>
		                        <th style="width: 125px"><center>SO Number</center></th>
		                        <th style="width: 125px"><center>Request By</center></th>
		                        <th><center>Target</center></th>
		                        <th><center>Request Date</center></th>
		                        <th><center>Status</center></th>
		                        <th><center>Respond</center></th>
		                        <th><center>Serviceable</center></th>
		                        <th><center>Unserviceable</center></th>
		                      </tr>
		                    </thead>
		                    <!-- <thead>
		                      <tr>
		                      	<form action="<?php //echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data">
			                      	<th></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th>
			                        	<select class="form-control input-sm" id="REQUEST_CATEGORY" name="REQUEST_CATEGORY" style="width: 100%;">
				                            <option value="">Choose</option>
				                            <?php
				                              //foreach ($requirement_category_list as $row_rc) {
				                            ?>
				                            <option value="<?php //echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php //echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
				                            <?php //} ?>
				                      	</select>
				                  	</th>
			                        <th><div class="form-horizontal"><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></div></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
		                    	</form>
		                      </tr>
		                    </thead> -->
		                    <tbody>
		                    	<tr>
		                      	<form action="<?php echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data">
			                      	<th></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th>
			                        	<select class="form-control input-sm" id="REQUEST_CATEGORY" name="REQUEST_CATEGORY" style="width: 100%;">
				                            <option value="">Choose</option>
				                            <?php
				                              foreach ($requirement_category_list as $row_rc) {
				                            ?>
				                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
				                            <?php } ?>
				                      	</select>
				                  	</th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
		                    	</form>
		                      </tr>
		                    <?php
	                          if($data_table) {
		                          $no = $jlhpage;
		                          foreach ($data_table as $rows) {
		                            $no++;
		                      ?>
		                      <tr>
		                      	<td>

		                          <div class="btn-group">

		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Update This Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
		                            <!-- <a href="#ModalView" data-toggle="modal"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button></a>
		                            <a href="#ModalViewLog" data-toggle="modal"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-tasks"></i></button></a>
		                            <a href="<?php //echo base_url('Working_order/edit_in_progress_wo'); //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Edit This New Working Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
		                            <a href="<?php //echo base_url('Working_order/cancel_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Cancel This New Working Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a> -->
		                          </div><!-- /.btn-group -->
		                        </td>
		                        <!-- <td><?php //echo $no; ?></td> -->
		                        <td><?php echo $rows->REFERENCE; ?></td>
		                        <td><?php echo $rows->REQUIREMENT_CATEGORY_NAME; ?></td>
		                        <td><?php echo $rows->PN_NAME; ?></td>
		                        <td><?php echo $rows->PN_DESC; ?></td>
		                        <td><?php echo $rows->DELIVERY_POINT_CODE; ?></td>
		                        <td><?php echo $rows->AIRCRAFT_REGISTRATION; ?></td>
		                        <td><?php echo $rows->CUSTOMER_PO.' - '.$rows->CUSTOMER_RO; ?></td>
		                        <td>-<?php //echo $rows->CUSTOMER_PO.' - '.$rows->CUSTOMER_RO; ?></td>
		                        <td>-</td>
		                        <td>-</td>
		                        <td><?php echo $rows->REQUEST_ORDER_DATE; ?></td>
		                        <!-- <td><?php //echo $rows->REQUESTED_DELIVERY_DATE; ?></td> -->
		                        <td><?php echo $rows->STATUS_ORDER_NAME; ?></td>
		                        <td>
		                          <div class="btn-group">
		                          	<?php if($rows->STATUS_ORDER==1) { ?>
		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Approve This Order ???')"><button class="btn btn-success btn-xs"><i class="fa fa-check"> Approve</i></button></a>
		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Cancel This Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"> Cancel</i></button></a>
		                          	<?php } ?>
		                          </div><!-- /.btn-group -->
		                        </td>
		                        <td>-</td>
		                        <td>-</td>
		                      </tr>
		                      <?php } }?>
		                    </tbody>
		                  </table>
		                  <br>
		                  <div class="col-md-3">
		                    <b><?php if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                  </div>
		                  <div class="col-md-6">
		                    <?php echo $paging; ?>
		                  </div>
		                  <div class="col-md-3 pull-right">
		                    <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data Request Order</button></a>
		                  </div>

		              </div>
	                  <br>
						<table id="tbpooling" class="table table-bordered table-hover table-striped">
		                    <thead>
		                      <tr>
		                      	<th><center>Act</center></th>
		                        <!-- <th style="width: 40px">NO</th> -->
		                        <th style="width: 150px"><center>RefNo</center></th>
		                        <th style="width: 150px"><center>Category</center></th>
		                        <th style="width: 100px"><center>Part No</center></th>
		                        <th style="width: 200px"><center>PN Desc</center></th>
		                        <th><center>Dest</center></th>
		                        <th><center>AC Reg</center></th>
		                        <th style="width: 125px"><center>Cust PO</center></th>
		                        <th style="width: 125px"><center>SO Number</center></th>
		                        <th style="width: 125px"><center>Request By</center></th>
		                        <th><center>Target</center></th>
		                        <th><center>Request Date</center></th>
		                        <th><center>Status</center></th>
		                        <th><center>Respond</center></th>
		                        <th><center>Serviceable</center></th>
		                        <th><center>Unserviceable</center></th>
		                      </tr>
		                    </thead>
		                    <!-- <thead>
		                      <tr>
		                      	<form action="<?php //echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data">
			                      	<th></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th>
			                        	<select class="form-control input-sm" id="REQUEST_CATEGORY" name="REQUEST_CATEGORY" style="width: 100%;">
				                            <option value="">Choose</option>
				                            <?php
				                              //foreach ($requirement_category_list as $row_rc) {
				                            ?>
				                            <option value="<?php //echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php //echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
				                            <?php //} ?>
				                      	</select>
				                  	</th>
			                        <th><div class="form-horizontal"><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></div></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th><input type="text" class="form-control input-sm" name="REFERENCE_S"></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
		                    	</form>
		                      </tr>
		                    </thead> -->
		                    <tbody>
		                    	<tr>
		                      	<form action="<?php echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data">
			                      	<th></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th>
			                        	<select class="form-control input-sm" id="REQUEST_CATEGORY" name="REQUEST_CATEGORY" style="width: 100%;">
				                            <option value="">Choose</option>
				                            <?php
				                              foreach ($requirement_category_list as $row_rc) {
				                            ?>
				                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
				                            <?php } ?>
				                      	</select>
				                  	</th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th><div class="form-group"><input type="text" class="form-control input-sm" name="REFERENCE_S"></div></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
			                        <th></th>
		                    	</form>
		                      </tr>
		                    <?php
	                          if($data_table) {
		                          $no = $jlhpage;
		                          foreach ($data_table as $rows) {
		                            $no++;
		                      ?>
		                      <tr>
		                      	<td>

		                          <div class="btn-group">

		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Update This Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-tasks"></i></button></a>
		                            <!-- <a href="#ModalView" data-toggle="modal"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button></a>
		                            <a href="#ModalViewLog" data-toggle="modal"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-tasks"></i></button></a>
		                            <a href="<?php //echo base_url('Working_order/edit_in_progress_wo'); //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Edit This New Working Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
		                            <a href="<?php //echo base_url('Working_order/cancel_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Cancel This New Working Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a> -->
		                          </div><!-- /.btn-group -->
		                        </td>
		                        <!-- <td><?php //echo $no; ?></td> -->
		                        <td><?php echo $rows->REFERENCE; ?></td>
		                        <td><?php echo $rows->REQUIREMENT_CATEGORY_NAME; ?></td>
		                        <td><?php echo $rows->PN_NAME; ?></td>
		                        <td><?php echo $rows->PN_DESC; ?></td>
		                        <td><?php echo $rows->DELIVERY_POINT_CODE; ?></td>
		                        <td><?php echo $rows->AIRCRAFT_REGISTRATION; ?></td>
		                        <td><?php echo $rows->CUSTOMER_PO.' - '.$rows->CUSTOMER_RO; ?></td>
		                        <td>-<?php //echo $rows->CUSTOMER_PO.' - '.$rows->CUSTOMER_RO; ?></td>
		                        <td>-</td>
		                        <td>-</td>
		                        <td><?php echo $rows->REQUEST_ORDER_DATE; ?></td>
		                        <!-- <td><?php //echo $rows->REQUESTED_DELIVERY_DATE; ?></td> -->
		                        <td><?php echo $rows->STATUS_ORDER_NAME; ?></td>
		                        <td>
		                          <div class="btn-group">
		                          	<?php if($rows->STATUS_ORDER==1) { ?>
		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Approve This Order ???')"><button class="btn btn-success btn-xs"><i class="fa fa-check"> Approve</i></button></a>
		                          	<a href="<?php echo base_url('index.php/Pooling/update_order').'/'.$rows->NO_ACCOUNT; ?>" onclick="return confirm('Are You Sure Want To Cancel This Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"> Cancel</i></button></a>
		                          	<?php } ?>
		                          </div><!-- /.btn-group -->
		                        </td>
		                        <td>-</td>
		                        <td>-</td>
		                      </tr>
		                      <?php } }?>
		                    </tbody>
		                  </table>
		                  <br>
		                  <div class="col-md-3">
		                    <b><?php if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                  </div>
		                  <div class="col-md-6">
		                    <?php echo $paging; ?>
		                  </div>
		                  <div class="col-md-3 pull-right">
		                    <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data Request Order</button></a>
		                  </div>

		              </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>
  $(function () {

    var table = $("#tbpooling").DataTable({

      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "serverSide": true,
       "scrollX": true,
      "ajax": {
        "url" : "https://dev.gmf-aeroasia.co.id/app_crm_tc/index.php/api/Pooling/list_table",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({

                0: json.data[i].REFERENCE,
                1: json.data[i].REQUIREMENT_CATEGORY_NAME,
                2: json.data[i].PN_NAME,
                3: json.data[i].PN_DESC,
                4: json.data[i].DELIVERY_POINT_CODE,
                5: json.data[i].AIRCRAFT_REGISTRATION,
                6: json.data[i].CUSTOMER_PO,
                7: json.data[i].CUSTOMER_RO,
                8: json.data[i].CUSTOMER_PO,
                9: json.data[i].CUSTOMER_RO,
                10: json.data[i].REQUEST_ORDER_DATE,
                11: json.data[i].REQUESTED_DELIVERY_DATE,
                12: json.data[i].STATUS_ORDER_NAME,
                13: json.data[i].STATUS_ORDER,
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        },{
          "targets": 0,
          "className": "dt-center",
          "data": null,
          "defaultContent":
          '<button title="Member" class="btMember btn btn-primary btn-xs" type="button"><i class="glyphicon glyphicon-list"></i></button>'
        }
      ]


    });

    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
