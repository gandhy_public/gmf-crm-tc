<style>
 .form-group.required .control-label:after {
  content:" *";
  font-size: 16px;
  color:red;
}
</style>
<section class="content-header">
   <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section id="order" class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <div class="tab-content">
            <legend>Request Info</legend>
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <center><p id="loading_order">Loading...</p></center>
                <div class="row row-order" style="display: none">

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Customer :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-green" id="order_customer"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Status :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-red" id="order_status"></span>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Reference No</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="REFERENCE" id="order_reference" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" id="order_pn" name="ID_PART_NUMBER">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" id="order_desc" name="COMPONENT_DESCRIPTION_V">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" id="order_category" name="COMPONENT_DESCRIPTION_V">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="REQUESTED_DELIVERY_DATE" id="order_delivery_date">
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-5 control-label">Target Delivery Date</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control input-sm" name="TARGET_DELIVERY_DATE" id="order_delivery_target">
                        </div>
                      </div> -->
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-sm-5 control-label">Resquested By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue" id="order_create_by"></span>
                            <span class="pull-left badge bg-yellow" id="order_create_at"></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-5 control-label"> Confirmed By / Date :</label>
                          <div class="col-sm-7">
                            <span class="pull-left badge bg-blue" id="order_confirm_by"></span>
                            <span class="pull-left badge bg-yellow" id="order_confirm_at"></span>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="ATA_CHAPTER" id="order_ata">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="AIRCRAFT_REGISTRATION" id="order_ac">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Transfer Point</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="TRANSFER_POINT" id="order_point">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <textarea <?= $order?> type="textarea" class="form-control" name="DESTINATION_ADDRESS" rows="8" cols="22" id="order_destination"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer PO/RO</label>
                        <div class="col-sm-7">
                          <input <?= $order?> type="text" class="form-control" name="CUSTOMER_PO" id="order_customer_po">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea <?= $order?> name="REMARKS_OF_CUSTOMER" type="textarea" class="form-control" rows="5" cols="22" id="order_remark">
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="serviceable" class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Serviceable Component</h3>
          <button type="button" id="service_update" class="btn btn-success pull-right">Update</button>
        </div>
        <div class="box-body">
          <center><p id="loading_service">Loading...</p></center>
          <div class="row row-service" style="display: none">
            <form action="<?= base_url()?>index.php/Pooling/create_service" class="form-horizontal form-serviceable" method="POST" enctype="multipart/form-data" name="form-serviceable" id="form-serviceable">
            <input type="hidden" name="order_id" value="<?= $order_id?>">
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red" id="service_status"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submiter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue" id="service_create_by"></span>
                      <span class="pull-left badge bg-yellow" id="service_create_at"></span>
                    </div>
                </div>
                <br>
                <div class="form-group required">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input  type="radio" name="SERVICE_SHIPMENT_TYPE" id="optionsRadios1" value="1" required>Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input  type="radio" name="SERVICE_SHIPMENT_TYPE" id="optionsRadios2" value="2">Overseas Shipment
                        </label>
                    </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Part Number Request</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="service_pn_request" name="SERVICE_PN_REQUEST" readonly>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Part Number Delivered</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="service_pn_delivered" name="SERVICE_PN_DELIVERED" required>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Serial Number Delivered</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="service_sn_delivered" name="SERVICE_SN_DELIVERED" required>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-5 control-label">Target Delivery Date</label>
                  <div class="col-sm-7">
                    <input <?= $order?> type="text" class="form-control input-sm" name="TARGET_DELIVERY_DATE" id="order_delivery_target" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="service_awb" name="SERVICE_AWB">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="service_flight" name="SERVICE_FLIGHT" >
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-5 control-label">Delivery Date</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="SERVICE_DELIVERY_DATE" id="service_delivery_date" required>
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-5 control-label">Airport/Destination</label>
                  <div class="col-sm-7">
                    <textarea class="form-control" id="service_destination_address" name="SERVICE_DESTINATION_ADDRESS" row="8" cols="3" readonly></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <!-- <input type="text" class="form-control" id="service_email" name="service_email"> -->
                    <select disabled="true" class="form-control input-sm select2" multiple="multiple" id="service_email" name="service_email[]" style="width: 100%;">
                      <option value="">Choose Existing Email</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                  <button type="submit" id="service_submit" class="btn btn-success pull-right">Submit Serviceable</button>
              </div>
            </div>
          </form>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<section id="unserviceable" class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Unserviceable Component</h3>
          <button type="button" id="unservice_update" class="btn btn-success pull-right">Update</button>
        </div>
        <div class="box-body">
          <div class="row">
            <form action="<?= base_url()?>index.php/Pooling/create_unservice" class="form-horizontal form-unserviceable" method="POST" id="form-unserviceable" name="form-unserviceable" enctype="multipart/form-data">
            <input type="hidden" name="order_id" value="<?= $order_id?>">
            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Status :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-red" id="unservice_status">STATUS UNSERVICE</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label">Submiter :</label>
                    <div class="col-sm-7">
                      <span class="pull-left badge bg-blue" id="unservice_submit"></span>
                      <span class="pull-left badge bg-yellow" id="unservice_submit_at"></span>
                    </div>
                </div>
                <br>
                <div class="form-group required">
                    <label class="col-sm-5 control-label">Send Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="unservice_send" id="unservice_send" value="1" required>Serviceable
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="unservice_send" id="unservice_send" value="2">Unserviceable
                        </label>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-5 control-label">Shipment Type</label>
                    <div class="col-sm-7">
                        <label class="radio-inline">
                            <input type="radio" name="unservice_shipment" id="unservice_shipment" value="1" required>Hand-Delivered
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="unservice_shipment" id="unservice_shipment" value="2">Overseas Shipment
                        </label>
                    </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Part Number Request</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_pn_request" name="unservice_pn_request" readonly required>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Part Number Removed</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_pn_removed" name="unservice_pn_removed" required>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Serial Number Removed</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_sn_removed" name="unservice_sn_removed" required>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">TSN</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="unservice_tsn" name="unservice_tsn" required>
                    <p id="alert_tsn"></p>
                  </div>
                  <label class="col-sm-2">FH</label>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">CSN</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="unservice_csn" name="unservice_csn" required>
                    <p id="alert_csn"></p>
                  </div>
                  <label class="col-sm-2">CY</label>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Target Unserviceable Return</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_target_return" name="unservice_target_return" required readonly>
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Removal Date From AC</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="unservice_removal_date" id="unservice_removal_date" required>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 col-xs-12">
              <div class="form-horizontal">
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">AC Registration</label>
                  <div class="col-sm-7">
                    <!-- <input type="text" class="form-control" id="unservice_ac" name="unservice_ac" required> -->
                    <select disabled="true" class="form-control input-sm select2 unservice_ac" id="unservice_ac" name="unservice_ac" style="width: 100%;" required>
                      <option value="" disabled selected>Search For Aircraft Registration</option>
                    </select>
                    <input type="text" class="form-control input-sm" name="unservice_ac_other" id="unservice_ac_other" maxlength="10" style="display: none">
                    <input type="hidden" class="form-control input-sm" name="unservice_ac_val" id="unservice_ac_val" maxlength="10">
                  </div>
                </div>
                <div class="form-group required">
                  <label class="col-sm-5 control-label">Removal Reason</label>
                  <div class="col-sm-7">
                    <textarea id="unservice_removal_reason" name="unservice_removal_reason" type="textarea" class="form-control" rows="5" cols="22" required></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">AWB No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_awb" name="unservice_awb">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Flight No.</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" id="unservice_flight" name="unservice_flight">
                  </div>
                </div>
                <div class="form-group  required">
                  <label class="col-sm-5 control-label">Delivery Date</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control"  name="unservice_delivery_date" id="unservice_delivery_date" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Remarks</label>
                  <div class="col-sm-7">
                    <textarea name="unservice_remark" id="unservice_remark" type="textarea" class="form-control" rows="5" cols="22"></textarea>
                  </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-5 control-label">Removal Reason Type</label>
                    <div class="col-sm-7">
                      <table border="0">
                        <tr>
                          <td>
                            <label class="radio-inline">
                              <input type="radio" name="unservice_removal_reason_type" id="unservice_removal_reason_type" value="1" required>Scheduled Removal
                            </label>
                          </td>
                          <td>&nbsp&nbsp</td>
                          <td>
                            <label class="radio-inline">
                              <input type="radio" name="unservice_removal_reason_type" id="unservice_removal_reason_type" value="2">Unscheduled removal
                            </label>
                          </td>
                          <td>&nbsp&nbsp</td>
                          <td>
                            <label class="radio-inline">
                              <input type="radio" name="unservice_removal_reason_type" id="unservice_removal_reason_type" value="3">Return Unused
                            </label>
                          </td>
                        </tr>
                      </table>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-5 control-label">Attention Email</label>
                  <div class="col-sm-7">
                    <!-- <input type="text" class="form-control" id="unservice_email" name="unservice_email"> -->
                    <select disabled="true" class="form-control input-sm select2" multiple="multiple" id="unservice_email" name="unservice_email[]" style="width: 100%;">
                      <option value="">Choose Existing Email</option>
                    </select>
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" id="unservice_submit" class="btn btn-success pull-right unservice_submit">Submit Unserviceable</button>
                </div>
              </div>
            </div>
          </form>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    <?php if($this->session->userdata('log_sess_level_user_role') == "1" || $this->session->userdata('log_sess_level_user_role') == "2"){?>
      $('#service_update').show();
      $('#unservice_update').hide();
    <?php }else{?>
      $('#service_update').hide();
      $('#unservice_update').show();
    <?php }?>

    <?php if($order_status == "4" || $order_status == "3" || $order_status == "1"){ ?>
      $('#service_update, #unservice_update').hide();
    <?php }?>

    /*$('#service_email, #unservice_email').multiple_emails({position: "bottom"});*/
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    var date_input=$('#service_delivery_date, #unservice_delivery_date'); 
    var options={
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      autoclose: true
    };
    $('#unservice_removal_date').datepicker(options);
    date_input.datetimepicker({
      format: 'YYYY-MM-DD HH:mm'
    });
    //$('.form-serviceable input').attr('disabled', true);
    $('input[type="text"], textarea, input[type="radio"]').attr('disabled', true);
    $('#service_submit').hide();
    $('.unservice_submit').hide();

    $.ajax({
     url:"<?php echo base_url() ?>index.php/api/Pooling/GetDetailOrder",
     type:'POST',
     data: {
      order_id: "<?= $order_id?>"
     },
     success:function(resp){
        resp = JSON.parse(resp);
        if(resp.status){
          $('#loading_order').hide();
          $('.row-order').show();

          $('#loading_service').hide();
          $('.row-service').show();
          $.each(resp.data , function(i,val) { 
            //FORM ORDER LEFT
            $('#order_customer').text(val.CUSTOMER_NAME);
            if(val.STATUS_ORDER == "1"){
              $('#order_status').text("Open Order & Waiting Approve");
            }else if(val.STATUS_ORDER == "2"){
              $('#order_status').text("In Progress");
            }else if(val.STATUS_ORDER == "3"){
              $('#order_status').text("Close");
            }else if(val.STATUS_ORDER == "4"){
              $('#order_status').text("Cancel Order");
            }
            $('#order_reference').val(val.REFERENCE);
            $('#order_pn').val(val.PART_NUMBER);
            $('#order_desc').val(val.PART_DESCRIPTION);
            $('#order_category').val(val.REQUIREMENT_CATEGORY_DESC);
            $('#order_delivery_date').val(val.DELIVERY_DATE.substring(0,16));
            if(val.DELIVERY_TARGET != null){
              $('#order_delivery_target').val(val.DELIVERY_TARGET.substring(0,16));
            }else{
              $('#order_delivery_target').val(0);
            }

            //FORM ORDER RIGHT
            $('#order_create_at').text(val.CREATE_AT.substring(0,16));
            $('#order_create_by').text(val.CREATE_BY);
            if(val.CONFIRM_AT != null){
              $('#order_confirm_at').text(val.CONFIRM_AT.substring(0,16));
            }
            $('#order_confirm_by').text(val.CONFIRM_BY);
            $('#order_ata').val(val.ATA_CHAPTER);
            $('#order_ac').val(val.AC_REGISTRATION);
            $('#order_point').val(val.DESTINATION_POINT);
            $('#order_destination').val(val.DESTINATION);
            $('#order_customer_po').val(val.CUSTOMER_ORDER);
            $('#order_remark').val(val.REMARK);

            //FORM SERVICEABLE
            if(val.STATUS_SERVICE == "1"){
              $('#service_status').text("Waiting Delivery");
            }else if(val.STATUS_SERVICE == "2"){
              $('#service_status').text("Delivered");
            }
            $('#service_pn_request').val(val.PART_NUMBER);
            $('#service_destination_address').val(val.DESTINATION);
            //FORM UNSERVICEABLE
            if(val.STATUS_UNSERVICE == "1"){
              $('#unservice_status').text("Waiting Delivery");
            }else if(val.STATUS_UNSERVICE == "2"){
              $('#unservice_status').text("Delivered");
            }
            $('#unservice_pn_request').val(val.PART_NUMBER);
          });
        }else{
          $('#loading_order').text(resp.message);
          $('#loading_service').text(resp.message);

          $('.row-order').hide();
          $('.row-service').hide();
        }
     }
    });

    $.ajax({
     url:"<?php echo base_url() ?>index.php/api/Pooling/GetDetailService",
     type:'POST',
     data: {
      order_id: "<?= $order_id?>",
      contract_id: "<?= $contract_id?>"
     },
     success:function(resp){
        resp = JSON.parse(resp);
        if(resp.status){
          $.each(resp.data , function(i,val) { 
            //FORM SERVICE LEFT
            $('#service_create_by').text(val.CREATE_BY);
            if(val.CREATE_AT != null){
              $('#service_create_at').text(val.CREATE_AT.substring(0,16));
            }
            $('input[name="SERVICE_SHIPMENT_TYPE"]').filter("[value='"+val.SHIPMENT_TYPE+"']").attr('checked', true);
            $('#service_pn_delivered').val(val.PN_DELIVERED);
            $('#service_sn_delivered').val(val.SN_DELIVERED);

            //FORM SERVICE RIGHT
            $('#service_awb').val(val.AWB_NO);
            $('#service_flight').val(val.FLIGHT_NO);
            if(val.DELIVERY_DATE != null){
              $('#service_delivery_date').val(val.DELIVERY_DATE.substring(0,16));
            }

            //FORM UNSERVICEABLE
            $('#unservice_target_return').val(val.TARGET_UNSERVICE_RETURN.substring(0,16));
          });
        }
     }
    });

    $.ajax({
     url:"<?php echo base_url() ?>index.php/api/Pooling/GetDetailUnservice",
     type:'POST',
     data: {
      order_id: "<?= $order_id?>"
     },
     success:function(resp){
        resp = JSON.parse(resp);
        if(resp.status){
          $.each(resp.data , function(i,val) { 
            //FORM SERVICE LEFT
            $('#unservice_submit').text(val.CREATE_BY);
            if(val.CREATE_AT != null){
              $('#unservice_submit_at').text(val.CREATE_AT.substring(0,16));
            }
            $('input[name="unservice_send"]').filter("[value='"+val.SEND_TYPE+"']").attr('checked', true);
            $('input[name="unservice_shipment"]').filter("[value='"+val.SHIPMENT_TYPE+"']").attr('checked', true);
            $('#unservice_pn_removed').val(val.PN_REMOVED);
            $('#unservice_sn_removed').val(val.SN_REMOVED);
            $('#unservice_tsn').val(val.TSN);
            $('#unservice_csn').val(val.CSN);
            $('#unservice_removal_date').val(val.REMOVAL_DATE);

            //FORM SERVICE RIGHT
            $('#unservice_ac_val').val(val.AC_REGISTRATION).attr("type","text").attr("readonly", true);
            //$('#unservice_ac').next(".select2-container").hide();
            $('#unservice_removal_reason').val(val.REMOVAL_REASON);
            $('#unservice_awb').val(val.AWB_NO);
            $('#unservice_flight').val(val.FLIGHT_NO);
            if(val.DELIVERY_DATE != null){
              $('#unservice_delivery_date').val(val.DELIVERY_DATE.substring(0,16));
            }
            $('#unservice_remark').val(val.REMARK);
            $('input[name="unservice_removal_reason_type"]').filter("[value='"+val.REMOVAL_REASON_TYPE+"']").attr('checked', true);
          });
        }
     }
    });

    $('#service_update').on("click", function(){
      $('.form-serviceable input,select').attr('disabled', false);
      $('#service_submit').show();
    })
    $('#unservice_update').on("click", function(){
      $('.form-unserviceable input,textarea,select').attr('disabled', false);
      $('.unservice_submit').show();
      if($("input[name='unservice_send']:checked").val() == "1"){
          $('#unservice_removal_date').val("").attr("disabled", true).attr("required", false);
          $('#unservice_ac').val("").attr("disabled", true).attr("required", false);
          $('#unservice_ac_val').val("");
          $('#unservice_ac_other').hide().val("").attr("required", false);
          $('#unservice_removal_reason').val("").attr("readonly", true).attr("required", false);
      }else if($("input[name='unservice_send']:checked").val() == "2"){
          $('#unservice_removal_date').attr("disabled", false).attr("required", true);
          $('#unservice_ac').attr("disabled", false).attr("required", true);
          $('#unservice_removal_reason').attr("readonly", false).attr("required", true);
      }
      //$('#unservice_ac').next(".select2-container").show().attr("required", true);
    })

    $('.form-serviceable').on('submit', function (e) {
      e.preventDefault();
      swal({
        title: "Are you sure want to Submit?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $(".form-serviceable").off("submit").submit();
        }
      });
    });
    $('.form-unserviceable').on('submit', function (e) {
      e.preventDefault();
      swal({
        title: "Are you sure want to Submit?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $(".form-unserviceable").off("submit").submit();
        }
      });
    });

    $('#unservice_ac').select2({
        placeholder: 'Select Aircraft Registration',
        width: '100%',
        minimumInputLength: 2,
        triggerChange: true,
        allowClear: true,
        ajax: {
          url: "<?=base_url()?>index.php/api/Pooling/get_fleet_list?contract=<?= $contract_id?>",
          dataType: 'json',
          delay: 450,
          processResults: function (data) {
            return {
              results: data
            }
          },
          cache: true
        }
    });
    $('#unservice_ac').change(function () {
        var unservice_ac = $('#unservice_ac').val();
        var unservice_ac_txt = $('#unservice_ac').select2('data');
        unservice_ac_txt = unservice_ac_txt[0]['text'];

        if (unservice_ac == 'OTHER') {
          $('#unservice_ac_other').show().attr("required", true);
        }else{
          $('#unservice_ac_other').hide().attr("required", false);
        }

        $('#unservice_ac_val').val(unservice_ac_txt);
    });

    $('#unservice_tsn').on("focus",function(){
      $('#alert_tsn').text("Input is required, if it's not filled the default is 0").css("color","red");
      $(this).val("0");
    });
    $('#unservice_csn').on("focus",function(){
      $('#alert_csn').text("Input is required, if it's not filled the default is 0").css("color","red");
      $(this).val("0");
    });

    $('#service_email, #unservice_email').select2({
        placeholder: 'Select Email',
        width: '100%',
        minimumInputLength: 3,
        triggerChange: true,
        allowClear: true,
        ajax: {
            url: "<?=base_url()?>index.php/api/Pooling/get_existing_email",
            dataType: 'json',
            delay: 450,
            processResults: function (data) {
                return {
                    results: data
                }
            },
            cache: true
        }
    });

    $('input[type=radio][name=unservice_send]').change(function() {
        val_radio = $(this).val();
        if (val_radio == '1') {
            $('#unservice_removal_date').val("").attr("disabled", true).attr("required", false);
            $('#unservice_ac').val("").attr("disabled", true).attr("required", false);
            $('#unservice_ac_val').val("");
            $('#unservice_ac_other').hide().val("").attr("required", false);
            $('#unservice_removal_reason').val("").attr("readonly", true).attr("required", false);
        }else {
            $('#unservice_removal_date').attr("disabled", false).attr("required", true);
            $('#unservice_ac').attr("disabled", false).attr("required", true);
            $('#unservice_removal_reason').attr("readonly", false).attr("required", true);
        }
    });
  });
</script>
