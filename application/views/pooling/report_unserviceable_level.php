<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <!-- <?php //$this->load->view($nav_tabs); ?> -->

        <div class="tab-content">
          <!-- /.tab-pane New Order-->    
          <!-- <div class="tab-pane" id="tab_new_order"> -->
            
            <legend>Filters</legend>
            <form id="filter_unserviceable" class="form-horizontal" >
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Date From</label>
                        <div class="col-sm-7">
                          <input class="form-control" id="start_date" name="start_date" placeholder="YYYY/MM/DD" type="text"/>                            
                        </div>
                      </div>
                    </div>
                  </div>                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Requested Date To</label>
                        <div class="col-sm-7">
                          <input class="form-control" id="end_date" name="end_date" placeholder="YYYY/MM/DD" type="text"/>                            
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Customer</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_CUSTOMER" name="ID_CUSTOMER" style="width: 100%;" required>
                            <option value="" disabled selected>Search For Customer</option>
                            <?php
                              foreach ($customer_list as $row) {
                            ?>
                            <option value="<?php echo $row->ID_CUSTOMER ?>"><?php echo $row->COMPANY_NAME; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>                            
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Contract</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_CONTRACT" name="ID_CONTRACT" style="width: 100%;" required>
                          </select>
                        </div>
                      </div>                            
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                <button type="submit" id="filter" name="filter" class="btn btn-success pull-right"><i class="fa fa-search"></i> Show Report</button>
              </div><!-- /.box-footer -->           
            </form>
          
          </div>
      </div>
    </div>
  </div>

  <div class="row" id="result" style="display: none">
    <div class="col-md-7">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">CHART</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>

        <div class="box-body">
            <div class="card-content" style="height: 300px">
                <canvas id="unserviceable-chart"></canvas>
            </div>
        </div>
      </div>
    </div>

    <div class="col-md-5">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">LIST</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-sm-12">
          <!-- style="width:670px; margin:0 auto;"          -->
           <table id="unserviceable-table" class="table table-bordered table-striped" style="width: 100%;">
              <thead>
                <tr>
                  <th>Serviceable Category</th>
                  <th>Ontime</th>
                  <th>Delayed</th>
                  <th>Total</th>
                  <th>Achievement</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>

        </div>
      </div>
    </div>
         
  </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    var start_date=$('#start_date'); 
    var end_date=$('#end_date');
    var options={
      format: 'yyyy/mm/dd',
      //container: container,
      todayHighlight: true,
      autoclose: true,
    };
    start_date.datepicker(options);
    end_date.datepicker(options);

    $('#ID_CUSTOMER').select2({
      width: '100%',
      <?php if(count($customer_list) > 1){?>
        placeholder: 'Select Customer',
        minimumInputLength: 2,
        triggerChange: true,
        allowClear: true,
        ajax: {
          url: "<?=base_url()?>index.php/api/Contract/SelectCustomer",
          dataType: 'json',
          delay: 450,
          processResults: function (data) {
            return {
              results: data
            }
          },
          cache: true
        }
      <?php }?>
    });

    $('#ID_CUSTOMER').change(function(){
      var cust = $(this).val();
      $('#ID_CONTRACT').empty();
      $.ajax({
        url: '<?= base_url('index.php/api/Pooling/get_contract_list'); ?>',
        type: 'GET',
        data: {
          customer: cust
        },
        success: function(resp) {
          resp = JSON.parse(resp);
          if(resp.body){
            $('#ID_CONTRACT').select2({
              data: resp.body,
              disabled: false,
              allowClear: true
            });
          }else{
            $('#ID_CONTRACT').select2({
              placeholder: 'No data contract',
              data: [{id: '', text: 'No data contract'}],
              disabled: true,
              allowClear: true
            });
          }
        }
      });
    });
  });
  var date = new Date();

  $('#start_date').val("<?= $start_date?>");
  $('#end_date').val("<?= $end_date?>");
</script>


<script>
  function get_percentage(value, total) {
    return total > 0 ? Number(((value / total) * 100).toFixed(2)) + '%' : '0%';
  }
$(document).ready(function() {  
  var unserviceableChartTarget = $('#unserviceable-chart');
  var unserviceableChartData = {
      labels: ["Category"],
      datasets: [
        {
          label: "Unserviceable",
          backgroundColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
          data: [0],
        }
      ]
    }
  var unserviceableChart = new Chart(unserviceableChartTarget, {
      type: 'bar',
      data: unserviceableChartData,
      options: {
        cornerRadius: 4,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: 100,
              callback: function(value, index, values) {
                return value + "%";
              }
            },
            gridLines: {
              zeroLineColor: '#ddd',
              color: 'rgba(234, 234, 234, .5)'
            }
          }],
          xAxes: [{
            barPercentage: 0.4,
            categoryPercentage: 0.6,
            gridLines: {
              zeroLineColor: '#eaeaea',
              color: 'rgba(234, 234, 234, .5)'
            }
          }]
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var type = data.datasets[tooltipItem.datasetIndex].label + ': ';

              var label = tooltipItem.yLabel ;
              if (label) label += '%';
              // label += Math.round(tooltipItem.yLabel * 100) / 100;
              return type + label;
            }
          }
        },
        legend: {
          // display: false,
          position: 'bottom'
        }
      }
    });

  $('#filter_unserviceable').on('submit', function(e) {
      e.preventDefault();
      
      var start_date = replaceChar($('#start_date').val(), '/', '-');
      var end_date = replaceChar($('#end_date').val(), '/', '-');
      var id_customer = $('#ID_CUSTOMER').val();
      var id_contract = $('#ID_CONTRACT').val();

      $.ajax({
        url: '<?= base_url('index.php/api/Pooling/GetReportUnserviceableLevel'); ?>',
        type: 'POST',
        data: {
          start_date: start_date,
          end_date: end_date,
          id_customer: id_customer,
          id_contract: id_contract
        },
        beforeSend: function() {
          $('#filter').prop('disabled', true);
          $('#filter').text('Processing...');
        },
        success: function(response) {
          $('#result').show();
          $('#filter').prop('disabled', false);
          $('#filter').html('<i class="fa fa-search"></i> Show Report');

          var data = $.parseJSON(response);
          console.log(data.chart.values.actual);
          // chart
          unserviceableChart.data.labels = data.chart.labels;
          unserviceableChart.data.datasets[0].data = data.chart.values.actual;
          unserviceableChart.update();

          // table
          var unserviceableTableData = '';
          var ontime = delayed = total = 0;
          $.each(data.list, function(key, val) {
            unserviceableTableData += '<tr><td>' + val.KATEGORI + '</td>';
            unserviceableTableData += '<td>' + val.JUMLAH_ONTIME + '</td>';
            unserviceableTableData += '<td>' + val.JUMLAH_DELAY + '</td>';
            unserviceableTableData += '<td>' + val.TOTAL + '</td>';
            unserviceableTableData += '<td>' + get_percentage(val.JUMLAH_ONTIME,val.TOTAL) + '</td></tr>';

            ontime += val.JUMLAH_ONTIME;
            delayed += val.JUMLAH_DELAY;
            total += val.TOTAL;
          });

          var unserviceableTableSummary = '';
          unserviceableTableSummary += '<tr><th>SUMMARY</th>';
          unserviceableTableSummary += '<th>' + ontime + '</th>';
          unserviceableTableSummary += '<th>' + delayed + '</th>';
          unserviceableTableSummary += '<th>' + total + '</th>';
          unserviceableTableSummary += '<th></th></tr>';

          $('#unserviceable-table tbody').html(unserviceableTableData);
          $('#unserviceable-table tfoot').html(unserviceableTableSummary);
        },
        complete: function() {
          $('#filter').prop('disabled', false);
          $('#filter').html('<i class="fa fa-search"></i> Show Report');
        }
      });
  });

  function replaceChar(target, search, replacement) {
      return target.split(search).join(replacement);
  };
});  
</script>
