<style>
 .form-group.required .control-label:after {
  content:" *";
  font-size: 16px;
  color:red;
}
</style>

<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <div class="tab-content">

            <!-- <legend>Order Info</legend> -->
            <!-- <form id="form-pooling" action="<?php echo base_url() ?>index.php/Pooling/insert_create_order" class="form-horizontal" method="post" enctype="multipart/form-data"> -->
            <form id="form-pooling" name="form-pooling" action="" class="form-horizontal" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group required">
                          <input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
                          <input type="hidden" name="customer" id="customer" value="<?php echo $customer; ?>" />
                          <label class="col-sm-5 control-label">Contract Request</label>
                          <div class="col-sm-7">
                            <select class="form-control input-sm select2" id="ID_CONTRACT" name="ID_CONTRACT" style="width: 100%;" required>
                              <option value="" disabled selected>Search For Contract</option>
                              <?php
                                foreach ($contract_list as $row) {
                              ?>
                              <option value="<?php echo $row->ID ?>"><?php echo $row->DESCRIPTION; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Request Part Number</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_PART_NUMBER" name="ID_PART_NUMBER" disabled="true" style="width: 100%;" required>
                            <option value="" disabled selected>Search For Part Number</option>
                           
                          </select>
                          <input type="text" class="form-control input-sm" name="OTHER_PN" id="OTHER_PN" maxlength="40">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                          <input type="hidden" class="form-control input-sm" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">
                          <input type="hidden" class="form-control input-sm" id="sla_time" name="sla_time">
                          <input type="hidden" class="form-control input-sm" id="sla_type" name="sla_type">
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Requirement Category</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_REQUIREMENT_CATEGORY" name="ID_REQUIREMENT_CATEGORY" style="width: 100%;" required disabled>
                            <option value="" disabled selected>Choose Requirement Category</option>
                            <?php
                              foreach ($requirement_category_list as $row_rc) {
                            ?>
                            <option value="<?php echo $row_rc->ID_REQUIREMENT_CATEGORY ?>"><?php echo $row_rc->REQUIREMENT_CATEGORY_NAME; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Requested Delivery Date</label>
                        <div class="col-sm-7">
                          <!-- <input type="date" class="form-control input-sm" id="REQUESTED_DELIVERY_DATE" name="REQUESTED_DELIVERY_DATE" required disabled> -->
                          <input type="text" class="form-control input-sm" id="REQUESTED_DELIVERY_DATE" name="REQUESTED_DELIVERY_DATE" required disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <!-- <label class="col-sm-5 control-label">Target Delivery Date</label> -->
                        <div class="col-sm-7">
                          <input type="hidden" class="form-control input-sm" name="TARGET_DELIVERY_DATE" id="TARGET_DELIVERY_DATE" readonly>
                        </div>
                      </div>
                     <!--  <div class="form-group">
                        <label class="col-sm-5 control-label">Maintenance</label>
                        <div class="col-sm-7">
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="No">No
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="MAINTENANCE" value="Yes">Yes
                          </label>
                        </div>
                      </div> -->

                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Ata Chapter</label>
                        <div class="col-sm-7">
                          <input type="text" disabled="true"  class="form-control input-sm" name="ATA_CHAPTER" id="ATA_CHAPTER">
                          <input type="hidden" class="form-control input-sm" name="ATA_CHAPTER_V" id="ATA_CHAPTER_V">
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Aircraft Registration</label>
                        <div class="col-sm-7">
                          <select disabled="true" class="form-control input-sm select2" id="ID_AIRCRAFT_REGISTRATION" name="ID_AIRCRAFT_REGISTRATION" style="width: 100%;" required>
                            <option value="" disabled selected>Search For Aircraft Registration</option>
                          
                          </select>
                          <input type="text" class="form-control input-sm" name="OTHER_AC" id="OTHER_AC" maxlength="10" required>
                          <input type="hidden" class="form-control input-sm" name="AIRCRAFT_REGISTRATION_V" id="AIRCRAFT_REGISTRATION_V">
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-5 control-label">Release Type</label>
                        <div class="col-sm-7">
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="EASA">EASA
                          </label>
                          <label class="checkbox-inline">
                            <input type="checkbox" name="RELEASE_TYPE" value="FAA">FAA
                          </label>
                        </div>
                      </div> -->
                     <!--  <div class="form-group">
                        <label class="col-sm-5 control-label">3 Digit Airport Code</label>
                        <div class="col-sm-7">
                          <div class="row">
                            <div class="col-sm-7">
                              <input type="text" class="form-control input-sm" name="AIRPORT_CODE" id="AIRPORT_CODE" placeholder="">
                            </div>
                            <!-- <div class="col-sm-5">
                              <input type="text" class="form-control input-sm" name="CUSTOMER_RO" placeholder="">
                            </div> 
                          </div>
                        </div>
                      </div> -->
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Transfer Point</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" id="ID_DELIVERY_POINT" name="ID_DELIVERY_POINT" style="width: 100%;" required disabled>
                            <option value="" disabled selected>Choose Transfer Point</option>
                          </select>
                          <input type="text" class="form-control input-sm" name="OTHER_DP" id="OTHER_DP" style="display: none" required>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Destination Address</label>
                        <div class="col-sm-7">
                          <textarea class="form-control input-sm" id="DESTINATION_ADDRESS" name="DESTINATION_ADDRESS" disabled="true" rows="8" cols="22" required=""></textarea>
                          <input type="hidden" class="form-control input-sm" name="DESTINATION_ADDRESS_V" id="DESTINATION_ADDRESS_V"></textarea>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-5 control-label">Customer Order</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="CUSTOMER_PO" id="CUSTOMER_PO" placeholder="" onBlur="checkCustomerOrder()" disabled="true" required><span id="notif_customer_order"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Remarks Of Customer</label>
                        <div class="col-sm-7">
                          <textarea name="REMARKS_OF_CUSTOMER" id="REMARKS_OF_CUSTOMER" type="textarea" class="form-control input-sm" disabled="true"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <br>
                <hr class="title-hr">
                <legend>Attention To </legend>

                <div class="row">

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Existing Email</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" multiple="multiple" id="EXISTING_EMAIL" name="EXISTING_EMAIL[]" style="width: 100%;">
                            <option value="">Choose Existing Email</option>
                            <!-- <option value="herdy.prabowo@gmail.com" selected>herdy.prabowo@gmail.com</option>
                            <option value="riyan.sasmitha@gmail.com">riyan.sasmitha@gmail.com</option>
                            <option value="liyana@gmail.com">liyana@gmail.com</option>
                            <option value="viar@gmail.com">viar@gmail.com</option>
                            <option value="nando@gmail.com">nando@gmail.com</option>
                            <option value="michael@gmail.com">michael@gmail.com</option> -->
                            <!-- <?php
                              //foreach ($request_category_list as $row_rc) {
                            ?>
                            <option value="<?php //echo $row_rc->ID_REQ_CAT ?>"><?php //echo $row_rc->REQ_CAT_NAME.' - '.$row_rc->REQ_CAT_DESC; ?></option>
                            <?php //} ?> -->
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">

                      <div class='form-group'>
                        <label class="col-sm-5 control-label">New Email</label>
                        <div class="col-sm-7">
                          <input type='text' id='new_email' name='new_email' class='form-control input-sm'>
                        </div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-warning" onclick="myreset()">Cancel</button>
                <button type="submit" id="submit" name="insert" class="btn btn-success pull-right">Submit Order</button>
              </div><!-- /.box-footer -->
            </form>

        </div>

      </div>
    </div>
  </div>
</section>


<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
  function myreset(){
    window.location.reload(true);
  }
  function checkCustomerOrder() {
    jQuery.ajax({
      url: "<?=base_url()?>index.php/api/Pooling/check_customer_order",
      data: {
        customer_order: $('#CUSTOMER_PO').val(),
        contract_id: $('#ID_CONTRACT').val()
      },
      type: "POST",
      success:function(data){
        $("#notif_customer_order").html(data);

        if (data == "<p style='color:red'>Costumer Order Has Been Used</p>") {
          $('#submit').attr('disabled', true);
        } else {
          $('#submit').attr('disabled', false);
        }
      }
    });
  }
</script>
<script type="text/javascript">
    $(document).ready(function () {
      $('#new_email').multiple_emails({position: "bottom"});
      var date_input=$('#REQUESTED_DELIVERY_DATE'); 
      var options={
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        autoclose: true,
      };
      // date_input.datepicker(options);
      date_input.datetimepicker({
        format: 'YYYY-MM-DD HH:mm'
      });
      $('#OTHER_AC').hide();
      $('#OTHER_PN').hide();
      
        $('#EXISTING_EMAIL').select2({
            placeholder: 'Select Email',
            width: '100%',
            minimumInputLength: 3,
            triggerChange: true,
            allowClear: true,
            ajax: {
                url: "<?=base_url()?>index.php/api/Pooling/get_existing_email",
                dataType: 'json',
                delay: 450,
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: true
            }
        });

        var customer = $('#customer').val();
        $('#ID_CONTRACT').select2({
            width: '100%'
        });
      
        $('#ID_CONTRACT').change(function () {
            var ID_CONTRACT = $('#ID_CONTRACT').val();
            $('#form-pooling #ID_PART_NUMBER').prop("disabled", false);
            $('#form-pooling #ID_AIRCRAFT_REGISTRATION').prop("disabled", false);
            $('#form-pooling #ID_DELIVERY_POINT').prop("disabled",false);
            $('#form-pooling #CUSTOMER_PO').prop("disabled",false);
            $('#form-pooling #REMARKS_OF_CUSTOMER').prop("disabled",false);

            if (ID_CONTRACT=='X') {
              $('#form-pooling #ATA_CHAPTER').prop("disabled", false);
              $('#form-pooling #COMPONENT_DESCRIPTION').prop("disabled", false);
              $('#form-pooling #COMPONENT_DESCRIPTION_V').prop("disabled", false);
            }else{
              $('#ID_PART_NUMBER').select2({
                placeholder: 'Select Part Number',
                width: '100%',
                minimumInputLength: 2,
                triggerChange: true,
                allowClear: true,
                ajax: {
                  url: "<?=base_url()?>index.php/api/Pooling/get_part_number?contract="+ID_CONTRACT,
                  dataType: 'json',
                  delay: 450,
                  processResults: function (data) {
                    return {
                      results: data
                    }
                  },
                  cache: true
                }
              })

              $('#ID_AIRCRAFT_REGISTRATION').select2({
                  placeholder: 'Select Aircraft Registration',
                  width: '100%',
                  minimumInputLength: 2,
                  triggerChange: true,
                  allowClear: true,
                  ajax: {
                    url: "<?=base_url()?>index.php/api/Pooling/get_fleet_list?contract="+ID_CONTRACT,
                    dataType: 'json',
                    delay: 450,
                    processResults: function (data) {
                      return {
                        results: data
                      }
                    },
                    cache: true
                  }
              })
              
              $('#form-pooling #ID_DELIVERY_POINT').html('').select2();
              $.ajax({
                  url: "<?=site_url('Pooling/get_delivery_point')?>/"+ID_CONTRACT, //memanggil function controller dari url
                  type: "POST", //jenis method yang dibawa ke function
                  success: function(data) {
                      // $('#ID_DELIVERY_POINT').remove();
                      data = $.parseJSON(data);
                      option = '<option value="" disabled selected>Choose Transfer Point</option>'+
                      '<option value="OTHER" data-x="OTHER">OTHER TRANSFER POINT</option>';
                      $.each(data, function(index, val){
                        option += '<option value="'+val['DELIVERY_POINT']+'" data-x="'+val['ID']+'">'+val['DELIVERY_POINT']+'</option>'
                        //var option = new Option(val['DELIVERY_POINT_CODE'], val['ID_DELIVERY_POINT'], true, true);
                        //$('#ID_DELIVERY_POINT').append(option);
                      });
                      $('#ID_DELIVERY_POINT').append(option);
                      //$('#ID_DELIVERY_POINT').trigger('change');
                  }
              });
            }
        });
        $('#ID_AIRCRAFT_REGISTRATION').change(function () {
            var ID_AIRCRAFT_REGISTRATION = $('#ID_AIRCRAFT_REGISTRATION').val();

            if (ID_AIRCRAFT_REGISTRATION == 'OTHER') {
              $('#OTHER_AC').show();
              $('#OTHER_AC').val('');
              $('#AIRCRAFT_REGISTRATION_V').val("");
            }else{
              $('#OTHER_AC').hide();
              $('#OTHER_AC').val(ID_AIRCRAFT_REGISTRATION);
              $('#AIRCRAFT_REGISTRATION_V').val($("#ID_AIRCRAFT_REGISTRATION option:selected").text());
            }
        });

         $('#REQUESTED_DELIVERY_DATE').change(function () {
            var ID_PART_NUMBER = $('#ID_PART_NUMBER').val();
            var ID_CONTRACT = $('#ID_CONTRACT').val();
            var DATE_DELIVERY = $(this).val();
            /*$('#TARGET_DELIVERY_DATE').val("Loading...");
            $.ajax({
              url: "<?=base_url()?>index.php/Pooling/calculate_target_delivery?part="+ID_PART_NUMBER+"&contract="+ID_CONTRACT+"&date="+DATE_DELIVERY,
              success: function(data) {
                  $('#TARGET_DELIVERY_DATE').val(data);
              }
            });*/
          });

        $('#ID_PART_NUMBER').change(function () {
            var ID_PART_NUMBER = $('#ID_PART_NUMBER').val();
            $('#form-pooling #ID_REQUIREMENT_CATEGORY').prop("disabled", false);
            $('#form-pooling #REQUESTED_DELIVERY_DATE').prop("disabled", false).val("");
            $('#TARGET_DELIVERY_DATE').val("");
            $('#OTHER_PN').val(ID_PART_NUMBER);
            if (ID_PART_NUMBER == 'OTHER') {
              $('#OTHER_PN').prop("required", true);
              $('#OTHER_PN').show();
              $('#OTHER_PN').val('');
              $("#COMPONENT_DESCRIPTION_V").prop("disabled",false).val("");
              $("#COMPONENT_DESCRIPTION").val("");
              $("#ATA_CHAPTER").val("");
              $("#sla_type").val("");
              $("#sla_time").val("");
              $("#ATA_CHAPTER_V").val("");
            }else{
              $('#OTHER_PN').prop("required", false);
              $('#OTHER_PN').hide();
              $('#OTHER_PN').val('');
              $("#COMPONENT_DESCRIPTION_V").val("Loading...").prop("disabled",true);
              $("#COMPONENT_DESCRIPTION").val("");
              $.ajax({
                  url: "<?=base_url()?>index.php/Pooling/get_part_number_contract/"+ID_PART_NUMBER, //memanggil function controller dari url
                  type: "POST", //jenis method yang dibawa ke function
                  data: "ID_PN="+ID_PART_NUMBER,
                  success: function(data) {
                      data = $.parseJSON(data);                
                      // console.log(data);
                      $("#ATA_CHAPTER").val(data.ATA);
                      $("#sla_type").val(data.SATUAN_SLA);
                      $("#sla_time").val(data.WAKTU_SLA);
                      $("#ATA_CHAPTER_V").val(data.ATA); 
                      $("#COMPONENT_DESCRIPTION").val(data.DESCRIPTION); //after you explained the JSON response
                      $("#COMPONENT_DESCRIPTION_V").val(data.DESCRIPTION);//after you explained the JSON response
                  }
              });
            }
          });

        $(window).keydown(function(event){
          if( (event.keyCode == 13)) {
            event.preventDefault();
            return false;
          }
        });

        $("#form-pooling").submit(function(event) {
          if (!$('#submit').is('[disabled=disabled]')) {
            event.preventDefault();
            var customer_order_val = $('#notif_customer_order').text();
            if(customer_order_val == "Customer Order Not Available"){
              $("#CUSTOMER_PO").focus();
              return;
            }


            $('#submit').text("Loading...").attr('disabled', true);
            var formData = new FormData($('#form-pooling')[0]);

            $.ajax({
              url:"<?php echo base_url() ?>index.php/Pooling/insert_create_order",
              type:'POST',
              dataType: 'text',
              data: formData,
              processData: false,
              contentType: false,
              success:function(response){
                    //$("#form-pooling")[0].reset();
                    var data = JSON.parse(response);
                    // window.location.href = "update_order/"+data.id;

                    swal({
                      title: data.message,
                      //text: "Once deleted, you will not be able to recover this imaginary file!",
                      icon: data.alert,
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                      } else {
                        location.reload();
                      }
                    });

                      /*swal( data.msg , {
                            icon: "success",
                      })
                      .then(function(willgo){
                          if (willgo) {
                              window.location.href = "update_order/"+data.id;
                          }
                      })*/   // reloading page
              }
            });
          }
        });   
          
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#ID_DELIVERY_POINT').change(function () {
            var selID_DP = $(this).find('option:selected');
            selID_DP = selID_DP.attr("data-x");

            if(selID_DP !='OTHER'){
              $.ajax({
                url: "<?=base_url()?>index.php/Pooling/get_delivery_point_address/", //memanggil function controller dari url
                type: "POST", //jenis method yang dibawa ke function
                data: "ID_DP="+selID_DP, //data yang akan dibawa di url
                success: function(data) {
                    data = $.parseJSON(data);
                    // alert(data.DELIVERY_POINT_ADDRESS);
                    // console.log(data);
                    $("#OTHER_DP").prop("required",false).hide();
                    $("#DESTINATION_ADDRESS").prop("disabled", true);
                    $("#DESTINATION_ADDRESS").val(data.DELIVERY_ADDRESS);
                    $("#DESTINATION_ADDRESS_V").val(data.DELIVERY_ADDRESS);  //after you explained the JSON response
                    // $("#DESTINATION_ADDRESS").attr('disabled', 'disabled');
                }
              });
            }else{
              $("#OTHER_DP").prop("required",true).show();
              $("#DESTINATION_ADDRESS").prop("disabled", false);
              $("#DESTINATION_ADDRESS").val('');
            }

        });
    });
</script>
