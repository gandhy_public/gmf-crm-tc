<style>

  .tfoot .input {
          width: 100%;
          padding: 3px;
          box-sizing: border-box;
      }

</style>
<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content">
    <?php 
        if($this->session->flashdata('alert_failed')){ 
            echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
        }elseif($this->session->flashdata('alert_success')){
            echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
        }
    ?>
    <div class="row">
      <div class="col-lg-12 col-xs-12 col-md-12">
        <div class="box box-solid">
        <div class="box-body with-export-excel">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Filter : </h3>
            </div>
            <div class="panel-body">
              <form id="form-filter" action="<?= base_url('index.php')?>/Pooling/list_order" method="GET" class="form-horizontal">
                <div class="filter-input row" style="margin-left:-10px; margin-right:-10px">
                  <div class="col col-md-4">
                    <div class="form-line">
                      <label for="by_date">Order Status</label>
                      <select class="form-control" id="status_order" name="status_order" style="width: 100%;">
                        <option value="" <?= ($st_order == "" ? "selected" : ""); ?>>All Status</option>
                        <option value="1" <?= ($st_order == "1" ? "selected" : "")?>>Open Order & Waiting Approve</option>
                        <option value="2" <?= ($st_order == "2" ? "selected" : "")?>>In Progress</option>
                        <option value="3" <?= ($st_order == "3" ? "selected" : "")?>>Close</option>
                        <option value="4" <?= ($st_order == "4" ? "selected" : "")?>>Cancel Order</option>
                      </select>
                    </div>
                  </div>
                  <div class="col col-md-4">
                    <div class="form-line">
                      <label for="by_date">Serviceable Status</label>
                      <select class="form-control" id="status_service" name="status_service" style="width: 100%;">
                        <option value="" <?= ($st_service == "" ? "selected" : ""); ?>>All Status</option>
                        <option value="1" <?= ($st_service == "1" ? "selected" : ""); ?>>Waiting Delivery</option>
                        <option value="2" <?= ($st_service == "2" ? "selected" : ""); ?>>Delivered</option>
                      </select>
                    </div>
                  </div>
                  <div class="col col-md-4">
                    <div class="form-line">
                      <label for="by_date">Unserviceable Status</label>
                      <select class="form-control" id="status_unservice" name="status_unservice" style="width: 100%;">
                        <option value="" <?= ($st_unservice == "" ? "selected" : ""); ?>>All Status</option>
                        <option value="1" <?= ($st_unservice == "1" ? "selected" : ""); ?>>Waiting Delivery</option>
                        <option value="2" <?= ($st_unservice == "2" ? "selected" : ""); ?>>Delivered</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="filter-input row" style="margin-left:-10px; margin-right:-10px">
                  <div class="col col-md-4">
                    <div class="form-line">
                      <label for="by_date">Contract</label>
                      <select class="form-control input-sm select2" id="contract" name="contract" style="width: 100%;">
                        <option value="" disabled selected>Search For Contract</option>
                        <?php
                          foreach ($contract_list as $row) {
                        ?>
                        <option value="<?php echo $row->ID ?>" <?= ($contract == $row->ID ? "selected" : ""); ?>><?php echo $row->DESCRIPTION; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-top:15px;margin-left:-10px;margin-right:-10px">
                  <div class="col col-md-12">
                    <button type="submit" id="btn-filter" class="btn btn-primary" style="margin-right:6px">Filter</button>
                    <a href="<?= base_url('index.php')?>/Pooling/list_order"><button type="button" id="btn-reset" class="btn btn-default">Reset</button></a>
                  </div>
                </div>
              </form>
            </div>
        </div>
          <table id="tbpooling" class="table table-bordered table-hover table-striped">
            <!-- <col width="9%">
            <col width="1%">
            <col width="1%">
            <col width="4%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="4%">
            <col width="11%">
            <col width="7%">
            <col width="7%">
            <col width="5%">
            <col width="7%">
            <col width="7%">
            <col width="1%">
            <col width="7%">
            <col width="7%"> -->
            <thead>
              <tr style="background-color: #37474f; color:#ffffff;">
              	<th><center>Action</center></th>
                <th><center>Status</center></th>
                <th><center>Serviceable</center></th>
                <th><center>Unserviceable</center></th>
                <th><center>RefNo</center></th>
                <th><center>Category</center></th>
                <th><center>Part No</center></th>
                <th><center>PN Desc</center></th>
                <th><center>Dest</center></th>
                <th><center>AC Reg</center></th>
                <th><center>Cust PO</center></th>
                <th><center>Request By</center></th>
                <th><center>Request Date</center></th>
                <th><center>Request Delivery Date</center></th>
                <th><center>Target Delivery</center></th>
                <th><center>Delivery Date Serviceable</center></th>
                <th><center>Delivery Date Unserviceable</center></th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </div>
     </div>
   </div>
 </div>
</section>

<form id="export-form" action="<?= base_url('index.php/export/Pooling/ExportListOrderToExcel'); ?>" method="POST">
  <input type="hidden" name="header">
  <input type="hidden" name="query">
</form>

<script>
  var queryListOrder = "";
  $(function () {
    $('#contract').select2({
        width: '100%'
    });
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    var table = $('#tbpooling').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        paging: true,
        sScrollX: true,
        ajax: {
            url: "<?= base_url('index.php/api/Pooling/GetListOrder/') ?>",
            data: {
              st_order: "<?= $st_order?>",
              st_service: "<?= $st_service?>",
              st_unservice: "<?= $st_unservice?>",
              contract: "<?= $contract?>"
            },
            type: "POST",
            complete: function(json, type) {
              queryListOrder = json.responseJSON.query;
            }
        },
        columns: [   
            {data: 'action', name: 'action', orderable: false},
            {data: 'st_order', name: 'st_order', orderable: false},
            {data: 'st_service', name: 'st_service', orderable: false},
            {data: 'st_unservice', name: 'st_unservice', orderable: false},
            {data: 'reff', name: 'reff', orderable: false},
            {data: 'category', name: 'category', orderable: false},
            {data: 'pn', name: 'pn', orderable: false},
            {data: 'pn_desc', name: 'pn_desc', orderable: false},
            {data: 'destination', name: 'destination', orderable: false},
            {data: 'ac', name: 'ac', orderable: false},
            {data: 'customer', name: 'customer', orderable: false},
            {data: 'request', name: 'request', orderable: false},
            {data: 'request_date', name: 'request_date', orderable: false},
            {data: 'request_date_deliv', name: 'request_date_deliv', orderable: false},
            {data: 'target_date', name: 'target_date', orderable: false},
            {data: 'date_service', name: 'date_service', orderable: false},
            {data: 'date_unservice', name: 'date_unservice', orderable: false},
        ],
        // fnInitComplete: function() {
        //   $('#tbpooling thead tr').each(function(){
        //     $(this).find('th:eq(8)').attr('nowrap', 'nowrap');
        //   });
        //   $('#tbpooling tbody tr').each(function(){
        //     $(this).find('td:eq(8)').attr('nowrap', 'nowrap');
        //   });
        // },
        columnDefs: [
          {
            targets: 8,
            className: 'dest'
          }
        ],
        dom: 'Blfrtip',
        buttons: []
    }); 

    $('#tbpooling tbody tr').each(function(){
      $(this).find('td:eq(8)').attr('nowrap', 'nowrap');
    });
    
    table.on("click", ".btn-confirm", function(){
      id_data = $(this).data('x');
      swal({
        title: 'Confirm Order',
        text: 'Are you sure want to confirm this order?',
        icon: 'success',
        buttons: true,
        dangerMode: true

      })
      .then(function(willUpdate){
          if (willUpdate) {
              window.location.href = "update_order/"+id_data+"/2";
          }
      })
    });

    table.on("click", ".btn-view", function(){
      id_data = $(this).data('x');
      window.location.href = "update_order/"+id_data+"/0";
    });

    table.on("click", ".btn-cancel", function(){
      id_data = $(this).data('x');
      swal({
        title: 'Confirm Order',
        text: 'Are you sure want to cancel this order?',
        icon: 'success',
        buttons: true,
        dangerMode: true

      })
      .then(function(willUpdate){
          if (willUpdate) {
              window.location.href = "update_order/"+id_data+"/4";
          }
      })
    });

    table.on("click", ".btn-complete", function(){
      id_data = $(this).data('x');
      swal({
        title: 'Confirm Order',
        text: 'Are you sure want to complete this order?',
        icon: 'success',
        buttons: true,
        dangerMode: true

      })
      .then(function(willUpdate){
          if (willUpdate) {
              window.location.href = "update_order/"+id_data+"/3";
          }
      })
    });

    // export button
    table.button().add(0, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#tbpooling thead tr th").not(':first').map(function() { 
          header.push($.trim($(this).find('center').text()));
        }).get();

        $('#export-form [name=header]').val(header);
        $('#export-form [name=query]').val(queryListOrder);
        
        setTimeout(function() {
          $('#export-form').submit();
        }, 1000);
      },
      text: 'Export to Excel',
      className: 'btn btn-success btn-sm'
    });

  });
</script>
