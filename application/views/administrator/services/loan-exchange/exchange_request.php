<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<?php if ($this->session->flashdata('alert_success')) { echo '<div id="alert_success" class="callout callout-success">'; echo '<p>'; echo $this->session->flashdata('alert_success'); echo '</p>'; echo '</div>'; } ?>
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<!--<div class="box-header">-->
		                    <!--<div class="col-md-12">
		                        <h3 class="box-title">Data Retail Order</h3>
		                    </div>-->
		                    <!--<div class="col-md-10">
		                        <div class="input-group">
		                            <div class="checkbox">
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Approved" name="status" value="Delivered">
		                                Delivered
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Material" name="status" value="Quotation Provided Awaiting Approval">
		                                Quotation Provided Awaiting Approval
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Under_Maintenance" name="status" value="Approval Received Repair Started">
		                                Approval Received Repair Started
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Repair" name="status" value="Ready to Deliver">
		                                Ready to Deliver
		                              </label>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-2 pull-right">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div>-->
		                    <!--<div class="col-md-2 pull-right">
		                        <div class="input-group">
		                            <select class="form-control select2" id="STATUS_PAID" name="STATUS_PAID">
		                                <option value="Paid" <?php //if($this->uri->segment(4)=='Waiting_Approved') { echo 'selected'; } ?>>Paid</option>
		                                <option value="Unpaid" <?php //if($this->uri->segment(4)=='Waiting_Material') { echo 'selected'; } ?>>Unpaid</option>    
		                            </select>
		                        </div>
		                    </div>-->
		                <!-- </div> -->
		                <br>
		                <div class="box-body table-responsive no-padding">
		                    <table id="example2" class="table table-bordered table-hover table-striped">
					            <thead>
					            <tr>
					              <th>No</th>
					              <th>Part Number</th>
					              <th>Exchange Type</th>
					              <th>Exchange Rate</th>
					              <th>Condition</th>
					              <th>Contact Info</th>
					            </tr>
					            </thead>
					            <tbody>
						            <?php
		                                if($data_table) { 
		                                    $no = 1;
		                                    foreach ($data_table as $rows) {
		                            ?>
		                            <tr <?php if ($this->session->flashdata('alert_success') && $no==1) echo 'style="background-color: #5fe87d;"'; ?>>
		                                <td><?php echo $no; ?></td>
		                                <td><?php echo $rows->PART_NUMBER; ?></td>
		                                <td><?php echo $rows->EXCHANGE_TYPE; ?></td>
		                                <td><?php echo $rows->EXCHANGE_RATE; ?></td>
		                                <td><?php echo $rows->CONDITION; ?></td>
		                                <td><?php echo $rows->CONTACT_INFO; ?></td>
		                                <!-- <td>
		                                	<?php 
			                                	//$source = $rows->APPROVAL_DATE;
			                                    //$date = new DateTime($source);
			                                    //echo $date->format('d-m-Y'); 
			                             	?>
		                                </td> -->
		                            </tr>
		                            <?php $no++; } }?>
					            </tbody>
					            <tfoot>
					            <tr>
					              <th>No</th>
					              <th>Part Number</th>
					              <th>Exchange Type</th>
					              <th>Exchange Rate</th>
					              <th>Condition</th>
					              <th>Contact Info</th>
					            </tr>
					            </tfoot>
					        </table>
		                    <!--<br>
		                    <div class="col-md-3">
		                        <b><?php //if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                    </div>
		                    <div class="col-md-6">
		                        <?php //echo $paging; ?>
		                    </div>
		                    <div class="col-md-3 pull-right">
		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php //echo $title; ?></button></a>
		                    </div>-->

		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000); // <-- time in milliseconds
  });
</script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>