<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<!--<div class="box-header">-->
		                    <!--<div class="col-md-12">
		                        <h3 class="box-title">Data Retail Order</h3>
		                    </div>-->
		                    <!--<div class="col-md-10">
		                        <div class="input-group">
		                            <div class="checkbox">
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Approved" name="status" value="Delivered">
		                                Delivered
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Material" name="status" value="Quotation Provided Awaiting Approval">
		                                Quotation Provided Awaiting Approval
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Under_Maintenance" name="status" value="Approval Received Repair Started">
		                                Approval Received Repair Started
		                              </label>
		                              <label>
		                                <input type="checkbox" class="cbstatus" id="Waiting_Repair" name="status" value="Ready to Deliver">
		                                Ready to Deliver
		                              </label>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-2 pull-right">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div>-->
		                    <!--<div class="col-md-2 pull-right">
		                        <div class="input-group">
		                            <select class="form-control select2" id="STATUS_PAID" name="STATUS_PAID">
		                                <option value="Paid" <?php //if($this->uri->segment(4)=='Waiting_Approved') { echo 'selected'; } ?>>Paid</option>
		                                <option value="Unpaid" <?php //if($this->uri->segment(4)=='Waiting_Material') { echo 'selected'; } ?>>Unpaid</option>    
		                            </select>
		                        </div>
		                    </div>-->
		                <!-- </div> -->
		                <br>
		                <div class="box-body table-responsive no-padding">
		                    <table id="example1" class="table table-bordered table-striped">
					            <thead>
					            <tr>
					              <th>No</th>
					              <th>Sales Order</th>
					              <th>Part Number</th>
					              <th>Description</th>
					              <!-- <th>Serial Number</th>
					              <th>U/S</th>
					              <th>Core Max</th>
					              <th>Start Date</th>
					              <th>End Date</th> -->
					            </tr>
					            </thead>
					            <tbody>
					            <tr>
					              <td>1</td>
					           	  <td>92737</td>
					              <td>897665439</td>
					              <!-- <td>YQWE781213Y</td> -->
					              <td>Wing</td>
					              <!-- <td>13-12-2017</td>
					              <td>13-03-2018</td> -->
					            </tr>
					            <tr>
					              <td>2</td>
					              <td>92738</td>
					              <td>897608139</td>
					              <!-- <td>YQWE178213Y</td> -->
					              <td>Gear</td>
					              <!-- <td>02-12-2017</td>
					              <td>02-03-2018</td> -->
					            </tr>
					            <tr>
					              <td>3</td>
					              <td>92739</td>
					              <td>897665739</td>
					              <!-- <td>YHGE781213Y</td> -->
					              <td>Engine</td>
					              <!-- <td>28-01-2018</td>
					              <td>28-04-2018</td> -->
					            </tr>   
					            <tr>
					              <td>4</td>
					              <td>92740</td>
					              <td>897165439</td>
					              <!-- <td>YLKE781213Y</td> -->
					              <td>Cabin</td>
					              <!-- <td>13-12-2017</td>
					              <td>31-03-2018</td> -->
					            </tr>
					            <tr>
					              <td>5</td>
					              <td>92741</td>
					              <td>899665439</td>
					              <!-- <td>YAIE781213Y</td> -->
					              <td>Valve</td>
					              <!-- <td>13-01-2018</td>
					              <td>13-04-2018</td> -->
					            </tr>
					            </tbody>
					            <tfoot>
					            <tr>
					              <th>No</th>
					              <th>Sales Order</th>
					              <th>Part Number</th>
					              <th>Description</th>
					              <!-- <th>Serial Number</th> 
					              <th>U/S</th>
					              <th>Core Max</th>
					              <th>Start Date</th>
					              <th>End Date</th> -->
					            </tr>
					            </tfoot>
					        </table>
		                    <!--<br>
		                    <div class="col-md-3">
		                        <b><?php //if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                    </div>
		                    <div class="col-md-6">
		                        <?php //echo $paging; ?>
		                    </div>
		                    <div class="col-md-3 pull-right">
		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php //echo $title; ?></button></a>
		                    </div>-->

		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
  })
</script>