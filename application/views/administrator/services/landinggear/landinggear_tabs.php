<section class="content-header">
  <h1>
        Landing Gear
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-edit"></i> TC Services Support</a></li>
         <li class="active">Landing Gear</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

        <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <!-- /.Tab Pane Project List -->
              <li class="active"><a class="fa fa-book" href="#tab_project_list" data-toggle="tab"> Project List</a></li>
              <!-- /.Tab Pane Overview -->
              <li><a class="fa fa-dashboard" href="#tab_overview" data-toggle="tab"> Overview</a></li>
              <!-- /.Tab Pane Profit Analysis -->
              <li><a class="fa fa-flag" href="#tab_profit_analysis" data-toggle="tab"> Profit Analysis</a></li>
              <!-- /.Tab Pane CSI Survey -->
              <li><a class="fa fa-space-shuttle" href="#tab_csi_survey" data-toggle="tab"> CSI Survey</a></li>
            </ul>


          <!-- /.Tab Pane Main -->
          <div class="tab-content">

            <!-- /.Tab Pane Project List -->
            <div class="tab-pane active" id="tab_project_list">
                <b>Project List</b>
            </div>


            <!-- /.Tab Pane Overview -->
            <div class="tab-pane" id="tab_overview">
                <b>Overview</b>
              <section class="content-header">
                <h1 align="center">
                  <u>Overview</u>
                  <br>802696
                </h1>
              </section>

              <section class="content">
                    <div class="box-body">
                    <div class="container">
                          <div class="row" style="background-color: #E0E0E0;">
                            <div class="col-sm-3">
                              <th>Landing Gear Owner</th><br>
                              <th>Customer          </th><br>
                              <th>Workscope         </th>
                            </div>
                            <div class="col-sm-3">
                              <th>: User Garuda</th><br>
                              <th>: Garuda Indonesia</th><br>
                              <th>: Minimum</th>
                            </div>
                            <div class="col-sm-3">
                              <th>Type               </th><br>
                              <th>Induction Date     </th><br>
                              <th>Contractual TAT    </th>
                            </div>
                            <div class="col-sm-3">
                              <th>: CFM56-7B26</th><br>
                              <th>: 15 Februari 2017</th><br>
                              <th>: 14</th>
                            
                            </div>
                          </div>
                    </div>
                    </div>
               <br>
        
              <div class="container">
                  <div class="row">

                    <div class="col-sm-3">
                      <div class="info-box">
                        <span class="info-box-icon" style="background-color: #65b328;"><i class="fa fa-calendar"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text"  >Actual TAT</span>
                          <span class="info-box-number">26 Days</span>
                          <span class="info-box-text"  >364 of 975</span>
                        </div>
                      </div>
                      <div class="info-box">
                        <span class="info-box-icon" style="background-color: #0dacbe;"><i class="fa fa-list-alt"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text"  >Job Card</span>
                          <span class="info-box-number">28 Days</span>
                          <span class="info-box-text"  >364 of 975</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-9" style="background-color:#E0E0E0;"><br>
                        <div class="progress">
                          <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:05%">
                            5% Complete (Open)
                          </div>
                        </div>

                        <div class="progress">
                          <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:67%">
                            67% Complete (In Progress)
                          </div>
                        </div>

                        <div class="progress">
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:28%">
                            28% Complete (Closed)
                          </div>
                        </div>
                    </div>

                  </div>
              </div>

              <div class="row">
                    <div class="container">
                      <div class="col-sm-12">          
                        <table class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                <th>NO</th>
                                <th>MAT</th>
                                <th>DESCRIPTION</th>
                                <th>STATUS</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <td>1</td>
                                <td>REM</td>
                                <td>FUEL TUBES P7&PB</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>REM</td>
                                <td>FWD SUMP OILSUPLY TUBE & ANTISIPHON TUBE</td>
                                <td>In Progress</td>
                              </tr>
                              <tr>
                                <td>3</td>
                                <td>REM</td>
                                <td>P12&PS12 AIRMANIFOLD, PS12 AIR SUPPLYTUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>4</td>
                                <td>REM</td>
                                <td>N1 SPEED SENSOR GUIDE TUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>5</td>
                                <td>REM</td>
                                <td>TUBING GUIDE TUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>6</td>
                                <td>REM</td>
                                <td>OIL SCAVENGE TUBES</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>7</td>
                                <td>REM</td>
                                <td>OIL SUPPLY TUBES</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>8</td>
                                <td>REM</td>
                                <td>NO1 BEARING VIBR SENSOR FRONT GUIDE TUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>9</td>
                                <td>REM</td>
                                <td>NO1 BEARING VIBR SENSOR REAR GUIDE TUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>10</td>
                                <td>REM</td>
                                <td>NO1 BEARING VIBR SENSOR REAR GUIDE TUBE</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                               <td>11</td>
                                <td>REM</td>
                                <td>AGB RH MOUNT LINK ASSEMBLY</td>
                                <td>In Progress</td>
                              </tr>
                              <tr>
                                <td>12</td>
                                <td>REM</td>
                                <td>AGB LH MOUNT LINK ASSEMBLY</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>13</td>
                                <td>DSY</td>
                                <td>OIL TANK UPPER SHOCK MOUNT</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>14</td>
                                <td>DSY</td>
                                <td>TGB REAR CLEVIS MOUNT</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>15</td>
                                <td>DSY</td>
                                <td>AGB BRACKET</td>
                                <td>In Progress</td>
                              </tr>
                              <tr>
                                <td>16</td>
                                <td>DSY</td>
                                <td>VARIABLE BLEED VALVE FEEDBACK ROD</td>
                                <td>In Progress</td>
                              </tr>
                              <tr>
                                <td>17</td>
                                <td>DSY</td>
                                <td>BRACKET</td>
                                <td>Open</td>
                              </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
              </div>
              </section>
            </div>
            
      
            <!-- /.tab-pane New Order-->    
            <div class="tab-pane" id="tab_profit_analysis">
                <h5>
                  Profit Analisys
                </h5>
                <form>
                  <section class="content-header">
                      <h1 align="center">
                          <u>Profit Analysis</u>
                          <br>802696
                      </h1>                      
                  </section>

                  <section class="content">
                       <div class="container">
                          <div class="row">
                            <div class="col-sm-6" style="background-color:white;">
                              <!-- #f4f1f4 -->
                              <th>Engine Owner : Danu</th><br>
                              <th>Customer : Garuda Indonesia</th><br>
                              <th>Workscope : Minimum</th>
                            </div>
                            <div class="col-sm-6" style="background-color:white;">
                              <th>Type : CFM56-7B26</th><br>
                              <th>Induction Date  : 16 Januari 2017</th><br>
                              <th>Contractual TAT  : 14</th>
                            </div>
                          </div>
                        </div>
                       <br>
                        <div class="row">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="info-box" style="background-color: #deebf7">
                              <span class="info-box-icon"><i class="fa fa-money"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Revenue</span>
                                <span class="info-box-number">$2.000.000</span>

                               <!--  <div class="progress">
                                  <div class="progress-bar" style="width: 100%"></div>
                                </div> -->                            
                              </div>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="info-box" style="background-color: #fbe5d6">
                              <span class="info-box-icon"><i class="fa fa-scissors"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Cost</span>
                                <span class="info-box-number">$1.500.000</span>

                                <!-- <div class="progress">
                                  <div class="progress-bar" style="width: 100%"></div>
                                </div> -->                                   
                              </div>
                            </div>
                          </div>                      
                      
                      <div class="row">
                         
                          <div class="col-md-4">
                            <div class="box box-success">
                              <div class="box-header with-border">
                                <h3 class="box-title">Total</h3>

                                <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                  </button>
                                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                              </div>
                              <div class="box-body chart-responsive">
                                <div class="chart" id="bar-chart" style="height: 300px;">
                                  <canvas id="bar-chart-grouped" width="800" height="500"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                      
                      <div class="col-md-4">
                            <div class="box box-success">
                              <div class="box-header with-border">
                                <h3 class="box-title">Job Card</h3>

                                <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                  </button>
                                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                              </div>
                              <div class="box-body chart-responsive">
                                <div class="chart" id="bar-chart" style="height: 300px;">
                                  <canvas id="bar-chart-grouped1" width="800" height="500"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                      
                      <div class="col-md-4">
                            <div class="box box-success">
                              <div class="box-header with-border">
                                <h3 class="box-title">MDR</h3>

                                <div class="box-tools pull-right">
                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                  </button>
                                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                              </div>
                              <div class="box-body chart-responsive">
                                <div class="chart" id="bar-chart" style="height: 300px;">
                                  <canvas id="bar-chart-grouped2" width="800" height="500"></canvas>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
                </section>          
                </form>
            </div>

            <!-- /.Tab Pane Order -->
            <div class="tab-pane" id="tab_eo_report" role="presentation">
              <b>
                EO Report
              </b>
                  <div class="row">
                      <div class="col-xs-12">
                          <table id="tabel-data" class="display" cellspacing="0" width="100%">
                                  <thead>
                                      <tr>
                                          <th>NO</th>
                                          <th>ACCOUNT</th>
                                          <th>REFERENCEC</th>
                                          <th>ASSIGNMENT</th>
                                          <th>DOCUMENT DATE</th>
                                          <th>CCY</th>
                                          <th>VALUE</th>
                                          <th>MHRS</th>
                                          <th>MAT</th>
                                          <th>OTHER</th>
                                          <th>PPN</th>
                                          <th>PPH</th>
                                          <th>ACTION</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <?php 
                                          $no = 1;
                                          foreach($tb_pooling as $tb_pool){ 
                                      ?>
                                      <tr>
                                          <td><?php echo $no++ ?></td>
                                          <td><?php echo $tb_pool->ACCOUNT ?></td>
                                          <td><?php echo $tb_pool->REFERENCE ?></td>
                                          <td><?php echo $tb_pool->ASSIGNMENT ?></td>
                                          <td><?php echo $tb_pool->DOCUMENT_DATE ?></td>
                                          <td><?php echo $tb_pool->CCY ?></td>
                                          <td><?php echo $tb_pool->VALUE ?></td>
                                          <td><?php echo $tb_pool->MHRS ?></td>
                                          <td><?php echo $tb_pool->MAT ?></td>
                                          <td><?php echo $tb_pool->OTHER ?></td>
                                          <td><?php echo $tb_pool->PPN ?></td>
                                          <td><?php echo $tb_pool->PPH ?></td>
                                          <td>
                                              <?php echo anchor('landinggear_control/edit/'.$tb_pool->NO_ACCOUNT,'Edit'); ?>
                                              <?php echo anchor('landinggear_control/hapus/'.$tb_pool->NO_ACCOUNT,'Hapus'); ?>
                                          </td>
                                      </tr>
                                          <?php } ?>
                                  </tbody>
                              </table>
                      </div>
                  </div>
            </div>

            <!-- /.Tab Pane CSI Survey -->
            <div class="tab-pane" id="tab_csi_survey" role="presentation">
              <h5>
                CSI Survey
              </h5>

              <div class="panel panel-default">
                <div class="panel-heading">Plugins jquery Jqwidgets</div>
                  <div class="panel-body">
                    <div class="form-group">    
                      <div class="col-md-12">
                        <div id='chartContainer' style="height: 400px;"></div>
                      </div>
                    </div>      
                  </div>
              </div>      

            </div>


    

              </div>                  
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

     </div>
        <!-- /.col -->
      </div>
</div>
</section>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.2.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jqwidgets/jqxdraw.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jqwidgets/jqxchart.core.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jqwidgets/jqxdata.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
// memanggil data array dengan JSON
var source =
     {
         datatype: "json",
         datafields: [
        { name: 'hasil' },
        { name: 'total' }
         ],
         url: '<?php echo base_url() ?>chart_pie/survey_framework'
     };
var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
// pengaturan jqxChart
  var settings = {
        title: "Survey Framework terbaik",
        description: "",
        enableAnimations: true,
        showLegend: true,
        showBorderLine: true,
        legendLayout: { left: 10, top: 160, width: 300, height: 200, flow: 'vertical' },
        padding: { left: 5, top: 5, right: 5, bottom: 5 },
        titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
        source: dataAdapter,
        colorScheme: 'scheme03',
        seriesGroups:
           [
            {
              type: 'pie',
              showLabels: true,
              series:
        [
                  { 
                     dataField: 'total',
           displayText: 'hasil',
                     labelRadius: 170,
                     initialAngle: 15,
                     radius: 145,
                     centerOffset: 0,
                     formatFunction: function (value) {
                    if (isNaN(value))
                      return value;
                      return parseFloat(value);
                                        },
                  }
                ]
             }
           ]
         };
     // Menampilkan Chart
      $('#chartContainer').jqxChart(settings);
   });
</script>


<!-- <script type="text/javascript">
$('.sparkline').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#00A65A', '#00c0ef', '#F7BA1C'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });

        $('.sparkline2').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#605ca8', '#d2d6de', '#f56954'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });



</script> -->
<!-- <script>
var bar = new Morris.Bar({
            element: 'bar-chart7',
            resize: true,
            data: [
               {y: 'Perform Run Up', a: 80 },
                {y: 'Opening', a: 90 },
                {y: 'Inspection', a: 70},
                {y: 'Repair', a: 60},
                {y: 'Install', a: 50},
                {y: 'FNC', a: 60},
                {y: 'RECT', a: 50}
            ],
            barColors: ['#f56954'],
            xkey: 'y',
            ykeys: ['a'],
            labels: [''],
            hideHover: 'auto'
        });

 var bar = new Morris.Bar({
            element: 'bar-chart3',
            resize: true,
            data: [
                {y: 'Open', a: <?php echo $total_jc_open; ?>, b: <?php echo $total_mdr_open; ?>},
                {y: 'Hangar', a: <?php echo $total_jc_hangar; ?>, b: <?php echo $total_mdr_hangar; ?>},
                {y: 'WSSS', a: <?php echo $total_jc_wsss; ?>, b: <?php echo $total_mdr_wsss; ?>},
                {y: 'WSSE', a: <?php echo $total_jc_wsse; ?>, b: <?php echo $total_mdr_wsse; ?>},
                {y: 'WSCB', a: <?php echo $total_jc_wscb; ?>, b: <?php echo $total_mdr_wscb; ?>},
                {y: 'WSLS', a: <?php echo $total_jc_wsls; ?>, b: <?php echo $total_mdr_wsls; ?>}
            ],
            barColors: ['#f56954', '#31b0d5'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['JC', 'MDR'],
            hideHover: 'auto'
        });

</script> -->
<!-- <script>        

/// Return with commas in between
var numberWithCommas = function(x) {
return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

var dataPack1 = [21000, 22000, 26000, 35000, 55000, 55000, 56000, 59000, 60000, 61000, 60100, 62000];
var dataPack2 = [1000, 1200, 1300, 1400, 1060, 2030, 2070, 4000, 4100, 4020, 4030, 4050];
var dates = ["A/P", "CBN", "E/A", "NDT", "PAINTING", "STR", 
           "TBR-SEALANT", "TBR-SHOP"];

// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

var bar_ctx = document.getElementById('bar-chart_122');
var bar_chart = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: dates,
        datasets: [
        {
            label: 'Close',
            data: dataPack1,
            backgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Grand Total',
            data: dataPack2,
            backgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        ]
    },
    options: {
         animation: {
          duration: 10,
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
          }
          }
         },
        scales: {
          xAxes: [{ 
            stacked: true, 
            gridLines: { display: false },
            }],
          yAxes: [{ 
            stacked: true, 
            ticks: {
              callback: function(value) { return numberWithCommas(value); },
             }, 
            }],
        }, // scales
        legend: {display: true}
    } // options
   }
);

</script> -->

<script>        

    // Return with commas in between
var numberWithCommas = function(x) {
return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

var dataPack3 = [21000, 22000, 26000, 35000, 55000, 55000, 56000, 59000, 60000, 61000, 60100, 62000];
var dataPack4 = [21000, 15000, 12000, 14000, 10060, 20030, 20070, 40000, 40100, 41020, 42030, 43050];
var dates1 = ["Open", "Hangar", "WSSS", "WSSE", "WSCB", "WSLS"];

// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

var bar_ctx = document.getElementById('bar-chart_144');
var bar_chart = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: dates1,
        datasets: [
        {
            label: 'MDR',
            data: dataPack3,
            backgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'JC',
            data: dataPack4,
            backgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        ]
    },
    options: {
         animation: {
          duration: 10,
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
          }
          }
         },
        scales: {
          xAxes: [{ 
            stacked: true, 
            gridLines: { display: false },
            }],
          yAxes: [{ 
            stacked: true, 
            ticks: {
              callback: function(value) { return numberWithCommas(value); },
             }, 
            }],
        }, // scales
        legend: {display: true}
    } // options
   }
);

</script>
<script type="text/javascript">
(function() {
  
        $('#example_sip2').DataTable({
            dom: 'Bfrtip',
      'paging': false,
            buttons: [
            ],
        })
    })
  
  $(function() {

    var t = $('#example_sip2').DataTable();
    var counter = 1;
  

 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            '<td><input type="text"></td>',
            counter +'.2',
            counter +'.3',
            counter +'<td><input type="button" id="deleteRow" value="Delete Row" onClick="removeRow(this)"/></td>'
        ] ).draw( false );
 
        counter++;
    } ) 
   
    // Automatically add a first row of data
    $('#addRow').click(); 
  
} )
  
  function removeRow(oButton) {
        var empTab = document.getElementById('example_sip2');
        empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);       // BUTTON -> TD -> TR.
    }

</script>
<script type="text/javascript">
(function() {
  
        $('#example_sip3').DataTable({
            dom: 'Bfrtip',
      'paging': false,
            buttons: [
            ],
        })
    })
  
  $(function() {

    var t = $('#example_sip3').DataTable();
    var counter = 1;
  

 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            '<td><input type="text"></td>',
            counter +'.2',
            counter +'.3',
            counter +'<td><input type="button" id="deleteRow" value="Delete Row" onClick="removeRow(this)"/></td>'
        ] ).draw( false );
 
        counter++;
    } )
  
   
    // Automatically add a first row of data
    $('#addRow').click();
  
  
  
  
} )
  
  function removeRow(oButton) {
        var empTab = document.getElementById('example_sip3');
        empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);       // BUTTON -> TD -> TR.
    }

</script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "scrollY": 200,
        "scrollX": true
    } );
} );
</script>
<script>

    $('#example3').DataTable( {
        "scrollY": 200,
        "scrollX": true
    } );
</script>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Plan Cost",
          backgroundColor: "#1f78b4",
          data: [520,300]
        }, {
          label: "Actual Cost",
          backgroundColor: "#fe7f0e",
          data: [408,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped1"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Mhrs Plan Cost",
          backgroundColor: "#548235",
          data: [720,300]
        }, {
          label: "Mhrs Act Cost",
          backgroundColor: "#a9d18e",
          data: [500,300]
        }, {
          label: "Mhrs Plan",
          backgroundColor: "#2e75b6",
          data: [600,700]
        }, {
          label: "Mhrs Act",
          backgroundColor: "#bdd7ee",
          data: [680,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
   new Chart(document.getElementById("bar-chart-grouped2"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Mhrs Plan Cost",
          backgroundColor: "#548235",
          data: [720,300]
        }, {
          label: "Mhrs Act Cost",
          backgroundColor: "#a9d18e",
          data: [500,300]
        }, {
          label: "Mhrs Plan",
          backgroundColor: "#2e75b6",
          data: [600,700]
        }, {
          label: "Mhrs Act",
          backgroundColor: "#bdd7ee",
          data: [720,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
</script>