<section class="content-header">
  <h1>
        Wings Pesawat
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project Summary</li>
          <li class="active">Wings Pesawat</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

   
        <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_dashboard" data-toggle="tab">Dashboard</a></li>
              <li><a href="#tab_jobcard" data-toggle="tab">Jobcard</a></li>
              <li><a href="#tab_production_planning" data-toggle="tab">Production Planning</a></li>
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Sales Order <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" data-toggle="tab" href="#tab_invoices">Inovoices</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" data-toggle="tab" href="#tab_estimates">Estimates</a></li>
                 
                </ul>
              </li>
              <li>
                <a href="#tab_mrm" data-toggle="tab">MRM</a>
              </li>
                 <li>
                <a href="#tab_ticket" data-toggle="tab">Ticket</a>
              </li>
                  <li>
                <a href="#tab_daily" data-toggle="tab">Daily</a>
              </li>
                  <li>
                <a href="#tab_files" data-toggle="tab">Files</a>
              </li>
                   <li>
                <a href="#tab_discussions" data-toggle="tab">Discussions</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_dashboard">
                <b>How to use:</b>

     <div class="row">
            <div class="col-lg-6 col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">JC STATUS</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body piechart col-md-4">
                    <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                         <?php echo $total_jc_close; ?>,<?php echo $total_jc_open; ?>,<?php echo $total_jc_progress; ?>
                    </div>
                </div>
                <div class="box-body piechart col-md-4">
                    <ul class="chart-legend clearfix">
                        <li><i class="fa fa-square text-aqua"></i> Open</li>
                        <li><i class="fa fa-square text-yellow"></i> Progress</li>
                        <li><i class="fa fa-square text-green"></i> Close</li>
                    </ul>
                </div>
                <div class="box-body piechart col-md-4">
                    <ul class="chart-legend clearfix">
                        <li class="text-aqua"><b><?php echo $percentage_jc_open_pie; ?>%</b></li>
                        <li class="text-yellow"><b><?php echo $percentage_jc_progress_pie; ?>%</b></li>
                        <li class="text-green"><b><?php echo $percentage_jc_close_pie; ?>%</b></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- ./col -->
     <div class="col-lg-6 col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">MDR STATUS</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body piechart col-md-4">
                    <div class="sparkline2" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                        <?php echo $total_mdr_close; ?>,<?php echo $total_mdr_open; ?>,<?php echo $total_mdr_progress; ?>
                    </div>
                </div>
                <div class="box-body piechart col-md-4">
                    <ul class="chart-legend clearfix">
                        <li><i class="fa fa-square text-gray"></i> Open</li>
                        <li><i class="fa fa-square text-red"></i> Progress</li>
                        <li><i class="fa fa-square text-purple"></i> Close</li>
                    </ul>
                </div>
                <div class="box-body piechart col-md-4">
                    <ul class="chart-legend clearfix">
                        <li class="text-aqua"><b><?php echo $percentage_mdr_open_pie; ?>%</b></li>
                        <li class="text-yellow"><b><?php echo $percentage_mdr_progress_pie; ?>%</b></li>
                        <li class="text-green"><b><?php echo $percentage_mdr_close_pie; ?>%</b></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- ./col -->
          
        <!-- ./col -->

        <!-- ./col -->
    </div>

    <div class="row">
      <div class="col-md-12">
            <!-- BAR CHART -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">JOBCARD PERPAHSE</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="bar-chart7" style="height: 300px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-8">
            <!-- BAR CHART -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Progress Status Per Area</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                 <div class="chart">
                 <canvas id="bar-chart_144" width="800" height="450"></canvas>
                </div>
               <!--  <div class="box-body chart-responsive">
                    <div class="chart" id="bar-chart_144" style="height: 300px;"></div>
                </div> -->
                <!-- /.box-body -->
            </div>
        </div>
     <div class="col-md-4">
            <!-- DONUT CHART -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Close All Area</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body chart-responsive">
                    <div style="height: 300px; position: relative;">
                        <div class="text-center close-area ">
                            <div class="border-bottom">
                                <h1 class="description-percentage text-green">100%</h1>
                                <p> TOTAL JOBCARD <b class="text-green">34</b>of <b class="text-red">34</b> </p>
                            </div>
                        </div>

                        <div class="text-center close-area">
                            <h1 class="description-percentage text-yellow">100%</h1>
                            <p> TOTAL MDR <b class="text-green">56</b>of <b class="text-red">56</b> </p>
                            
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Material Status</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body chart-responsive" style="height: 320px;">
                    <div><p class ="pull-right">50</p></div>
                    <p>NIL STOCK</p> <p class ="pull-right">50%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-aqua progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                    <div><p class ="pull-right">60</p></div>
                    <p>SOA</p> <p class ="pull-right">60%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-succes progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                     <div><p class ="pull-right">100</p></div>
                    <p>CUST. SPLY</p><p class ="pull-right">100%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-red progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                    <div><p class ="pull-right">100</p></div>
                    <p>ORDERED PURC</p><p class ="pull-right">100%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                <div><p class ="pull-right">100</p></div>
                    <p>CUSTOME</p><p class ="pull-right">100%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>
                      <div><p class ="pull-right">78</p></div>
                    <p>SHIPMENT</p><p class ="pull-right">78%</p>
                    <div class="progress">
                        <div class="progress-bar progress-bar-violet progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                            <span class="sr-only">78% Complete (success)</span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col (LEFT) -->

               <div class="col-md-8">
            <!-- BAR CHART -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">JOBCARD BY SKILL</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="chart">
                 <canvas id="bar-chart_122" width="800" height="450"></canvas>
                </div>
                
                <!-- <div class="charts">
                    <canvas id="bar-chart_x"  style="height: 300px;">
                </div>  -->
            </div>
                <!-- /.box-body -->
            </div>
        </div>
      
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_jobcard">
               <h5>
               JobCard
                   </h5>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_production_planning">
                 <h5>
               Production Planning
                   </h5>
               
              <section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">

                    <table class="tabledata2 table table-bordered table-striped" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Revision</th>
                                <th>Order Type</th>
                                <th>Description</th>
                                <th>Plant</th>
                                <th>#</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $listProject ?>
                        </tbody>
                        <tfoot>
                        <th>No.</th>
                        <th>Revision</th>
                        <th>Order Type</th>
                        <th>Description</th>
                        <th>Plant</th>
                        <th>#</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

</section>
              </div>
               <div class="tab-pane" id="tab_invoices" role="presentation">
                invoices
               
              </div>
                 <div class="tab-pane" id="tab_estimates" role="presentation">
               estimates
              </div>
                <div class="tab-pane" id="tab_mrm">
           
                               <section class="content">
                     <h5>
                MRM
                   </h5>
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

   <div class="row">
        <div class="col-md-12">
            <div class="box">
                 <div class="box-header">
                 
                </div>
                <!-- /.box-header -->
                <div class="box-body">
<table id="example" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Part Number</th>
                <th>Alternate Part Number</th>
                <th>Material Description</th>
                <th>Mat Type</th>
                <th>IPC</th>
                <th>ORDER Number</th>
                <th>STO Number</th>
                 <th>Outbound Delivery</th>
                 <th>TO Number</th>
                   <th>Issued by</th>
                <th>Job Card Number</th>
                <th>Card Type</th>
                <th>MRM Issue Date</th>
                 <th> Qty Required Per Single Item PN</th>
                <th>Total QTY Required for All  Job/Task</th>
                <th>UOM</th>
                 <th>Input Stock Manually</th>
                  <th>Storage Location</th>
                <th>Material Fulfillment Status</th>
                <th>Fulfillment Status Date</th>         
                <th>FULFILLMENT REMARK</th>
                <th>Date of PO</th>
                <th>Purchase Order PO</th>
                <th>AWB Number</th>
                <th>AWB Date</th>
                <th>Qty Delivered</th>
                <th>Qty
Remain</th>
                 <th>Material Remark/Note</th> 
                <th>#</th>
            </tr>
        </thead>
        <tbody>
           
            <tr>
          <td>1</td>
          <td>CD00763F1</td>
           <td>7600008-101</td>
          <td>FILTER</td>
            <td>
          <select class="control" name="">
            <option value=""></option>
            <option value="ROT">ROT</option>
            <option value="REP">REP</option>
            <option value="EXP">EXP</option>
            <option value="CON">CON</option>
           </select>
              </td>
               <td></td>
           <td>802078965</td>
            <td></td>
             <td></td>
              <td>FROM AAI</td>
               <td></td>
            <td>4N-21-025-06-01</td>
                    <td>
          <select class="control" name="">
            <option value=""></option>
            <option value="JC">JC</option>
            <option value="MDR">MDR</option>
            <option value="AD">AD</option>
          
           </select>
              </td>
          <td>05-02-2018 10:56:49</td>
           <td>4</td>
           <td>8</td>
                   <td>
          <select class="control" name="">
            <option value=""></option>
            <option value="EA">EA</option>
            <option value="FT">FT</option>
            <option value="L">L</option>
            <option value="LB">LB</option>
            <option value="QT">QT</option>
            <option value="ROL">ROL</option>
            <option value="M">M</option>
           </select>
              </td>
           <td>4</td>
            <td>F ATAS</td>
          <td><select class="control" name="">
            <option value=""></option>
            <option value="DLVR FULL to Production">DLVR FULL to Production</option>
            <option value="
DLVR PARTIAL to Production">
DLVR PARTIAL to Production</option>
            <option value="WAITING Customer Supply">WAITING Customer Supply</option>
            <option value="ROBBING Desicion">ROBBING Desicion</option>
           </select></td>
           <td>29/01/2018</td>
            <td>AAI will ship from Jeddah</td>
             <td>29/01/2018</td>
              <td>7566566</td>
               <td>889944886</td>
                <td>29/01/2018</td>
                 <td>4</td>
                  <td>0</td>
          <td>ROT PART</td>
          
          <td> <div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
           
          </tr>

      
        </tbody>
    </table>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
              </div>
                 <div class="tab-pane" id="tab_ticket">
            
  <section class="content">
                     <h5>
                Tickets
                   </h5>
                   <div class="row">
        <div class="col-md-12">
            <div class="box">
                 <div class="box-header">
                 
                </div>
                <!-- /.box-header -->
                <div class="box-body">
<button  id="btnCreate" data-target='.mymodal1' data-cache='true' data-toggle='modal' class="btn bg-navy btn-flat margin" href="<?php echo base_url(); ?>index.php/administrator/projects/modal_tickets" >Create</button>
<table id="example3" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <th>No.</th>
                <th>Subject</th>
                <th>Department</th>
                <th>Project</th>
                <th>Service</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Last Replay</th>
                <th></th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
         <?php echo $listticket ?>
     </tr>
        </tbody>
        <tfoot>
            <tr>
                
               <th>No.</th>
                <th>Subject</th>
                <th>Department</th>
                <th>Project</th>
                <th>Service</th>
                <th>Priority</th>
                <th>Status</th>
                <th>Last Replay</th>
              
            </tr>
        </tfoot>
    </table>
        </div>
    </div>
</div>
</div>

</section>

              </div>
                <div class="tab-pane" id="tab_daily">
                  
                   <section class="content">
                     <h5>
                 DAILY REPORT
                   </h5>

<table id="example_sip" class="table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Area</th>
                <th>#</th>
                
            </tr>
        </thead>
        <tbody>
          <tr>
          <td>1</td>
          <td>Engine</td>
          <td> <div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
           
          </tr>
           <tr>
          
           <td>2</td>
          <td>WING & FLT CONTROL</td>
          <td><div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
         
          </tr>
           <tr>
          
           <td>3</td>
          <td>FWD & AFT CARGO</td>
          <td><div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
          </tr>
               <tr>
          
           <td>4</td>
          <td>LDG/HYD</td>
          <td><div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
         
          </tr>
               <tr>
          
           <td>5</td>
          <td>TAIL & APU </td>
          <td><div class="input-group margin">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span></button>
                  <ul class="dropdown-menu">
                    <li><a data-target='.mymodal' data-cache='false' data-toggle='modal' href="<?php echo base_url(); ?>index.php/administrator/projects/modal_daily_day">Set Daily</a></li>
                    <li><a href="#">View</a></li>
                    <li><a href="#">Delete</a></li>
                   
                  </ul>
                </div>
                <!-- /btn-group -->
              
              </div></td>
         
          </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Area</th>
                <th>#</th>
              
            </tr>
        </tfoot>
    </table>
</section>
              </div>
                  <div class="tab-pane" id="tab_files">
            files
              </div>
                 <div class="tab-pane" id="tab_discussions">
           discussions
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

     
        <!-- /.col -->
      </div>
        <div class="modal fade mymodal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
     <div class="modal fade mymodal1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content"></div>
        </div>
    </div>
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
$('.sparkline').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#00A65A', '#00c0ef', '#F7BA1C'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });

        $('.sparkline2').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#605ca8', '#d2d6de', '#f56954'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });



</script>
<script>
var bar = new Morris.Bar({
            element: 'bar-chart7',
            resize: true,
            data: [
               {y: 'Perform Run Up', a: 80 },
                {y: 'Opening', a: 90 },
                {y: 'Inspection', a: 70},
                {y: 'Repair', a: 60},
                {y: 'Install', a: 50},
                {y: 'FNC', a: 60},
                {y: 'RECT', a: 50}
            ],
            barColors: ['#f56954'],
            xkey: 'y',
            ykeys: ['a'],
            labels: [''],
            hideHover: 'auto'
        });

 var bar = new Morris.Bar({
            element: 'bar-chart3',
            resize: true,
            data: [
                {y: 'Open', a: <?php echo $total_jc_open; ?>, b: <?php echo $total_mdr_open; ?>},
                {y: 'Hangar', a: <?php echo $total_jc_hangar; ?>, b: <?php echo $total_mdr_hangar; ?>},
                {y: 'WSSS', a: <?php echo $total_jc_wsss; ?>, b: <?php echo $total_mdr_wsss; ?>},
                {y: 'WSSE', a: <?php echo $total_jc_wsse; ?>, b: <?php echo $total_mdr_wsse; ?>},
                {y: 'WSCB', a: <?php echo $total_jc_wscb; ?>, b: <?php echo $total_mdr_wscb; ?>},
                {y: 'WSLS', a: <?php echo $total_jc_wsls; ?>, b: <?php echo $total_mdr_wsls; ?>}
            ],
            barColors: ['#f56954', '#31b0d5'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['JC', 'MDR'],
            hideHover: 'auto'
        });

</script>
<script>        

/// Return with commas in between
var numberWithCommas = function(x) {
return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

var dataPack1 = [21000, 22000, 26000, 35000, 55000, 55000, 56000, 59000, 60000, 61000, 60100, 62000];
var dataPack2 = [1000, 1200, 1300, 1400, 1060, 2030, 2070, 4000, 4100, 4020, 4030, 4050];
var dates = ["A/P", "CBN", "E/A", "NDT", "PAINTING", "STR", 
           "TBR-SEALANT", "TBR-SHOP"];

// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

var bar_ctx = document.getElementById('bar-chart_122');
var bar_chart = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: dates,
        datasets: [
        {
            label: 'Close',
            data: dataPack1,
            backgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Grand Total',
            data: dataPack2,
            backgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        ]
    },
    options: {
         animation: {
          duration: 10,
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
          }
          }
         },
        scales: {
          xAxes: [{ 
            stacked: true, 
            gridLines: { display: false },
            }],
          yAxes: [{ 
            stacked: true, 
            ticks: {
              callback: function(value) { return numberWithCommas(value); },
             }, 
            }],
        }, // scales
        legend: {display: true}
    } // options
   }
);

</script>

<script>        

    // Return with commas in between
var numberWithCommas = function(x) {
return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

var dataPack3 = [21000, 22000, 26000, 35000, 55000, 55000, 56000, 59000, 60000, 61000, 60100, 62000];
var dataPack4 = [21000, 15000, 12000, 14000, 10060, 20030, 20070, 40000, 40100, 41020, 42030, 43050];
var dates1 = ["Open", "Hangar", "WSSS", "WSSE", "WSCB", "WSLS"];

// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

var bar_ctx = document.getElementById('bar-chart_144');
var bar_chart = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: dates1,
        datasets: [
        {
            label: 'MDR',
            data: dataPack3,
            backgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        {
            label: 'JC',
            data: dataPack4,
            backgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
            hoverBorderWidth: 2,
            hoverBorderColor: 'lightgrey'
        },
        ]
    },
    options: {
         animation: {
          duration: 10,
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
          }
          }
         },
        scales: {
          xAxes: [{ 
            stacked: true, 
            gridLines: { display: false },
            }],
          yAxes: [{ 
            stacked: true, 
            ticks: {
              callback: function(value) { return numberWithCommas(value); },
             }, 
            }],
        }, // scales
        legend: {display: true}
    } // options
   }
);

</script>
<script type="text/javascript">
(function() {
  
        $('#example_sip2').DataTable({
            dom: 'Bfrtip',
      'paging': false,
            buttons: [
            ],
        })
    })
  
  $(function() {

    var t = $('#example_sip2').DataTable();
    var counter = 1;
  

 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            '<td><input type="text"></td>',
            counter +'.2',
            counter +'.3',
            counter +'<td><input type="button" id="deleteRow" value="Delete Row" onClick="removeRow(this)"/></td>'
        ] ).draw( false );
 
        counter++;
    } ) 
   
    // Automatically add a first row of data
    $('#addRow').click(); 
  
} )
  
  function removeRow(oButton) {
        var empTab = document.getElementById('example_sip2');
        empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);       // BUTTON -> TD -> TR.
    }

</script>
<script type="text/javascript">
(function() {
  
        $('#example_sip3').DataTable({
            dom: 'Bfrtip',
      'paging': false,
            buttons: [
            ],
        })
    })
  
  $(function() {

    var t = $('#example_sip3').DataTable();
    var counter = 1;
  

 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            '<td><input type="text"></td>',
            counter +'.2',
            counter +'.3',
            counter +'<td><input type="button" id="deleteRow" value="Delete Row" onClick="removeRow(this)"/></td>'
        ] ).draw( false );
 
        counter++;
    } )
  
   
    // Automatically add a first row of data
    $('#addRow').click();
  
  
  
  
} )
  
  function removeRow(oButton) {
        var empTab = document.getElementById('example_sip3');
        empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);       // BUTTON -> TD -> TR.
    }

</script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "scrollY": 200,
        "scrollX": true
    } );
} );
</script>
<script>

    $('#example3').DataTable( {
        "scrollY": 200,
        "scrollX": true
    } );
</script>