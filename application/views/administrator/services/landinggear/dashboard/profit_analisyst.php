<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <i class="fa fa-bar-chart-o"></i> -->

          <!--     <h3 class="box-title">Line Chart</h3> -->

              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
                      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #deebf7">
            <span class="info-box-icon"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Revenue</span>
              <span class="info-box-number">$2.000.000</span>

             <!--  <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div> -->
                 
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box" style="background-color: #fbe5d6">
            <span class="info-box-icon"><i class="fa fa-scissors"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Cost</span>
              <span class="info-box-number">$1.500.000</span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div> -->
                 
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
     
      
      </div>
    
    
    <div class="row">
       
        <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Total</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
        </div>
    
    <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Job Card</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped1" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
        </div>
    
    <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">MDR</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped2" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
          </div>
         </div>
       </div>
        </div>

      </div>
  </div>
</div>
<br>
      
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Plan Cost",
          backgroundColor: "#1f78b4",
          data: [520,300]
        }, {
          label: "Actual Cost",
          backgroundColor: "#fe7f0e",
          data: [408,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped1"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Mhrs Plan Cost",
          backgroundColor: "#548235",
          data: [720,300]
        }, {
          label: "Mhrs Act Cost",
          backgroundColor: "#a9d18e",
          data: [500,300]
        }, {
          label: "Mhrs Plan",
          backgroundColor: "#2e75b6",
          data: [600,700]
        }, {
          label: "Mhrs Act",
          backgroundColor: "#bdd7ee",
          data: [680,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
   new Chart(document.getElementById("bar-chart-grouped2"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "Mhrs Plan Cost",
          backgroundColor: "#548235",
          data: [720,300]
        }, {
          label: "Mhrs Act Cost",
          backgroundColor: "#a9d18e",
          data: [500,300]
        }, {
          label: "Mhrs Plan",
          backgroundColor: "#2e75b6",
          data: [600,700]
        }, {
          label: "Mhrs Act",
          backgroundColor: "#bdd7ee",
          data: [720,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
</script>


            <!-- /.box-body-->
          </div>
          <!-- /.box -->


        </div>

      </div>

</div>
      
</section>