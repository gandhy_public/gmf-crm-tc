<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small>#<?php echo $data_order_detail['SALES_ORDER']; ?></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="col-sm-2">
        <img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="125"/>
      </div>
      <div class="col-sm-3">
        <h4>GMF Aerosia | Customer Name </h4>
      </div>
      <div class="col-sm-7">
        <h2 align="left">ORDER DETAIL</h2>
      </div>
      <!-- <h2 class="page-header">
        
        <i class="fa fa-globe"></i> GMF Aerosia. | Customer Name
        <small class="pull-right">Date: 2/10/2014</small>
      </h2> -->
    </div><!-- /.col -->
    <div class="col-xs-12">
      <div class="col-sm-2">
        <h2>ORDER</h2>
        <h2>#<?php echo $data_order_detail['SALES_ORDER']; ?> </h2>
      </div>
      <div class="col-sm-10">
        <h3>MAINTENANCE</h3>
        <h3>FINISHED</h3>
      </div>
      <!-- <h2 class="page-header">
        
        <i class="fa fa-globe"></i> GMF Aerosia. | Customer Name
        <small class="pull-right">Date: 2/10/2014</small>
      </h2> -->
    </div><!-- /.col -->
    <hr>
  </div>
  <!-- info row -->
  <br>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>SHIP TO :</b>
      <br>
      BANDARA SOEKARNO HATTA
      <br>
      
      <b>ORDER CREATION :</b>
      <br>
      GMF AOG Desk
      <br>
      
      <b>BILL INFORMATION :</b>
      <br>
      Paid
      <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>OUTBOUND SHIPPING INFO :</b>
      <br>
      -
      <br>
      
      <b>ORDER TYPE :</b>
      <br>
      <?php echo $data_order_detail['PART_NUMBER']; ?> - <?php echo $data_order_detail['PART_NAME']; ?>
      <br>
      
      <b>OUTBOUND AIRWAYS BILL NUMBER :</b>
      <br>
      #883874
      <br>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>END USER :</b>
      <br>
      Landing Gear
      <br>
      <b>CUSTOMER :</b>
      <br>
      CITILINK INDONESIA
      <br>
    </div>
  </div>

  <!-- <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <b>SHIP TO :</b><br>
      <?php //echo $data_invoice['SHIP_TO']; ?>
      <b>ORDER CREATION :</b><br>
      <?php //echo $data_invoice['ORDER_CREATION']; ?>
      <b>BILL INFORMATION :</b><br>
      <?php //echo $data_invoice['STATUS_PAID']; ?>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>OUTBOUND SHIPPING INFO :</b><br>
      <?php //echo $data_invoice['SHIP_INFO']; ?>
      <b>ORDER TYPE :</b><br>
      <?php //echo $data_invoice['ORDER_TYPE']; ?>
      <b>OUTBOUND AIRWAYS BILL NUMBER :</b><br>
      <?php //echo $data_invoice['BILL_NUMBER']; ?>
    </div>
    <div class="col-sm-4 invoice-col">
      <b>END USER :</b><br>
      <?php //echo $data_invoice['END_USER']; ?>
      <b>CUSTOMER :</b><br>
      <?php //echo $data_invoice['CUSTOMER']; ?>
    </div>
  </div> -->

  <!-- Table row -->
  <!-- <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Serial #</th>
            <th>Description</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Call of Duty</td>
            <td>455-981-221</td>
            <td>El snort testosterone trophy driving gloves handsome</td>
            <td>$64.50</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Need for Speed IV</td>
            <td>247-925-726</td>
            <td>Wes Anderson umami biodiesel</td>
            <td>$50.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Monsters DVD</td>
            <td>735-845-642</td>
            <td>Terry Richardson helvetica tousled street art master</td>
            <td>$10.70</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Grown Ups Blue Ray</td>
            <td>422-568-642</td>
            <td>Tousled lomo letterpress</td>
            <td>$25.99</td>
          </tr>
        </tbody>
      </table> -->
    <!--</div>
  </div>--><!-- /.row -->
  <br>
  <div class="row">
    <!-- accepted payments column -->
    <div class="col-lg-6">
      <!-- <p class="lead">Payment Methods:</p>
      <img src="../../dist/img/credit/visa.png" alt="Visa">
      <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
      <img src="../../dist/img/credit/american-express.png" alt="American Express">
      <img src="../../dist/img/credit/paypal2.png" alt="Paypal">
      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
      </p> -->
    </div><!-- /.col -->
    <div class="col-lg-6">
      <p class="lead">TOTAL PAYMENT</p>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th style="width:50%">Subtotal:</th>
            <td>23,430 USD</td>
          </tr>
          <tr>
            <th>Tax</th>
            <td>0 USD</td>
          </tr>
          <tr>
            <th>Total:</th>
            <td>33,590 USD</td>
          </tr>
        </table>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row no-print">
    <div class="col-xs-12">
      <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
      <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
      <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
    </div>
  </div>

  <!-- this row will not appear when printing -->
</section><!-- /.content -->
<div class="clearfix"></div>

