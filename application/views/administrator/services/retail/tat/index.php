<section class="content-header">
    <h1>
        CRM TC
        <small>Pooling PBTH</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Pooling</a></li>
        <li class="active">Pooling</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                   <table id="tabel-data" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>ACCOUNT</th>
                                <th>REFERENCEC</th>
                                <th>ASSIGNMENT</th>
                                <th>DOCUMENT DATE</th>
                                <th>CCY</th>
                                <th>VALUE</th>
                                <th>MHRS</th>
                                <th>MAT</th>
                                <th>OTHER</th>
                                <th>PPN</th>
                                <th>PPH</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = 1;
                                foreach($tb_pooling as $tb_pool){ 
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $tb_pool->ACCOUNT ?></td>
                                <td><?php echo $tb_pool->REFERENCE ?></td>
                                <td><?php echo $tb_pool->ASSIGNMENT ?></td>
                                <td><?php echo $tb_pool->DOCUMENT_DATE ?></td>
                                <td><?php echo $tb_pool->CCY ?></td>
                                <td><?php echo $tb_pool->VALUE ?></td>
                                <td><?php echo $tb_pool->MHRS ?></td>
                                <td><?php echo $tb_pool->MAT ?></td>
                                <td><?php echo $tb_pool->OTHER ?></td>
                                <td><?php echo $tb_pool->PPN ?></td>
                                <td><?php echo $tb_pool->PPH ?></td>
                                <td>
                                    <?php echo anchor('pooling_control/edit/'.$tb_pool->NO_ACCOUNT,'Edit'); ?>
                                    <?php echo anchor('pooling_control/hapus/'.$tb_pool->NO_ACCOUNT,'Hapus'); ?>
                                </td>
                            </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>