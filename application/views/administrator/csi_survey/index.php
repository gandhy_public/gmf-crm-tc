<style type="text/css">
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

    fieldset, label { margin: 0; padding: 0; }
    

    /****** Style Star Rating Widget *****/

    .rating { 
        border: none;
        float: left;
    }

    .rating > input { display: none; } 
    .rating > label:before { 
        margin: 10px;
        font-size: 1.25em;
        font-family: FontAwesome;
        display: inline-block;
        content: "\f005";
    }

    .rating > .half:before { 
        content: "\f089";
        position: absolute;
    }

    .rating > label { 
        color: #ddd; 
        float: right; 
    }

    /***** CSS Magic to Highlight Stars on Hover *****/

    .rating > input:checked ~ label, /* show gold star when clicked */
    .rating:not(:checked) > label:hover, /* hover current star */
    .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

    .rating > input:checked + label:hover, /* hover current star when changing rating */
    .rating > input:checked ~ label:hover,
    .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
    .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
</style>

<section class="content-header">
    <h1 align="center">
        CUSTOMER SATISFACTION SURVEY
    </h1>
    <p align="center">____________________________________________________</p>
    <div style="width:800px; margin:0 auto;">
    <div class="form-inline" align="center">
        <div class="form-group">
            <select class="form-control" id="category_menu" name="category_menu">
                <option value="Pooling">Pooling</option>
                <option value="Loan Exchange">Loan Exchange</option>
                <option value="Retail">Retail</option>
                <option value="Landing Gear">Landing Gear</option>
            </select>
        </div>
        <div class="form-group">            
            <select class="form-control" id="list_menu" name="list_menu">
                <option>-- Select document --</option>
            </select>
        </div>
    </div>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> CSI Survey</a></li>
        <!-- <li class="active"><i class="fa fa-table"></i> CSI Survey</li>
    </ol> -->
</section>


<!-- Main content -->
<section class="content">
     
   <!--  <div class="col-lg-5" style="margin: 0;"> -->
   <div style="width:800px; margin:0 auto;">
    <div class="form-inline">
    </div>
    <form action="<?php echo base_url(); ?>index.php/administrator/csi_survey/submit_survey" method="POST">
        <h5>The purpose of the questionnaire is to collect your feedback on GMF's performance and services</h5>
        <h5 style="color: red;">*Required</h5>
      <div class="form-group">
        <label for="inputName">Please fill in your Name <span style="color: red">*</span></label>
        <input type="text" class="form-control" id="inputName" placeholder="Your Answer" required>
      </div>

      <div class="form-group">
        <label for="companyName">You are filling this form on behalf to <span style="color: red">*</span></label>
        <select class="form-control" id="companyName">
          <option>Choose</option>
          <option>Garuda Indonesia</option>
          <option>Citilink</option>
          <option>Sriwijaya Air</option>
          <option>Lion Air</option>
          <option>Air Asia</option>
          <option>Aero Eagle</option>
          <option>Angkatan Udara Republik Indonesia</option>
          <option>CFMI</option>
          <option>Eastar Jet</option>
          <option>Cardig Air</option>
          <option>Trigana Air</option>
          <option>K-Mile</option>
          <option>Others (please fill in at the LSN column)</option>
        </select>
        <small class="form-text text-muted">Fill in your company name.</small>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Please fill in your email address<span style="color: red">*</span></label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Answer" required>
      </div>

      <div class="form-group">
        <label for="inputLSN">Please fill in your product LSN for this form <span style="color: red">*</span></label>
        <input type="text" class="form-control" id="inputName" placeholder="Your Answer" required>
      </div>

      <div class="form-group">
                <label for="rateServices">Please rate our technical quality of product and services<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Quality</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>                
                                <div class="rating">
                                    <input type="radio" id="quality-star5" name="quality-rating" value="5" /><label class = "full" for="quality-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="quality-star45" name="quality-rating" value="4.5"/><label class="half" for="quality-star45" title=" 4.5 stars"></label>
                                    <input type="radio" id="quality-star4" name="quality-rating" value="4" /><label class = "full" for="quality-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="quality-star35" name="quality-rating" value="3.5"/><label class="half" for="quality-star35" title=" 3.5 stars"></label>
                                    <input type="radio" id="quality-star3" name="quality-rating" value="3" /><label class = "full" for="quality-star3" title="3 stars"></label>
                                    <input type="radio" id="quality-star25" name="quality-rating" value="2.5"/><label class="half" for="quality-star25" title=" 2.5 stars"></label>
                                    <input type="radio" id="quality-star2" name="quality-rating" value="2" /><label class = "full" for="quality-star2" title="2 stars"></label>
                                    <input type="radio" id="quality-star15" name="quality-rating" value="1.5"/><label class="half" for="quality-star15" title=" 1.5 stars"></label>
                                    <input type="radio" id="quality-star1" name="quality-rating" value="1" /><label class = "full" for="quality-star1" title=" 1 star"></label>
                                    <input type="radio" id="quality-star05" name="quality-rating" value="0.5"/><label class="half" for="quality-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputReason">Please give reason for your rating above about Quality <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="quality-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="rateTat">Please rate our TAT (Turn Around Time) for package and unplanned finish time<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Turn Around Time</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="tat-star5" name="tat-rating" value="5" /><label class = "full" for="tat-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="tat-star45" name="tat-rating" value="4.5"/><label class="half" for="tat-star45" title=" 3.5 stars"></label>
                                    <input type="radio" id="tat-star4" name="tat-rating" value="4" /><label class = "full" for="tat-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="tat-star35" name="tat-rating" value="3.5"/><label class="half" for="tat-star35" title=" 3.5 stars"></label>
                                    <input type="radio" id="tat-star3" name="tat-rating" value="3" /><label class = "full" for="tat-star3" title="3 stars"></label>
                                    <input type="radio" id="tat-star25" name="tat-rating" value="2.5"/><label class="half" for="tat-star25" title=" 2.5 stars"></label>
                                    <input type="radio" id="tat-star2" name="tat-rating" value="2" /><label class = "full" for="tat-star2" title="2 stars"></label>
                                    <input type="radio" id="tat-star15" name="tat-rating" value="1.5"/><label class="half" for="tat-star15" title=" 1.5 stars"></label>
                                    <input type="radio" id="tat-star1" name="tat-rating" value="1" /><label class = "full" for="tat-star1" title=" 1 star"></label>
                                    <input type="radio" id="tat-star05" name="tat-rating" value="0.5"/><label class="half" for="tat-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputReasontat">Please give reason for your rating above about TAT <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="tat-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="ratePrice">Please rate our price and overall cost<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Price</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="price-star5" name="price-rating" value="5" /><label class = "full" for="price-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="price-star45" name="price-rating" value="4.5"/><label class="half" for="price-star45" title="4.5 stars"></label>
                                    <input type="radio" id="price-star4" name="price-rating" value="4" /><label class = "full" for="price-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="price-star35" name="price-rating" value="3.5"/><label class="half" for="price-star35" title="3.5 stars"></label>
                                    <input type="radio" id="price-star3" name="price-rating" value="3" /><label class = "full" for="price-star3" title="3 stars"></label>
                                    <input type="radio" id="price-star25" name="price-rating" value="2.5"/><label class="half" for="price-star25" title="2.5 stars"></label>
                                    <input type="radio" id="price-star2" name="price-rating" value="2" /><label class = "full" for="price-star2" title="2 stars"></label>
                                    <input type="radio" id="price-star15" name="price-rating" value="1.5"/><label class="half" for="price-star05" title=" 1.5 stars"></label>
                                    <input type="radio" id="price-star1" name="price-rating" value="1" /><label class = "full" for="price-star1" title=" 1 star"></label>
                                    <input type="radio" id="price-star05" name="price-rating" value="0.5"/><label class="half" for="price-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputReasonprice">Please give reason for your rating above about Price <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="price-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="ratePostdelivery1">Please rate our after maintenance support services<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Post Delivery</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="services-star5" name="services-rating" value="5" /><label class = "full" for="services-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="services-star45" name="services-rating" value="4.5"/><label class="half" for="services-star45" title="4.5 stars"></label>
                                    <input type="radio" id="services-star4" name="services-rating" value="4" /><label class = "full" for="services-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="services-star35" name="services-rating" value="3.5"/><label class="half" for="services-star35" title="3.5 stars"></label>
                                    <input type="radio" id="services-star3" name="services-rating" value="3" /><label class = "full" for="services-star3" title="3 stars"></label>
                                    <input type="radio" id="services-star25" name="services-rating" value="2.5"/><label class="half" for="services-star25" title="2.5 stars"></label>
                                    <input type="radio" id="services-star2" name="services-rating" value="2" /><label class = "full" for="services-star2" title="2 stars"></label>
                                    <input type="radio" id="services-star15" name="services-rating" value="1.5"/><label class="half" for="services-star15" title="1.5 stars"></label>
                                    <input type="radio" id="services-star1" name="services-rating" value="1" /><label class = "full" for="services-star1" title=" 1 star"></label>
                                    <input type="radio" id="services-star05" name="services-rating" value="0.5"/><label class="half" for="services-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="ratePostdelivery2">Please rate our correctness and timeliness of invoices<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Post Delivery</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="timelines-star5" name="timelines-rating" value="5" /><label class = "full" for="timelines-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="timelines-star45" name="timelines-rating" value="4.5"/><label class="half" for="timelines-star45" title="4.5 stars"></label>
                                    <input type="radio" id="timelines-star4" name="timelines-rating" value="4" /><label class = "full" for="timelines-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="timelines-star35" name="timelines-rating" value="3.5"/><label class="half" for="timelines-star35" title="3.5 stars"></label>
                                    <input type="radio" id="timelines-star3" name="timelines-rating" value="3" /><label class = "full" for="timelines-star3" title="3 stars"></label>
                                    <input type="radio" id="timelines-star25" name="timelines-rating" value="2.5"/><label class="half" for="timelines-star25" title="2.5 stars"></label>
                                    <input type="radio" id="timelines-star2" name="timelines-rating" value="2" /><label class = "full" for="timelines-star2" title="2 stars"></label>
                                    <input type="radio" id="timelines-star15" name="timelines-rating" value="1.5"/><label class="half" for="timelines-star15" title="1.5 stars"></label>
                                    <input type="radio" id="timelines-star1" name="timelines-rating" value="1" /><label class = "full" for="timelines-star1" title=" 1 star"></label>
                                    <input type="radio" id="timelines-star05" name="timelines-rating" value="0.5"/><label class="half" for="timelines-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputReasonpost">Please give reason for your rating for 2 questions about Post Delivery <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="services-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="rateCustomerservices">Please rate our overall services towards you<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Customer oriented attitude of maintenance, engine owner, customer service staff, and accout managers</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="overall-star5" name="overall-rating" value="5" /><label class = "full" for="overall-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="overall-star45" name="overall-rating" value="4.5"/><label class="half" for="overall-star45" title="4.5 stars"></label>
                                    <input type="radio" id="overall-star4" name="overall-rating" value="4" /><label class = "full" for="overall-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="overall-star35" name="overall-rating" value="3.5"/><label class="half" for="overall-star35" title="3.5 stars"></label>
                                    <input type="radio" id="overall-star3" name="overall-rating" value="3" /><label class = "full" for="overall-star3" title="3 stars"></label>
                                    <input type="radio" id="overall-star25" name="overall-rating" value="2.5"/><label class="half" for="overall-star25" title="2.5 stars"></label>
                                    <input type="radio" id="overall-star2" name="overall-rating" value="2" /><label class = "full" for="overall-star2" title="2 stars"></label>
                                    <input type="radio" id="overall-star15" name="overall-rating" value="1.5"/><label class="half" for="overall-star15" title="1.5 stars"></label>
                                    <input type="radio" id="overall-star1" name="overall-rating" value="1" /><label class = "full" for="overall-star1" title=" 1 star"></label>
                                    <input type="radio" id="overall-star05" name="overall-rating" value="0.5"/><label class="half" for="overall-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputCustomerservice">Please give reason for your rating above about Customer Services <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="overall-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="rateDocumentation">Please rate our time and accuracy of documentation and management of information<span style="color: red">*</span></label><br>
                <small class="form-text text-muted" style="font-size: 15px;">Documentation</small>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <fieldset>
                                <div class="rating">
                                    <input type="radio" id="documentation-star5" name="documentation-rating" value="5" /><label class = "full" for="documentation-star5" title="Awesome - 5 stars"></label>
                                    <input type="radio" id="documentation-star45" name="documentation-rating" value="4.5"/><label class="half" for="documentation-star45" title="4.5 stars"></label>
                                    <input type="radio" id="documentation-star4" name="documentation-rating" value="4" /><label class = "full" for="documentation-star4" title="Pretty good - 4 stars"></label>
                                    <input type="radio" id="documentation-star35" name="documentation-rating" value="3.5"/><label class="half" for="documentation-star35" title="3.5 stars"></label>
                                    <input type="radio" id="documentation-star3" name="documentation-rating" value="3" /><label class = "full" for="documentation-star3" title="3 stars"></label>
                                    <input type="radio" id="documentation-star25" name="documentation-rating" value="2.5"/><label class="half" for="documentation-star25" title="2.5 stars"></label>
                                    <input type="radio" id="documentation-star2" name="documentation-rating" value="2" /><label class = "full" for="documentation-star2" title="2 stars"></label>
                                    <input type="radio" id="documentation-star15" name="documentation-rating" value="1.5"/><label class="half" for="documentation-star15" title="1.5 stars"></label>
                                    <input type="radio" id="documentation-star1" name="documentation-rating" value="1" /><label class = "full" for="documentation-star1" title=" 1 star"></label>
                                    <input type="radio" id="documentation-star05" name="documentation-rating" value="0.5"/><label class="half" for="documentation-star05" title=" 0.5 stars"></label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="inputDocumentation">Please give reason for your rating above about Documentation <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="documentation-reason" placeholder="Your Answer" required>
            </div>

            <div class="form-group">
                <label for="inputHandlingproject">Please give reason for your rating about our works in handling your projects <span style="color: red">*</span></label>
                <input type="text" class="form-control" id="inputName" name="project-reason" placeholder="Your Answer" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<!-- <div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-auto">
      <form>
          <h5>The purpose of the questionnaire is to collect your feedback on GMF's performance and services</h5>
          <h5 style="color: red;">*Required</h5>
               <div class="col-xs-4">
                <label for="name">Please fill in your Name <span style="color:red;">*</span></label>
                <input class="form-control" id="name" type="text" placeholder="Your Answer">
               </div>

               <div class="col-xs-4">
                <label for="name">Please fill in your email address <span style="color:red;">*</span></label>
                <input class="form-control" id="name" type="text" placeholder="Your Answer">
               </div>
      </form>
    </div>
  </div> -->
</section>

<!-- Script -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

<script type='text/javascript'>
  $(document).ready(function(){
 
    // City change
    $('#category_menu').change(function(){
      var tipe = $(this).val();

      // AJAX request
      $.ajax({
        url:'<?=base_url()?>index.php/csi_survey/data'+tipe,
        method: 'post',
        //data: {tipe: tipe},
        dataType: 'json',
        success: function(response){

          // Remove options 
          $('#list_menu').find('option').not(':first').remove();

          // Add options
          $.each(response,function(index,data){
             $('#list_menu').append('<option value="'+data['doc_no']+'">'+data['doc_no']+'</option>');
          });
        }
     });
   });
 
 });
 </script>