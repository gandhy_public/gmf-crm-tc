<section class="content-header">
    <h1>
        CRM TC
        <small>Pre-Order</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Order</a></li>
        <li class="active">Pre-Order</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                  <a href='<?php echo base_url("index.php/tb_order/tambah"); ?>'>Tambah Data</a><br><br>
                    <table id="tabel-data" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ORDER</th>
                                <th>STATUS</th>
                                <th>CUSTOMER</th>
                                <th>MAINTENANCE</th>
                                <th>DUE DATE</th>
                                <th colspan="2">Aksi</th>
                            </tr>
                        </thead>
                
                    </table>
                    <!-- /.description-block -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>