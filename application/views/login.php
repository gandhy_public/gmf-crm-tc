<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GMF CRM | Log in</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/LogoIcon.png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href=""><img src="<?php echo base_url(); ?>assets/dist/img/logo-gmf.png"/></a>
                <h3>COMPONENT SERVICES</h3>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body" style="border-radius: 8px;">
                <p class="login-box-msg">Please login to the customers form using username/email and password.</p>
                <p style="color: red">
                    <?= $this->session->flashdata('login')?>
                </p>

                <form action="<?php echo base_url(); ?>index.php/login2/action_login" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" style="border-radius: 5px;" placeholder="Username" required autofocus>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" style="border-radius: 5px;" placeholder="Password" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4 pull-right">
                            <button type="submit" style="border-radius: 3px;" class="btn btn-primary btn-block btn-flat">Log In</button>
                        </div>
                    </div>
                    <br>
                    <p class="login-box-msg">Contact <b>spoc-ict@gmf-aeroasia.co.id</b> if you are experiencing difficulty logging in.</p>
                </form>
            </div>
            <p class="copyright-login">Develop By PT Sinergi Informatika Semen Indonesia</p>
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>

        <!-- <div id="slider">
            <div id="slide-holder">
                <div class="slide"><img src="<?php //echo base_url(); ?>bg1.jpg" alt="" /></div>
                <div class="slide"><img src="<?php //echo base_url(); ?>bg2.jpg" alt="" /></div>
                <div class="slide"><img src="<?php //echo base_url(); ?>bg3.jpg" alt="" /></div>
             </div>
        </div> -->
    </body>
</html>
