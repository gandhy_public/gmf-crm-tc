<section class="content-header">
    <h1>
        Component and Material
        <small>Slider</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content" style="background:" img src="<?php echo base_url(); ?>assets/dist/img/Garuda-Indonesia_GMF_Aero.jpg" width="50">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
      <div class="col-lg-12 col-xs-12">
        <div class="box box-solid">

          <div class="box-header with-border" style="background: #CCFFFF">
            <h3 class="box-title">TC Status</h3>              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          <li><a href="#">Something else here</a></li>
                          <li class="divider"></li>
                          <li><a href="#">Separated link</a></li>
                        </ul>
                  </div>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
          </div>

          <!-- /.box-header -->

          <!-- Sales Chart Canvas -->
          <!-- /.chart-responsive 1 -->
          <div class="box-body piechart col-md-4" style="background: #9FC5E8">
            <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                <?php echo $total_waiting_approved; ?>, 
                <?php echo $total_waiting_material; ?>,  
                <?php echo $total_under_maintenance; ?>, 
                <?php echo $total_waiting_repair; ?>,
                <?php echo $total_finished; ?>
            </div>
          </div>

          <div class="box-body piechart col-md-4" style="background: #9FC5E8">
            <ul class="chart-legend clearfix">
              <li style="color:#ADFF2F"><i class="fa fa-square"> </i> Waiting Approved   </li> <!-- greenyellow -->
              <li style="color:#00FFFF"><i class="fa fa-square"> </i> Waiting Material   </li> <!-- aqua -->
              <li style="color:#FFFF00"><i class="fa fa-square"> </i> Under Maintenance  </li> <!-- yellow -->
              <li style="color:#ff0000"><i class="fa fa-square"> </i> Waiting Repair     </li> <!-- red -->
              <li style="color:#CC00CC"><i class="fa fa-square"> </i> Finished           </li> <!-- //violet -->
          </div>

          <div class="box-body piechart col-md-4" style="background: #9FC5E8">
            <ul class="chart-legend clearfix">
              <li style="color:#ADFF2F"> <b><?php echo $percentage_waiting_approved;  ?>%</b></li> <!-- greenyellow -->
              <li style="color:#00FFFF"> <b><?php echo $percentage_waiting_material;  ?>%</b></li> <!-- aqua -->
              <li style="color:#FFFF00"> <b><?php echo $percentage_under_maintenance; ?>%</b></li> <!-- yellow -->
              <li style="color:#ff0000"> <b><?php echo $percentage_waiting_repair;    ?>%</b></li> <!-- red -->
              <li style="color:#CC00CC"> <b><?php echo $percentage_finished;          ?>%</b></li> <!-- //violet -->
            </ul>
           </div>
          
          <div class="col-md-12">
            <div id='chartContainer' style="height: 400px;"></div>
          </div>

          <div class="clearfix" style="background: #9FC5E8"></div> <!-- /.chart-responsive 2 -->

           </div> <!-- /.box -->
        </div>    <!-- /.col -->
    </div>        <!-- /.row -->
           

   <!-- ./box-body -->
   <div class="box-footer">
    <div class="col-md-12">
     <div class="row">

        <div class="box-header with-border" style="background: #CCFFFF">
         <h3 class="box-title">Summary Table</h3>              
          <div class="box-tools pull-right">
           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <div class="btn-group">
               <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
               <i class="fa fa-wrench"></i></button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
               </div>
           <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>

        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
          <h5 class="description-header">MAINTENANCE</h5>
          <span class="description-text text-black"> MARKET</span>
         </div>
        <!-- /.description-block -->
        </div>

        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#ADFF2F">Waiting Approved</h5> <!-- greenyellow -->
          <span class="description-percentage" style="color:#ADFF2F"><!-- <i class="fa fa-caret-up"></i> 24%-->
            <b><?php echo $total_waiting_approved; ?></b>
          </span>
         </div>
        <!-- /.description-block -->
        </div>
      
        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#00FFFF">Waiting Material</h5> <!-- aqua -->
          <span class="description-percentage" style="color:#00FFFF"><!-- <i class="fa fa-caret-left"></i> 22%-->
            <b><?php echo $total_waiting_material; ?></b>
          </span>                   
         </div>
         <!-- /.description-block -->
        </div>
        
        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
          <h5 class="description-header" style="color:#FFFF00">Under Maintenance</h5> <!-- yellow -->
          <span class="description-percentage" style="color:#FFFF00"><!-- <i class="fa fa-caret-up"></i> 20% -->
            <b><?php echo $total_under_maintenance; ?></b>
           </span>                   
         </div>
         <!-- /.description-block -->
        </div>

        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
           <h5 class="description-header" style="color:#FF0000">Waiting Repair</h5> <!-- red -->
           <span class="description-percentage" style="color:#FF0000"><!-- <i class="fa fa-caret-up"></i> 20% -->
            <b><?php echo $total_waiting_repair; ?></b>
           </span>                   
         </div>
         <!-- /.description-block -->
        </div>

        <!-- /.col -->
        <div class="col-sm-2 col-xs-6" style="background: #999999">
         <div class="description-block border-right">
           <h5 class="description-header" style="color: #CC00CC">Finished</h5> <!-- //violet -->
           <span class="description-percentage" style="color:#CC00CC"><!-- <i class="fa fa-caret-down"></i> 22% -->
            <b><?php echo $total_finished; ?></b>
           </span>                   
         </div>
         <!-- /.description-block -->
        </div>
        
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-footer -->
            
            
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
            <!-- MAP & BOX PANE -->
            <!-- /.box -->
            <div class="row">
                <div class="col-md-6">
                    <!-- DIRECT CHAT -->
                    <!--/.direct-chat -->
                </div>
                <!-- /.col -->

                <div class="col-md-6">
                    <!-- USERS LIST -->
                    <!--/.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Orders</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Account ID</th>
                                    <th>Reference</th>
                                    <th>Assigment</th>
                                    <th>Document Date</th>
                                    <th>22</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90020470</td>
                                    <td><span class="label label-success">Shipped</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00a65a" data-height="20">11/29/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90020468</td>
                                    <td><span class="label label-warning">Pending</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f39c12" data-height="20">11/29/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90020471</td>
                                    <td><span class="label label-danger">Delivered</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f56954" data-height="20">11/30/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90020471</td>
                                    <td><span class="label label-info">Processing</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00c0ef" data-height="20">12/6/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90020723</td>
                                    <td><span class="label label-warning">Pending</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f39c12" data-height="20">12/9/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90021519</td>
                                    <td><span class="label label-danger">Delivered</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#f56954" data-height="20">12/29/2017</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="pages/examples/invoice.html">SIY02</a></td>
                                    <td>90021614</td>
                                    <td><span class="label label-success">Shipped</span></td>
                                    <td>
                                        <div class="sparkbar" data-color="#00a65a" data-height="20">12/30/2017</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        
            <!-- /.info-box -->

            <!-- /.box -->

            <!-- PRODUCT LIST -->
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>


<!-- /.warna Pie Chart -->
<script>
        $('.sparkline').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#ADFF2F', '#00FFFF', '#FFFF00', '#ff0000', '#EE82EE'],
                    /*sliceColors: ['#1ABC9C', '#2ECC71', '#3498DB', '#9B59B6', '#34495E'],*/
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });

        $('.sparkline2').sparkline('html',
                {
                    type: 'pie',
                    height: '11.0em',
                    sliceColors: ['#605ca8', '#d2d6de', '#f56954', '#BF360C'],
                    highlightLighten: 1.1,
                    tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                });
   
</script>
<script type="text/javascript">
$(document).ready(function () {
// memanggil data array dengan JSON
var source =
     {
         datatype: "json",
         datafields: [
        { name: 'hasil' },
        { name: 'total' }
         ],
         url: '<?php echo base_url() ?>home_control/survey_framework'
     };
var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });
// pengaturan jqxChart
  var settings = {
        title: "Survey Framework terbaik",
        description: "",
        enableAnimations: true,
        showLegend: true,
        showBorderLine: true,
        legendLayout: { left: 10, top: 160, width: 300, height: 200, flow: 'vertical' },
        padding: { left: 5, top: 5, right: 5, bottom: 5 },
        titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
        source: dataAdapter,
        colorScheme: 'scheme03',
        seriesGroups:
           [
            {
              type: 'pie',
              showLabels: true,
              series:
        [
                  { 
                     dataField: 'total',
           displayText: 'hasil',
                     labelRadius: 170,
                     initialAngle: 15,
                     radius: 145,
                     centerOffset: 0,
                     formatFunction: function (value) {
                    if (isNaN(value))
                      return value;
                      return parseFloat(value);
                                        },
                  }
                ]
             }
           ]
         };
     // Menampilkan Chart
      $('#chartContainer').jqxChart(settings);
   });
</script>