<style type="text/css">
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
    
    .chart-box {
        display: block;
        width: 50%;
        border: 1px solid #ccc;
        border-radius: 3px;
        padding: 5px 10px;
        margin-left: auto;
        margin-right: auto;
        position: relative;  
    }

    .chart-box > .title {
        display: flex;
    }

    .chart-box > .title > p {
        font-size: 1.5em;
        font-weight: bold;
        color: #808080;
    }

    .chart-box > .fa {
        position: absolute;
        right: 10px;
        top: 12px;
        font-size: 2em;
    }

    h1 > sub {
        font-size: .5em;
    }

    .chart-box > .info > .status {
        padding: 5px 20px;
        background: #00a65a;
        color: #fff;
        font-weight: bold;
        border-radius: 0px 20px 20px 0px;
        margin-left: -11px;
    }
    
    .chart-box > .info {
        display: flex;
    }

    .chart-box > .info > .rank {
    }

    .chart-box > .info > .rank > h1 {    
        margin: 0px
    }

</style>

<section>
    <?php 
        if($this->session->flashdata('alert_failed')){ 
            echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
        }elseif($this->session->flashdata('alert_success')){
            echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
        }
    ?>
</section>

<section class="content-header">
    <h1 align="center">
        CUSTOMER SATISFACTION SURVEY
    </h1>
    <button class="btn btn-default btn_summary" type="button" align="left">View Summary</button>
</section>
<section class="content csi-survey">
    <div class="row">
        <div class="col-md-12">
            <div class="box">

                <form method="post" action="<?= base_url()?>index.php/Csi_survey/create_survey">
                <div class="box-body">
                    <div class="option-box flex-center">
                        <div class="option-item flex-middle">
                            <span class="title">Service Type</span>
                            <select class="form-control" name="type">
                                <option value="repair shop" selected>Repair Shop</option>
                                <option value="component">Component Management</option>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Customer</span>
                            <select class="form-control" name="customer" readonly>
                                <option value="<?= $customer_list[0]->ID_CUSTOMER?>"><?= $customer_list[0]->COMPANY_NAME?></option>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Period</span>
                            <select class="form-control" name="period" readonly>
                                <?php
                                $period = date('m');
                                if($period <= 6){
                                ?>
                                <option value="1">1st Semester</option>
                                <?php }else{?>
                                <option value="2">2nd Semester</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="feedback-area">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="feedback-value">
                                    <h4 class="title"><span>Q</span>uality</h4>
                                    <input type="number" class="form-control" name="quality" min="1" max="5" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="feedback-value">
                                    <h4 class="title"><span>C</span>ost</h4>
                                    <input type="number" class="form-control" name="cost" min="1" max="5" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="feedback-value">
                                    <h4 class="title"><span>D</span>elivery</h4>
                                    <input type="number" class="form-control" name="delivery" min="1" max="5" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="feedback-value">
                                    <h4 class="title"><span>S</span>ervices</h4>
                                    <input type="number" class="form-control" name="services" min="1" max="5" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="recommendation-area">
                        <div class="range-box">
                            <div class="title">Value Desc.</div>
                            <ul>
                                <li><span style="background:#3C4858">1</span> Very Bad</li>
                                <li><span style="background:#FF7849">2</span> Bad</li>
                                <li><span style="background:#FFAB00">3</span> Fair</li>
                                <li><span style="background:#009EEB">4</span> Good</li>
                                <li><span style="background:#13CE66">5</span> Very Good</li>
                            </ul>
                        </div>
                        <div class="recommendation-box">
                            <span>Recommendation</span>
                            <textarea name="recomendation" rows="5" class="form-control" placeholder="Type your recomendation here"></textarea>
                            <button class="btn btn-primary" type="submit" name="insert" value="1">Submit</button>
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</section>

<script>
  $(document).ready(function () {

    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);

    $(".select2").select2();

    $('#category_menu').change(function(){
        var tipe = $('#category_menu').val();
        $('#list_menu').select2();
        //AJAX request
        $.ajax({
            url:"<?php echo base_url();?>index.php/Csi_survey/data/"+tipe,
            type: "POST",
            success: function(data){
                data = $.parseJSON(data);
                console.log(data+"x");
                       
                $('#list_menu').find('option').not(':first').remove();

                $.each(data,function(index, val){
                    var option = new Option(val['doc_no'], val['doc_no'], true, true);
                    $('#list_menu').append(option).trigger('change');
                });

                $('#list_menu').trigger('change');
            }
        });
    });

    $('.btn_summary').on("click",function(){
        window.location.href = "<?= base_url()?>index.php/Csi_survey/summary";
    });

  });
</script>