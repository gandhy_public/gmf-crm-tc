<style type="text/css">
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
    
    .chart-box {
        display: block;
        width: 50%;
        border: 1px solid #ccc;
        border-radius: 3px;
        padding: 5px 10px;
        margin-left: auto;
        margin-right: auto;
        position: relative;  
    }

    .chart-box > .title {
        display: flex;
    }

    .chart-box > .title > p {
        font-size: 1.5em;
        font-weight: bold;
        color: #808080;
    }

    .chart-box > .fa {
        position: absolute;
        right: 10px;
        top: 12px;
        font-size: 2em;
    }

    h1 > sub {
        font-size: .5em;
    }

    .chart-box > .info > .status {
        padding: 5px 20px;
        background: #00a65a;
        color: #fff;
        font-weight: bold;
        border-radius: 0px 20px 20px 0px;
        margin-left: -11px;
    }
    
    .chart-box > .info {
        display: flex;
    }

    .chart-box > .info > .rank {
    }

    .chart-box > .info > .rank > h1 {    
        margin: 0px
    }

</style>

<section>
    <?php 
        if($this->session->flashdata('alert_failed')){ 
            echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
        }elseif($this->session->flashdata('alert_success')){
            echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
        }
    ?>
</section>

<section class="content-header">
    <h1 align="center">
        CUSTOMER SATISFACTION SURVEY
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="option-box flex-center">
                        <div class="option-item flex-middle">
                            <span class="title">Service Type</span>
                            <select class="form-control" name="TYPE" id="TYPE">
                                <option value="repair shop" selected>Repair Shop</option>
                                <option value="component">Component</option>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Customer</span>
                            <select class="form-control select2" name="CUSTOMER" id="CUSTOMER">
                                <?php foreach($customer_list as $key){?>
                                <option value="<?= $key->ID_CUSTOMER?>"><?= $key->COMPANY_NAME?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Year</span>
                                <select class="form-control select2" name="TAHUN" id="TAHUN">
                                <?php foreach($year_list as $key){?>
                                <option value="<?= $key['YEAR']?>"><?= $key['YEAR']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="option-item">
                            <button class="btn btn-primary" id="search">Search</button>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-3">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Service Type :</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="TYPE" id="TYPE">
                                            <option value="repair shop">Repair Shop</option>
                                            <option value="component">Component</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Customer :</label>
                                    <div class="col-sm-8">
                                        <select name="CUSTOMER" id="CUSTOMER" class="form-control select2">
                                            <?php foreach($customer_list as $key){?>
                                            <option value="<?= $key->ID_CUSTOMER?>"><?= $key->COMPANY_NAME?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Year :</label>
                                    <div class="col-sm-8">
                                        <select name="TAHUN" id="TAHUN" class="form-control select2">
                                            <?php foreach($year_list as $key){?>
                                            <option value="<?= $key['YEAR']?>"><?= $key['YEAR']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <button class="btn btn-primary" id="search">Search</button>
                                    </div>
                                    <div class="col-sm-7"></div>
                                </div>
                            </div>
                        </div>                       
                    </div> -->

                    <div class="row">
                        <center><p class="loading">Loading get data...</p></center>
                    </div>

                    <div class="row summary">
                        <div class="col-md-6">
                            <div class="chart-box">
                                <div class="title">
                                    <p>1 <sup>st</sup> Semester</p>
                                </div>
                                <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #00a65a"></i>
                                <div class="info">
                                    <span class="status" id="status_1">
                                        Null
                                    </span>
                                    <div class="rank">
                                        <h1 style="margin: 0 0 0 50px !important ;font-style: bold;">
                                            <span id="nilai_huruf_1">X</span> 
                                            <span style="font-size: 0.5em" id="nilai_angka_1">( 0 )</span>
                                        </h1> 
                                    </div>                                        
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <center><label for="email">Recomendation : </label> </center>
                                <textarea name="recomendation_1" id="recomendation_1" rows="5" class="form-control"></textarea>
                            </div>
                            <br>
                            <div class="form-group">
                                <center><label for="email">Follow Up : </label> </center>
                                <textarea name="follow_1" id="follow_1" rows="5" class="form-control" data-x=""></textarea>
                            </div>                            
                        </div>
                        <div class="col-md-6">
                            <div class="chart-box">
                                <div class="title">
                                    <p>2 <sup>nd</sup> Semester</p>
                                </div>
                                <i class="fa fa-arrow-circle-down" aria-hidden="true" style="color: #dd4b39" id="up_down"></i>
                                <div class="info">
                                    <span class="status" id="status_2">
                                        Null
                                    </span>
                                    <div class="rank">
                                         <h1 style="margin: 0 0 0 50px !important ;font-style: bold;">
                                            <span id="nilai_huruf_2">X</span> 
                                            <span style="font-size: 0.5em" id="nilai_angka_2">( 0 )</span>
                                        </h1> 
                                    </div>                                        
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <center><label for="email">Recomendation : </label> </center>
                                <textarea name="recomendation_2" id="recomendation_2" rows="5" class="form-control"></textarea>
                            </div>
                            <br>
                            <div class="form-group">
                                <center><label for="email">Follow Up : </label> </center>
                                <textarea name="follow_2" id="follow_2" rows="5" class="form-control" data-x=""></textarea>
                            </div>                            
                        </div>                        
                    </div>

                    <div class="row summary">
                        <center> <button class="btn btn-primary btn_submit">Submit</button></center>  
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<script>
  $(document).ready(function () {

    $('.loading').hide();
    $('.btn_submit').hide();

    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 5000);

    $(".select2").select2();

    $('#search').on("click",function(){
        var customer = $('#CUSTOMER').val();
        var type = $('#TYPE').val();
        var tahun = $('#TAHUN').val();

        $('.summary').hide();
        $('.loading').text('Loading get data...');
        $('.loading').show();
        
        $.ajax({
            url:"<?php echo base_url();?>index.php/Csi_survey/detail",
            type: "POST",
            data:{
                customer:customer,
                tahun:tahun,
                type:type
            },
            success: function(data){
                if(data){
                    data = JSON.parse(data);

                    //Semester 1
                    var color1 = '';
                    switch (data[0].status) {
                        case 'Very Good': color1 = '#13b55b'; break;
                        case 'Good': color1 = '#009EEB'; break;
                        case 'Fair': color1 = '#FFAB00'; break;
                        case 'Bad': color1 = '#FF7849'; break;
                        case 'Very Bad': color1 = '#3C4858'; break;
                    }
                    $('#status_1').text(data[0].status);
                    $('#status_1').css('background', color1);
                    $('#nilai_huruf_1').text(data[0].nilai_huruf);
                    $('#nilai_angka_1').text("( "+data[0].nilai_angka+" )");
                    $('#recomendation_1').val(data[0].recomendation).prop('readonly', true);
                    if(data[0].recomendation == ""){
                        $('#follow_1').val(data[0].follow_up).prop('readonly', true);
                        $('#follow_1').attr('data-x', data[0].id);
                    }else{
                        $('#follow_1').val(data[0].follow_up);
                        $('#follow_1').attr('data-x', data[0].id);
                    }

                    //Semester 2
                    var color2 = '';
                    switch (data[0].status) {
                        case 'Very Good': color2 = '#13b55b'; break;
                        case 'Good': color2 = '#009EEB'; break;
                        case 'Fair': color2 = '#FFAB00'; break;
                        case 'Bad': color2 = '#FF7849'; break;
                        case 'Very Bad': color2 = '#3C4858'; break;
                    }
                    $('#status_2').text(data[1].status);
                    $('#status_2').css('background', color2);
                    $('#nilai_huruf_2').text(data[1].nilai_huruf);
                    $('#nilai_angka_2').text("( "+data[1].nilai_angka+" )");
                    $('#recomendation_2').val(data[1].recomendation).prop('readonly', true);
                    if(data[1].recomendation == ""){
                        $('#follow_2').val(data[1].follow_up).prop('readonly', true);
                        $('#follow_2').attr('data-x', data[1].id);
                    }else{
                        $('#follow_2').val(data[1].follow_up);
                        $('#follow_2').attr('data-x', data[1].id);
                    }
                    if(data[1].up_down == "up"){
                        $('#up_down').removeClass("fa-arrow-circle-down");
                        $('#up_down').addClass("fa-arrow-circle-up");
                        $('#up_down').css("color","#00a65a");
                    }else{
                        $('#up_down').removeClass("fa-arrow-circle-up");
                        $('#up_down').addClass("fa-arrow-circle-down");
                        $('#up_down').css("color","#dd4b39");
                    }


                    $('.summary').show();
                    $('.loading').hide(); 
                    $('.btn_submit').show();
                }else{
                    $('.loading').text('No data available');
                }            
            }
        });
    });

    $('.btn_submit').on("click", function(){
        var follow_1 = $('#follow_1').val();
        var follow_2 = $('#follow_2').val();

        var id_follow_1 = $('#follow_1').data('x');
        var id_follow_2 = $('#follow_2').data('x');

        $.ajax({
            url:"<?php echo base_url();?>index.php/Csi_survey/follow_up",
            type: "POST",
            data:{
                follow_1:follow_1,
                follow_2:follow_2,
                id_follow_1:id_follow_1,
                id_follow_2:id_follow_2
            },
            success: function(data){
                data = JSON.parse(data);
                if(data[0].semester1 == "sukses"){
                    $.notify("Success Follow Up Semester 1", "success");
                }else if(data[0].semester1 == "gagal"){
                    $.notify("Failed Follow Up Semester 1", "error");
                }else{
                    
                }

                if(data[0].semester2 == "sukses"){
                    $.notify("Success Follow Up Semester 2", "success");
                }else if(data[0].semester2 == "gagal"){
                    $.notify("Failed Follow Up Semester 2", "error");
                }else{
                    
                }
            }
        });
    });

  });
</script>