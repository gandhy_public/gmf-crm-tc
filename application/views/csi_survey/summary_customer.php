<style type="text/css">
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
    
    .chart-box {
        display: block;
        width: 50%;
        border: 1px solid #ccc;
        border-radius: 3px;
        padding: 5px 10px;
        margin-left: auto;
        margin-right: auto;
        position: relative;  
    }

    .chart-box > .title {
        display: flex;
    }

    .chart-box > .title > p {
        font-size: 1.5em;
        font-weight: bold;
        color: #808080;
    }

    .chart-box > .fa {
        position: absolute;
        right: 10px;
        top: 12px;
        font-size: 2em;
    }

    h1 > sub {
        font-size: .5em;
    }

    .chart-box > .info > .status {
        padding: 5px 20px;
        background: #00a65a;
        color: #fff;
        font-weight: bold;
        border-radius: 0px 20px 20px 0px;
        margin-left: -11px;
    }
    
    .chart-box > .info {
        display: flex;
    }

    .chart-box > .info > .rank {
    }

    .chart-box > .info > .rank > h1 {    
        margin: 0px
    }

</style>

<section>
    <?php 
        if($this->session->flashdata('alert_failed')){ 
            echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
        }elseif($this->session->flashdata('alert_success')){
            echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
        }
    ?>
</section>

<section class="content-header">
    <h1 align="center">
        CUSTOMER SATISFACTION SURVEY
    </h1>
    <button class="btn btn-default btn_form" type="button" align="left">Form CSI Survey</button>
</section>
<section class="content csi-survey">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="option-box flex-center">
                        <div class="option-item flex-middle">
                            <span class="title">Service Type</span>
                            <select class="form-control" name="TYPE" id="TYPE">
                                <option value="repair shop" selected>Repair Shop</option>
                                <option value="component">Component Management</option>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Customer</span>
                            <select class="form-control select2" name="CUSTOMER" id="CUSTOMER">
                                <?php foreach($customer_list as $key){?>
                                <option value="<?= $key->ID_CUSTOMER?>"><?= $key->COMPANY_NAME?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="option-item flex-middle">
                            <span class="title">Year</span>
                                <select class="form-control select2" name="TAHUN" id="TAHUN">
                                <?php foreach($year_list as $key){?>
                                <option value="<?= $key['YEAR']?>"><?= $key['YEAR']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="option-item">
                            <button class="btn btn-primary" id="search">Search</button>
                        </div>
                    </div>
                    <div class="loading">
                        <p id="loading"></p>
                    </div>
                    <div class="summary-area">
                        <div class="summary-item">
                            <div class="summary-card">
                                <div class="summary-header">
                                    <div class="title">1<sup>st</sup> Semester</div>
                                    <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #00a65a"></i>
                                </div>
                                <div class="summary-body flex-middle">
                                    <div class="summary-status" id="status_1">Status</div>
                                    <div class="summary-value">
                                        <span class="alpha" id="nilai_huruf_1">X</span>
                                        <span class="number" id="nilai_angka_1">( 0 )</span>
                                    </div>
                                </div>
                            </div>
                            <div class="recommendation-box center" style="margin-top: 40px;">
                                <span>Recommendation</span>
                                <textarea name="recomendation_1" id="recomendation_1" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="recommendation-box center">
                                <span>Follow Up</span>
                                <textarea name="follow_1" id="follow_1" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="range-box">
                            <div class="title">Value Desc.</div>
                            <ul>
                                <li><span style="font-size: 15"><b>A</b> =</span> 4.0 - 5.0</li>
                                <li><span style="font-size: 15"><b>B</b> =</span> 3.0 - 3.9</li>
                                <li><span style="font-size: 15"><b>C</b> =</span> 2.0 - 2.9</li>
                                <li><span style="font-size: 15"><b>D</b> =</span> 1.0 - 1.9</li>
                                <li><span style="font-size: 15"><b>E</b> =</span> 0.0 - 0.9</li>
                                <li><span style="font-size: 15"><b>X</b> =</span> No data</li>
                            </ul>
                        </div>
                        <div class="summary-item">
                            <div class="summary-card">
                                <div class="summary-header">
                                    <div class="title">2<sup>nd</sup> Semester</div>
                                    <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #00a65a" id="up_down"></i>
                                </div>
                                <div class="summary-body flex-middle">
                                    <div class="summary-status" id="status_2">Status</div>
                                    <div class="summary-value">
                                        <span class="alpha" id="nilai_huruf_2">X</span>
                                        <span class="number" id="nilai_angka_2">( 0 )</span>
                                    </div>
                                </div>
                            </div>
                            <div class="recommendation-box center" style="margin-top: 40px;">
                                <span>Recommendation</span>
                                <textarea name="recomendation_2" id="recomendation_2" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="recommendation-box center">
                                <span>Follow Up</span>
                                <textarea name="follow_2" id="follow_2" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div style="text-align:center;margin-bottom:30px;">
                        <button class="btn btn-primary btn_submit">Submit</button>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
  $(document).ready(function () {

    $('.loading').show();
    $('#loading').text("Please search the survey first");
    $('.btn_submit').hide();
    $('.summary-area').hide();
    $("#CUSTOMER").val("<?= $customer?>").change().prop("disabled",true);

    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);

    $(".select2").select2();

    $('#search').on("click",function(){
        var type = $('#TYPE').val();
        var customer = $('#CUSTOMER').val();
        var tahun = $('#TAHUN').val();

        $('.summary-area').hide();
        $('.loading').show();
        $('.loading p').text('Loading get data...');
        
        $.ajax({
            url:"<?php echo base_url();?>index.php/Csi_survey/detail",
            type: "POST",
            data:{
                type:type,
                customer:customer,
                tahun:tahun
            },
            success: function(data){
                if(data){
                    data = JSON.parse(data);

                    //Semester 1
                    var color1 = '';
                    switch (data[1].status) {
                        case 'Very Good': color1 = '#13b55b'; break;
                        case 'Good': color1 = '#009EEB'; break;
                        case 'Fair': color1 = '#FFAB00'; break;
                        case 'Bad': color1 = '#FF7849'; break;
                        case 'Very Bad': color1 = '#3C4858'; break;
                    }
                    $('#status_1').text(data[1].status);
                    $('#status_1').css('background', color1);
                    $('#nilai_huruf_1').text(data[1].nilai_huruf);
                    $('#nilai_angka_1').text("( "+data[1].nilai_angka+" )");
                    $('#recomendation_1').val(data[1].recomendation).prop('readonly', true);
                    $('#follow_1').val(data[1].follow_up).prop('readonly', true);
                    $('#follow_1').attr('data-x', data[1].id);

                    //Semester 2
                    var color2 = '';
                    switch (data[1].status) {
                        case 'Very Good': color2 = '#13b55b'; break;
                        case 'Good': color2 = '#009EEB'; break;
                        case 'Fair': color2 = '#FFAB00'; break;
                        case 'Bad': color2 = '#FF7849'; break;
                        case 'Very Bad': color2 = '#3C4858'; break;
                    }
                    $('#status_2').text(data[2].status);
                    $('#status_2').css('background', color2);
                    $('#nilai_huruf_2').text(data[2].nilai_huruf);
                    $('#nilai_angka_2').text("( "+data[2].nilai_angka+" )");
                    $('#recomendation_2').val(data[2].recomendation).prop('readonly', true);
                    $('#follow_2').val(data[2].follow_up).prop('readonly', true);
                    $('#follow_2').attr('data-x', data[2].id);
                    
                    if(data[2].up_down == "up"){
                        $('#up_down').removeClass("fa-arrow-circle-down");
                        $('#up_down').addClass("fa-arrow-circle-up");
                        $('#up_down').css("color","#00a65a");
                    }else{
                        $('#up_down').removeClass("fa-arrow-circle-up");
                        $('#up_down').addClass("fa-arrow-circle-down");
                        $('#up_down').css("color","#dd4b39");
                    }


                    $('.summary-area').show();
                    $('.loading').hide(); 
                    $('.btn_submit').hide();
                }else{
                    $('.loading p').text('No data available');
                }            
            }
        });
    });

    $('.btn_submit').on("click", function(){
        var follow_1 = $('#follow_1').val();
        var follow_2 = $('#follow_2').val();

        var id_follow_1 = $('#follow_1').data('x');
        var id_follow_2 = $('#follow_2').data('x');

        $.ajax({
            url:"<?php echo base_url();?>index.php/Csi_survey/follow_up",
            type: "POST",
            data:{
                follow_1:follow_1,
                follow_2:follow_2,
                id_follow_1:id_follow_1,
                id_follow_2:id_follow_2
            },
            success: function(data){
                data = JSON.parse(data);
                if(data[0].semester1 == "sukses"){
                    $.notify("Success Follow Up Semester 1", "success");
                }else if(data[0].semester1 == "gagal"){
                    $.notify("Failed Follow Up Semester 1", "error");
                }else{
                    
                }

                if(data[0].semester2 == "sukses"){
                    $.notify("Success Follow Up Semester 2", "success");
                }else if(data[0].semester2 == "gagal"){
                    $.notify("Failed Follow Up Semester 2", "error");
                }else{
                    
                }
            }
        });
    });

    $('.btn_form').on("click",function(){
        window.location.href = "<?= base_url()?>index.php/Csi_survey";
    });

  });
</script>