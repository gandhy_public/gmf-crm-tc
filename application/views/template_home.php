<!DOCTYPE html>
<html>
	<head>
		<link rel="shortcut icon" href="assets/dist/img/LogoIcon.png">
		<title>Home Component & Material</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/bower_components/font-awesome/css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.php" id="logo">Component and Material</a></h1>
								<hr />
								<p>HOME COMPONENT AND MATERIAL - GMF</p>
							</header>
							<!-- <footer>
								<a href="http://localhost/crm-tc2/" class="button circled scrolly">Login</a>
							</footer> -->
						</div>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><img src="assets/dist/img/logo-gmf.png" height="px" width="134px alt="" />
                                <li><a href="index.php">Home</a></li>
                                <li>
									<a href="#">About TC</a>
									<ul>
										<li><a href="#">TC Concern</a></li>
										<li><a href="#">TC Journey</a></li>
										<li><a href="#">TC Principles</a></li>									
                                    </ul>
								</li>
								<li>
									<a href="#">TC Service</a>
									<ul>
										<li><a href="#">Pooling PBTH</a></li>
										<li><a href="#">Loan Excange</a></li>
										<li><a href="#">Retail</a></li>	
										<li><a href="#">Landing Gear</a></li>								
                                    </ul>
								</li>
								<li>
									<a href="<?php echo base_url() ?>index.php/login"> Login</a>
								</li>
                                <!-- <li>
									<a href="#">Dashboard</a>
									<ul>
										<li><a href="#">1. Pooling PBTH</a></li>
										<li><a href="#">2. Loan Excange</a></li>
										<li><a href="#">3. Retail</a></li>									
                                    </ul>
								</li> -->
							</ul>
						</nav>

				</div>


			<!-- Footer -->
				<div id="footer">
					<div class="container">
						<div class="row">

							<!-- Tweets -->
								<section class="4u 12u(mobile)">
									<header>
										<h2 class="icon fa-twitter circled"><span class="label">Tweets</span></h2>
									</header>
									<ul class="divided">
										<li>
											<article class="tweet">
												Amet nullam fringilla nibh nulla convallis tique ante sociis accumsan.
												<span class="timestamp">5 minutes ago</span>
											</article>
										</li>
										<li>
											<article class="tweet">
												Hendrerit rutrum quisque.
												<span class="timestamp">30 minutes ago</span>
											</article>
										</li>
										<li>
											<article class="tweet">
												Curabitur donec nulla massa laoreet nibh. Lorem praesent montes.
												<span class="timestamp">3 hours ago</span>
											</article>
										</li>
										<li>
											<article class="tweet">
												Lacus natoque cras rhoncus curae dignissim ultricies. Convallis orci aliquet.
												<span class="timestamp">5 hours ago</span>
											</article>
										</li>
									</ul>
								</section>

							<!-- Posts -->
								<section class="4u 12u(mobile)">
									<header>
										<h2 class="icon fa-file circled"><span class="label">Posts</span></h2>
									</header>
									<ul class="divided">
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Nisl fermentum integer</a></h3>
												</header>
												<span class="timestamp">3 hours ago</span>
											</article>
										</li>
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Phasellus portitor lorem</a></h3>
												</header>
												<span class="timestamp">6 hours ago</span>
											</article>
										</li>
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Magna tempus consequat</a></h3>
												</header>
												<span class="timestamp">Yesterday</span>
											</article>
										</li>
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Feugiat lorem ipsum</a></h3>
												</header>
												<span class="timestamp">2 days ago</span>
											</article>
										</li>
									</ul>
								</section>

							

						</div>
						<hr />
						<div class="row">
							<div class="12u">

								<!-- Contact -->
									<section class="contact">
										<header>
											<h3>Nisl turpis nascetur interdum?</h3>
										</header>
										<p>Urna nisl non quis interdum mus ornare ridiculus egestas ridiculus lobortis vivamus tempor aliquet.</p>
										<ul class="icons">
											<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
											<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
											<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
											<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
											<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
											<li><a href="#" class="icon fa-linkedin"><span class="label">Linkedin</span></a></li>
										</ul>
									</section>

								<!-- Copyright -->
									<div class="copyright">
										<ul class="menu">
											<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
										</ul>
									</div>

							</div>

						</div>
					</div>
				</div>

		</div>

		<!-- Scripts -->
			<script src="<?php echo base_url(); ?>assets/build/js/jquery.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/build/js/jquery.dropotron.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/build/js/jquery.scrolly.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/build/js/jquery.onvisible.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/build/js/skel.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/build/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo base_url(); ?>assets/build/js/main.js"></script>

	</body>
</html>