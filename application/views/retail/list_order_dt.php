<style>
/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 0px;
    cursor: pointer;
    font-size: 13px;
    font-family: Roboto,sans-serif;
    font-weight: 300;
    line-height: 1.571429;
    color: #37474f;
    text-align: left;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #eee;
    border-radius: 4px;

}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #009933;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

 hr.style2 {
    height: 10px;
    border: 0;
    box-shadow: 0 10px 10px -10px #080808 inset;

}
</style>
<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <!-- <div class="col-md-12">
                  <h3 class="box-title">Data Retail Order</h3>
              </div> -->
              <div id="ch-group" class="col-lg-12 col-md-12 col-xs-12">

              </div>
              <br>
              <div class="col-lg-12 col-md-12 col-xs-12" style=" margin:0 auto;">
                  <hr class="style2">
              </div>
              <div class="form-inline" align="justify-content-md-center">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="input-group margin">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger">Payment Status</button>
                    </div>
                    <!-- /btn-group -->
                    <select class="form-control" data-style="btn-outline btn-primary" id="STATUS_PAID" name="STATUS_PAID">
                            <option value="" >--Select--</option>
                            <option value="Paid " >PAID</option>
                            <option value="Unpaid" >UNPAID</option>
                    </select>
                  </div>
                </div>  
              </div>

            </div>
		                <br>
		                <div class="box-body no-padding">
                        <div class=" table-responsive ">
  		                    <table id="tbList" class="table  table-bordered table-hover table-striped">
  		                        <thead style="background-color: #3c8dbc; color:#ffffff;">
  		                            <tr>
                                      <th><center>Act</center></th>
  		                                <th style="width: 40px">NO</th>
  		                                <th>SALES ORDER</th>
  		                                <th>MO</th>
  		                                <th>PURCHASE ORDER</th>
  		                                <th>PART NUMBER</th>
  		                                <th>PART NAME</th>
  		                                <th>SERIAL NUMBER</th>
  		                                <th>RECEIVED DATE</th>
  		                                <th>QUOTATION DATE</th>
  		                                <th>APPROVAL DATE</th>
  		                                <th>TAT</th>
  		                                <th>STATUS</th>
  		                                <th>DELIVERY DATE</th>
  		                                <th>REMARKS</th>
  		                                <th>SALES BILLING</th>
  		                            </tr>
  		                        </thead>
  		                        <tbody>
  		                            <?php
  		                                if($data_table) {
  		                                    $no = 0;
  		                                    foreach ($data_table as $rows) {
  		                                    $no++;
  		                            ?>
  		                             <tr>
                                  <td>
                                    <div class="btn-group">
                                            <a onclick="detailBtn('<?php echo base_url('index.php/Retail/order_detail').'/'.$rows['MAINTENANCE_ORDER']?>')"><button class="btn btn-success btn-xs"><i class="fa fa-file-text"></i></button></a></div>
                                        </td>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo ltrim($rows['SALES_ORDER'], '0'); ?></td>
                                    <td><?php echo $rows['MAINTENANCE_ORDER']; ?></td>
                                    <td><?php echo $rows['PURCHASE_ORDER']; ?></td>
                                    <td><?php echo $rows['PART_NUMBER']; ?></td>
                                    <td><?php echo $rows['PART_NAME']; ?></td>
                                    <td><?php echo $rows['SERIAL_NUMBER']?></td>       
                                    <td>
                                      <?= $rows['RECEIVED_DATE']; ?>
                                    </td>
                                    <td>
                                      <?= $rows['QUOTATION_DATE']; ?>
                                    </td>
                                    <td>
                                      <?= $rows['APPROVAL_DATE']; ?>
                                    </td>
                                    <td id="color_tat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" 
                                        value="<?php echo $rows['TAT']; ?>"/><?php echo $rows['TAT']; ?> Days</td>
                                    <td><?php echo $rows['TXT_STAT']?></td>
                                    <td>
                                     <?= $rows['DELIVERY_DATE']; ?>
                                    </td>
                                    <td><?php echo $rows['REMARKS']?></td>
                                    <td><?php echo $rows['BILLING_STATUS']?></td>
                                    <!-- <td>
                                      <div class="btn-group">
                                        <a href="<?php //echo base_url('index.php/Retail/order_detail').'/'.$rows['ID_RETAIL'] ?>" onclick="return confirm('Are You Sure Want To Show This Order Detail ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-file-text"></i></button></a>
                                        <a href="#ModalView" data-toggle="modal"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button></a>
                                        <a href="#ModalViewLog" data-toggle="modal"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-tasks"></i></button></a>
                                        <a href="<?php //echo base_url('Working_order/edit_in_progress_wo'); //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Edit This New Working Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
                                        <a href="<?php //echo base_url('Working_order/cancel_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Cancel This New Working Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a>
                                      </div>
                                    </td>-->
                                </tr>
                                <?php } }?>
  		                        </tbody>
  		                    </table>

  		                    <!-- <div class="col-md-3">
  		                        <b><?php //if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
  		                    </div>
  		                    <div class="col-md-6">
  		                        <?php// echo $paging; ?>
  		                    </div>
  		                    <div class="col-md-3 pull-right">
  		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php echo $title; ?></button></a>
  		                    </div> -->
                        </div>
		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<!-- <script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#tbList thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
        if (jQuery.inArray(index, unsearchable)==-1) {
        
          $(this).html( '<input id="col'+index+'_filter" data-column="'+index+'" type="text" class="form-control columns-filter" placeholder="Search '+title+'" />' );
        }else{
            $(this).empty();
        }
    });
    var table = $("#tbList").DataTable({

      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "ordering": false,
      "ajax": {
        "url" : "<?= site_url('api/Retail/list_order') ?>",
        "type": 'post',
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({
                1: json.data[i].ROW,
                2: json.data[i].SALES_ORDER,
                3: json.data[i].MAINTENANCE_ORDER,
                4: json.data[i].PURCHASE_ORDER,
                5: json.data[i].PART_NUMBER,
                6: json.data[i].PART_NAME,
                7: json.data[i].SERIAL_NUMBER,
                8: json.data[i].RECEIVED_DATE,
                9: json.data[i].QUOTATION_DATE,
                10: json.data[i].APPROVAL_DATE,
                11: json.data[i].TAT,
                12:json.data[i].TXT_STAT,
                13:json.data[i].DELIVERY_DATE,
                14:json.data[i].REMARKS,
                15:json.data[i].BILLING_STATUS,
              })
            }
            return return_data;

        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        },{
          "targets": 0,
          "className": "dt-center",
          "data": null,
          "defaultContent":
          '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
   });

    $('#tbList').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = table.row( $(this).parents('tr') ).data();
        var no_ = data[3];

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "order_detail/"+no_;
         },500);

   } );

   
    $("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();

    var listStatus = <?php echo json_encode($listStatus); ?>;
      console.log(listStatus);

      str = "<div class='col-md-4'>";
      $.each(listStatus, function(index, el){


          str += "<label> "+
                  "<input type='checkbox' class='cbstatus' id='"+el.STATUS+"' name='status' value='"+el.STATUS+"'>"+
                  el.STATUS+
            "</label><br>";
            if (index%2==1) {
              str +="</div>";
              str +="<div class='col-md-4' >";
            }


      });
          str +="</div>";

        $('#ch-group').append(str);
      $(".cbstatus").click(function() {

        if ($('.cbstatus:checkbox:checked').length>0) {

          //use radio values
          var v = $('.cbstatus:checked')[0].value;
          console.log(v);
          //now filter in column 2, with no regex, no smart filtering, no inputbox,not case sensitive
          tblo.fnFilter(v, 12, false, false, false, false);
        }else{
          tblo.fnFilter('', 12, false, false, false, false);

        }
      });

  });

</script> -->
<script type="text/javascript">
    var tblo;
      
    $(document).ready(function() {
      //Flat red color scheme for iCheck
      

      tblo = $('#tbList').dataTable( {
        dom: 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columnDefs: [
            {
                "targets": [ 11 ],
                "visible": true,
                "searchable": true
            }
        ]
      } );
      //$("#iduserrole").select2({ width: 'resolve' });
      $(".select2").select2();
      

      var listStatus = <?php echo json_encode($listStatus); ?>;
      // console.log(listStatus);

      str = "";
      $.each(listStatus, function(index, el){
          // str += "<div class='form-group'><label>"+
          //           "<input type='checkbox' class='cbstatus flat-red' id='"+el.STATUS+"' name='status' value='"+el.STATUS+"'>"+  
          //           el.STATUS+
          //         "</label></div>";

          str += "<div class='col-md-4'>"+
                 "<label class='container'> "+
                  el.STATUS+
                  "<input type='checkbox' class='cbstatus' id='"+el.STATUS+"' name='status' value='"+el.STATUS+"'>"+
                  "<span class='checkmark'></span></label><br></div>";

            // if (index%2==1) {
            //   str +="</div>";
            //   str +="<div class='col-md-4' >";
            // }
      });
          // str +="</div>";

      $('#ch-group').append(str);
     
      $(".cbstatus").click(function() {
        // if ($('.cbstatus:checkbox:checked').length>0) {

        //   //use radio values
        //   var v = $('.cbstatus:checked')[0].value;
        //   console.log(v);
        //   //now filter in column 2, with no regex, no smart filtering, no inputbox,not case sensitive
        //   tblo.fnFilter(v, 12, false, false, false, false);
        // }else{
        //   tblo.fnFilter('', 12, false, false, false, false);
        // }
        var types = $('input:checkbox[name="status"]:checked').map(function() {
          return '^' + this.value + '\$';
        }).get().join('|');
        //filter in column 0, with an regex, no smart filtering, no inputbox,not case sensitive
        tblo.fnFilter(types, 12, true, false, false, false);
            });
      
      


      $("#STATUS_PAID").change(function() {
      
        if ($('#STATUS_PAID').val()) {
      
          //use radio values
          var v = $('#STATUS_PAID').val();
          console.log(v);
          //now filter in column 2, with no regex, no smart filtering, no inputbox,not case sensitive
          tblo.fnFilter(v, 15, true, true, true, false);
        }else{
          tblo.fnFilter('', 15, false, true, true, true);
      
        }
      });
    });


    function detailBtn(url){

      alertify.confirm("View the order detail ?",
        function(){
          window.location = url;
        });
    }


      // var ckbox = $("input[name='status']");
      // var chkId = '';
      // $('input').on('click', function() {
      //
      //   if (ckbox.is(':checked')) {
      //     $("input[name='status']:checked").each ( function() {
      //           chkId = $(this).val() + ",";
      //       chkId = chkId.slice(0, -1);
      //     });
      //       // var ID_LSN    = "<?php //echo $ID_LSN; ?>";
      //       window.location = "<?=base_url()?>index.php/Retail/order_filter?status="+chkId+"&status_paid=";
      //   }
      // });
    // });
</script>

<!-- <script type="text/javascript">
  function view(id_status)
  {
    $('#ModalView').modal();
    $.ajax({
      url : "<?php echo base_url('index.php/Seleksi/view/')?>/" + id_status,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data);

        document.getElementById("TitleView").innerHTML              = data.NO_ORDER + " - " + data.STATUS_PAID;
        document.getElementById("ViewORDERDETAIL").innerHTML        = "<b>: </b>" + data.ORDERDETAIL;
        document.getElementById("ViewCUSTOMERPO").innerHTML         = "<b>: </b>" + data.CUSTOMERPO;
        document.getElementById("ViewESTIMATEDDATE").innerHTML      = "<b>: </b>" + data.ESTIMATEDDATE;
        document.getElementById("ViewLASTUPDATE").innerHTML         = "<b>: </b>" + data.LASTUPDATE;
        document.getElementById("ViewCUSTOMER").innerHTML           = "<b>: </b>" + data.CUSTOMER;
        document.getElementById("ViewORDERDATE").innerHTML          = "<b>: </b>" + data.ORDERDATE;
        document.getElementById("ViewPARTNUMBER").innerHTML         = "<b>: </b>" + data.PARTNUMBER;
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
    });
    // $.ajax({
    //   url : "<?php //echo base_url('Seleksi/view_log/')?>/" + id_pelamar,
    //   type: "GET",
    //   dataType: "JSON",
    //   success: function(data)
    //   {
    //     $('#table_log tbody tr').remove()

    //     console.log(data);
    //     var tbody = $("<tbody />"),tr;
    //     var no =1;
    //     $.each(data,function(_,obj) {
    //         tr = $("<tr />");
    //         tr.append("<td>"+no+"</td>")
    //         tr.append("<td>"+obj.nama_status+"</td>")
    //         tr.append("<td>"+obj.date_log+"</td>")
    //         tr.appendTo(tbody);
    //         no++;
    //     });

    //     tbody.appendTo("#table_log");
    //   },
    //   error: function (jqXHR, textStatus, errorThrown)
    //   {
    //       alert('Error get data from ajax');
    //   }
    // });
  }
</script>
 -->