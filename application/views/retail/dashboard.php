<section class="content-header">
  <div class="left">
    <h1>DASHBOARD RETAIL</h1>
  </div>
  <?php $this->load->view($link_directory); ?>
</section>
<section class="content custom">
  <div class="row">
    <div class="col col-md-12">
      <div class="card">
        <div class="card-header">
          <h2 class="title">Filter</h2>
        </div>
        <div class="card-body">
          <div class="filter-input row" style="margin-bottom: 0">
            <div class="col col-md-3">
              <div class="form-line">
                <label for="by_date_type">By Date Type</label>
                <select id="by_date_type" name="by_date_type" class="form-control">
                  <option value="create_date" <?= $filter == 'create_date' ? 'selected' : '';?>>Create Date</option>
                  <option value="close_date" <?= $filter == 'close_date' ? 'selected' : '';?>>Complete Date</option>
                </select>
              </div>
            </div>
            <div class="col col-md-3">
              <div class="form-line">
                <label for="by_customer">By Customer</label>
                <select id="by_customer" name="by_customer" class="form-control">
                  <?php if(count($customer) != 1){?>
                  <option value="all" selected>All Customer</option>
                  <?php }?>
                  <?php foreach($customer as $cust){?>
                    <option value="<?= $cust['ID_CUSTOMER']?>"><?= $cust['COMPANY_NAME']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="col col-md-3">
              <div class="form-line">
                <label for="by_month">By Month</label>
                <select id="by_month" name="by_month" class="form-control">
                  <option value="all" selected>All Month</option>
                  <option value="01" <?= $bulan == '01' ? 'selected' : '';?>>January</option>
                  <option value="02" <?= $bulan == '02' ? 'selected' : '';?>>February</option>
                  <option value="03" <?= $bulan == '03' ? 'selected' : '';?>>March</option>
                  <option value="04" <?= $bulan == '04' ? 'selected' : '';?>>April</option>
                  <option value="05" <?= $bulan == '05' ? 'selected' : '';?>>May</option>
                  <option value="06" <?= $bulan == '06' ? 'selected' : '';?>>June</option>
                  <option value="07" <?= $bulan == '07' ? 'selected' : '';?>>July</option>
                  <option value="08" <?= $bulan == '08' ? 'selected' : '';?>>August</option>
                  <option value="09" <?= $bulan == '09' ? 'selected' : '';?>>September</option>
                  <option value="10" <?= $bulan == '10' ? 'selected' : '';?>>October</option>
                  <option value="11" <?= $bulan == '11' ? 'selected' : '';?>>November</option>
                  <option value="12" <?= $bulan == '12' ? 'selected' : '';?>>December</option>
                </select>
              </div>
            </div>
            <div class="col col-md-3">
              <div class="form-line">
                <label for="by_year">By Year</label>
                <select id="by_year" name="by_year" class="form-control">
                  <option value="close_date">Complete Date</option>
                  <?php
                  $get_year = date('Y');
                  $i = $get_year;
                  while ($i >= 2014) {
                    $selected = $tahun == $i ? 'selected' : '';
                    echo "<option value='$i' $selected>$i</option>";
                    $i--;
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div style="position: relative">
    <div id="empty_result" style="font-size: 24px;text-align: center;padding-top: 20px;display:none">No data to display</div>
    <div id="result" class="row" style="margin-bottom:0;display:none">
      <div class="col col-md-6">
        <div class="row" id="component_status" style="margin-left: -5px;margin-right: -5px;">
        </div>
      </div>
      <div class="col col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="title">TAT ACHIEVEMENT</h3>
          </div>
          <div class="card-body">
            <div id="tat_achievement" style="width: 100%;height:400px"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="overlay-custom">
      <div id='loader' style='text-align: center;margin-top: 50px;text-align:center'><img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg' class="center"></div>
    </div>
  </div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>

<script>
  $(document).ready(function(){
    $('#by_customer').select2();
  });
$('.overlay-custom').show();
document.addEventListener("DOMContentLoaded", function(event) {
  var date_type = $('#by_date_type').val();
  var customer = $('#by_customer').val();
  var month = $('#by_month').val();
  var year = $('#by_year').val();
  updateDashboard(date_type, customer, month, year);

  var tatAchievementChart = AmCharts.makeChart("tat_achievement", {
    "type": "serial",
    "theme": "light",
    "categoryField": "category",
    "colors": ["#12335E","rgba(18,51,94,.05)"],
    "startDuration": 1,
    "categoryAxis": {
      "gridPosition": "start"
    },
    "trendLines": [],
    "graphs": [
      {
        "balloonText": "[[title]] of [[category]]:[[value]]",
        "fillAlphas": 1,
        "id": "AmGraph-1",
        "precision": 0,
        "title": "Process",
        "type": "column",
        "valueField": "column-1"
      },
      {
        "balloonText": "[[title]] of [[category]]:[[value]]",
        "fillAlphas": 1,
        "id": "AmGraph-2",
        "title": "Max.Limit 100%",
        "type": "column",
        "valueField": "column-2"
      }
    ],
    "guides": [],
    "valueAxes": [
      {
        "id": "ValueAxis-1",
        "stackType": "100%",
        "title": "",
        "unit": "%",
      }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {
      "enabled": true,
      "useGraphSettings": true
    },
    "titles": [],
    // "startEffect" : "easeInSine",
    "dataProvider": [
      {
        "category": "0",
        "column-1": "0",
        "column-2": "0",
      },
      {
        "category": "> 9 days < 21 days",
        "column-1": "0",
        "column-2": "0",
      },
      {
        "category": "> 20 days",
        "column-1": "0",
        "column-2": "0",
      }
    ]
  });

  function updateDashboard(date_type, customer, month, year) {
    $.ajax({
      url: '<?= base_url('index.php/Retail/change_dashboard')?>',
      type: 'POST',
      data: {
        date_type: date_type,
        customer: customer,
        month: month,
        year: year
      },
      beforeSend: function() {
        $('.overlay-custom').show();
        $('#empty_result').hide();
        $('#result').css('opacity', '.5');
      },
      success: function(response) {
        var data = $.parseJSON(response);
        $("#component_status").html("");

        if(data.provider.length > 0 ) {
          var totalComponentStatus = 0;
          $.each(data.provider, function(index, item) {
            totalComponentStatus += item.JUMLAH;
          });

          $.each(data.provider, function(index, item) {
            var base_url = '<?= base_url("index.php/retail")?>';
            var link = base_url+'/order_status?status='+item.STATUS+'&tahun='+year+'&customer='+customer+'&bulan='+month+'&filterby='+date_type;
            var percentage = Math.round(((item.JUMLAH/totalComponentStatus) * 100) * 100) / 100;
            var component = '<div class="col col-md-6" style="padding-left: 5px;padding-right: 5px;">' +
                '<div class="card status" style="margin-bottom:10px;">' +
                  '<div class="card-header"><h3 class="title">'+item.STATUS+'</h3></div>' +
                  '<div class="card-body">' +
                    '<div class="count"><span class="value">'+item.JUMLAH+'</span><span class="text">Total</span></div>' +
                    '<div class="progress-area">' +
                      '<div class="inner">' +
                        '<div class="progress-header">' +
                          '<div class="percentage-number">'+percentage+'% <span>of '+totalComponentStatus+'</span></div>' +
                          '<a href="'+link+'" class="detail">detail <i class="fa fa-chevron-right"></i></a>' +
                        '</div>' +
                        '<div class="progress"><div class="progress-bar" style="width:'+percentage+'%"></div></div>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
              '</div>';
            $("#component_status").append(component);
          });
          
          tatAchievementChart.dataProvider = [
            {
              "category": "< 10 days",
              "column-1": data.chart[2],
              "column-2": data.chart[3]
            },
            {
              "category": "> 9 days < 21 days",
              "column-1": data.chart[1],
              "column-2": data.chart[3]
            },
            {
              "category": "> 20 days",
              "column-1": data.chart[0],
              "column-2": data.chart[3]
            }
          ];
          tatAchievementChart.validateData();
          $('#result').show();
          $('#result').css('opacity', '1');
        } else {
          $('#empty_result').show();
          $('#result').hide();
        }
      },
      complete: function() {
        $('.overlay-custom').hide();
      }
    });
  }

  $('#by_date_type, #by_customer, #by_month, #by_year').change(function () {
    var date_type = $('#by_date_type').val();
    var customer = $('#by_customer').val();
    var month = $('#by_month').val();
    var year = $('#by_year').val();
    updateDashboard(date_type, customer, month, year);
  });
});
</script>