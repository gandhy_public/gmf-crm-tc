        <ul class="nav nav-tabs">
        	<?php
        		if($this->uri->segment(2)=='order_status'){
        	?>
        			<li <?php if($this->uri->segment(2)=='dashboard') { echo 'class="active"'; } ?>><a class="fa fa-dashboard" onClick="goBack()" style="cursor: pointer;"> Back to Dashboard</a></li>
        	<?php			
        		}
        		else if($this->uri->segment(2)=='order_detail2'){
        	?>
        			<li <?php if($this->uri->segment(2)=='dashboard') { echo 'class="active"'; } ?>><a class="fa fa-list back-tab" onClick="goBack()" style="cursor: pointer;"> Back to List of Order Details</a></li>
        	<?php			
        		}
        	?>
          
          <!-- <li <?php if($this->uri->segment(2)=='finance') { echo 'class="active"'; } ?>><a class="fa fa-money" href="<?php echo base_url(); ?>index.php/Retail/finance"> Finance</a></li> -->
        </ul>

        <script>
			function goBack() {
			    window.history.back();
			}
		</script>

        <style type="text/css">
          .back-tab{
            color: black;
            margin-bottom: 2px;
          }
          
          .nav > li > a{
            color: black;
          }

          .nav > li > a:hover{
            background-color: #fff;
            border: 1px solid #fff;
            color: #868383;
          }
        </style>

