<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
                <!-- <div class="col-md-12">
                    <h3 class="box-title">Data Retail Order</h3>
                </div> -->
              <div class="col-md-12">
                  <h3 class="box-title">Status : <span class="badge bg-orange"> <?php echo $_GET['status']; ?> </span>
                  </h3>
                  <input type="hidden" id="status_order" value="<?php echo $_GET['status']; ?>"  />
                  <input type="hidden" id="tahun" value="<?php echo $_GET['tahun']; ?>"  />
                  <input type="hidden" id="kunnr" value="<?php echo $_GET['customer']; ?>"  />
                  <input type="hidden" id="bulan" value="<?php echo $_GET['bulan']; ?>"  />
                  <input type="hidden" id="filterby" value="<?php echo $_GET['filterby']; ?>"  />
              </div>

               <!--  <div class="col-md-2 pull-right">
                    <div class="input-group">
                        <select class="form-control " id="STATUS_PAID" name="STATUS_PAID">
                            <option value="" >-- Select one --</option>
                            <option value="PAID" >Paid</option>
                            <option value="UNPAID" >Unpaid</option>
                        </select>
                    </div>
                </div> -->
            </div>
            <br>
            <div class="box-body no-padding">
                <!-- <div class=" table-responsive "> -->
                  <table id="tbList2" class="table  table-bordered table-hover table-striped">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                          <tr>
                              <th style="width: 70px"><center>Act</center></th>
                              <th>NO</th>
                              <th>SALES ORDER</th>
                              <th>MO</th>
                              <th>PURCHASE ORDER</th>
                              <th>PART NUMBER</th>
                              <th>PART NAME</th>
                              <th>SERIAL NUMBER</th>
                              <th>RECEIVED DATE</th>
                              <th>QUOTATION DATE</th>
                              <th>APPROVAL DATE</th>
                              <th>TAT</th>
                              <th>TAT APPROVAL</th>
                              <th>STATUS</th>
                              <th>DELIVERY DATE</th>
                              <th>REMARKS</th>
                              <!-- <th>SALES BILLING</th> -->
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                <!-- </div> -->
            </div>
           </section>
         </div>
       </div>
     </div>
   </div>
 </section>
 <div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="" id="form-remarks" name="form-remarks">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Edit Remarks</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <div class="form-group">
            <label class="col-sm-5 control-label">MAINTENANCE ORDER</label>
            <div class="col-sm-7">
              <input type="text" disabled="true"  class="form-control input-sm" name="AUFNR2" id="AUFNR2">
              <input type="hidden" class="form-control input-sm" name="AUFNR" id="AUFNR">
            </div>
          </div>
          <br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Remark</label>
            <div class="col-sm-7">
              <textarea style="min-height: 130px;" class="form-control input-sm" id="REMARKS" name="REMARKS"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="submit">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  $(function () {
    var unsearchable = [0,10,11,12,13,14];
    $('#tbList2 thead tr#searchtr th').each( function () {
        var title = $(this).text();
        var index = $(this)[0].cellIndex;
    } );
    var status = $('#status_order').val() ;
    var tahun = $('#tahun').val() ;
    var kunnr = $('#kunnr').val() ;
    var bulan = $('#bulan').val();
    var type  = $('#filterby').val();
    //var parameters = "{'status':'" +  + "'}";
    var tablo = $("#tbList2").DataTable({
      "dom": 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
      "buttons": [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            // 'pdfHtml5'
        ],
      "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
      "responsive": true,
      "processing": true,
      "language": {
          "processing": "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        // "<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span>Processing"
        },
      "serverSide": true,
      "scrollX": true,
      "ordering": true,
      "ajax": {
        "url" : "<?= site_url('api/Retail/order_status2') ?>",
        "type": 'post',
        "data": {'status':status, 'tahun':tahun, 'kunnr':kunnr, 'bulan':bulan, 'filterby':type},
        "dataSrc" : function(json){
          var return_data = [];

            json.draw = json.draw;
            json.recordsFiltered = json.recordsFiltered;
            json.recordsTotal = json.recordsTotal;

            /* ReOrdering json result */

            for(var i=0;i< json.data.length; i++){
              return_data.push({
                1: json.data[i].RowNum,
                2: 1*(json.data[i].SALES_ORDER),
                3: json.data[i].MAINTENANCE_ORDER,
                4: json.data[i].PURCHASE_ORDER,
                5: json.data[i].PART_NUMBER,
                6: json.data[i].PART_NAME,
                7: json.data[i].SERIAL_NUMBER,
                8: json.data[i].RECEIVED_DATE,
                9: json.data[i].QUOTATION_DATE,
                10: json.data[i].APPROVAL_DATE,
                11: json.data[i].TAT,
                12: json.data[i].TAT_APPROVAL,
                13:json.data[i].TXT_STAT,
                14:json.data[i].DELIVERY_DATE,
                15:json.data[i].REMARKS,
                // 16:json.data[i].BILLING_STATUS,
                //15:json.data[i].BILLING_STATUS,
              })
            }
            /* set new token after request completed */
            // localStorage.setItem("api_token", json.token);

            /* Set User Permission */
            return return_data;
        }
      },
      "columnDefs": [
        {
          "targets": [],
          "visible": false,
          "searchable": false
        },{
          "targets": 0,
          "className": "dt-center",
          "data": null,
          "defaultContent":
          '<?php if ($this->session->userdata('log_sess_id_customer')){ ?><button title="View Detail" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-list"></i></button> <?php } else { ?> <button title="View Detail" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-list"></i></button>&nbsp; <button title="Update" class="btUpdate2 btn btn-success btn-xs" type="button"><i class="fa fa-edit"></i></button> <?php } ?>'
        }
      ]
    });

    $('#searchtr input.columns-filter').on( 'keyup click', function () {
       filterColumn( $(this).attr('data-column') );
    });

    $('#tbList2').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = tablo.row( $(this).parents('tr') ).data();
        var no_ = data[3];
        var st = data[13];

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "order_detail2?order="+no_+"&status="+st+"&tahun="+tahun+"&customer="+kunnr;
         },500);

    });

     $('#tbList2').on( 'click', 'tbody tr .btUpdate2', function (e) {
        var data = tablo.row( $(this).parents('tr') ).data();
        var no_ = data[3];
        $.ajax({
          type: 'POST',
          url: '<?=base_url('index.php/api/Retail/order_edit/')?>',
          data: { 
              'AUFNR': no_, 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $('#REMARKS').val(obj.REMARKS);
              $('#AUFNR').val(obj.AUFNR);
              $('#AUFNR2').val(obj.AUFNR);
              $('#exampleModalCenter').modal('show');
          }
        });
      
    });

   $("#form-remarks").submit(function(event) {
      /* Act on the event */
      event.preventDefault();
      $('#submit').text("Proses Simpan");
      $('#submit').attr('disabled', true);
      var formData = new FormData($('#form-remarks')[0]);
       $.ajax({
         url:"<?php echo base_url() ?>index.php/Retail/update_remarks",
         type:'POST',
         dataType: 'text',
         data: formData,
         processData: false,
         contentType: false,
         success:function(response){
              $("#form-remarks")[0].reset();
              var data = JSON.parse(response);
              // window.location.href = "update_order/"+data.id;
                swal( data.msg , {
                      icon: "success",
                })
                .then(function(willgo){
                    if (willgo) {
                        location.reload();
                    }
                })   // reloading page
             }
          });
        });
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();

  });

</script>