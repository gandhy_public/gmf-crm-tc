<style>
  .form-group-select2 .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 28px;
  }

  .form-group-select2 .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px;
  }

  .form-group-select2 .select2-container--default .select2-selection--single {
    height: 30px;
  }
</style>

<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <div class="tab-content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">AGING</h3>
                  <div style="margin-top: 10px">
                    <form id="tat-aging" class="form-inline" method="GET">
                      <div class="form-group form-group-select2">                        
                        <select class="form-control input-sm select2" name="cur_customer">
                          <?php if(!$this->session->userdata('log_sess_id_customer')){?>
                          <option value="all" <?= ($cur_customer == "all" ? 'selected="selected"' : '')?>>ALL CUSTOMER</option>
                          <?php }?>
                          <?php
                          foreach($customer_list as $val) {
                            $selected = ($val->ID_CUSTOMER == $cur_customer ? 'selected="selected"' : '');
                            if($val->COMPANY_NAME){
                              echo "<option value='{$val->ID_CUSTOMER}' {$selected}>{$val->COMPANY_NAME}</option>";
                            }else{
                              echo "<option value='{$val->ID_CUSTOMER}' {$selected}>No Company Name ({$val->ID_CUSTOMER})</option>";
                            }
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">                        
                        <select class="form-control input-sm" name="cur_code">
                          <?php
                          if(count($cur_conf)){
                            foreach($cur_conf as $val_cur) {
                              $selected = ($val_cur['WAERK'] == $cur_code ? 'selected="selected"' : '');
                              echo "<option value='{$val_cur['WAERK']}' {$selected}>{$val_cur['WAERK']}</option>";
                            }
                          }else{
                            echo "<option value='null'>NULL</option>";
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">                        
                        <select class="form-control input-sm" name="cur_type">
                          <option value="sum_data" <?= ($cur_type == "sum_data" ? 'selected="selected"' : ''); ?>>SUMMARY DATA</option>
                          <option value="sum_billing" <?= ($cur_type == "sum_billing" ? 'selected="selected"' : ''); ?>>SUMMARY BILLING</option>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-sm btn-default">
                        <i class="fa fa-refresh"></i>
                      </button>
                    </form>
                    
                  </div>
                </div>
                <div class="box-body">
                <!-- <div id="bar-chart1" style="height:450px"></div> -->
                  <div id="tat_aging_chart" style="height:350px"></div>
                </div>
                <!-- </div> -->
                <!-- <div class="overlay">
                  <i class="fa fa-refresh fa-spin"></i>
                </div> --><!-- /.box-body -->
              </div><!-- /.box -->
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">INVOICE OUTSTANDING</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                <div class="box-body" style="padding:30px 10px;">

                  <div class="row">
                    <div class="col-lg-8 col-xs-12">
                      <div class="form-horizontal">
                        <div class="form-group" style="margin:0">
                          <label class="col-sm-5 control-label">Aging</label>
                          <div class="col-sm-7">
                            <select class="form-control select2" id="RANGE" name="RANGE" style="width: 100%" required>
                              <option value="">Choose</option>
                              <option value="1">0 - 30 Days</option>
                              <option value="2">31 - 60 Days</option>
                              <option value="3">61 - 90 Days</option>
                              <option value="4">> 90 Days</option>
                              <!-- <?php
                                //foreach ($part_number_list as $row_pn) {
                              ?>
                              <option value="<?php //echo $row_pn->ID_PN ?>"><?php //echo $row_pn->PN_NAME; ?></option>
                              <?php //} ?> -->
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 col-xs-12">
                      <div class="form-horizontal">
                        <div>
                          <a href="#" onclick="alertify.warning('Choose Aging First');"  id="searchRange" name="searchRange" class="btn btn-success"> <i class="fa fa-download"> Download</i></a>

                        </div>
                      </div>
                    </div>
                  </div>

                </div><!-- /.box-body -->

              </div><!-- /.box-info -->

              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">HISTORICAL TRANSACTION</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                <div class="box-body"style="padding:20px">
                  <!-- <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12"> -->
                      <div class="form-horizontal">
                        <div class="form-group row">
                          <label class="col-sm-2 control-label">From</label>
                          <div class="col-sm-10 input-group date" style="padding: 0 15px;">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="date" id="range_start" class="form-control" name="start_date">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2 control-label">To</label>
                          <div class="col-sm-10 input-group date" style="padding: 0 15px;">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="date" id="range_end" class="form-control" name="end_date">
                          </div>
                        </div>
                      </div>
                    <!-- </div>
                  </div> -->
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <!-- <p class="text-red" style="text-align:right;margin:0;line-height:34px;"><b>* Max 3 Month Earlier</b></p> -->
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                      <a href="#" id="download_historical"><button class="btn btn-block btn-success"><i class="fa fa-download"></i>  Download </button></a>
                    </div>
                  </div>
                </div>
              </div><!-- /.box-info -->
            </div><!-- /.col -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#RANGE').on('change', function(){
        $data =  $(this).val();
        $customer = $('select[name=cur_customer]').val();
        if ($data) {
          $('#searchRange').attr('onclick', '');
          $('#searchRange').attr('href', '<?= site_url('api/Retail/write_outstanding') ?>/'+$data+'/'+$customer).attr('target','_blank');

        }else{
          $('#searchRange').attr('onclick', 'alertify.warning("Choose Aging First");');
          $('#searchRange').attr('href', '#');
        }

    });
    $('#download_historical').on('click', function(event){
        $data_start = $('#range_start').val();
        $data_end = $('#range_end').val();
        $customer = $('select[name=cur_customer]').val();

        if (!$data_start) {
          $('#range_start').focus();
          return;
        }

        if (!$data_end) {
          $('#range_end').focus();
          return;
        }


        if ($data_end && $data_start) {
          $('#download_historical').attr('href', '<?= site_url('api/Retail/write_historical') ?>/'+$data_start+'/'+$data_end+'/'+$customer).attr('target','_blank');

          // var date1 = new Date($data_start);
          // var date2 = new Date($data_end);
          // var timeDiff = Math.abs(date2.getTime() - date1.getTime());
          // var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
          // if (diffDays<=90) 
          // $('#download_historical').attr('href', '<?= site_url('retail/finance_historical') ?>/'+$data_start+'/'+$data_end);
          // else{
          //   alertify.warning('Max range 3 Month.');
          // }
        }else{
          $('#download_historical').attr('href', 'javascript:;');
        }
        event.stopPropagation();
    });


  //td color
  // for (var i=1; i<=<?php// echo $no; ?>; i++){
  //   var start_ =  document.getElementById('start_'+i).value;
  //   var end_ =  document.getElementById('end_'+i).value;
  //   if(end_ - start_ > 0){
  //     document.getElementById('color_curtat_'+i).style.backgroundColor='#f1c1c0';
  //   }else{
  //     document.getElementById('color_curtat_'+i).style.backgroundColor='#dbecc6';
  //   }
  // }


  // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>

<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>
<!-- <script>
  $(document).ready(function() {
    var tat_aging_target = document.getElementById("tat_aging_chart");
    var tat_aging_data = {
      labels: ["0-30 Days", ">30 Days", ">60 Days"],
      datasets: [
        {
          label: "TAT Aging",
          backgroundColor: "#12335E",
          data: [
            <?= $tat_aging[0]; ?>,
            <?= $tat_aging[1]; ?>,
            <?= $tat_aging[2]; ?>
          ]
        }
      ]
    };
    var tat_aging_chart = new Chart(tat_aging_target, {
      type: 'bar',
      data: tat_aging_data,
      options: {
        barValueSpacing: 20,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                if (Math.floor(label) === label) return label;
              },
            }
          }]
        },
        legend: {
          position: 'bottom'
        },
      }
    });
  });
</script> -->
<script>
  $(document).ready(function() {

    // --------------------------------------------

    $('body').on('change', '.cur_code', function(){
      var cur_code = $(".cur_code :selected").val();      
      window.location = '<?=base_url('index.php')?>' + '/Retail/finance?cur_code=' + cur_code;
    });

    // --------------------------------------------

    var cur_customer = $('select[name=cur_customer]').val();
    var cur_code = $('select[name=cur_code]').val();
    var cur_type = $('select[name=cur_type]').val();

    $.ajax({
      url: '<?= base_url("index.php/api/retail/get_tat_aging"); ?>',
      type: 'POST',
      data: {
        cur_customer: cur_customer,
        cur_code: cur_code,
        cur_type: cur_type
      },
      success: function(response) {
        generate_chart($.parseJSON(response));
      }
    });

    function generate_chart(data) {
      var chart = AmCharts.makeChart("tat_aging_chart", {
        "type": "serial",
        "theme": "light",
        "dataProvider": data,
        "graphs": [{
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "fillColorsField": "color",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "type": "column",
          "valueField": "total",
          "showHandOnHover": true
        }],
        "colors": ["#12335E"],
        "categoryField": "aging",
        "categoryAxis": {
          "gridPosition": "start",
        },
        "chartCursor": {
          "fullWidth": true,
          "cursorAlpha": 0.1,
          "cursorColor": "#12335E",
          "color": "#ffffff",
          "cursorPosition": "middle"
        //   "listeners": [{
        //     "event": "changed",
        //     "method": function(ev) {
        //       // Log last cursor position
        //       ev.chart.lastCursorPosition = ev.index;
        //     }
        //   }]
        },
        "listeners": [{
          "event": "clickGraphItem",
          "method": function(ev) {
            // Set a jQuery click event on chart area
            var cur_customer = $.trim('<?=$cur_customer?>');
            var cur_code = cur_customer == 'all' ? '' : $.trim($('[name=cur_code]').val());

            window.location = '<?=base_url('index.php')?>' + '/retail/finance_range/'+cur_customer+'/'+ev.item.dataContext.code_range+'/'+cur_code;

            // console.log(ev);
            // console.log(ev.item.category);
            // console.log(ev.item.values.value);
            // jQuery(ev.chart.chartDiv).on("click", function(e) {
            //   // Check if last cursor position is present
            //   if (!isNaN(ev.chart.lastCursorPosition)) {
            //     // console.log("clicked on " + ev.chart.dataProvider[ev.chart.lastCursorPosition].country);
            //   }
            // })
          }
        }]
      });
    };
  });

</script>