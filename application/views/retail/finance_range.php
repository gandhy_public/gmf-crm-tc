<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
  
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title"><?=$customer_name?> (<?=$finance_range?>)</h3>
      <div class="box-tools pull-left">
        <a href="<?=base_url('index.php/retail/finance2')?>" class="btn btn-sm btn-default">Back To Dashboard</a>
      </div>
    </div>
    <div class="box-body">
      <table id="tbFinance" class="table table-bordered table-hover table-striped" width="100%">
         <thead style="background-color: #3c8dbc; color:#ffffff;">
           <tr>
             <th style="width: 50px; text-align: center;">No</th>
             <?php if(!empty($cur_customer)) { ?>
             <th style="text-align: center;">Customer</th>
             <?php } ?>
             <th style="text-align: center;">Document Number</th>
             <th style="text-align: center;">Amount</th>
             <th style="text-align: center;">Currency</th>
             <th style="text-align: center;">Status Billing</th>
           </tr>
         </thead>
         <tbody>
            <?php foreach($finance_data as $k => $data) { ?> 
              <tr>
                <td><center><?=($k+1)?></center></td>
                <?php if(!empty($cur_customer)) { ?>
                <td><?=$data->COMPANY_NAME?></td>
                <?php } ?>
                <td><?=$data->VBELN?></td>
                <td style="text-align: right;"><?=number_format((float) $data->NETWR,2,',','.');?></td>
                <td><center><?=$data->WAERK?></center></td>
                <td><center><?=$data->STATUS_BILLING?></center></td>
              </tr>
            <?php } ?>
         </tbody>
       </table>
    </div>
  </div>

</section>

<script>
  $(document).ready(function(){
    /*$('.modul_menu').select2({
      placeholder: "Select a menu"
    });*/
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function () {

    $('#tbFinance').DataTable();  
  });
</script>
