<section class="content-header">
  	<h1>
      	<?php echo $title; ?>
        <small></small> <!-- <span class="badge bg-<?php if($_GET['status']=='Delivered') { echo 'green'; } elseif($_GET['status']=='Quotation Provided Awaiting Approval') { echo 'aqua'; } elseif($_GET['status']=='Approval Received Repair Started') { echo 'red'; } elseif($_GET['status']=='Ready to Deliver') { echo 'yellow'; } elseif($_GET['status']=='Finished') { echo 'green'; } ?>"> <?php echo $_GET['status']; ?> </span> -->
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
		                    <div class="col-md-12">
		                        <h3 class="box-title">Status : <span class="badge bg-orange"> <?php echo $_GET['status']; ?> </span></h3>
		                    </div>
		                    <!-- <div class="col-md-2 pull-right">
		                        <div class="input-group" style="width: 150px;">
		                            <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                            <div class="input-group-btn">
		                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		                            </div>
		                        </div>
		                    </div> -->
		                </div>
		                <br>
		                <div class="box-body table-responsive no-padding">
		                    <table id="tbList" class="table table-bordered table-hover table-striped">
		                        <thead  style="background-color: #3c8dbc; color:#ffffff;">
		                            <tr>
		                            	<th><center>Act</center></th>
		                                <th style="width: 40px">NO</th>
		                                <th>SALES ORDER</th>
		                                <th>MO</th>
		                                <th>PURCHASE ORDER</th>
		                                <th>PART NUMBER</th>
		                                <th>PART NAME</th>
		                                <th>SERIAL NUMBER</th>
		                                <th>RECEIVED DATE</th>
		                                <th>QUOTATION DATE</th>
		                                <th>APPROVAL DATE</th>
		                                <th>TAT</th>
		                                <th>STATUS</th>
		                                <th>DELIVERY DATE</th>
		                                <th>REMARKS</th>
		                                <!-- <th><center>Act</center></th> -->
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php
		                                if($data_table) { 
		                                    $no = 0;
  		                                    foreach ($data_table as $rows) {
  		                                    $no++; 
  		                                    if ($rows['TXT_STAT'] == $_GET['status'] ){ ?>
		                            <tr>
		                            	<td>
			                            	<div class="btn-group">
	                                          <!-- <a href="<?php //echo base_url('index.php/Retail/order_detail2').'/'.$rows['MAINTENANCE_ORDER']?>"><button class="btn btn-primary btn-xs"><i class="fa fa-file-text"></i></button></a> -->
	                                          <a onclick="detailBtn('<?php echo base_url('index.php/Retail/order_detail2').'?order='.$rows['MAINTENANCE_ORDER'].'&status='.$rows['TXT_STAT']?>')"><button class="btn btn-success btn-xs"><i class="fa fa-file-text"></i></button></a></div>
	                                      	</div>
                                      	</td>
		                                <td><?php echo $no; ?></td>
		                                <td><?php echo ltrim($rows['SALES_ORDER'], '0'); ?></td>
		                                <td><?php echo $rows['MAINTENANCE_ORDER']; ?></td>
		                                <td><?php echo $rows['PURCHASE_ORDER']; ?></td>
		                                <td><?php echo $rows['PART_NUMBER']; ?></td>
		                                <td><?php echo $rows['PART_NAME']; ?></td>
		                                <td><?php echo $rows['SERIAL_NUMBER']?></td>       
		                                <td>
		                                	<?= $rows['RECEIVED_DATE'];	?>
		                               	</td>
		                                <td>
		                                	<?= $rows['QUOTATION_DATE']; ?>
		                              	</td>
		                                <td>
		                                	<?= $rows['APPROVAL_DATE']; ?>
		                                </td>
		                                <td id="color_tat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" 
		                                    value="<?php echo $rows['TAT']; ?>"/><?php echo $rows['TAT']; ?> Days</td>
		                                <td><?php echo $rows['TXT_STAT']?></td>
		                                <td>
		                                	<?php echo $rows['DELIVERY_DATE'];?>
		                                </td>
		                                <td><?php echo $rows['REMARKS']?></td>
		                                <!-- <td>
		                                  <div class="btn-group">
		                                    <a href="<?php echo base_url('index.php/Retail/order_detail').'/'.$rows['ID_RETAIL'] ?>" onclick="return confirm('Are You Sure Want To Show This Order Detail ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-file-text"></i></button></a>
		                                    <a href="#ModalView" data-toggle="modal"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button></a>
		                                    <a href="#ModalViewLog" data-toggle="modal"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-tasks"></i></button></a>
		                                    <a href="<?php //echo base_url('Working_order/edit_in_progress_wo'); //echo base_url('Working_order/edit_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Edit This New Working Order ???')"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button></a>
		                                    <a href="<?php //echo base_url('Working_order/cancel_new_wo').'/'.$rows->idrequest ?>" onclick="return confirm('Are You Sure Want To Cancel This New Working Order ???')"><button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button></a>
		                                  </div>
		                                </td> -->
		                            </tr>
		                            <?php } } }?>
		                        </tbody>
		                    </table>
		                    <br>
		                   <!--  <div class="col-md-3">
		                        <b><?php if($total_rows) { echo '<p class="text-yellow">'.$total_rows.' Total Data Records </p>'; } else { echo '<p class="text-yellow"> No Data Records </p>'; } ?></b>
		                    </div>
		                    <div class="col-md-6">
		                        <?php echo $paging; ?>
		                    </div>
		                    <div class="col-md-3 pull-right">
		                        <a href="<?php //echo base_url('Working_order/download_new_wo').'/'.$type.'/'.$key.'/'.$start.'/'.$end; ?>" taget=_blank><button class="btn btn-block btn-success" ><i class="fa fa-download"></i>  Download Data <?php echo $title; ?></button></a>
		                    </div> -->

		                </div>
	                  <br>

					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script>
	var tblo;
    $(document).ready(function() {
      tblo = $('#tbList').dataTable( {
      	dom: 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        columnDefs: [
            {
                "targets": [ 11 ],
                "visible": true,
                "searchable": true
            }
        ]
      } );
      //$("#iduserrole").select2({ width: 'resolve' });
      $(".select2").select2();

  });

  function detailBtn(url){

      alertify.confirm("View the order detail ?",
        function(){
          window.location = url;
        });
    }
</script>