<section class="content-header">
  <div class="left">
    <h1>SEARCH by RO (DASHBOARD)</h1>
  </div>
  <?php $this->load->view($link_directory); ?>
</section>
<section class="content custom">
  <div class="row">
    <div class="col col-md-12">
      <div class="card">
        <div class="card-header">
          <h2 class="title">Search</h2>
        </div>
        <div class="card-body">
          <form action="<?= base_url()?>index.php/Retail/search_dashboard" method="post">
          <div class="filter-input row" style="margin-bottom: 0">
            <div class="col col-md-3">
              
            </div>
            <div class="col col-md-5">
              <div class="form-line">
                <input type="text" class="form-control input-sm" id="search" name="search" required placeholder="Input RO Number" <?= ($key != "" ? "value='$key'" : "")?>>
              </div>
            </div>
            <div class="col col-md-2">
              <div class="form-line">
                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-refresh"></i> SEARCH</button>
              </div>
            </div>
            <div class="col col-md-2">
              
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div style="position: relative">
    <div id="empty_result" style="font-size: 24px;text-align: center;padding-top: 20px;display:none">No data to display</div>
    <div id="result" class="row" style="margin-bottom:0;display:none">
      <div class="col col-md-12">
        <div class="row" id="component_status" style="margin-left: -5px;margin-right: -5px;">
          <div class="box">
           <div class="box-body">
             <table id="tbGroup" class="table table-bordered table-hover table-striped" width="100%">
               <thead style="background-color: #3c8dbc; color:#ffffff;">
                 <tr>
                   <th><center> No </center></th>
                   <th><center> RO Number </center></th>
                   <th><center> Part Number </center></th>
                   <th><center> Serial Number </center></th>
                   <th><center> Status </center></th>
                   <th><center> Estimate Date </center></th>
                 </tr>
               </thead>
               <tbody>
                  <?php $no=1;foreach($data as $key){$date = DateTime::createFromFormat('dmY', $key['GLTRP']);?>
                    <tr>
                      <td><?= $no++?></td>
                      <td><?= $key['BSTNK']?></td>
                      <td><?= $key['PART_NUMBER']?></td>
                      <td><?= $key['SERIAL_NUMBER']?></td>
                      <td><?= $key['TXT_STAT']?></td>
                      <td><?= ($date ? $date->format('Y-m-d') : '0000-00-00')?></td>
                    </tr>
                  <?php }?>
               </tbody>
             </table>
           </div>
         </div>
        </div>
      </div>
    </div>
    <div class="overlay-custom">
      <div id='loader' style='text-align: center;margin-top: 50px;text-align:center'><img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg' class="center"></div>
    </div>
  </div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>

<script>
  $(document).ready(function(){
    <?php if(count($data) > 0){ ?>
      $('#result').show();
    <?php }else{?>
      $('#empty_result').show();
    <?php }?>
  });
</script>