<style>
div.container4 {
    height: 10em;
    position: relative }
div.container4 p {
    margin: 0;
    background: yellow;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%)
     }
img.center {
  display: block;
   margin-left: auto;
   margin-right: auto;
 }

#chartdiv {
  width: 100%;
  height: 400px;
  font-size: 11px;
}

.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
} 
</style>
<section class="content-header">
    <h1>
      <!--<?php //echo strtoupper($title) ?>-->
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">

       <!--  <?php $this->load->view($nav_tabs); ?> -->

        <div class="tab-content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-default">
                <!-- <div class="box box-success"> -->
                <!-- <div class="box-header with-border"> -->
                <!-- <div class="box box-widget widget-user"> -->
            <!-- Add the bg color to the header using any of the bg-* classes -->
				<!-- <div class="widget-user-header bg-black" style="background: url(<?php echo base_url(); ?>assets/dist/img/GMF.jpg) center center;">
                    <h3 class="widget-user-username">Order Progress</h3>
                    <h5 class="widget-user-desc"></h5>
                  </div>
                  <div class="widget-user-image">
                    <img class="img-circle" src="<?php echo base_url(); ?>assets/dist/img/gmf_logo.jpg" alt="User Avatar">
                  </div> -->
                <!-- </div> -->
				<!-- </div> -->
              <div class="box-header with-border">
                <div class="col-md-12">
                  <div class="col-md-3"> 
                    <div class="col-md-4 form-control-label">
                      <label>Filter By</label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="form-line">
                                <select id="filter_by" name="filter_by" class="form-control">
                                  <option value="create_date">Create Date</option>
                                  <option value="close_date" selected>Complete Date</option>
                                </select>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-3">               
                    <?php if($this->session->userdata('log_sess_id_user_role')!=3){ ?>
                    <div class="col-md-4 form-control-label">
                      <label>Customer</label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="form-line">
                              <select id="customer" class="form-control">
                                  <option value="all" >All Customer</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                  </div>
                  <div class="col-md-3"> 
                    <div class="col-md-4 form-control-label">
                      <label>Month</label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                          <div class="form-line">
                              <select id="period_month" name="period_month" class="form-control" placeholder="- choose period -">
                                <option value="all">All Month</option>
                                <option value="01" <?php echo ($bulan == '01') ? 'selected' : '';?>>January</option>
                                <option value="02" <?php echo ($bulan == '02') ? 'selected' : '';?>>February</option>
                                <option value="03" <?php echo ($bulan == '03') ? 'selected' : '';?>>March</option>
                                <option value="04" <?php echo ($bulan == '04') ? 'selected' : '';?>>April</option>
                                <option value="05" <?php echo ($bulan == '05') ? 'selected' : '';?>>May</option>
                                <option value="06" <?php echo ($bulan == '06') ? 'selected' : '';?>>June</option>
                                <option value="07" <?php echo ($bulan == '07') ? 'selected' : '';?>>July</option>
                                <option value="08" <?php echo ($bulan == '08') ? 'selected' : '';?>>August</option>
                                <option value="09" <?php echo ($bulan == '09') ? 'selected' : '';?>>September</option>
                                <option value="10" <?php echo ($bulan == '10') ? 'selected' : '';?>>October</option>
                                <option value="11" <?php echo ($bulan == '11') ? 'selected' : '';?>>November</option>
                                <option value="12" <?php echo ($bulan == '12') ? 'selected' : '';?>>December</option>
                              </select>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-3"> 
                    <div class="col-md-4 form-control-label">
                      <label>Year</label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="form-line">
                                <select id="period_year" name="period_year" class="form-control" placeholder="- choose period -">
									<?php
									$thn_skr = date('Y');
									for ($x = $thn_skr; $x >= 2014; $x--) {
									?>
										<option value="<?php echo $x; ?>" <?php echo ($tahun == $x) ? 'selected' : ''; ?>><?php echo $x ?></option>
									<?php
									}
									?>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
                <div class="box-body">
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="box">
                        <div class="box-header with-border">
                          <h3 class="box-title">STATUS ORDER</h3>

                         <!--  <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div> -->
                        </div>
                        <div class="box-body">
                            <div id='loader' class="container4" style='display: none;'>
                              <img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg' class="center">
                            </div>
                           <div id="chartdiv"></div>
                        </div>
                        <!-- <div style="display: none;" class="loading-mro" id="loader1" style=""></div> -->
                      </div>
                    </div>
                   <!--  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                      
                    </div> -->
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-lg-5 col-xs-12">
                      <div class="box-body no-padding">
                        <table class="table table-responsive table-bordered table-hover table-striped" id="list_status">
                          <thead style="background-color: #3c8dbc; color:#ffffff;">
                            <tr>
                              <th style="width: 150px"><center>Component Status</center></th>
                              <th style="width: 150px"><center>Count Of Status</center></th>
                              <th><center>Act</center></th>
                            </tr>
                          </thead>
                          <tbody>
                            <!-- <?php
                              if (isset($listStatus)) {
                                foreach ($listStatus as $key => $value) {
                                  if ($value->JUMLAH != 0){
                                  ?>
                                  <tr>
                                   <td><?= $value->STATUS ?></td>
                                   <td><?= $value->JUMLAH ?></td>
                                   <td class="align-center"><a href="<?php echo base_url() ?>index.php/Retail/order_status?status=<?= $value->STATUS ?>" style="" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-right"></i></a></td>
                                 </tr>
                                  <?php
                                  }
                                }
                              }
                             ?> -->
                          </tbody>
                          <tfoot>
                            <tr>
                              <td style="background: #ccfff5 "><b>All List Order</b></td>
                              <td style="background: #ccfff5" id="total"><b><?php echo $sum_grand_total; ?></b></td>
                              <td style="background: #ccfff5"></td>
                            </tr>
                          </tfoot>
                        </table>
                       </div>
                      </div>
                    <div class="col-lg-7 col-xs-12">
                      <div class="box">
                        <div class="box-header with-border">
                          <h3 class="box-title">TAT ACHIEVEMENT</h3>

                          <!-- <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div> -->
                        </div>
                        <div class="box-body">
                          <div class="chart">
                               <div id="chartdiv2" style="width: 100%; height: 350px; background-color: #FFFFFF;" ></div>
                          </div>
                        </div>
                      </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
 </section>

<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/pie.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<!-- <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> -->


<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    var tahun = $('#period_year').val();
    var kunnr = $('#customer').val();
    var bulan = $('#period_month').val();
    var type  = $('#filter_by').val();
    var data_table = <?= json_encode($listStatus) ?>;
    var datatable = $('#list_status').DataTable({
        // dom: 'Blfrtip',
        data : data_table,
        paging : false,
        searching : false,
        columns: [
          { data: 'STATUS' },
          { data: 'JUMLAH' },
          // { data: '' }
        ],
        columnDefs: [
        {
          "targets": [2],
          "className": "dt-center",
          "render": function ( data, type, full) {
                return '<button title="Detail" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-arrow-circle-right"></i></button>';
         }
        }],
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            var kunnr = $('#customer').val();
            $( api.column( 1 ).footer() ).html(total);
            $( api.column( 2 ).footer() ).html('<a href="<?php echo base_url() ?>index.php/Retail/list_order?tahun='+tahun+'&customer='+kunnr+'&bulan='+bulan+'&filterby='+type+'" style="" class="btn btn-primary btn-xs"><i class="fa fa-arrow-circle-right"></i></a>');
        }

      });

    $('#customer').select2({
            width: '100%',
            minimumInputLength: 2,
            triggerChange: true,
            allowClear: true,
            data: [{id: '', text: 'All Customer'}],
            ajax: {
                url: "<?=base_url()?>index.php/api/Retail/select_customer?tahun="+tahun,
                // minimumInputLength: 5,
                dataType: 'json',
                delay: 450,
                processResults: function (data) {
                    var response = data.body;
                    var cust = [{id: 'all', text: 'All Customer'}];
                    // var mdr = {id: 1, text: 'MDR', children: []};

                    $.each(response, function (key, val) {
                      cust.push({id: val.KUNNR, text: val.NAME1});
                      // cust.concat([{id: val.KUNNR, text: val.NAME1}]);
                    });
                    // console.log(cust);
                    return {
                        results: cust
                    }
                },
                cache: true
            }
        });

    $('#list_status').on( 'click', 'tbody tr .btUpdate', function (e) {
        var data = datatable.row( $(this).parents('tr') ).data();
        var status = data.STATUS;
        var tahun = $('#period_year').val();
        var kunnr = $('#customer').val();
        var bulan = $('#period_month').val();
        var type  = $('#filter_by').val();
        console.log(kunnr);

          e.preventDefault();
         $(this).animate({
             opacity: 0 //Put some CSS animation here
         }, 500);
         setTimeout(function(){
           // OK, finished jQuery staff, let's go redirect
           window.location.href = "order_status?status="+status+"&tahun="+tahun+"&customer="+kunnr+"&bulan="+bulan+"&filterby="+type;
         },500);
    });

    function updateDashboard(tahun, kunnr, bulan, type){
      $.ajax({
        url: "<?=base_url()?>index.php/Retail/change_dashboard?tahun="+tahun+"&kunnr="+kunnr+"&bulan="+bulan+"&filterby="+type, //memanggil function controller dari url
        type: "POST", 
        beforeSend: function(){
          // Show image container
          $("#loader").show();
          $("#chartdiv").hide();
          $("#chartdiv2").hide();
          $("#list_status").hide();
          
         },
        success: function(data) {
          data = $.parseJSON(data);
          // console.log(data);
          datatable.clear().draw();
          datatable.rows.add(data.listStatus); // Add new data
          datatable.columns.adjust().draw();
          $('#customer').val();
          chart.dataProvider = data.provider;
          chart.validateData();
          chart2.dataProvider = [
                                {
                                  "category": "> 20 days",
                                  "column-1": data.chart[0],
                                  "column-2": data.chart[3]
                                },
                                {
                                  "category": "> 9 days < 21 days",
                                  "column-1": data.chart[1],
                                  "column-2": data.chart[3]
                                },
                                {
                                  "category": "< 10 days",
                                  "column-1": data.chart[2],
                                  "column-2": data.chart[3]
                                }
                              ];
          chart2.validateData();
        },
        complete:function(data){
          // Hide image container
          $("#loader").hide();
          $("#chartdiv").show();
          $("#chartdiv2").show();
          $("#list_status").show();
         }
      });
    }

    $('#customer').change(function () {
      var tahun = $('#period_year').val();
      var kunnr = $('#customer').val();
      var bulan = $('#period_month').val();
      var type  = $('#filter_by').val();
      // console
      updateDashboard(tahun, kunnr, bulan, type);
    });

    $('#period_month').change(function () {
      var tahun = $('#period_year').val();
      var kunnr = $('#customer').val();
      var bulan = $('#period_month').val();
      var type  = $('#filter_by').val();
      // console
      updateDashboard(tahun, kunnr, bulan, type);
    });

    $('#period_year').change(function () {
      var tahun = $('#period_year').val();
      var kunnr = $('#customer').val();
      var bulan = $('#period_month').val();
      var type  = $('#filter_by').val();
      // console
      updateDashboard(tahun, kunnr, bulan, type);
    });

    $('#filter_by').change(function () {
      var tahun = $('#period_year').val();
      var kunnr = $('#customer').val();
      var bulan = $('#period_month').val();
      var type  = $('#filter_by').val();
      // console
      updateDashboard(tahun, kunnr, bulan, type);
    });

    var chart2 = AmCharts.makeChart("chartdiv2",
    {
      "type": "serial",
      "categoryField": "category",
      // "angle": 30,
      // "depth3D": 30,
      "colors": [
        "#00e673",
        "#ccffe6",
        
      ],
      "startDuration": 1,
      "categoryAxis": {
        "gridPosition": "start"
      },
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[title]] of [[category]]:[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "precision": 0,
          "title": "Process",
          "type": "column",
          "valueField": "column-1"
        },
        {
          "balloonText": "[[title]] of [[category]]:[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-2",
          "title": "Max.Limit 100%",
          "type": "column",
          "valueField": "column-2"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "stackType": "100%",
          "title": "",
		  "unit": "%",
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {
        "enabled": true,
        "useGraphSettings": true
      },
      "titles": [],
      "dataProvider": [
        {
          "category": "> 20 days",
          "column-1": "<?= $chart[0] ?>",
          "column-2": "<?= $chart[3] ?>"
        },
        {
          "category": "> 9 days < 21 days",
          "column-1": "<?= $chart[1] ?>",
          "column-2": "<?= $chart[3] ?>"
        },
        {
          "category": "< 10 days",
          "column-1": "<?= $chart[2] ?>",
          "column-2": "<?= $chart[3] ?>"
        }
      ]
    });

    var dataProvider = <?= $provider ?>;
    var chart = AmCharts.makeChart( "chartdiv", {
      "type": "pie",
      "startDuration": 0,
      "theme": "light",
      "addClassNames": true,
      "legend":{
        "position":"right",
        "marginRight":100,
        "autoMargins":false
      },
      "innerRadius": "30%",
      "defs": {
        "filter": [{
          "id": "shadow",
          "width": "200%",
          "height": "200%",
          "feOffset": {
            "result": "offOut",
            "in": "SourceAlpha",
            "dx": 0,
            "dy": 0
          },
          "feGaussianBlur": {
            "result": "blurOut",
            "in": "offOut",
            "stdDeviation": 5
          },
          "feBlend": {
            "in": "SourceGraphic",
            "in2": "blurOut",
            "mode": "normal"
          }
        }]
      },
      "dataProvider": dataProvider,
      "valueField": "JUMLAH",
      "titleField": "STATUS",
      // "labelsEnabled": false,
      // "pullOutRadius": 0,
      // "outlineAlpha": 0.4,
      // "depth3D": 15,
      "colors": [
                "#9dddfb",
                "#8fd693",
                "#fbbbb6",
                "#ffcc80",
                "#ff3399",
                "#d2a679",
                "#4d79ff",
                "#8a8a5c",
                "#66ff99",
                "#ffb3d9",
                "#004d99"
              ],
      "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      // "angle": 20,
      "export": {
      "enabled": true
      }
    });
    chart.addListener("init", handleInit);

    chart.addListener("rollOverSlice", function(e) {
      handleRollOver(e);
    });

    function handleInit(){
      chart.legend.addListener("rollOverItem", handleRollOver);
    }

    function handleRollOver(e){
      var wedge = e.dataItem.wedge.node;
      wedge.parentNode.appendChild(wedge);
    }
  });

 
</script>

<!-- <script type="text/javascript">
      AmCharts.makeChart("chartdiv2",
        {
          "type": "serial",
          "categoryField": "category",
          "angle": 30,
          "depth3D": 30,
          "colors": [
            "#1a75ff",
            "#80b3ff",
          ],
          "startDuration": 1,
          "categoryAxis": {
            "gridPosition": "start"
          },
          "trendLines": [],
          "graphs": [
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-1",
              "precision": 0,
              "title": "Process",
              "type": "column",
              "valueField": "column-1"
            },
            {
              "balloonText": "[[title]] of [[category]]:[[value]]",
              "fillAlphas": 1,
              "id": "AmGraph-2",
              "title": "Max.Limit 100%",
              "type": "column",
              "valueField": "column-2"
            }
          ],
          "guides": [],
          "valueAxes": [
            {
              "id": "ValueAxis-1",
              "stackType": "100%",
              "title": ""
            }
          ],
          "allLabels": [],
          "balloon": {},
          "legend": {
            "enabled": true,
            "useGraphSettings": true
          },
          "titles": [],
          "dataProvider": [
            {
              "category": "> 20 days",
              "column-1": "<?= $chart[0] ?>",
              "column-2": "<?= $chart[3] ?>"
            },
            {
              "category": "> 9 days < 21 days",
              "column-1": "<?= $chart[1] ?>",
              "column-2": "<?= $chart[3] ?>"
            },
            {
              "category": "< 10 days",
              "column-1": "<?= $chart[2] ?>",
              "column-2": "<?= $chart[3] ?>"
            }
          ]
        }
      );
    </script> -->
<!-- <script>
    // Load pertama kali
    $(document).ready(function() {
        // load_chart_year();
        load_chart_region();
        // load_chart_business_segments();
        // back_grafik_year();
    });

    function load_chart_region() {
        $.ajax({
                url: "<?= site_url('api/Retail/dashboard') ?>",
                // data: {
                //     'year_from': year_from,
                //     'year_to': year_to,
                //     'aircraft_type': aircraft_type,
                //     'engine_type_id': engine_type,
                //     'operator_id': operator,
                //     'tipe': 0
                // },
                type: 'POST',
                dataType: "JSON",
                // beforeSend: function() {
                //     $("#loader1").fadeIn(1000);
                // },
                error: function (jqXHR, textStatus, errorThrown){
                    // notif('error', "Sorry cannot load data, please check your connection");
                },
                success: function(data) {
                    var prov = (data.provider);
                    window.alert(prov);
                    // notif('success', 'Data region successfully displayed');
                    AmCharts.makeChart( "chartdiv", {
                              "type": "pie",
                              "theme": "light",
                              "dataProvider": data.provider,
                              "valueField": "JUMLAH",
                              "titleField": "STATUS",
                              // "labelsEnabled": false,
                              "autoMargins": false,
                              "marginTop": 0,
                              "marginBottom": 0,
                              "marginLeft": 0,
                              "marginRight": 0,
                              // "pullOutRadius": 0,
                              "outlineAlpha": 0.4,
                              "depth3D": 15,
                              "colors": [
                                        "#2E7D32",
                                        "#29B6F6",
                                        "#f44336",
                                        "#FFA726"
                                      ],
                              "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                              "angle": 20,
                              "export": {
                                "enabled": true
                              }
                    });
                }
            })
            .done(function() {
                // $("#loader1").fadeOut(1000, function() {
                    $("#chartdiv").fadeIn(1000);
                // });
            })
    }
</script> -->