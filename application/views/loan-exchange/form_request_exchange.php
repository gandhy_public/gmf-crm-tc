<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
    if ($this->session->flashdata('alert_failed')) { 
      echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
    } ?>
  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <div class="tab-content">

            <form action="<?php echo base_url() ?>index.php/Loan_exchange/insert_request_exchange" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3">
                    <div class="form-horizontal">
                      <p id="loading" style="text-align:center">Loading Get Data...</p>

                      <div class="form-group request">
                        <label class="col-sm-3 control-label">Part Number</label>
                        <div class="col-sm-9">
                          <input class="form-control input-sm " id="PART_NUMBER" name="PART_NUMBER" style="width: 100%;" required>                                                          
                        </div>
                      </div>
                      <div class="form-group available">
                        <label class="col-sm-3 control-label">Part Condition</label>
                        <div class="col-sm-9">
                          <select class="form-control input-sm select2" id="EXCHANGE_TYPE" name="EXCHANGE_TYPE" style="width: 100%;">
                            <option value="">Choose Exchange Type</option>
                            <option value="All Condition">All Condition</option>
                            <option value="SVC">SVC</option>
                            <option value="OHC">OHC</option>
                            <option value="NE">NE</option>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group available">
                        <label class="col-sm-3 control-label">Exchange Type</label>
                        <div class="col-sm-9">
                          <select class="form-control input-sm select2" id="EXCHANGE_RATE" name="EXCHANGE_RATE" style="width: 100%;">
                            <option value="">Choose Exchange Rate</option>
                            <option value="Flat Rate">Flat Rate</option>
                            <option value="Exchange Fee + Cost">Exchange Fee + Cost</option>
                            <option value="Outright">Outright</option>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group available">
                        <label class="col-sm-3 control-label">Contact Info</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control input-sm" id="CONTACT_INFO" name="CONTACT_INFO"> 
                        </div>
                      </div>

                    </div>
                  </div>                                        
                </div>
              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Request</button>
              </div><!-- /.box-footer -->           
            </form>
          
        </div>   
      </div>
    </div>
  </div>
</section>

<script>
  // function notifikasi() {
  //   $('.menu').html("");
  //   $.ajax({
  //     url: "<?= base_url()?>index.php/Loan_exchange/summary_loan_exchange",
  //     type: "POST",
  //     success: function (resp) { 
  //       resp = JSON.parse(resp);
  //       var HTML = '';
  //       var TOTAL = resp.length;
  //       $.each(resp, function (i, item) {

  //           if(item.JENIS == "loan"){
  //             var URL = "<?= base_url()?>index.php/Loan_exchange/loan_request";
  //             if(item.STATUS == "request"){
  //                 status = 'New Request Loan';
  //             }else if(item.STATUS == "process"){
  //                 status = 'Request Loan Waiting Process';
  //             }else{
  //                 status = 'Request Loan Available';
  //             }
  //           }else{
  //             var URL = "<?= base_url()?>index.php/Loan_exchange/exchange_request";
  //             if(item.STATUS == "request"){
  //                 status = 'New Request Exchange';
  //             }else if(item.STATUS == "process"){
  //                 status = 'Request Exchange Waiting Process';
  //             }else{
  //                 status = 'Request Exchange Available';
  //             }
  //           }

  //           HTML += 
  //               '<li>'+
  //                 '<a href="'+URL+'">'+
  //                   '<i class="fa fa-warning text-yellow"></i> <span>'+item.JUMLAH+'</span> '+status+''+
  //                 '</a>'+
  //               '</li>';
  //       });

  //       $('.menu').append(HTML);
  //       if(TOTAL == 0){
  //         $('.notif').hide();
  //       }else{
  //         $('.notif').show();
  //         $('.notif').text(TOTAL);
  //       }
  //     }
  //   });
  // }
  $(document).ready(function () {

    var status_form = "<?= $status_form?>";
    if(status_form == "request"){

      $('.request').show();
      $('.available').hide();
      $('#loading').hide();

    }else if(status_form == "available"){

      var id_exchange = "<?= $this->uri->segment(3)?>";
      $('#loading').show();

      $.ajax({
        url: "<?= base_url()?>index.php/Loan_exchange/detail_exchange",
        type: "POST",
        data: {
          id:id_exchange
        },
        success: function (resp) { 
          $('#loading').hide();
          resp = JSON.parse(resp);

          $('#PART_NUMBER').val(resp[0].PART_NUMBER).prop('readonly', true);

          $('.request').show();
          $('.available').show();

          $('#PART_NUMBER').prop("required",true);
          $('#EXCHANGE_TYPE').prop("required",true);
          $('#EXCHANGE_RATE').prop("required",true);
          $('#CONTACT_INFO').prop("required",true);
        }
      });

      $('.form-horizontal').on('submit', function(e){
        $('#insert').text("Loading...");
        e.preventDefault();
        $.ajax({
            url: "<?= base_url()?>index.php/Loan_exchange/finish_exchange/"+id_exchange,
            type: "POST",
            data: $(this).serialize(),
            success: function (resp) {              
              if(resp){
                  $.notify("Success Request", "success");
                  $('#insert').text("Request");
                  window.location.href = "<?= base_url()?>index.php/Loan_exchange/exchange_request";
              }else{
                  $.notify("Failed Request, please try again", "error");
                  $('#insert').text("Try Again");
              }
              // notifikasi();
            }
        });
      });

    }

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000);

    $(".select2").select2();

  });
</script>