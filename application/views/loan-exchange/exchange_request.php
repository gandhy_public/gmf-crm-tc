<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<section class="content">
	<?php 
    if ($this->session->flashdata('alert_success')) { 
      echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>';
    } ?>
	<div class="row">
		<div class="col-md-12">
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs2); ?>
				<div class="tab-content">
					<section class="content">
            <div class="box-body no-padding with-export-excel with-export-csv">
              <table id="requestexchange" class="table table-bordered table-hover table-striped" width="100%">
  	            <thead style="background-color: #3c8dbc; color:#ffffff;">
    	            <tr>
    	              <th>No</th>
    	              <th>Part Number</th>
    	              <th>Exchange Type</th>
    	              <th>Exchange Rate</th>
    	              <th>Contact Info</th>
                    <th>Information</th>
                    <th>Status</th>
                    <th>Action</th>
    	            </tr>
  	            </thead>
  	            <tbody>
  	            </tbody>
	           </table>
            </div> 
					</section>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal loan_exchange fade" id="modal_edit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/Loan_exchange/update_exchange')?>" id="form_edit">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Exchange</h3>
        </div>
        <div class="modal-body">
          <input type="hidden" name="id" id="id">
          <div class="form-group row">
            <label class="col-sm-4 control-label">Status Request</label>
            <div class="col-sm-8">
              <select class="form-control" id="status" name="status" style="width: 100%;" required>
                <option value="">--Select Status Request Loan--</option>
                <option value="request">REQUEST</option>
                <option value="available">AVAILABLE</option>
                <option value="not available">NOT AVAILABLE</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 control-label">Information Request</label>
            <div class="col-sm-8">
              <textarea style="min-height: 130px;" class="form-control input-sm" id="keterangan" name="keterangan" placeholder="Input information about this data request"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="btn_submit">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<form id="export-form" action="<?= base_url('index.php/export/LoanExchange/ExportExchangeToExcel'); ?>" method="POST">
  <input type="hidden" name="header">
  <input type="hidden" name="query">
</form>
<form id="export-formCSV" action="<?= base_url('index.php/export/LoanExchange/ExportExchangeToCSV'); ?>" method="POST">
  <input type="hidden" name="header">
  <input type="hidden" name="query">
</form>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000);

    $(".select2").select2();
    var table = $('#requestexchange').DataTable({
        processing: true,
        language: {
          processing: "<img src='<?php echo base_url(); ?>assets/dist/img/images/loader-64x/Double-Ring-72px.svg'>"
        },
        serverSide: true,
        paging: true,
        sScrollX: true,
        ajax: {
            url: "<?= base_url('index.php/Loan_exchange/get_loan_exchange/exchange'); ?>",
            type: "POST"
        },
        dom: 'Blfrtip',
        columns: [      
            {data: 'no', name: 'no', orderable: false},
            {data: 'part_number', name: 'part_number', orderable: true},
            {data: 'type', name: 'type', orderable: true},
            {data: 'rate', name: 'rate', orderable: true},
            {data: 'contact_info', name: 'contact_info', orderable: true},
            {data: 'information', name: 'information', orderable: true},
            {data: 'status', name: 'status', orderable: true},
            {data: 'action', name: 'action', orderable: false},
        ],
        buttons: []
    });

    table.on('click', '#btn_edit', function () {
        var id_exchange = $(this).data("x");

        $.ajax({
            url: "<?= base_url()?>index.php/Loan_exchange/detail_exchange",
            type: "POST",
            data: {
              id:id_exchange
            },
            success: function (resp) {              
              resp = JSON.parse(resp);

              $('#modal_edit').modal("show");
              $("#status").val(resp[0].STATUS).change();
              $("#keterangan").val(resp[0].KETERANGAN);
              $("#id").val(resp[0].ID_EXCHANGE_REQUEST);
            }
        });
    });

    table.on('click', '#btn_form', function () {
        var id_exchange = $(this).data("x");

        window.location.href = "<?= base_url()?>index.php/Loan_exchange/form_request_exchange/"+id_exchange;
    });

    table.on('click', '#btn_done', function () {
        var id_exchange = $(this).data("x");

        $.ajax({
            url: "<?= base_url()?>index.php/Loan_exchange/done_exchange/"+id_exchange,
            type: "POST",
            success: function (resp) {              
              if(resp){
                    $.notify("Success Update Data", "success");
                }else{
                    $.notify("Failed Update Data", "error");
                }
                table.ajax.reload();
                // notifikasi();
            }
        });
    });

     table.button().add(0, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#requestexchange thead tr th").not(':last').map(function() { 
         header.push($.trim($(this).text()));
        }).get();
      
        $('#export-form [name=header]').val(header);
        // $('#export-form [name=query]').val(queryExchange);
        
        setTimeout(function() {
          $('#export-form').submit();
        }, 1000);
      },
      text: 'Export to Excel',
      className: 'btn btn-success btn-sm'
    });
     table.button().add(1, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#requestexchange thead tr th").not(':last').map(function() { 
          header.push($.trim($(this).text()));
        }).get();

        $('#export-formCSV [name=header]').val(header);
        // $('#export-formCSV [name=query]').val(queryExchange);
        
        setTimeout(function() {
          $('#export-formCSV').submit();
        }, 1000);
      },
      text: 'Export to CSV',
      className: 'btn btn-info btn-sm'
    });

    $('#form_edit').on('submit', function(e){
      $('#btn_submit').text("Loading...");
      e.preventDefault();
      $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: $(this).serialize(),
            success: function (resp) {              
              if(resp){
                    $('#modal_edit').modal("hide");
                    $.notify("Success Update Data", "success");
                    table.ajax.reload();
                    $('#btn_submit').text("Save changes");
                }else{
                    $('#modal_edit').modal("hide");
                    $.notify("Failed Update Data", "error");
                    table.ajax.reload();
                    $('#btn_submit').text("Save changes");
                }
                // notifikasi();
            }
        });
    });


  });
</script>