<?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                     <!--    <li <?php if($this->uri->segment(1)=='Home_control') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Dashboard">
                                <i class="fa fa-home"></i> <span>Dashboard</span>
                            </a>
                        </li> -->

                        <li <?php if($this->uri->segment(1)=='Users') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Users">
                                <i class="fa fa-users"></i> <span> Users</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=='customers') { echo 'class="active"'; } ?>>
                        	<a href="<?php echo base_url() ?>index.php/Customers"> 
                        		<i class="fa fa-users"></i><span> Customers</span>
                        	</a>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==1)) { ?>
                        <li class="treeview <?php if(($this->uri->segment(1)=='Pooling') or ($this->uri->segment(1)=='Contract'))  { echo 'active"'; } ?>">
                            <!-- <a href="<?php echo base_url() ?>index.php/Pooling">
                                <i class="fa fa-object-ungroup"></i> <span>Pooling PBTH</span> -->
                            <!-- </a> -->
                              <a href="#">
                                <i class="fa fa-object-ungroup"></i> <span>Pooling</span>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                              </a>
                            <ul class="treeview-menu">
        					            <li>
        					            	<a href="<?php echo base_url(); ?>index.php/Pooling/list_order"> 
        					            	  <i class="fa fa-list"></i>
                                  <span>List Order</span>
                                  
                                </a>
        					            </li>
						          <?php if ($this->session->userdata('log_sess_id_user_role')==3) {
						          ?>
						          <li>
                                    <a href="<?php echo base_url(); ?>index.php/Pooling/create_order">
                                    <i class="fa fa-edit"></i>
                                    <span>Create Order</span></a>
                                  </li>
						        <?php } else { ?>
						          <li><a href="<?php echo base_url(); ?>index.php/Contract"> 
                                    <i class="fa fa-pencil"></i>
                                    <span>Create Contract</span></a></li>
						        
						        <li><a href="<?php echo base_url(); ?>index.php/Contract/list_contract">
                                    <i class="fa fa-list"></i>
                                    <span>List Contract</span></a></li>
						        <li class="treeview">
						            <a href="#"> 
                                        <i class="fa fa-circle-o"></i>Configuration
								        <span class="pull-right-container">
							              <i class="fa fa-angle-left pull-right"></i>
							            </span>	
							        </a>
						            <ul class="treeview-menu">
						              <!-- <li><a href="<?php echo base_url(); ?>index.php/master/Part">
                                        <i class="fa fa-cubes"></i>
                                        <span> Part Number </span></a></li>
						              <li><a href="<?php echo base_url(); ?>index.php/master/Aircraft">
                                        <i class="fa fa-plane"></i>
                                        <span> Aircraft Registration </span></a></li> -->
						              <li><a href="<?php echo base_url(); ?>index.php/master/ReqCategory">
                                        <i class="fa fa-sitemap"> </i>
                                        <span>Requirement Category</span></a></li>
						              <!-- <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/Users/list_user"> User</a></li> -->
						            </ul>
						          </li>
                                  <?php } ?>
                                   <li> <a href="<?php echo base_url(); ?>index.php/Pooling/report_serviceable_level/"> <i class="fa fa-bar-chart"></i>
                                    <span>Serviceable Level</span></a></li>
                                  <li> <a href="<?php echo base_url(); ?>index.php/Pooling/report_unserviceable_level/"> <i class="fa fa-bar-chart"></i>
                                    <span>Unserviceable Level</span></a></li>
					        </ul>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==2)) { ?>
                        <li class="treeview <?php if($this->uri->segment(1)=='Loan_exchange') { echo 'active"'; } ?>">
                            <a href="#">
                                <i class="fa fa-usd"></i> <span>Loan Exchange</span>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>     
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/Loan_exchange">
                                        <i class="fa fa-list"></i> <span> All Order </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/Loan_exchange/loan_request">
                                        <i class="fa fa-list"></i> <span> All Request </span>
                                    </a>
                                </li>
                                <li class="treeview">
                                    <a href="#"> 
                                        <i class="fa fa fa-edit"></i> Form Request
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span> 
                                    </a>
                                    <ul class="treeview-menu">
                                      <li><a href="<?php echo base_url(); ?>index.php/Loan_exchange/form_request_loan/"><i class="fa fa-circle-o"></i>
                                      <span> Form Request Loan </span></a></li>
                                      <li><a href="<?php echo base_url(); ?>index.php/Loan_exchange/form_request_exchange/"><i class="fa fa-circle-o"></i>
                                      <span> Form Request Exchange </span></a></li>
                                      <!-- <li role="presentation"><a class="fa fa-user" href="<?php echo base_url(); ?>index.php/Users/list_user"> User</a></li> -->
                                    </ul>
                                </li>
                            </ul>   
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==3)) { ?>
                        <li <?php if($this->uri->segment(1)=='Retail') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Retail">
                                <i class="fa fa-tags"></i> <span> Retail </span>
                            </a>
                        </li>
                        <?php } ?>

                        <?php if(($this->session->userdata('log_sess_id_user_role')==1 || $this->session->userdata('log_sess_id_user_role')==3) || ($this->session->userdata('log_sess_id_user_role')==2 && $this->session->userdata('log_sess_id_user_group')==4)) { ?>
                        <li <?php if($this->uri->segment(1)=='Landing_gear') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Landing_gear"><i class="fa fa-gears"></i> <span> Landing Gear</span>
                                <!--<i class="fa fa-angle-left pull-right"></i>-->
                            </a>
                            <!--<ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/Dashboard2">
                                        <i class="fa fa-folder-o"></i> <span> Project List</span>
                                    </a>
                                </li>
                            </ul>-->
                        </li>
                        <?php } ?>

                        <li <?php if($this->uri->segment(1)=='Csi_survey') { echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url() ?>index.php/Csi_survey">
                                <i class="fa fa-table"></i><span> CSI Survey </span>
                            </a>

                        </li>

















                            <li class="dropdown notifications-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <?php if($jumlah_order > 0){?>
                                <span class="label label-warning"><?= ($jumlah_order > 0 ? "1" : "0")?></span>
                                <?php }?>
                              </a>
                              <ul class="dropdown-menu">
                                <li class="header">You have <?= ($jumlah_order > 0 ? "1" : "0")?> notifications</li>
                                <li>
                                  <!-- inner menu: contains the actual data -->
                                  <?php if($jumlah_order > 0){?>
                                  <ul class="menu">
                                    <li>
                                      <a href="<?= base_url('index.php/Pooling/list_order?new=1')?>">
                                        <i class="fa fa-warning text-yellow"></i> Ada data order baru
                                        <small class="label pull-right bg-green"><?= $jumlah_order?> new data</small>
                                      </a>
                                    </li>
                                  </ul>
                                  <?php }?>
                                </li>
                              </ul>
                            </li>