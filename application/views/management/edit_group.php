<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <a href="<?= base_url('index.php')?>/management/menu"><button type="button" class="btn btn-primary pull-right">Back To List Group</button></a>
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#edit_group" aria-controls="home" role="tab" data-toggle="tab"><?= $title_tab?></a></li>    
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="edit_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/update_group"?>" method="post" id="form-edit-group">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <input type="hidden" id="group_id" name="group_id" value="<?= $this->uri->segment(3)?>"> 
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Group Name</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="group" name="group" required> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Status Group</label>
                                    <div class="col-sm-6">
                                      <select class="form-control input-sm" id="status" name="status" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose Status Group</option>
                                        <option value="1">Active</option>
                                        <option value="0">Non-Active</option>
                                      </select> 
                                    </div>
                                  </div>
                                  <?php
                                  $modul = array_keys($data_menu);
                                  for($a=0;$a<count($modul);$a++){
                                  ?>
                                  <hr>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Select Menu in Module <?= $modul[$a]?></label>
                                    <div class="col-sm-6"> 
                                      <?php
                                      for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                      ?>
                                      <input type="checkbox" class="<?=$modul[$a]?>" name="<?=$modul[$a]?>[]" value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>">&nbsp&nbsp<?= $data_menu[$modul[$a]][$b]['menu']?><br>
                                      <?php }?>


                                      <!-- <select class="js-example-placeholder-multiple js-states form-control modul_menu" name="<?=$modul[$a]?>[]" id="<?=$modul[$a]?>" multiple="multiple">
                                        <?php
                                        for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                        ?>
                                        <option value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>"><?= $data_menu[$modul[$a]][$b]['menu']?></option>
                                        <?php }?>
                                      </select> -->
                                    </div>
                                  </div>
                                  <?php }?>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right btn-submit">Update Menu Management</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<script>
  $(document).ready(function(){
    <?php if($view == "true"){ ?>
    $("#form-edit-group :input").prop("disabled", true);
    $(".btn-submit").hide();
    //$('#form-edit-group').find('input, textarea, button, select').attr('disabled','disabled');
    <?php }?>

    <?php foreach($master as $key){?>
    $("input:checkbox[value='<?= $key->menu_id."_".$key->modul_id?>']").attr("checked",true);
    <?php }?>
    $("#group").val('<?= $master[0]->name?>');
    $("#status").val('<?= $master[0]->status?>').change();
  });
  $(function () {
    $('.alert').delay(8000).fadeOut();
    //$(".alert").hide(15000);
  });
</script>
