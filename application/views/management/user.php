<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#list_user" aria-controls="profile" role="tab" data-toggle="tab">List User</a></li>
                     <li role="presentation" ><a href="#create_user" aria-controls="home" role="tab" data-toggle="tab">Create User</a></li>  
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="list_user">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbUser" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th><center> No </center></th>
                                 <th><center> Name </center></th>
                                 <th><center> Username </center></th>
                                 <th><center> Email </center></th>
                                 <th><center> User Role </center></th>
                                 <th><center> User Group </center></th>
                                 <th><center> Status </center></th>
                                 <th><center> Act </center></th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="create_user">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/create_user"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="nama" name="nama" required placeholder="Name"> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="username" name="username" required placeholder="Username">
                                      <input type="checkbox" id="ldap" name="ldap">&nbspusing LDAP ? 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-4">
                                      <input type="password" class="form-control input-sm" id="password" name="password" placeholder="Password"> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-4">
                                      <input type="email" class="form-control input-sm" id="email" name="email" required placeholder="E-mail"> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Role</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm" id="role" name="role" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose User Role</option>
                                        <?php 
                                          foreach ($role as $data) { 
                                        ?>
                                        <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                                        <?php } ?>
                                      </select>                                                                
                                    </div>
                                  </div>
                                  <div class="form-group select-customer">
                                    <label class="col-sm-2 control-label">Customer</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="customer" name="customer" style="width: 100%;">
                                        <option value="" disabled selected>Choose Customer</option>
                                        <?php 
                                          foreach ($customer as $data) { 
                                        ?>
                                        <option value="<?= $data->ID_CUSTOMER ?>"><?= $data->COMPANY_NAME; ?></option>
                                        <?php } ?>
                                      </select>         
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Group</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="group" name="group" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose User Group</option>
                                        <?php 
                                          foreach ($group as $data) { 
                                        ?>
                                        <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                                        <?php } ?>
                                      </select>                                                                
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create User</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>


<!-- Form Edit Modal -->
<div class="modal fade" id="modalEdit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/management/update_user')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit User</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id-edit" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" id="nama-edit" name="nama" required> 
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="col-sm-3">Username</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" id="username-edit" name="username" required> 
                </div>
              </div> -->
              <div class="form-group">
                <label class="col-sm-3">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control input-sm" id="email-edit" name="email" required> 
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="col-sm-3">User Role</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm" id="role-edit" name="role" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Role</option>
                    <?php 
                      foreach ($role as $data) { 
                    ?>
                    <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                    <?php } ?>
                  </select>                                                                
                </div>
              </div> -->
              <div class="form-group select-customer-edit">
                <label class="col-sm-3">Customer</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm select2" id="customer-edit" name="customer" style="width: 100%;">
                    <option value="" disabled selected>Choose Customer</option>
                    <?php 
                      foreach ($customer as $data) { 
                    ?>
                    <option value="<?= $data->ID_CUSTOMER ?>"><?= $data->COMPANY_NAME; ?></option>
                    <?php } ?>
                  </select>         
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">User Group</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm select2" id="group-edit" name="group" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Group</option>
                    <?php 
                      foreach ($group as $data) { 
                    ?>
                    <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                    <?php } ?>
                  </select>                                                                
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">Status User</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm" id="status-edit" name="status" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Status</option>
                    <option value="1">Active</option>
                    <option value="0">Non Active</option>
                  </select>                                                                
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Form Edit Password Modal -->
<div class="modal fade" id="modalPassword" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/management/update_password')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Password User</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id-password" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3">New Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control input-sm" id="password-reset" name="password" required> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">Confirm New Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control input-sm" id="repassword-reset" name="repassword" required> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update Password</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('#customer').select2();
    $('#group').select2();
    $('#customer-edit').select2();
    $('#group-edit').select2();
    $('.select-customer').hide();
    $('[data-toggle="tooltip"]').tooltip();  
  });
  $(function () {
    $('.alert').delay(8000).fadeOut();
    //$(".alert").hide(15000);

    $('#tbUser').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/management/list_user')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'role'},
            {data: 'group', name: 'group'},
            {data: 'status', name: 'status'},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

    $('#role').on('change', function() {
      var role = $('#role').val();
      if(role == 4){
        $('.select-customer').show();
      }else{
        $('.select-customer').hide();
      }
    })
    $('#role-edit').on('change', function() {
      var role = $('#role-edit').val();
      if(role == 4){
        $('.select-customer-edit').show();
      }else{
        $('.select-customer-edit').hide();
      }
    })
    $("#ldap").change(function() {
      if(this.checked) {
        $('#password').prop('disabled', true);
      }else{
        $('#password').prop('disabled', false);
      }
    });

    $("#tbUser").on("click",".btn_edit", function(){
        var id = $(this).data('x');

        var span = document.createElement("span");
        span.innerHTML = "Anda yakin akan mengedit data user ini?";

        $('#modalEdit').modal('hide');
        $.ajax({
          type: 'POST',
          url: '<?=base_url('index.php/management/detail_edit_user/')?>',
          data: { 
              'id': id
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              if(obj[0].role_id != 4){
                $('.select-customer-edit').hide();
              }else{
                $('.select-customer-edit').show();
              }

              $('#id-edit').val(id);
              $('#nama-edit').val(obj[0].nama);
              //$("#username-edit").val(obj[0].username);
              $('#email-edit').val(obj[0].email);
              //$('#role-edit').val(obj[0].role_id).change();
              $('#group-edit').val(obj[0].group_id).change();
              $('#customer-edit').val(obj[0].customer_id).change();
              $('#status-edit').val(obj[0].is_active).change();

          }
        });
        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $('#modalEdit').modal('show');
            }else{
              swal("Data tidak diedit!");
            }
        });
    });

    $("#tbUser").on("click",".btn_reset", function(){
        var id = $(this).data('x');

        var span = document.createElement("span");
        span.innerHTML = "Anda yakin akan mereset password user ini?";

        $('#modalPassword').modal('hide');
        $('#id-password').val(id);
        
        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $('#modalPassword').modal('show');
            }else{
              swal("Password tidak direset!");
            }
        });
    });

  });
</script>
