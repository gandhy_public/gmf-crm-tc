<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#list_group" aria-controls="profile" role="tab" data-toggle="tab">List Group</a></li>
                    <li role="presentation" ><a href="#create_group" aria-controls="home" role="tab" data-toggle="tab">Create Group</a></li>    
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="list_group">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbGroup" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th><center> No </center></th>
                                 <th><center> Group Name </center></th>
                                 <th><center> Status </center></th>
                                 <th><center> Act </center></th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="create_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/create_group"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Group Name</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="group" name="group" required> 
                                    </div>
                                  </div>
                                  <?php
                                  $modul = array_keys($data_menu);
                                  for($a=0;$a<count($modul);$a++){
                                  ?>
                                  <hr>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Select Menu in Module <?= $modul[$a]?></label>
                                    <div class="col-sm-6"> 
                                      <?php
                                      for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                      ?>
                                      <input type="checkbox" name="<?=$modul[$a]?>[]" value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>">&nbsp&nbsp<?= $data_menu[$modul[$a]][$b]['menu']?><br>
                                      <?php }?>


                                      <!-- <select class="js-example-placeholder-multiple js-states form-control modul_menu" name="<?=$modul[$a]?>[]" id="<?=$modul[$a]?>" multiple="multiple">
                                        <?php
                                        for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                        ?>
                                        <option value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>"><?= $data_menu[$modul[$a]][$b]['menu']?></option>
                                        <?php }?>
                                      </select> -->
                                    </div>
                                  </div>
                                  <?php }?>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create Menu Management</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<script>
  $(document).ready(function(){
    /*$('.modul_menu').select2({
      placeholder: "Select a menu"
    });*/
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function () {
    $('.alert').delay(8000).fadeOut();
    //$(".alert").hide(15000);

    $('#tbGroup').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/management/list_group')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'group', name: 'group'},
            {data: 'status', name: 'status'},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

    $("#tbGroup").on("click",".btn_edit", function(){
        var id = $(this).data('x');

        var span = document.createElement("span");
        span.innerHTML = "Anda yakin akan mengedit data menu group ini?";

        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              window.location = "<?= base_url('index.php')?>/management/detail_edit_group/"+id+"?v=false";
            }else{
              swal("Data tidak diedit!");
            }
        });
    });

    $("#tbGroup").on("click",".btn_detail", function(){
        var id = $(this).data('x');
        window.location = "<?= base_url('index.php')?>/management/detail_edit_group/"+id+"?v=true";
    });
  });
</script>
