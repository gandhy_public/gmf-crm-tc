<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#list_currency" aria-controls="profile" role="tab" data-toggle="tab">List Currency</a></li>
                    <li role="presentation" ><a href="#create_currency" aria-controls="home" role="tab" data-toggle="tab">Add Currency</a></li>
                    <li class="pull-right">
                      <a href="<?= base_url('index.php')."/currency/recalculate"?>" class="btn btn-success btn-sm">
                        <i class="fa fa-refresh margin-right-5"></i> Refresh Exchange Currency
                      </a>
                    </li>
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="list_currency">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbCurrency" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th>Code</th>
                                 <th>Label</th>
                                 <th>Nominal</th>
                                 <th>Last Update</th>
                                 <th>Action</th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="create_currency">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/currency/create"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Label</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="label" name="label" required>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Code</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="code" name="code" required placeholder="USD, EUR, etc.">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">
                                        Add Currency
                                      </button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<script>
  $(document).ready(function(){
    /*$('.modul_menu').select2({
      placeholder: "Select a menu"
    });*/
    $('[data-toggle="tooltip"]').tooltip();
  });
  $(function () {
    $('.alert').delay(8000).fadeOut();
    //$(".alert").hide(15000);

    $('#tbCurrency').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/currency/api_table')?>',
        columns: [            
            {data: 'code', name: 'code'},            
            {data: 'label', name: 'label'},
            {data: 'value', name: 'value'},
            {data: 'last_update', name: 'last_update'},
            {data: 'action', name: 'action',orderable: false}
        ],
    });

    $("#tbCurrency").on("click",".btn-delete", function(){
        var code = $(this).data('code');

        var span = document.createElement("span");
        span.innerHTML = "Anda yakin akan menghapus data ini?";

        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              window.location = "<?= base_url('index.php')?>/currency/delete/"+code;
            }else{
              swal("Data tidak diedit!");
            }
        });
    });
  });
</script>
