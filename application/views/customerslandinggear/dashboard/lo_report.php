<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
      <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                  <table class="table">
                <thead>
                  <tr>
                      <th>Week</th>
                      <th>Detail Report</th>
                      <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>W1</td>
                        <td style="background-color: white;">1. Induction date 27 March 2017<br>
                            2. Fan Blades removal finished on 28 March 2017<br>
                            3. Fan & Booster module removal finished on 29 March 2017<br>
                            4. QEC removal finished on 28 March 2017<br>
                            5. LPT Major Module removal finished on 29 March 2017<br>
                            6. Bearing 1 & 2 Supp removal finished on 29 March 2017<br>
                            7. External Core removal finished on 30 March 2017<br>
                            8. Core Major Module removal finished on 30 March 2017<br>
                            9. IGB removal finished on 31 March 2017<br>
                            10. TGB removal finished on 31 March 2017<br>
                            11. HPT Rotor removal finished on 31 March 2017<br>
                            12. HPT Nozzle removal finished on 31 March 2017
                        </td>
                      <td>7 April 2017</td>
                    </tr>
    
                    <tr>
                        <td>W2</td>
                        <td style="background-color: white;">1. Bearing 1 & 2 Supp removal finished on 03 April 2017<br>
                            2. Disassy Combustion assy finished on 03 April 2017<br>
                            3. AGB Component removal finished on 06 April 2017<br>
                            4. AGB removal finished on 06 April 2017<br>
                            5. Disassy HPT Rotor in progress<br>
                            6. Disassy HPT Nozzle in progress<br>
                            7. Insurance & Lessor will visit GMF-Engineshp on 12 April 2017
                        </td>
                      <td>7 April 2017</td>
                    </tr>

                    <tr>
                        <td>W3</td>
                        <td style="background-color: white;">1. Internal process in progress<br>
                            2. OV components removal has been finished on 07 April 2017<br>
                            3. HPT Nozzle disassy has been finished on 08 April 2017<br>
                            4. HPT shroud disassy has been finished on 08 April 2017<br>
                            5. HPT Rotor blade removal has been finished on 11 April 2017<br>
                            6. HPT Disk removal was pending due to tools trouble(tools will be ready on monday 17 April 2017)<br>
                            7. Witness by lessor & insurance has been finished on 12 April 2017, lessor & insurance were agree with the workscope upgrading by GA with
                        </td>
                      <td>14 April 2017</td>
                    </tr>
                </tbody>
              </table>
              </div>
          </div>
      </div>
</div>
</section>