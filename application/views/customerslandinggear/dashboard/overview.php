<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4" style="background-color:white; height: 750px">
      <p> 
          <div class="info-box">
            <span class="info-box-icon" style="background-color: #65b328;"><i class="fa fa-calendar"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Actual TAT</span>
                  <span class="info-box-number" style="font-size: 30px">26 Days</span>
               </div>
            </div>          <!-- /.info-box -->

            <br><div class="info-box">
            <span class="info-box-icon" style="background-color: #0dacbe;"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Job Card</span>
              <span class="info-box-number" style="font-size: 30px">28%</span>
              <span class="info-box-text">364 of 975</span>
            </div>
          </div>
      </p>

      <div class="col-sm-12" style="width:333px; margin:0 auto;">
        <p>Removal:
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%; background-color:#08aebe;">
                100%
                </div>
            </div>
        </p>
        <p>Disassambly:
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:75%; background-color:#08aebe;">
                75%
               </div>
            </div>
        </p>
        <p>Inspection:
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:40%; background-color:#08aebe;">
                40%
                </div>
            </div>
        </p>
        <p>Repair:
             <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:10%; background-color:#08aebe;">
                10%
                </div>
            </div>
        </p>
        <p>Assembly:
               <div class="progress">
                <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:60%; background-color:#08aebe;">
                60%
                </div>
            </div>
        </p>
        <p>Install:
              <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:89%; background-color:#08aebe;">
                89%
                </div>
            </div>
        </p>
        <p>Test:
               <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:46%; background-color:#08aebe;">
                46%
                </div>
            </div>
        </p>
        </div>
      </div>

    <div class="col-sm-8" style="background-color:white; height: 750px">
      <p>
        <div class="box-body">
            <div class="col-sm-12" style="width:700px; margin:0 auto;">
              <div class="progress">
                <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:67%">
                    22% Complete (Open)
                </div>
            </div>
            <div class="progress">
              <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:83%">
                    83% Complete (In Progress)
              </div>
            </div>
            <div class="progress">
              <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:28%">
                    24% Complete (Closed)
              </div>
        </div>
        <br>

        <div class="box-body">
                  <!-- <div class="container"> -->
                    <div class="col-sm-12" style="width:670px; margin:0 auto;">         
                       <table id="example" class="table table-bordered table-striped" style="width: 100%;">
                          <thead>
                            <tr>
                              <th>NO</th>
                              <th>MAT</th>
                              <th>DESCRIPTION</th>
                              <th>STATUS</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>NE</td>
                              <td>SUPPORT, REAR G</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>2</td>
                              <td>NS</td>
                              <td>GUIDE REAR ROU</td>
                              <td>In Progress</td>
                            </tr>
                            <tr>
                              <td>3</td>
                              <td>NS</td>
                              <td>REAR CONE</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>4</td>
                              <td>NS</td>
                              <td>SCREW REAR HANGING</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>5</td>
                              <td>NE</td>
                              <td>GUIDE, REAR, GUN</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>6</td>
                              <td>NS</td>
                              <td>REAR SPACER</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>7</td>
                              <td>NE</td>
                              <td>MOUNT ASSY NEAR</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>8</td>
                              <td>NS</td>
                              <td>SUPPORT ASSY REAR</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>9</td>
                              <td>NS</td>
                              <td>REAR-KLM</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>10</td>
                              <td>NS</td>
                              <td>REAR COVER ASS</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>11</td>
                              <td>NS</td>
                              <td>SPRING, REAR AXLE</td>
                              <td>In Progress</td>
                            </tr>
                            <tr>
                              <td>12</td>
                              <td>NS</td>
                              <td>TUNNEL, REAR-KL9</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>13</td>
                              <td>NS</td>
                              <td>GOUTIRE</td>
                              <td>Closed</td>
                            </tr>
                            <tr>
                              <td>14</td>
                              <td>NS</td>
                              <td>TIRE 9.00 X 15 TR</td>
                              <td>Closed</td>
                            </tr>
                          </tbody>
                        </table>
                  <!-- </div> -->
                  </div>
                </div>
      </p>
    </div>
  </div>
</div>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
</section>