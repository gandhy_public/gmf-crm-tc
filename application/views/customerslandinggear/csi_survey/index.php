<section class="content-header">
    <h1 align="center">
        CUSTOMER SATISFACTION SURVEY
    </h1>
    <p align="center">_____________________________</p>
    <h4 align="center">Project ESN : 802696</h4>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">CSI Survey</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
   <!--  <div class="col-lg-5" style="margin: 0;"> -->
   <div style="width:500px; margin:0 auto;">
    <form action="">
        <h5>The purpose of the questionnaire is to collect your feedback on GMF's performance and services</h5>
        <h5 style="color: red;">*Required</h5>
      <div class="form-group">
        <label for="inputName">Please fill in your Name <span style="color: red">*</span></label>
        <input type="text" class="form-control" id="inputName" placeholder="Your Answer" required>
      </div>
      <div class="form-group">
        <label for="companyName">You are filling this form on behalf to <span style="color: red">*</span></label>
        <select class="form-control" id="companyName">
          <option>Choose</option>
          <option>Garuda Indonesia</option>
          <option>Citilink</option>
          <option>Sriwijaya Air</option>
          <option>Lion Air</option>
          <option>Air Asia</option>
          <option>Aero Eagle</option>
          <option>Angkatan Udara Republik Indonesia</option>
          <option>CFMI</option>
          <option>Eastar Jet</option>
          <option>Cardig Air</option>
          <option>Trigana Air</option>
          <option>K-Mile</option>
          <option>Others (please fill in at the ESN column)</option>
        </select>
        <small class="form-text text-muted">Fill in your company name.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Please fill in your email address<span style="color: red">*</span></label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Answer" required>
     </div>
     <div class="form-group">
        <label for="inputESN">Please fill in your product ESN for this form <span style="color: red">*</span></label>
        <input type="text" class="form-control" id="inputName" placeholder="Your Answer" required>
      </div>
      <div class="form-group">
        <label for="rateServices">Please rate our technical quality of product and services<span style="color: red">*</span></label><br>
         <small class="form-text text-muted">Quality</small>
          <div class="form-check form-check-inline">
          <label>Unsatisfied</label>&nbsp;&nbsp;
             <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
             <label class="form-check-label" for="inlineRadio1"></label>

             <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
             <label class="form-check-label" for="inlineRadio2"></label>

             <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
             <label class="form-check-label" for="inlineRadio3"></label>

             <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
             <label class="form-check-label" for="inlineRadio4"></label>

             <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option5">
             <label class="form-check-label" for="inlineRadio5"></label>&nbsp;&nbsp;
             <label>Very Satisfied</label>
          </div>
          
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<!-- <div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-auto">
      <form>
          <h5>The purpose of the questionnaire is to collect your feedback on GMF's performance and services</h5>
          <h5 style="color: red;">*Required</h5>
               <div class="col-xs-4">
                <label for="name">Please fill in your Name <span style="color:red;">*</span></label>
                <input class="form-control" id="name" type="text" placeholder="Your Answer">
               </div>

               <div class="col-xs-4">
                <label for="name">Please fill in your email address <span style="color:red;">*</span></label>
                <input class="form-control" id="name" type="text" placeholder="Your Answer">
               </div>
      </form>
    </div>
  </div> -->
</section>