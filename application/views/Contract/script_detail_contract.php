<script>
  var queryPartNumber = "";
  var queryFleet = "";

  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
  });
  
  $("#form-upload").submit(function(evt){
    swal({
      title: 'WARNING!',
      text: "Don't refresh or reload this page, upload file is processing!",
      icon: 'warning',
      buttons: false
    });

    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    $('#button-save').text("Processing...").attr("disabled", true);

    setTimeout(function() {
      $.ajax({
        url: "<?php echo base_url('index.php/api/Contract/CekUpload')?>",
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        beforeSend: function() {
          $('#button-save').text("Processing...");
        },
        success: function (response) {
          if(response){
            swal({
              title: "WARNING",
              text: "The previous Detail Contract data will be deleted if you upload a new file, do you approve it?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $.ajax({
                  url: "<?php echo base_url('index.php/api/Contract/Updel')?>",
                  type: 'POST',
                  data: formData,
                  async: false,
                  cache: false,
                  contentType: false,
                  enctype: 'multipart/form-data',
                  processData: false,
                  success: function (response) {
                    $('.content').append(response);
                  }
                });
                /*swal("Poof! Your imaginary file has been deleted!", {
                  icon: "success",
                });*/
              } else {
                swal("Data has not changed!");
              }
            });
          }else{
            $.ajax({
              url: "<?php echo base_url('index.php/api/Contract/Upload')?>",
              type: 'POST',
              data: formData,
              async: false,
              cache: false,
              contentType: false,
              enctype: 'multipart/form-data',
              processData: false,
              success: function (response) {
                $('.content').append(response);
              }
            });
          }
        }
      });
    }, 3000);
  });

  $(function () {	
    var tbPartNumber = $('#tbPartNumber').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/Contract/ListPn/'.$this->uri->segment(3))?>",
            type: "POST",
            complete: function(json, type) {
              queryPartNumber = json.responseJSON.query;
            }
        },
        columns: [                      
          {data: 'ata', name: 'ata', orderable: false},
          {data: 'pn', name: 'pn', orderable: false},
          {data: 'desc', name: 'desc', orderable: false},
          {data: 'ess', name: 'ess', orderable: false},
          {data: 'mel', name: 'mel', orderable: false},
          {data: 'sla', name: 'sla', orderable: false},
          {data: 'unserviceable_return', name: 'unserviceable_return', orderable: false},
          {data: 'service_level_category', name: 'service_level_category', orderable: false},
          {data: 'target_service_level', name: 'target_service_level', orderable: false}
        ],
        dom: 'Blfrtip',
        buttons: [
        ]
    });

    // export button
    tbPartNumber.button().add(0, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#tbPartNumber thead tr th").map(function() { 
          header.push($.trim($(this).text()));
        }).get();

        $('#export-form').attr("action", "<?= base_url('index.php/export/Contract/ExportPartNumberToExcel'); ?>");
        $('#export-form [name=header]').val(header);
        $('#export-form [name=query]').val(queryPartNumber);
        
        setTimeout(function() {
          $('#export-form').submit();
        }, 1000);
      },
      text: 'Export to Excel',
      className: 'btn btn-success btn-sm'
    });
	
    var tbFleet = $('#tbFleet').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/Contract/ListFlt/'.$this->uri->segment(3))?>",
            type: "POST",
            complete: function(json, type) {
              queryFleet = json.responseJSON.query;
            }
        },
        columns: [                       
          {data: 'regis', name: 'regis', orderable: false},
          {data: 'type', name: 'type', orderable: false},
          {data: 'msn', name: 'msn', orderable: false},
          {data: 'mfg', name: 'mfg', orderable: false},
        ],
        dom: 'Blfrtip',
        buttons: [
        ]
    });

    // export button
    tbFleet.button().add(0, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#tbFleet thead tr th").map(function() { 
          header.push($.trim($(this).text()));
        }).get();

        $('#export-form').attr("action", "<?= base_url('index.php/export/Contract/ExportFleetToExcel'); ?>");
        $('#export-form [name=header]').val(header);
        $('#export-form [name=query]').val(queryFleet);
        
        setTimeout(function() {
          $('#export-form').submit();
        }, 1000);
      },
      text: 'Export to Excel',
      className: 'btn btn-success btn-sm'
    });
  });

</script>