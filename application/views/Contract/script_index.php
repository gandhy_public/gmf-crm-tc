<script>
  var table;
  var tableFleet;
  var tablePartNumber;
  var tableDeliv;

  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    
    var date_input=$('#mfggate'); 
    var options={
      format: 'yyyy/mm/dd',
      todayHighlight: true,
      autoclose: true,
    };
    date_input.datepicker(options);

    $('#CUSTOMER').select2({
      width: '100%',
      <?php if(count($customer) == 0){?>
        placeholder: 'Select Customer',
        minimumInputLength: 2,
        triggerChange: true,
        allowClear: true,
        ajax: {
          url: "<?=base_url()?>index.php/api/Contract/SelectCustomer",
          dataType: 'json',
          delay: 450,
          processResults: function (data) {
            return {
              results: data
            }
          },
          cache: true
        }
      <?php }?>
    });
  });

  $(function(){
    //datatables
    tableFleet = $('#tbFleet').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
    tablePartNumber = $('#tbPartNumber').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
    tableFleet.clear().draw();
    tablePartNumber.clear().draw();

    $('#form-contract').one('submit', function (e) {
      e.preventDefault();
      swal({
        title: "Are you sure want to Submit?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          var dataFleet       = tableFleet.rows().data();
          var dataPartNumber  = tablePartNumber.rows().data();

          $.each(dataFleet, function(index, val){
            AIRCRAFT_REGISTRATION = $("<input>").attr('hidden', 'true').attr("name", "fleet[AIRCRAFT_REGISTRATION][]").val(val[0]);
            $('#form-contract').append(AIRCRAFT_REGISTRATION);
            TYPE = $("<input>").attr('hidden', 'true').attr("name", "fleet[TYPE][]").val(val[1]);
            $('#form-contract').append(TYPE);
            MSN = $("<input>").attr('hidden', 'true').attr("name", "fleet[MSN][]").val(val[2]);
            $('#form-contract').append(MSN);
            MFN_GATE = $("<input>").attr('hidden', 'true').attr("name", "fleet[MFN_GATE][]").val(val[3]);
            $('#form-contract').append(MFN_GATE);
          });

          $.each(dataPartNumber, function(index, val){
            ATA = $("<input>").attr('hidden', 'true').attr("name", "pn[ATA][]").val(val[0]);
            $('#form-contract').append(ATA);
            ID_PN = $("<input>").attr('hidden', 'true').attr("name", "pn[ID_PN][]").val(val[1]);
            $('#form-contract').append(ID_PN);
            DESCRIPTION = $("<input>").attr('hidden', 'true').attr("name", "pn[DESCRIPTION][]").val(val[2]);
            $('#form-contract').append(DESCRIPTION);
            ESS = $("<input>").attr('hidden', 'true').attr("name", "pn[ESS][]").val(val[3]);
            $('#form-contract').append(ESS);
            MEL = $("<input>").attr('hidden', 'true').attr("name", "pn[MEL][]").val(val[4]);
            $('#form-contract').append(MEL);
            TARGET_SLA = $("<input>").attr('hidden', 'true').attr("name", "pn[TARGET_SLA][]").val(val[5]);
            $('#form-contract').append(TARGET_SLA);
            TUR = $("<input>").attr('hidden', 'true').attr("name", "pn[TUR][]").val(val[6]);
            $('#form-contract').append(TUR);
            SLC = $("<input>").attr('hidden', 'true').attr("name", "pn[SLC][]").val(val[7]);
            $('#form-contract').append(SLC);
            TSL = $("<input>").attr('hidden', 'true').attr("name", "pn[TSL][]").val(val[8]);
            $('#form-contract').append(TSL);
          });
          $(this).submit();
        }else{
          location.reload();
        }
      });
    });

    ////=============================
    // INISTIALISASI TABLE FLEET
    $('#btnFleet').on('click', function(e){
      $type = $('#form-fleet #type').val();
      $msn = $('#form-fleet #msn').val();
      $mfggate = $('#form-fleet #mfggate').val();
      $aircraft_registration = $('#form-fleet #aircraft_registration').val();

      if ($type && $msn && $mfggate && $aircraft_registration) {
          $data = {
            '0' : $aircraft_registration,
            '1' : $type,
            '2' : $msn,
            '3' : $mfggate,
            '4' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> ',
            '10' : $aircraft_registration,
          };
          addRow(tableFleet, $data);

          $("#form-fleet :input").val("");
          $('#form-fleet #aircraft_registration').focus();
      }else if (!$type) {
        $('#type').focus();
      }else if (!$msn) {
        $('#msn').focus();
      }else if (!$mfggate) {
        $('#mfggate').focus();
      }
    });

    $('#tbFleet ').on( 'click','tbody tr button.btnRemove',function(){
      tableFleet
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

    ////=============================
    // INISTIALISASI TABLE PART NUMBER
    $('#btnPN').on('click', function(e){
      $ata = $('#form-pn #ata').val();
      $part_number = $('#form-pn #part_number').val();
      $description = $('#form-pn #description').val();
      $ess = $('#form-pn #ess').val();
      $mel = $('#form-pn #mel').val();
      $sla = $('#form-pn #sla').val();
      $day_sla = $('#day_sla').val();
      $tur = $('#form-pn #tur').val();
      $slc = $('#form-pn #slc').val();
      $tsl = $('#form-pn #tsl').val();

      if($ata && $part_number && $description && $ess && $mel && $sla && $tur && $slc && $tsl){
        $data = {
          '0' : $ata,
          '1' : $part_number,
          '2' : $description,
          '3' : $ess,
          '4' : $mel,
          '5' : $sla+" "+$day_sla,
          '6' : $tur,
          '7' : $slc,
          '8' : $tsl,
          '9' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> '
        };
        addRow(tablePartNumber, $data);

        $("#form-pn :input").val("");
        $('#day_sla').val("hours");
        $('#ata').focus();
      }else if(!$ata){
        console.log("2");
        $('#ata').focus();
      }else if(!$description){
        console.log("3");
        $('#description').focus();
      }else if(!$ess){
        console.log("4");
        $('#ess').focus();
      }else if(!$mel){
        console.log("5");
        $('#mel').focus();
      }else if(!$sla){
        console.log("6");
        $('#sla').focus();
      }else if(!$tur){
        $('#tur').focus();
      }else if(!$slc){
        $('#slc').focus();
      }else if(!$tsl){
        $('#tsl').focus();
      }
    });

    $('#tbPartNumber ').on( 'click','tbody tr button.btnRemove',function(){
      tablePartNumber
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

    ////=============================
    // INISTIALISASI TABLE DELIVERY
    $('#btnDeliv').on('click', function(e){
      $('#form-deliv #code').focus();
      $code = $('#form-deliv #code').val();
      $address = $('#form-deliv #address').val();
      //reset form input after click add button
      $('#form-deliv #code').val("");
      $('#form-deliv #address').val("");

      if ($code && $address) {
          $data = {
            '0' : $code,
            '1' : $address,
            '2' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> '
          };
          addRow(tableDeliv, $data);
      }else if (!$code) {
        $('#code').focus();
      }else if (!$address) {
        $('#address').focus();
      }
    });

    $('#tbDeliv ').on( 'click','tbody tr button.btnRemove',function(){
      tableDeliv
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

  });

  function removeRow(a){

  }
  function addRow($table, data){
    ($table).row.add(data ).draw();
  }
</script>