<style type="text/css">
  .disabled-select {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:3px;
    cursor:not-allowed;
    position:absolute;
    top:0;
    bottom:0;
    right:0;
    left:0;
  }
  select[readonly].select2-hidden-accessible + .select2-container {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:100px;

    pointer-events: none;
    touch-action: none;
  }
</style>
<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <section class="content">
        <div class="box-header">
          <div class="box-tools">
          </div>
        </div>
        <div class="box-body no-padding">
          <form id="form-contract" enctype="multipart/form-data" method="post" action="<?= base_url()?>index.php/api/Contract/CreateContract" class="form-horizontal">
            <div class="section">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Contract Number</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control input-sm" id="CONTRACT_NUMBER" name="CONTRACT_NUMBER" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Customer</label>
                    <div class="col-sm-7">
                      <select id="CUSTOMER" name="CUSTOMER" class="form-control" required <?= (count($customer) > 0) ? "readonly" : ""?>>
                        <?php foreach($customer as $key){?>
                        <option value="<?= $key['ID_CUSTOMER']?>"><?= $key['COMPANY_NAME']?></option>
                        <?php }?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Description Contract</label>
                    <div class="col-sm-7">
                      <textarea style="min-height: 130px;" class="form-control input-sm" id="DESC_CONTRACT" name="DESC_CONTRACT" required></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Transfer Point</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control input-sm" id="DELIVERY_POINT" name="DELIVERY_POINT" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Destination Address</label>
                    <div class="col-sm-7">
                      <textarea style="min-height: 130px;" class="form-control input-sm" id="DELIVERY_ADDRESS" name="DELIVERY_ADDRESS" required></textarea>
                    </div>
                  </div>
                  <div class="form-group pull-right">
                    <button class="btn btn-primary" type="submit" id="btnSubmitContract" > <i class="fa fa-save"> </i> Create Contract</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="section">
              <div class="col-md-12">
                <div>
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                  </ul>
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                      <div class="row" style="padding-bottom: 30px; padding-top: 10px;">
                        <div class="box">
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                      <label class="">Aircraft Registration</label>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <label class="">Type</label>
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <label class="">MSN</label>
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <label class="">MFG DATE</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div id="form-fleet" class="col-md-12">
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="aircraft_registration" name="aircraft_registration">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="type" name="type">
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <input class="form-control" id="msn" name="msn">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="mfggate" name="mfggate" type="text"/>
                                  </div>
                                </div>
                                <div class="col-md-1">
                                  <button class="btn btn-primary" id="btnFleet" type="button" ><i class="fa fa-plus"></i></button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="box">
                          <div class="box-body">
                            <table id="tbFleet" class="table table-bordered table-hover table-striped">
                              <thead>
                                <tr>
                                  <th> Aircraft Registration </th>
                                  <th> Type </th>
                                  <th> MSN </th>
                                  <th> MFG Date </th>
                                  <th> Act </th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                      <div class="box">
                        <div class="box-body">
                          <div class="row">
                            <div class="row">
                              <form class=" form-horizontal">
                                <div class="col-md-12" id="form-pn">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">ATA</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="ata" id="ata">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">Part Number</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" id="part_number" name="part_number">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">Description</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="description" id="description">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">ESS</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="ess" id="ess">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">MEL</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="mel" id="mel">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group form-inline">
                                      <label class="col-sm-5 control-label">TARGET SLA</label>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control input-sm" name="sla" id="sla">
                                      </div>
                                      <div class="col-sm-3">
                                        <select class="form-control input-sm select2" id="day_sla" name="day_sla">
                                          <option value="hours">Hours</option>
                                          <option value="days">Days</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">TARGET UNSERVICEABLE RETURN</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="tur" id="tur">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">SERVICE LEVEL CATEGORY</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="slc" id="slc">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">TARGET SERVICE LEVEL</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="tsl" id="tsl">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                      <button class="btn btn-primary" id="btnPN" type="button" ><i class="fa fa-plus"></i></button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="box">
                        <div class="box-body">
                          <table id="tbPartNumber" class="table table-bordered table-hover table-striped">
                            <thead>
                              <tr>
                                <th> ATA </th>
                                <th> Part Number </th>
                                <th> Description </th>
                                <th> ESS </th>
                                <th> MEL </th>
                                <th> Target SLA </th>
                                <th> Target Unserviceable Return </th>
                                <th> Service Level Category </th>
                                <th> Target Service Level </th>
                                <th> Act </th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="pull-right">
                    <button class="btn btn-primary" type="submit" id="btnSubmitContract" > <i class="fa fa-save"> </i> Create Contract</button>
                  </div>
                </div>
              </div>
            </div> -->
          </form>
        </div>
      </section>
    </div>
  </div>
</section>
<?php $this->load->view($script)?>
