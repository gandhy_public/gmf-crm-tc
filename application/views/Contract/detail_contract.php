<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <div class="box-header contract">
              <div class="main-title">
                <div class="title"><?= $desc['contract_number']; ?></div>
              </div>
              <div class="detail">
                <div class="detail-item">
                  <span class="title"><b><u>Description</u></b></span>
                  <p class="value"><?= $desc['contract_desc']; ?></p>
                </div>
                <div class="detail-item">
                  <span class="title"><b><u>Delivery Point</u></b></span>
                  <p class="value"><?= $desc['delivery_point']; ?></p>
                </div>
                <div class="detail-item">
                  <span class="title"><b><u>Delivery Address</u></b></span>
                  <p class="value"><?= str_replace('\r\n', "<br />", json_encode($desc['delivery_address'])); ?></p>
                </div>
              </div>
            </div>
            <div class="box-body" style="padding:0">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding:0">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                     <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                     <li role="presentation" ><a href="#content-upload" aria-controls="home" role="tab" data-toggle="tab">Upload Data</a></li>       
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="profile">
                       <div class="box">
                         <div class="box-body with-export-excel">
						            	  <!-- <button class="btn btn-flat bg-navy editor_create"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbPartNumber" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> ATA </th>
                                 <th> Part Number </th>
                                 <th> Description </th>
                                 <th> ESS </th>
                                 <th> MEL </th>
                                 <th> Target SLA </th>
                                 <th> Unserviceable Return </th>
                                 <th> Service Level Category </th>
                                 <th> Target Service Level </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="home">
                        <div class="box">
                          <div class="box-body with-export-excel">
                            <!-- <button class="btn btn-flat bg-navy editor_create2"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbFleet" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff; width:100%;">
                                <tr>
                                   <th> Aircraft Registration </th>
                                   <th> Type </th>
                                   <th> MSN </th>
                                   <th> MFG Date </th>
                                </tr>
                              </thead>
                              <tbody>

                               </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="content-upload">
                        <div class="box">
                          <div class="box-body">
                            <form id="form-upload" enctype="multipart/form-data">
                              <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Upload Data</label>
                                  <div class="col-sm-9">
                                  <input type="file" class="form-control" name="files" id="files" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                                  <input type="hidden" name="url" value="<?php echo current_url();?>">
                                  <input type="hidden" name="ct_num" value="">
                                  <input type="hidden" name="ct_id" value="<?php echo $this->uri->segment(3)?>">
                                  </div>
                                </div>
                                <br><br>
                                <div class="form-group col-sm-12">
                                  <a href="<?php echo base_url("public/TEMPLATE_DETAIL_CONTRACT.xlsx");?>" style="cursor: pointer;">Download Format Upload <i class="fa fa-cloud-download"></i></a>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button>
                                <!-- <button type="button" class="pull-right btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button> -->
                              </div>
                            </form>
                          </div>
                        </div>
                     </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>
</section>

<form id="export-form" action="" method="POST">
  <input type="hidden" name="header">
  <input type="hidden" name="query">
</form>

<?php $this->load->view($script)?>
