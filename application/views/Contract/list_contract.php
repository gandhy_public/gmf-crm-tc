<style type="text/css">
  select[readonly].select2-hidden-accessible + .select2-container {
    background-color:#d5d5d5;
    opacity:0.5;
    border-radius:100px;

    pointer-events: none;
    touch-action: none;
  }
</style>
<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <div class="box-header">
                      <!-- <h3 class="box-title"><?php //echo $title; ?></h3> -->
                      <div class="box-tools">

                      </div>
                    </div>
                    <div class="box-body no-padding with-export-excel">
                    <table id="tbcontract" class="table table-bordered table-hover table-striped" width="100%">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                        <tr>
                          <th width="2%"><center> No </center></th>
                          <th width="10%"><center> Contract Number </center></th>
                          <th width="10%"><center> Customer </center></th>
                          <th width="20%"><center> Transfer Point </center></th>
                          <th width="20%"><center> Destination Address </center></th>
                          <th width="30%"><center> Description </center></th>
                          <th width="8%"><center> Action </center></th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                    <br>

          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>
</section>

<!-- Form Edit Modal -->
<div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/api/Contract/UpdateCt')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Contract</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" name="id" id="id">
          <div class="form-group">
            <label class="col-sm-5 control-label">Contract Number</label>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" id="CONTRACT_NUMBER" name="CONTRACT_NUMBER" required>
            </div>
          </div>
          <br><br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Transfer Point</label>
            <div class="col-sm-7">
              <input type="text" class="form-control input-sm" id="DELIVERY_POINT" name="DELIVERY_POINT" required>
            </div>
          </div>
          <br><br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Destination Address</label>
            <div class="col-sm-7">
              <textarea style="min-height: 130px;" class="form-control input-sm" id="DELIVERY_ADDRESS" name="DELIVERY_ADDRESS" required></textarea>
            </div>
          </div>
          <br><br><br><br><br><br><br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Description Contract</label>
            <div class="col-sm-7">
              <textarea style="min-height: 130px;" class="form-control input-sm" id="DESC_CONTRACT" name="DESC_CONTRACT" required></textarea>
            </div>
          </div>
          <br><br><br><br><br><br><br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Customer</label>
            <div class="col-sm-7">
              <select class="form-control select2" id="CUSTOMER" name="CUSTOMER" style="width: 100%;" required <?= (count($customer) == 1) ? "readonly" : ""?>>
                <?php
                  foreach ($customer as $row) {
                ?>
                <option value="<?= $row['ID_CUSTOMER'] ?>"><?= $row['COMPANY_NAME'] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<form id="export-form" action="<?= base_url('index.php/export/Contract/ExportListContractToExcel'); ?>" method="POST">
  <input type="hidden" name="header">
  <input type="hidden" name="query">
</form>
<?php $this->load->view($script)?>
