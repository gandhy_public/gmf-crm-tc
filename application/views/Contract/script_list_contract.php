<script>
  var queryListContract = "";
  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    
    $('#CUSTOMER').select2();
  });
  $(function () {
    var tbContract = $('#tbcontract').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/Contract/ListCt/')?>",
            type: "POST",
            complete: function(json, type) {
              queryListContract = json.responseJSON.query;
            }
        },
        columns: [       
            {data: 'no', name: 'no'},
            {data: 'contract_number', name: 'contract_number'},            
            {data: 'customer', name: 'customer', orderable: false},
            {data: 'delivery_point', name: 'delivery_point', orderable: false},
            {data: 'delivery_address', name: 'delivery_address', orderable: false},
            {data: 'description', name: 'description', orderable: false},
            {data: 'act', name: 'act', orderable: false}
        ],
        dom: 'Blfrtip',
        buttons: []
    });

    $(".select2").select2();

    $("#tbcontract").on("click",".btn_del", function(){
        var id = $(this).data('x');
        var number = $(this).data('y');

        var span = document.createElement("span");
        span.innerHTML = "You will delete the contract data with the <br><b>Contract Number : "+number+"</b>";
        swal({
          title: "WARNING",
          content: span,
          //text: "Apakah data CONTRACT dengan CONTRACT NUMBER : "+number+" akan dihapus ?",
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?php echo base_url('index.php/api/Contract/DeleteCt')?>",
                type: 'POST',
                data: {
                  'id':id,
                  'contract_number':number
                },
                success: function (response) {
                  //$('.content').append(response);
                  if (response) {
                    /*swal("Data berhasil dihapus!", {
                      icon: "success",
                    });*/
                    swal({
                      text: "Data successfuly deleted!",
                      icon: "success",
                      showConfirmButton: true
                    }).then(function() {
                        window.location.href = "<?php echo base_url('index.php/Contract/ListContract')?>";
                    });
                  }else{
                    /*swal("Data berhasil dihapus!", {
                      icon: "success",
                    });*/
                    swal({
                      text: "Data fail deleted!",
                      icon: "error",
                      showConfirmButton: true
                    }).then(function() {
                        window.location.href = "<?php echo base_url('index.php/Contract/ListContract')?>";
                    });
                  }
                }
              });
            }else{
              swal("Data not deleted!");
            }
        });
    });

    $("#tbcontract").on("click",".btn_edit", function(){
        var id = $(this).data('x');
        var number = $(this).data('y');

        var span = document.createElement("span");
        span.innerHTML = "Will you edit the contract data with the <br><b>Contract Number : "+number+"</b>";

        $('#exampleModalCenter').modal('hide');
        $.ajax({
          type: 'POST',
          url: '<?=base_url('index.php/api/Contract/DetailCt/')?>',
          data: { 
              'id': id, 
              'ct_number': number 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $('#id').val(id);

              $('#CONTRACT_NUMBER').val(obj.contract_number);
              $('#DELIVERY_POINT').val(obj.delivery_point);
              $('#DELIVERY_ADDRESS').val(obj.delivery_address);
              $('#DESC_CONTRACT').val(obj.description);
              $("#CUSTOMER").val(obj.customer).change();
          }
        });

        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $('#exampleModalCenter').modal('show');
            }else{
              swal("Data not changed!");
            }
        });
    });

    // export button
    tbContract.button().add(0, {
      action: function(e, dt, button, config) {
        var header = [];
        $("#tbcontract thead tr th").not(':last').map(function() { 
          header.push($.trim($(this).find('center').text()));
        }).get();

        $('#export-form [name=header]').val(header);
        $('#export-form [name=query]').val(queryListContract);
        
        setTimeout(function() {
          $('#export-form').submit();
        }, 1000);
      },
      text: 'Export to Excel',
      className: 'btn btn-success btn-sm'
    });
  });

</script>