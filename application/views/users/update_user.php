<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php if ($this->session->flashdata('alert_failed')) { echo '<div id="alert_failed" class="callout callout-danger">'; echo '<p>'; echo $this->session->flashdata('alert_failed'); echo '</p>'; echo '</div>'; } ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        
        <?php $this->load->view($nav_tabs); ?>

        <div class="tab-content">
            
            <legend><?php echo $title; ?></legend>
            <form action="<?php echo base_url() ?>index.php/Users/action_update" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-12 col-xs-12">
                    <div class="form-horizontal">
                      <input type="hidden" class="form-control input-sm" id="ID_USER" name="ID_USER" value="<?php echo $data_user["ID_USER"]; ?>">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control input-sm" id="NAME" name="NAME" value="<?php echo $data_user["NAME"]; ?>"> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control input-sm" id="USERNAME" name="USERNAME" value="<?php echo $data_user["USERNAME"]; ?>"> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control input-sm" id="PASSWORD" name="PASSWORD" value="" placeholder="Type New Password If You Want To Change"> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control input-sm" id="EMAIL" name="EMAIL" value="<?php echo $data_user["EMAIL"]; ?>"> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">User Role</label>
                        <div class="col-sm-3">
                          <select class="form-control input-sm select2" id="ID_USER_ROLE" name="ID_USER_ROLE" style="width: 100%;" required>
                            <option value="">Choose User Role</option>
                            <?php 
                              foreach ($data_user_role as $row_ur) { 
                            ?>
                            <option value="<?php echo $row_ur->ID_USER_ROLE ?>" <?php if($data_user["ID_USER_ROLE"]==$row_ur->ID_USER_ROLE) { echo 'selected'; } ?>><?php echo $row_ur->USER_ROLE; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">User Group</label>
                        <div class="col-sm-3">
                          <select class="form-control input-sm select2" id="ID_USER_GROUP" name="ID_USER_GROUP" style="width: 100%;" disabled>
                            <option value="">Choose User Group</option>
                            <?php 
                              foreach ($data_user_group as $row_ug) { 
                            ?>
                            <option value="<?php echo $row_ug->ID_USER_GROUP ?>" <?php if($data_user["ID_USER_GROUP"]==$row_ug->ID_USER_GROUP) { echo 'selected'; } ?>><?php echo $row_ug->USER_GROUP; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Customer</label>
                        <div class="col-sm-4">
                          <select class="form-control input-sm select2" id="ID_CUSTOMER" name="ID_CUSTOMER" style="width: 100%;" disabled>
                            <option value="">Choose Customer</option>
                            <?php 
                              foreach ($data_customer as $row_c) { 
                            ?>
                            <option value="<?php echo $row_c->ID_CUSTOMER ?>" <?php if($data_user["ID_CUSTOMER"]==$row_c->ID_CUSTOMER) { echo 'selected'; } ?>><?php echo $row_c->COMPANY_NAME; ?></option>
                            <?php } ?>
                          </select>                                                                
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="col-sm-5 control-label">Component Description</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" disabled="true" id="COMPONENT_DESCRIPTION_V" name="COMPONENT_DESCRIPTION_V">
                          <input type="hidden" class="form-control input-sm" id="COMPONENT_DESCRIPTION" name="COMPONENT_DESCRIPTION">                  
                        </div>
                      </div> -->
                      
                      <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Condition</label>
                        <div class="col-sm-6">
                          <textarea name="CONDITION" type="textarea" class="form-control input-sm" rows="5" cols="22"></textarea> 
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Contact Info</label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control input-sm" id="CONTACT_INFO" name="CONTACT_INFO"> 
                        </div>
                      </div> -->

                    </div>
                  </div>                
                                         
                </div>
                
                <!-- <br>
                <hr class="title-hr">
                <legend>Attention To </legend>
                
                <div class="row">                
                                         
                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">Existing Email</label>
                        <div class="col-sm-7">
                          <select class="form-control input-sm select2" multiple="multiple" id="EXISTING_EMAIL" name="EXISTING_EMAIL" style="width: 100%;" required>
                            <option value="">Choose Existing Email</option>
                            <option value="herdy.prabowo@gmail.com" selected>herdy.prabowo@gmail.com</option>
                            <option value="riyan.sasmitha@gmail.com">riyan.sasmitha@gmail.com</option>
                            <option value="liyana@gmail.com">liyana@gmail.com</option>
                            <option value="viar@gmail.com">viar@gmail.com</option>
                            <option value="nando@gmail.com">nando@gmail.com</option>
                            <option value="michael@gmail.com">michael@gmail.com</option>
                          </select>
                        </div>
                      </div>             
                    </div>
                  </div>

                  <div class="col-lg-6 col-xs-12">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 1 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL1">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 2 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL2">  
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-5 control-label">New Email 3 </label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control input-sm" name="EMAIL3">  
                        </div>
                      </div>

                    </div>
                  </div>

                </div> -->

              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" id="insert" name="insert" class="btn btn-success pull-right">Request</button>
              </div><!-- /.box-footer -->           
            </form>
          
        </div>
            
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function () {

    setTimeout(function() {
      $('#alert_failed').fadeOut('slow');
      $('#alert_success').fadeOut('slow');
    }, 5000); // <-- time in milliseconds

    var ID_USER_ROLE = $('#ID_USER_ROLE').val();
    if (ID_USER_ROLE == 1)
    {
        $("#ID_USER_GROUP").attr('disabled', 'disabled');
        $("#ID_CUSTOMER").attr('disabled', 'disabled');
        document.getElementById("ID_USER_GROUP").required = false;
        document.getElementById("ID_CUSTOMER").required = false;
    }
    else if (ID_USER_ROLE == 2)
    {
        $("#ID_CUSTOMER").attr('disabled', 'disabled');
        document.getElementById("ID_USER_GROUP").required = false;
    }
    else if (ID_USER_ROLE == 3)
    {
        $("#ID_USER_GROUP").removeAttr('disabled', 'disabled');
        $("#ID_CUSTOMER").removeAttr('disabled', 'disabled');
        document.getElementById("ID_USER_GROUP").required = false;
        document.getElementById("ID_CUSTOMER").required = false;
    }

    function UserRoleChange() {
      // Set
      $("#ID_USER_GROUP").removeAttr('disabled', 'disabled');
      $("#ID_CUSTOMER").removeAttr('disabled', 'disabled');
      document.getElementById("ID_USER_GROUP").required = false;
      document.getElementById("ID_CUSTOMER").required = false;
      
      var ID_USER_ROLE = $('#ID_USER_ROLE').val();
      if (ID_USER_ROLE == 1)
      {
          $("#ID_USER_GROUP").attr('disabled', 'disabled');
          $("#ID_CUSTOMER").attr('disabled', 'disabled');
          document.getElementById("ID_USER_GROUP").required = false;
          document.getElementById("ID_CUSTOMER").required = false;
      }
      else if (ID_USER_ROLE == 2)
      {
          $("#ID_CUSTOMER").attr('disabled', 'disabled');
          document.getElementById("ID_USER_GROUP").required = false;
      }
      else if (ID_USER_ROLE == 3)
      {
          $("#ID_USER_GROUP").removeAttr('disabled', 'disabled');
          $("#ID_CUSTOMER").removeAttr('disabled', 'disabled');
          document.getElementById("ID_USER_GROUP").required = false;
          document.getElementById("ID_CUSTOMER").required = false;
      }
    }

    $('#ID_USER_ROLE').change(function () { 
        // Set
        UserRoleChange();
    });

  });
</script>
<script>
  $(function () {
    //$("#iduserrole").select2({ width: 'resolve' });
    $(".select2").select2();
  });
</script>