<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GMF - Component Services</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <!-- Logo Icon Web -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/LogoIcon.png">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/themes/bootstrap.min.css"/>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <!-- DataTables -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/alt/AdminLTE-bootstrap-social.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <!-- Bootstrap 3.3.6 -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.css"/>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css"> --> 
    <!-- JQwidget -->
    <!-- <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.base.css">
    <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.darkblue.css"> -->
    <!-- Jquery -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>
    <!-- JavaScript -->
    <script src="<?= base_url() ?>assets/plugins/alertifyjs/alertify.min.js"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/alertify.min.css"/>
	<link rel="stylesheet" href="<?= base_url(); ?>assets/Editor-1.7.4/css/editor.dataTables.min.css" />
       

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('assets/bower_components/slick/slick.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/new-season.css'); ?>">

    <style>
      /*.example-modal .modal {
          position: relative;
          top: auto;
          bottom: auto;
          right: auto;
          left: auto;
          display: block;
          z-index: 1;
      }

      .example-modal .modal {
          background: transparent !important;
      }*/
      .title-hr {
          margin-bottom: 0px;
      }

      body {
          margin: 0;
          font-family: Roboto,sans-serif;
          font-size: 12px;
          font-weight: 300;
          line-height: 1.571429;
          color: #37474f;
          text-align: left;
          background-color: #fff;
      }

      .treeview-menu>li>a {
          padding: 5px 5px 5px 15px;
          display: block;
          font-size: 13px;
      }

      .nav-tabs > li {
          float: left;
          margin-bottom: -1px;
          font-size: 15px;
          font-family: Roboto,sans-serif;
          font-weight: 300;
      }

      .skin-black .sidebar-menu>li.active>a {
          border-left-color: #fff;
          /*border-top-color: #fff;
          border-bottom-color: #fff;*/
      }
      .skin-black .sidebar-menu>li:hover>a, .skin-black .sidebar-menu>li.active>a, .skin-black .sidebar-menu>li.menu-open>a {
          color: #fff;
          background: #1e282c;
      }
      .skin-black .sidebar-menu>li>a {
          border-left: 5px solid transparent;
         /* border-top: 1px solid transparent;
          border-bottom: 1px solid transparent;*/
      }

      #overlay{
        position:fixed;
        z-index:99999;
        top:0;
        left:0;
        bottom:0;
        right:0;
        background:rgba(0,0,0,0.9);
        transition: 1s 0.4s;
      }
      #progress{
        height:1px;
        background:#fff;
        position:absolute;
        width:0;
        top:50%;
      }
      #progstat{
        font-size:0.7em;
        letter-spacing: 3px;
        position:absolute;
        top:50%;
        margin-top:-40px;
        width:100%;
        text-align:center;
        color:#fff;
      }

        .skin-black-light .sidebar-menu>li>a {
            border-left: 8px solid transparent;
           /* border-top: 1px solid transparent;
            border-bottom: 1px solid transparent;*/
        }

        .skin-black-light .sidebar-menu>li.active>a {
            font-weight: 600;
            border-left-color: #00c0ef;
        }
        .skin-black-light .sidebar-menu>li:hover>a, .skin-black-light .sidebar-menu>li.active>a {
            color: #000;
            background: #ecf0f5;
            /*border-left: 5px solid transparent;*/
        }

        .skin-black-light .sidebar-menu .treeview-menu>li>a {
            color: #000;
        }
        .skin-black-light .sidebar-menu .treeview-menu>li>a:hover {
            color: #777;
        }

        #overlay{
          position:fixed;
          z-index:99999;
          top:0;
          left:0;
          bottom:0;
          right:0;
          background:rgba(0,0,0,0.9);
          transition: 1s 0.4s;
        }
        #progress{
          height:1px;
          background:#fff;
          position:absolute;
          width:0;
          top:50%;
        }
        #progstat{
          font-size:0.7em;
          letter-spacing: 3px;
          position:absolute;
          top:50%;
          margin-top:-40px;
          width:100%;
          text-align:center;
          color:#fff;
        }

      /*div.dataTables_wrapper div.dataTables_processing {
         top: 50%;
      }*/
      div.dataTables_processing { z-index: 1; }
    </style>

        .widget-user .widget-user-image>img {
            width: 140px;
            height: auto;
            border: 3px solid #fff;
        }

        /*div.dataTables_wrapper div.dataTables_processing {
           top: 50%;
        }*/
        div.dataTables_processing { z-index: 1; }

        </style>
        <!-- Jquery -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>

        <!-- JavaScript -->
    <script src="<?= base_url() ?>assets/plugins/alertifyjs/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/alertifyjs/css/alertify.min.css"/>

    <!-- For the plug-in dependencies -->
    <script type="text/javascript" src='http://code.jquery.com/jquery-latest.min.js'></script>
   
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
     -->
     <script type="text/javascript">
        //  $(window).load(function() {
        //     // Animate loader off screen
        //     $(".se-pre-con").fadeOut();
        // });
      alertify.set('notifier','position', 'top-center');
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
      // (function(){
      //   // $(window).load(function() {
      //   //   // Animate loader off screen
      //   //   $(".se-pre-con").fadeOut("slow");
      //   // });
      $(function() { // waiting for the page to bo be fully loaded
        $('.se-pre-con').fadeOut('slow', function() { // fading out preloader
          $(this).remove(); // removing it from the DOM
        });
      });
    </script>

    <script type="text/javascript" src="<?= base_url()?>assets/multiple-emails.js"></script>
    <link type="text/css" rel="stylesheet" href="<?= base_url()?>assets/multiple-emails.css" />

    <script src="<?= base_url()?>assets/notify.min.js"></script>
    <script src="<?= base_url()?>assets/notify.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>                       
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
  </head>



    <!-- <body class="hold-transition skin-black-light sidebar-mini fixed"> -->
    <body class="hold-transition skin-black sidebar-mini sidebar-collapse fixed">
        <!-- <div id="overlay">
            <div id="progstat"></div>
            <div id="progress"></div>
        </div> -->
        <div class="se-pre-con"></div>
        <div class="wrapper">
            <header class="main-header">

                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b>
                    <b>CRM</b> Component</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                  
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown notifications-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning notif"></span>
                              </a>
                              <ul class="dropdown-menu">
                                <li>
                                  <ul class="menu">
                                    
                                  </ul>
                                </li>
                              </ul>
                            </li>
                            <?php
                            $CI =& get_instance();
                            $CI->load->model('Pooling_model', '', TRUE);
                            $jumlah_order = $CI->Pooling_model->get_counter_new();
                            ?>
                            <!-- PASTE NOTIFIKASI DI SINI -->
                            <!--  <?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                                <li style="padding-top: 7px; background-color: #444444;">
                                    <select id="select2" class="form-control" style="width: 100%;>
                                        <option value="">All Customer</option>
                                    </select>
                                </li>
                            <?php }?> -->
                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <!-- The user image in the navbar-->
                                  <img src="<?php echo base_url() ?>assets/dist/img/user/avatar5.png<?php //if($this->session->userdata('log_sess_user_image')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="user-image" alt="User Image">
                                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                  <span class="hidden-xs"><?php echo $this->session->userdata('log_sess_username'); ?></span>
                                </a>
                                <ul class="dropdown-menu" style="width:300px;">
                                  <!-- The user image in the menu -->
                                  <li class="user-header" style="height:200px;">
                                    <img src="<?php echo base_url() ?>assets/dist/img/user/avatar5.png<?php //if($this->session->userdata('log_sess_userimage')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="img-circle" alt="User Image">
                                    <p>
                                      <?php echo $this->session->userdata('log_sess_name').' - '.$this->session->userdata('log_sess_user_role'); ?>
                                    </p>
                                    <!-- <p class="text-left">
                                      <?php if($this->session->userdata('log_sess_id_user_role')==1) { ?>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } elseif($this->session->userdata('log_sess_id_user_role')==2) { ?>
                                        <small>User Group : <?php echo $this->session->userdata('log_sess_user_group'); ?></small>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } elseif($this->session->userdata('log_sess_id_user_role')==3) { ?>
                                        <!-- <small>User Group : <?php //echo $this->session->userdata('log_sess_user_group'); ?></small> -->
                                        <!-- <small>Cust. Name : <?php echo $this->session->userdata('log_sess_customer_name'); ?></small>
                                        <small>Last Login : <?php echo $this->session->userdata('log_sess_last_login'); ?></small>
                                      <?php } ?>
                                    </p> -->
                                  </li>
                                  <!-- Menu Footer-->
                                  <li class="user-footer">
                                    <!-- <div class="pull-left">
                                      <a href="<?php echo base_url('Profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                                    </div> -->
                                    <div class="pull-right">
                                      <a href="<?php echo base_url('index.php/Logout'); ?>" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                  </li>
                                </ul>
                            </li>
                            <!-- / User Account Menu -->
                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() ?>assets/dist/img/user/avatar5.png<?php //if($this->session->userdata('log_sess_user_image')) { echo $this->session->userdata('log_sess_userimage'); } else { echo 'user-man.png'; } ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('log_sess_name') ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                            <!-- <a href="#"><i class="fa fa-user"></i> Administrator</a> -->
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                      <?php
                      $data_menu = json_decode($_COOKIE['menu'],true);
                      $modul = array_keys($data_menu);
                      
                      for($a=0;$a<count($modul);$a++){
                        $menu_active = explode("=", $modul[$a]);
                        if(strpos($menu_active[2], $this->uri->segment(1)) !== false){
                          $active = "active";
                        }else{
                          $active = "";
                        }
                      ?>
                      <?php if(count($data_menu[$modul[$a]]) == 1){ $menu=explode("=",$data_menu[$modul[$a]][0]['m']);?>
                      <li class="# <?= $active?>">
                      <a href="<?= base_url("index.php")."/$menu[1]"?>">
                      <?php } else { echo "<li class='treeview $active'><a href='javascript:;'>"; }?>
                          <i class="<?php $logo = explode("=", $modul[$a]); echo $logo[1]?>"></i> <span><?= $logo[0]?></span>
                          <?php if(count($data_menu[$modul[$a]]) != 1){?>
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                          <?php }?>
                        </a>
                        <?php 
                          if(count($data_menu[$modul[$a]]) != 1){
                            echo "<ul class='treeview-menu'>";
                            for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                              $menu2 = explode("=", $data_menu[$modul[$a]][$b]['m']);
                        ?>
                          <li>
                            <a href="<?= base_url('index.php')."/$menu2[1]"?>"> 
                              <i class="<?= $data_menu[$modul[$a]][$b]['l']?>"></i>
                              <span><?= $menu2[0]?></span>
                            </a>
                          </li>
                        <?php
                            }
                            echo "</ul>";
                          }
                        ?>
                      </li>
                      <?php
                      }
                      ?>














                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.1.0
                </div>
                <strong>Develop By</strong> PT Sinergi Informatika Semen Indonesia
            </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div>
        <!-- ./wrapper -->
        <script type="text/javascript">
          $(document).ready(function () {

            var level_login = "<?= $this->session->userdata('log_sess_level_user_role')?>";
            $('.menu').html("");
            $.ajax({
              url: "<?= base_url()?>index.php/Loan_exchange/summary_loan_exchange",
              type: "POST",
              success: function (resp) { 
                resp = JSON.parse(resp);
                var HTML = '';
                var TOTAL = resp.length;
                $.each(resp, function (i, item) {

                    if(item.JENIS == "loan"){
                      var URL = "<?= base_url()?>index.php/Loan_exchange/loan_request";
                      if(item.STATUS == "request"){
                          status = 'New Request Loan';
                      }else if(item.STATUS == "process"){
                          status = 'Request Loan Waiting Process';
                      }else{
                          status = 'Request Loan Available';
                      }
                    }else{
                      var URL = "<?= base_url()?>index.php/Loan_exchange/exchange_request";
                      if(item.STATUS == "request"){
                          status = 'New Request Exchange';
                      }else if(item.STATUS == "process"){
                          status = 'Request Exchange Waiting Process';
                      }else{
                          status = 'Request Exchange Available';
                      }
                    }

                    HTML += 
                        '<li>'+
                          '<a href="'+URL+'">'+
                            '<i class="fa fa-warning text-yellow"></i> <span>'+item.JUMLAH+'</span> '+status+''+
                          '</a>'+
                        '</li>';
                });

                $('.menu').append(HTML);
                if(TOTAL == 0){
                  $('.notif').hide();
                }else{
                  $('.notif').show();
                  $('.notif').text(TOTAL);
                }
              }
            });

          });
        </script>


        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
        <!-- FastClick -->
        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script> -->
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <!-- SlimScroll -->

        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
        <!-- ChartJS -->
        <!-- <script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.js"></script> -->
        <!-- <script src="<?php //echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    	<script src="<?php //echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script> -->

        <script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
        <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>

    
    

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"> -->
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
        
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
		
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/pie-chart.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/Editor-1.7.4/js/dataTables.editor.min.js" type="text/javascript"></script>
       


       <!--  <script>
		$(function () {
		$("#example").DataTable();
		});
		</script> -->

       <!--  <script>
            $(document).ready(function(){
              $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
              });

                $('#select2').on('change', function(){
                  var ID_CUSTOMER = $(this).val();
                

                  console.log(ID_CUSTOMER);
                });

                // setCustomerSearch();

            });

            function setCustomerSearch($search = ''){
                var sSelect = $('#select2').select2({
                     placeholder: "All Customers"
                });
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url("api/Customers/datalist") ?>/'+$search,
                    dataType: 'json'
                }).then(function (data) {
                    // create the option and append to Select2

                    $.each(data, function(index, el){

                        var option = new Option(el.COMPANY_NAME, el.ID_CUSTOMER, true, true);
                        sSelect.append(option);
                    });

                    // manually trigger the `select2:select` event
                    // studentSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         id: data.ID_CUSTOMER,
                    //         text: data.COMPANY_NAME,
                    //     }
                    // });
                });
            }
        </script> -->


        <!-- New Season -->
        
        <script src="<?= base_url('assets/bower_components/slick/slick.min.js'); ?>"></script>

        <script>
          document.addEventListener("DOMContentLoaded", function(event) {
            $('.welcome-slider').slick({
              dots: true,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              adaptiveHeight: true
            });
          });

          function notifikasi() {
            $('.menu').html("");
              $.ajax({
                url: "<?= base_url()?>index.php/Loan_exchange/summary_loan_exchange",
                type: "POST",
                success: function (resp) { 
                  resp = JSON.parse(resp);
                  var HTML = '';
                  var TOTAL = resp.length;
                  $.each(resp, function (i, item) {

                      if(item.JENIS == "loan"){
                        var URL = "<?= base_url()?>index.php/Loan_exchange/loan_request";
                        if(item.STATUS == "request"){
                            status = 'New Request Loan';
                        }else if(item.STATUS == "process"){
                            status = 'Request Loan Waiting Process';
                        }else{
                            status = 'Request Loan Available';
                        }
                      }else{
                        var URL = "<?= base_url()?>index.php/Loan_exchange/exchange_request";
                        if(item.STATUS == "request"){
                            status = 'New Request Exchange';
                        }else if(item.STATUS == "process"){
                            status = 'Request Exchange Waiting Process';
                        }else{
                            status = 'Request Exchange Available';
                        }
                      }

                      HTML += 
                          '<li>'+
                            '<a href="'+URL+'">'+
                              '<i class="fa fa-warning text-yellow"></i> <span>'+item.JUMLAH+'</span> '+status+''+
                            '</a>'+
                          '</li>';
                  });

                  $('.menu').append(HTML);
                  if(TOTAL == 0){
                    $('.notif').hide();
                  }else{
                    $('.notif').show();
                    $('.notif').text(TOTAL);
                  }
                }
              });
          }

          setInterval(function(){
            notifikasi();
          }, 10000);
        </script>

    </body>
    
</html>
