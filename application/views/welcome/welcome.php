<!-- Main content -->
<section class="content welcome">
    <div class="col-lg-12 col-md-12 col-xs-12 carousel-container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <?php for($b=1;$b<=9;$b++){?>
          <li data-target="#myCarousel" data-slide-to="<?= $b?>"></li>
          <?php }?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?= base_url()?>assets/dist/img/slider/1.jpg" style="width:100%;">
          </div>

          <?php for($a=2;$a<=10;$a++){?>
          <div class="item">
            <img src="<?= base_url()?>assets/dist/img/slider/<?= $a?>.jpg" style="width:100%;">
          </div>
          <?php }?>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</section>



