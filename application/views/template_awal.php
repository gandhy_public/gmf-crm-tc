<!DOCTYPE html>
<html>
    <head>                
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
        <script src=" https://code.jquery.com/jquery-1.12.4.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GMF - Component Services</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Logo Icon Web -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/LogoIcon.png">        
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link type="text/css" rel="stylesheet" href="<? echo base_url(); ?>includes/css/bootstrap.css"/>
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- css datatable -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <!-- Morris charts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
        <!-- JQwidget -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.base.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jqwidgets/styles/jqx.darkblue.css"> 
    </head>

    <body class="hold-transition skin-black sidebar-mini fixed">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="index.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b> <b>CRM</b>Component</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            
                            <!-- Notifications: style can be found in dropdown.less -->
                            
                            <!-- Tasks: style can be found in dropdown.less -->
                            
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>assets/dist/img/user1-128x128.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Admin Component Services</span>
                                </a>

                                <!-- Profile Click Sign Out -->
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>assets/dist/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                                        <p>Riyan Sasmytha</p>
                                        <p>
                                           <i class="fa fa-user"></i> Profile Clik Administrator
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>assets/dist/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Admin TC</p>
                            <a href="#"><i class="fa fa-user"></i> Administrator</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li <?php if (isset($index)){ ?> class="active treeview menu-open" <?php } ?>>
                            <a href="<?php echo base_url() ?>administrator/home_control">
                                <i class="fa fa-home"></i> <span>Home</span>
                            </a>
                        </li>
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>TC Service Support</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li <?php if (isset($pooling_tabs)){ ?> class="active" <?php } ?>>
                                    <a href="<?php echo base_url() ?>administrator/pooling_control/dashboard">
                                        <i class="fa fa-indent"></i> Pooling PBTH</a></li>

                                <li <?php if (isset($loan_index)){ ?> class="active" <?php } ?>>
                                    <a href="<?php echo base_url() ?>administrator/loan_control/loan_index">
                                        <i class="fa fa-refresh"></i> Loan Excange</a></li>

                                <li class="treeview"> 
                                    <a href="#"><i class="fa fa-google-wallet"></i> Retail
                                      <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                      </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li <?php if (isset($index2)){ ?> class="active" <?php } ?>>
                                            <a href="<?php echo base_url() ?>administrator/retail_control/all_retail">
                                            <i class="fa fa-database"></i> Order</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-money"></i> TAT</a></li>
                                    </ul>
                                </li>

                                <li class="treeview">
                                    <a href="#"><i class="fa fa-gears"></i> Landing Gear
                                        <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                      </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li <?php if (isset($dashboard_menu)){ ?> class="active" <?php } ?>>
                                            <a href="<?php echo base_url() ?>administrator/dashboard">
                                            <i class="fa fa-folder-o"></i> Project List</a>
                                        </li>
                                        <li><a href="<?php echo base_url() ?>administrator/dashboard/csi_survey">
                                            <i class="fa fa-table"></i> CSI Survey</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li <?php if (isset($list_control)){ ?> class="active" <?php } ?>>
                            <a href="<?php echo base_url() ?>administrator/user_control/list_user">
                                <i class="fa fa-users"></i> <span> User Setting</span>
                                <!-- <span class="pull-right-container"> 
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span> -->
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2018.</strong> All rights
                reserved.
            </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <?php if(isset($is_grocery)): ?>
            <?php foreach($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
            <?php foreach($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if(!isset($is_grocery)): ?>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script> 
        <?php endif; ?>

        


        <!-- <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script> -->
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap  -->
        <script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- ChartJS -->
        <script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard2.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  -->
        <script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <!-- <script type="text/javascript" charset="utf-8" src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
        <script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/raphael/raphael.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
        
        <script>
            $(document).ready(function() {
                $('#tabel-data').DataTable( {
                    dom: 'Bfrtip',
                    scrollX: true,
                    scrollY: true,
                    buttons: [
                        /*'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'*/
                    ]
                } );
            } );
        </script>

        <script>
            $(document).ready(function(){
              $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
              });
            });
        </script>

        
    </body>
</html>
