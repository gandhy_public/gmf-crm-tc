<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <div class="box-header">
                      <!-- <h3 class="box-title"><?php //echo $title; ?></h3> -->
                      <div class="box-tools">

                      </div>
                    </div>
                    <div class="box-body no-padding">
                    <table id="tbcontract" class="table table-bordered table-hover table-striped" width="100%">
                      <thead style="background-color: #3c8dbc; color:#ffffff;">
                        <tr>
                          <th width="3%"><center> No </center></th>
                          <th width="12%"><center> Contract Number </center></th>
                          <th width="30%"><center> Customer </center></th>
                          <!-- <th width="20%"><center> Delivery Point </center></th>
                          <th width="20%"><center> Delivery Address </center></th> -->
                          <th width="35%"><center> Description </center></th>
                          <th width="8%"><center> Action </center></th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                    <br>

          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<!-- Form Edit Modal -->
<div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form method="post" action="<?= base_url('index.php/Contract/update')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Contract</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" name="id" id="id">
          <input type="hidden" name="ct_number" id="ct_number">
          <div class="form-group">
            <label class="col-sm-5 control-label">Description Contract</label>
            <div class="col-sm-7">
              <textarea style="min-height: 130px;" class="form-control input-sm" id="DESC_CONTRACT" name="DESC_CONTRACT" required></textarea>
            </div>
          </div>
          <br><br><br><br><br><br><br><br><br>
          <div class="form-group">
            <label class="col-sm-5 control-label">Customer</label>
            <div class="col-sm-7">
              <select class="form-control select2" id="CUSTOMER" name="CUSTOMER" style="width: 100%;" required>
                <?php
                  foreach ($customer_list as $row) {
                ?>
                <option value="<?php echo $row->ID_CUSTOMER ?>"><?php echo $row->COMPANY_NAME; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    
    $('#CUSTOMER').select2();
  });
  $(function () {
    $('#tbcontract').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_ct/')?>',
        columns: [       
            {data: 'no', name: 'no'},
            {data: 'contract_number', name: 'contract_number'},            
            {data: 'customer', name: 'customer', orderable: false},
            /*{data: 'delivery_point', name: 'delivery_point', orderable: false},
            {data: 'delivery_address', name: 'delivery_address', orderable: false},*/
            {data: 'description', name: 'description', orderable: false},
            {data: 'act', name: 'act', orderable: false}
        ],
    });

    $(".select2").select2();

    $("#tbcontract").on("click",".btn_del", function(){
        var id = $(this).data('x');
        var number = $(this).data('y');

        var span = document.createElement("span");
        span.innerHTML = "Apakah data CONTRACT dengan <b>CONTRACT NUMBER : "+number+"</b> akan dihapus ?";
        swal({
          title: "WARNING",
          content: span,
          //text: "Apakah data CONTRACT dengan CONTRACT NUMBER : "+number+" akan dihapus ?",
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?php echo base_url('index.php/contract/delete')?>",
                type: 'POST',
                data: {
                  'id':id,
                  'contract_number':number
                },
                success: function (response) {
                  //$('.content').append(response);
                  if (response) {
                    /*swal("Data berhasil dihapus!", {
                      icon: "success",
                    });*/
                    swal({
                      text: "Data berhasil dihapus!",
                      icon: "success",
                      showConfirmButton: true
                    }).then(function() {
                        window.location.href = "<?php echo base_url('index.php/Contract/list_contract')?>";
                    });
                  }else{
                    /*swal("Data berhasil dihapus!", {
                      icon: "success",
                    });*/
                    swal({
                      text: "Data gagal dihapus!",
                      icon: "error",
                      showConfirmButton: true
                    }).then(function() {
                        window.location.href = "<?php echo base_url('index.php/Contract/list_contract')?>";
                    });
                  }
                }
              });
            }else{
              swal("Data tidak dihapus!");
            }
        });
    });

    $("#tbcontract").on("click",".btn_edit", function(){
        var id = $(this).data('x');
        var number = $(this).data('y');

        var span = document.createElement("span");
        span.innerHTML = "Apakah data CONTRACT dengan <b>CONTRACT NUMBER : "+number+"</b> akan diedit ?";

        $('#exampleModalCenter').modal('hide');
        $.ajax({
          type: 'POST',
          url: '<?=base_url('index.php/api/Contract/detail_edit/')?>',
          data: { 
              'id': id, 
              'ct_number': number 
          },
          success: function(resp){
              var obj = JSON.parse(resp);
              $('#DESC_CONTRACT').val(obj.description);
              $("#CUSTOMER").val(obj.customer).change();
              $('#id').val(id);
              $('#ct_number').val(number);

          }
        });

        swal({
          title: "WARNING",
          content: span,
          html:true,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
              $('#exampleModalCenter').modal('show');
            }else{
              swal("Data tidak diedit!");
            }
        });
    });
  });

</script>
