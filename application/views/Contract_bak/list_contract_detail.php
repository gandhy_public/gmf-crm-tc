<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <div class="box-header contract">
              <div class="main-title">
                <div class="title"><?= $desc['contract_number']; ?></div>
              </div>
              <div class="detail">
                <div class="detail-item">
                  <span class="title">Description</span>
                  <p class="value"><?= $desc['contract_desc']; ?></p>
                </div>
                <div class="detail-item">
                  <span class="title">Delivery Point</span>
                  <p class="value"><?= $desc['delivery_point']; ?></p>
                </div>
                <div class="detail-item">
                  <span class="title">Delivery Address</span>
                  <p class="value"><?= $desc['delivery_address']; ?></p>
                </div>
              </div>
            </div>
            <div class="box-body" style="padding:0">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding:0">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                     <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                     <li role="presentation" ><a href="#content-upload" aria-controls="home" role="tab" data-toggle="tab">Upload Data</a></li>       
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="profile">
                       <div class="box">
                         <div class="box-body">
						            	  <!-- <button class="btn btn-flat bg-navy editor_create"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbPartNumber" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> No </th>
                                 <th> ATA </th>
                                 <th> Part Number </th>
                                 <th> Description </th>
                                 <th> ESS </th>
                                 <th> MEL </th>
                                 <th> Target SLA </th>
                                 <th> Unserviceable Return </th>
                                 <th> Service Level Category </th>
                                 <th> Target Service Level </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="home">
                        <div class="box">
                          <div class="box-body">
                            <!-- <button class="btn btn-flat bg-navy editor_create2"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbFleet" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff; width:100%;">
                                <tr>
                                   <th> No </th>
                                   <th> Aircraft Registration </th>
                                   <th> Type </th>
                                   <th> MSN </th>
                                   <th> MFG Date </th>
                                </tr>
                              </thead>
                              <tbody>

                               </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="deliv">
                        <div class="box">
                          <div class="box-body">
                            <!-- <button class="btn btn-flat bg-navy editor_create3"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbDeliv" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff; width:100%;">
                                <tr>
                                   <th> No </th>
                                   <th> Delivery Point Code </th>
                                   <th> Delivery Point Address </th>
                                </tr>
                              </thead>
                              <tbody>

                               </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="content-upload">
                        <div class="box">
                          <div class="box-body">
                            <form id="form-upload" enctype="multipart/form-data">
                              <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Upload Data</label>
                                  <div class="col-sm-9">
                                  <input type="file" class="form-control" name="files" id="files" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                                  <input type="hidden" name="url" value="<?php echo current_url();?>">
                                  <input type="hidden" name="ct_num" value="<?php echo $this->uri->segment(3)?>">
                                  <input type="hidden" name="ct_id" value="<?php echo $this->uri->segment(4)?>">
                                  </div>
                                </div>
                                <br><br>
                                <div class="form-group col-sm-12">
                                  <a href="<?php echo base_url("public/TEMPLATE_DETAIL_CONTRACT.xlsx");?>" style="cursor: pointer;">Download Format Upload <i class="fa fa-cloud-download"></i></a>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button>
                                <!-- <button type="button" class="pull-right btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button> -->
                              </div>
                            </form>
                          </div>
                        </div>
                     </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>
<script>
  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
  });
  
  $("#form-upload").submit(function(evt){
    swal({
      title: 'WARNING!',
      text: 'Dont refresh or reload this page, upload file is processing!',
      icon: 'warning',
      buttons: false
    });

    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    $('#button-save').text("Processing...");
      
    $.ajax({
      url: "<?php echo base_url('index.php/contract/cek_upload')?>",
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      enctype: 'multipart/form-data',
      processData: false,
      beforeSend: function() {
        $('#button-save').text("Processing...");
      },
      success: function (response) {
        if(response){
          swal({
            title: "WARNING",
            text: "Data detail contract sebelumnya akan dihapus jika Anda mengupload file baru, apakah Anda menyetujuinya ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?php echo base_url('index.php/contract/updel')?>",
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                  $('.content').append(response);
                }
              });
              /*swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
              });*/
            } else {
              swal("Data tidak ada perubahan!");
            }
          });
        }else{
          $.ajax({
            url: "<?php echo base_url('index.php/contract/upload')?>",
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function (response) {
              $('.content').append(response);
            }
          });
        }
      }
    });

    return false;
  });

  $(function () {	
    $('#tbPartNumber').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_pn/'.$this->uri->segment(4))?>',
        columns: [            
          {data: 'no', name: 'no'},            
          {data: 'ata', name: 'ata'},
          {data: 'pn', name: 'pn'},
          {data: 'desc', name: 'desc', orderable: false},
          {data: 'ess', name: 'ess'},
          {data: 'mel', name: 'mel'},
          {data: 'sla', name: 'sla'},
          {data: 'unserviceable_return', name: 'unserviceable_return'},
          {data: 'service_level_category', name: 'service_level_category'},
          {data: 'target_service_level', name: 'target_service_level'}
        ],
    });
	
    $('#tbFleet').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_flt/'.$this->uri->segment(4))?>',
        columns: [            
          {data: 'no', name: 'no'},            
          {data: 'regis', name: 'regis'},
          {data: 'type', name: 'type'},
          {data: 'msn', name: 'msn'},
          {data: 'mfg', name: 'mfg'},
        ],
    });
  });

</script>
