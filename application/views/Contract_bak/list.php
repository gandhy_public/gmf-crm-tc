<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<?php $this->load->view($nav_tabs); ?>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <h3 class="box-title"><?php echo $title; ?></h3>
              <div class="box-tools">
              </div>
            </div>
            <div class="box-body no-padding">
			          <table id="tbCategory" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Name</center></th>
                      <th><center>Email</center></th>
                      <th><center>Address</center></th>
                      <th><center>Act</center></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div>
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>

var table;

  $(function () {
    //datatables
    table = $('#tbCategory').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('api/customers')?>",
            "type": "POST",
            "dataSrc" : function(json){
              var return_data = [];

              json.draw = json.draw;
              json.recordsFiltered = json.recordsFiltered;
              json.recordsTotal = json.recordsTotal;

                for(var i=0;i< json.data.length; i++){
                  return_data.push({
                    0: json.data[i].NO,
                    9: json.data[i].RowNum,
                    1: json.data[i].COMPANY_NAME,
                    2: json.data[i].COMPANY_EMAIL,
                    3: json.data[i].COMPANY_ADDRESS
                  })
                }
                return return_data;
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [],
            "visible": false,
            "searchable": false
          },{
            "targets": -1,
            "className": "dt-center",
            "data": null,
            "defaultContent":
            '<button title="update" class="btUpdate btn btn-primary btn-xs" type="button"><i class="fa fa-edit"></i></button>'+
            '<button title="delete" class="btUpdate btn btn-danger btn-xs" type="button"><i class="fa fa-times"></i></button>'
          }]

    });
  });
</script>
