<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <div class="box-header">
              <h3><?= $desc?></h3>
              <!-- <h3 class="box-title"><?php //echo $title; ?></h3> -->
              <!-- <div class="box-tools"> -->
               <?php
                if (is_array($detailcontract )) {
                  foreach ($detailcontract as $row) {
                ?>
                <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="box box-solid">
                    <div class="box body">
                      <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Delivery Address
                      </h4>
                      <div class="media">
                        <div class="media-left">
                          <span class="info-box-icon"><i class="fa fa-truck"></i></span>
                        </div>
                        <div class="media-body">
                          <div class="clearfix">
                            <p><?= $row['DELIVERY_ADDRESS']?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="box box-solid">
                    <div class="box body">
                      <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Delivery Point
                      </h4>
                      <div class="media">
                        <div class="media-left">
                          <span class="info-box-icon"><i class="fa fa-street-view"></i></span>
                        </div>
                        <div class="media-body">
                          <div class="clearfix">
                            <p><?= $row['DELIVERY_POINT']?></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <?php }} ?>
              <!-- </div> -->
            </div>
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                     <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                     <li role="presentation" ><a href="#content-upload" aria-controls="home" role="tab" data-toggle="tab">Upload Data</a></li>       
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="profile">
                       <div class="box">
                         <div class="box-body">
						            	  <!-- <button class="btn btn-flat bg-navy editor_create"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbPartNumber" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> No </th>
                                 <th> ATA </th>
                                 <th> Part Number </th>
                                 <th> Description </th>
                                 <th> ESS </th>
                                 <th> MEL </th>
                                 <th> Target SLA </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="home">
                        <div class="box">
                          <div class="box-body">
                            <!-- <button class="btn btn-flat bg-navy editor_create2"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbFleet" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff; width:100%;">
                                <tr>
                                   <th> No </th>
                                   <th> Aircraft Registration </th>
                                   <th> Type </th>
                                   <th> MSN </th>
                                   <th> MFG Date </th>
                                </tr>
                              </thead>
                              <tbody>

                               </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="deliv">
                        <div class="box">
                          <div class="box-body">
                            <!-- <button class="btn btn-flat bg-navy editor_create3"><i class="fa fa-plus"></i><span>Create new record</span></button> -->
                            <table id="tbDeliv" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff; width:100%;">
                                <tr>
                                   <th> No </th>
                                   <th> Delivery Point Code </th>
                                   <th> Delivery Point Address </th>
                                </tr>
                              </thead>
                              <tbody>

                               </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="content-upload">
                        <div class="box">
                          <div class="box-body">
                            <form id="form-upload" enctype="multipart/form-data">
                              <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Upload Data</label>
                                  <div class="col-sm-9">
                                  <input type="file" class="form-control" name="files" id="files" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                                  <input type="hidden" name="url" value="<?php echo current_url();?>">
                                  <input type="hidden" name="ct_num" value="<?php echo $this->uri->segment(3)?>">
                                  <input type="hidden" name="ct_id" value="<?php echo $this->uri->segment(4)?>">
                                  </div>
                                </div>
                                <br><br>
                                <div class="form-group col-sm-12">
                                  <a href="<?php echo base_url("public/TEMPLATE_DETAIL_CONTRACT.xlsx");?>" style="cursor: pointer;">Download Format Upload <i class="fa fa-cloud-download"></i></a>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-flat bg-navy" id="button-save"> Save</button>
                                <!-- <button type="button" class="pull-right btn btn-flat btn-default"  data-dismiss="modal"><i class="fa fa-rotate-left"></i> Cancel</button> -->
                              </div>
                            </form>
                          </div>
                        </div>
                     </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>
<script>
  $("#form-upload").submit(function(evt){
    swal({
      title: 'WARNING!',
      text: 'Dont refresh or reload this page, upload file is processing!',
      icon: 'warning',
      buttons: false
    });

    evt.preventDefault();
    var formData = new FormData($(this)[0]);
    $('#button-save').text("Processing...");
      
    $.ajax({
      url: "<?php echo base_url('index.php/contract/cek_upload')?>",
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      enctype: 'multipart/form-data',
      processData: false,
      beforeSend: function() {
        $('#button-save').text("Processing...");
      },
      success: function (response) {
        if(response){
          swal({
            title: "WARNING",
            text: "Data detail contract sebelumnya akan dihapus jika Anda mengupload file baru, apakah Anda menyetujuinya ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?php echo base_url('index.php/contract/updel')?>",
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                  $('.content').append(response);
                }
              });
              /*swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
              });*/
            } else {
              swal("Data tidak ada perubahan!");
            }
          });
        }else{
          $.ajax({
            url: "<?php echo base_url('index.php/contract/upload')?>",
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function (response) {
              $('.content').append(response);
            }
          });
        }
      }
    });

    return false;
  });

  $(function () {
	var editor = new $.fn.dataTable.Editor( {
    "ajax": {
			"url" : "<?=base_url('index.php/api/Contract/edit_delete_pn/')?>",
			"data": function ( d ) {
				d.ID_CONTRACT = <?= $this->uri->segment(4) ?>;
      }
		},
    "table": "#tbPartNumber",
		"idSrc": "id",
        "fields": [ {
                "label": "ATA :",
                "name": "ata"
            }, {
                "label": "Part Number :",
                "name": "pn"
            }, {
                "label": "Description :",
                "name": "desc"
            }, {
                "label": "ESS :",
                "name": "ess"
            }, {
                "label": "MEL:",
                "name": "mel"
            },
			{
                "label": "SLA:",
                "name": "waktu_sla",
            },
			 {
                "label": "Satuan SLA:",
                "name": "satuan_sla",
				"type":  "select",
                "options": [
                    { label: "Hours", value: "hours" },
                    { label: "Days", value: "days" },
                ]
            }
        ]
    } );
 
    // New record
    $('button.editor_create').on('click', function (e) {
        e.preventDefault();
 
        editor.create( {
            title: 'Create new record',
            buttons: 'Add'
        } );
    } );
 
    // Edit record
    $('#tbPartNumber').on('click', 'button.editor_edit', function (e) {
        e.preventDefault();
 
        editor.edit( $(this).closest('tr'), {
            title: 'Edit record',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    $('#tbPartNumber').on('click', 'button.editor_remove', function (e) {
        e.preventDefault();
 
        editor.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this record?',
            buttons: 'Delete'
        } );
    } );
	
    $('#tbPartNumber').DataTable({
        serverSide: true,
        processing: true,
        // ajax: '<?=base_url('index.php/api/Contract/detail_pn/'.$this->uri->segment(4))?>',
        ajax: {
          url: '<?= base_url('index.php/api/Contract/detail_pn/'.$this->uri->segment(4)); ?>',
          type: "POST"
        },
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'ata', name: 'ata'},
            {data: 'pn', name: 'pn'},
            {data: 'desc', name: 'desc', orderable: false},
            {data: 'ess', name: 'ess'},
            {data: 'mel', name: 'mel'},
            {data: 'sla', name: 'sla'},
			// {
      //           data: null,
      //           className: "center",
      //           defaultContent: '<button class="btn btn-success btn-xs btn_edit editor_edit"><i class="fa fa-edit"></i></button>  <button class="btn btn-danger btn-xs btn_del editor_remove"><i class="fa fa-trash"></i></button>'
      //       }
        ],
    });

	
	editor2 = new $.fn.dataTable.Editor( {
		"ajax": {
			"url" : "<?=base_url('index.php/api/Contract/edit_delete_fleet/')?>",
			"data": function ( d ) {
				d.ID_CONTRACT = <?= $this->uri->segment(4) ?>;
			  }
		},
        "table": "#tbFleet",
		"idSrc": "id",
        "fields": [ {
                "label": "Aircraft Registration :",
                "name": "regis"
            }, {
                "label": "Type :",
                "name": "type"
            }, {
                "label": "MSN :",
                "name": "msn"
            }, {
                "label": "MFG Date :",
                "name": "mfg",
				"type": "datetime"
            }
        ]
    } );
 
	// New record
    $('button.editor_create2').on('click', function (e) {
        e.preventDefault();
 
        editor2.create( {
            title: 'Create new record',
            buttons: 'Add'
        } );
    } );
	
    // Edit record
    $('#tbFleet').on('click', 'button.editor_edit', function (e) {
        e.preventDefault();
 
        editor2.edit( $(this).closest('tr'), {
            title: 'Edit record',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    $('#tbFleet').on('click', 'button.editor_remove', function (e) {
        e.preventDefault();
 
        editor2.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this record?',
            buttons: 'Delete'
        } );
    } );
	
    $('#tbFleet').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_flt/'.$this->uri->segment(4))?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'regis', name: 'regis'},
            {data: 'type', name: 'type'},
            {data: 'msn', name: 'msn'},
            {data: 'mfg', name: 'mfg'},
			// {
      //           data: null,
      //           className: "center",
      //           defaultContent: '<button class="btn btn-success btn-xs btn_edit editor_edit"><i class="fa fa-edit"></i></button>  <button class="btn btn-danger btn-xs btn_del editor_remove"><i class="fa fa-trash"></i></button>'
      //       }
        ],
    });
	
	editor3 = new $.fn.dataTable.Editor( {
		"ajax": {
			"url" : "<?=base_url('index.php/api/Contract/edit_delete_deliv/')?>",
			"data": function ( d ) {
				d.ID_CONTRACT = <?= $this->uri->segment(4) ?>;
			  }
		},
        "table": "#tbDeliv",
		"idSrc": "id",
        "fields": [ {
                "label": "Delivery Point Code :",
                "name": "point",
				"type":  "textarea",
            }, {
                "label": "Delivery Point Address :",
                "name": "address",
				"type":  "textarea",
            }
        ]
    } );
 
	// New record
    $('button.editor_create3').on('click', function (e) {
        e.preventDefault();
 
        editor3.create( {
            title: 'Create new record',
            buttons: 'Add'
        } );
    } );
	
    // Edit record
    $('#tbDeliv').on('click', 'button.editor_edit', function (e) {
        e.preventDefault();
 
        editor3.edit( $(this).closest('tr'), {
            title: 'Edit record',
            buttons: 'Update'
        } );
    } );
 
    // Delete a record
    $('#tbDeliv').on('click', 'button.editor_remove', function (e) {
        e.preventDefault();
 
        editor3.remove( $(this).closest('tr'), {
            title: 'Delete record',
            message: 'Are you sure you wish to remove this record?',
            buttons: 'Delete'
        } );
    } );

    $('#tbDeliv').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/api/Contract/detail_dlv/'.$this->uri->segment(4))?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'point', name: 'point'},
            {data: 'address', name: 'address'},
			{
                data: null,
                className: "center",
                defaultContent: '<button class="btn btn-success btn-xs btn_edit editor_edit"><i class="fa fa-edit"></i></button>  <button class="btn btn-danger btn-xs btn_del editor_remove"><i class="fa fa-trash"></i></button>'
            }
        ],
    });

    //$("#iduserrole").select2({ width: 'resolve' });
    

    //$("#iduserrole").select2({ width: 'resolve' });
    //$(".select2").select2();
  });

</script>
