<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
  <?php 
      if($this->session->flashdata('alert_failed')){ 
          echo '<div id="alert_failed" class="callout callout-danger"><p>'.$this->session->flashdata('alert_failed').'</p></div>'; 
      }elseif($this->session->flashdata('alert_success')){
          echo '<div id="alert_success" class="callout callout-success"><p>'.$this->session->flashdata('alert_success').'</p></div>'; 
      }
  ?>
  <div class="row">
    <div class="col-md-12">
      <section class="content">
        <div class="box-header">
          <div class="box-tools">
          </div>
        </div>
        <div class="box-body no-padding">
          <form id="form-contract" enctype="multipart/form-data" method="post" action="<?= site_url('contract/create') ?>" class="form-horizontal">
            <div class="section">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Contract Number</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control input-sm" id="CONTRACT_NUMBER" name="CONTRACT_NUMBER" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Customer</label>
                    <div class="col-sm-7">
                      <select id="CUSTOMER" name="CUSTOMER" class="form-control" required>
                        <option value="">Choose Customer</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Description Contract</label>
                    <div class="col-sm-7">
                      <textarea style="min-height: 130px;" class="form-control input-sm" id="DESC_CONTRACT" name="DESC_CONTRACT" required></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Delivery Point</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control input-sm" id="DELIVERY_POINT" name="DELIVERY_POINT" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label">Delivery Address</label>
                    <div class="col-sm-7">
                      <textarea style="min-height: 130px;" class="form-control input-sm" id="DELIVERY_ADDRESS" name="DELIVERY_ADDRESS" required></textarea>
                    </div>
                  </div>
                  <div class="form-group pull-right">
                    <button class="btn btn-primary" type="submit" id="btnSubmitContract" > <i class="fa fa-save"> </i> Create Contract</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="section">
              <div class="col-md-12">
                <div>
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Fleet</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Part Number List</a></li>
                  </ul>
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                      <div class="row" style="padding-bottom: 30px; padding-top: 10px;">
                        <div class="box">
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                      <label class="">Aircraft Registration</label>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <label class="">Type</label>
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <label class="">MSN</label>
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <label class="">MFG DATE</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div id="form-fleet" class="col-md-12">
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="aircraft_registration" name="aircraft_registration">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="type" name="type">
                                  </div>
                                </div>
                                <div class="col-md-2">
                                  <div class="col-md-12">
                                    <input class="form-control" id="msn" name="msn">
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <div class="col-md-12">
                                    <input class="form-control" id="mfggate" name="mfggate" type="text"/>
                                  </div>
                                </div>
                                <div class="col-md-1">
                                  <button class="btn btn-primary" id="btnFleet" type="button" ><i class="fa fa-plus"></i></button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="box">
                          <div class="box-body">
                            <table id="tbFleet" class="table table-bordered table-hover table-striped">
                              <thead>
                                <tr>
                                  <th> Aircraft Registration </th>
                                  <th> Type </th>
                                  <th> MSN </th>
                                  <th> MFG Date </th>
                                  <th> Act </th>
                                </tr>
                              </thead>
                              <tbody>

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                      <div class="box">
                        <div class="box-body">
                          <div class="row">
                            <div class="row">
                              <form class=" form-horizontal">
                                <div class="col-md-12" id="form-pn">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">ATA</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="ata" id="ata">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">Part Number</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" id="part_number" name="part_number">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">Description</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="description" id="description">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">ESS</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="ess" id="ess">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">MEL</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="mel" id="mel">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-5">
                                    <div class="form-group form-inline">
                                      <label class="col-sm-5 control-label">TARGET SLA</label>
                                      <div class="col-sm-3">
                                        <input type="text" class="form-control input-sm" name="sla" id="sla">
                                      </div>
                                      <div class="col-sm-3">
                                        <select class="form-control input-sm select2" id="day_sla" name="day_sla">
                                          <option value="hours">Hours</option>
                                          <option value="days">Days</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">TARGET UNSERVICEABLE RETURN</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="tur" id="tur">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">SERVICE LEVEL CATEGORY</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="slc" id="slc">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-5 control-label">TARGET SERVICE LEVEL</label>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" name="tsl" id="tsl">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                      <button class="btn btn-primary" id="btnPN" type="button" ><i class="fa fa-plus"></i></button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="box">
                        <div class="box-body">
                          <table id="tbPartNumber" class="table table-bordered table-hover table-striped">
                            <thead>
                              <tr>
                                <th> ATA </th>
                                <th> Part Number </th>
                                <th> Description </th>
                                <th> ESS </th>
                                <th> MEL </th>
                                <th> Target SLA </th>
                                <th> Target Unserviceable Return </th>
                                <th> Service Level Category </th>
                                <th> Target Service Level </th>
                                <th> Act </th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="pull-right">
                    <button class="btn btn-primary" type="submit" id="btnSubmitContract" > <i class="fa fa-save"> </i> Create Contract</button>
                  </div>
                </div>
              </div>
            </div> -->
          </form>
        </div>
      </section>
    </div>
  </div>
</section>





<script>
  var table;
  var tableFleet;
  var tablePartNumber;
  var tableDeliv;

  $(document).ready(function(){
    setTimeout(function() {
        $('#alert_failed').fadeOut('slow');
        $('#alert_success').fadeOut('slow');
    }, 8000);
    
    var date_input=$('#mfggate'); 
    var options={
      format: 'yyyy/mm/dd',
      todayHighlight: true,
      autoclose: true,
    };
    date_input.datepicker(options);

    $('#CUSTOMER').select2({
      placeholder: 'Select Customer',
      width: '100%',
      minimumInputLength: 2,
      triggerChange: true,
      allowClear: true,
      ajax: {
        url: "<?=base_url()?>index.php/api/Contract/select_customer",
        dataType: 'json',
        delay: 450,
        processResults: function (data) {
          return {
            results: data
          }
        },
        cache: true
      }
    });
  });

  $(function(){
    //datatables
    tableFleet = $('#tbFleet').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
    tablePartNumber = $('#tbPartNumber').DataTable({
      "paging":   false,
      "ordering": false,
      "info":     false
    });
    tableFleet.clear().draw();
    tablePartNumber.clear().draw();

    $('#form-contract').one('submit', function (e) {
      e.preventDefault();
      swal({
        title: "Are you sure want to Submit?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          var dataFleet       = tableFleet.rows().data();
          var dataPartNumber  = tablePartNumber.rows().data();

          $.each(dataFleet, function(index, val){
            AIRCRAFT_REGISTRATION = $("<input>").attr('hidden', 'true').attr("name", "fleet[AIRCRAFT_REGISTRATION][]").val(val[0]);
            $('#form-contract').append(AIRCRAFT_REGISTRATION);
            TYPE = $("<input>").attr('hidden', 'true').attr("name", "fleet[TYPE][]").val(val[1]);
            $('#form-contract').append(TYPE);
            MSN = $("<input>").attr('hidden', 'true').attr("name", "fleet[MSN][]").val(val[2]);
            $('#form-contract').append(MSN);
            MFN_GATE = $("<input>").attr('hidden', 'true').attr("name", "fleet[MFN_GATE][]").val(val[3]);
            $('#form-contract').append(MFN_GATE);
          });

          $.each(dataPartNumber, function(index, val){
            ATA = $("<input>").attr('hidden', 'true').attr("name", "pn[ATA][]").val(val[0]);
            $('#form-contract').append(ATA);
            ID_PN = $("<input>").attr('hidden', 'true').attr("name", "pn[ID_PN][]").val(val[1]);
            $('#form-contract').append(ID_PN);
            DESCRIPTION = $("<input>").attr('hidden', 'true').attr("name", "pn[DESCRIPTION][]").val(val[2]);
            $('#form-contract').append(DESCRIPTION);
            ESS = $("<input>").attr('hidden', 'true').attr("name", "pn[ESS][]").val(val[3]);
            $('#form-contract').append(ESS);
            MEL = $("<input>").attr('hidden', 'true').attr("name", "pn[MEL][]").val(val[4]);
            $('#form-contract').append(MEL);
            TARGET_SLA = $("<input>").attr('hidden', 'true').attr("name", "pn[TARGET_SLA][]").val(val[5]);
            $('#form-contract').append(TARGET_SLA);
            TUR = $("<input>").attr('hidden', 'true').attr("name", "pn[TUR][]").val(val[6]);
            $('#form-contract').append(TUR);
            SLC = $("<input>").attr('hidden', 'true').attr("name", "pn[SLC][]").val(val[7]);
            $('#form-contract').append(SLC);
            TSL = $("<input>").attr('hidden', 'true').attr("name", "pn[TSL][]").val(val[8]);
            $('#form-contract').append(TSL);
          });
          $(this).submit();
        }else{
          location.reload();
        }
      });
    });

    ////=============================
    // INISTIALISASI TABLE FLEET
    $('#btnFleet').on('click', function(e){
      $type = $('#form-fleet #type').val();
      $msn = $('#form-fleet #msn').val();
      $mfggate = $('#form-fleet #mfggate').val();
      $aircraft_registration = $('#form-fleet #aircraft_registration').val();

      if ($type && $msn && $mfggate && $aircraft_registration) {
          $data = {
            '0' : $aircraft_registration,
            '1' : $type,
            '2' : $msn,
            '3' : $mfggate,
            '4' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> ',
            '10' : $aircraft_registration,
          };
          addRow(tableFleet, $data);

          $("#form-fleet :input").val("");
          $('#form-fleet #aircraft_registration').focus();
      }else if (!$type) {
        $('#type').focus();
      }else if (!$msn) {
        $('#msn').focus();
      }else if (!$mfggate) {
        $('#mfggate').focus();
      }
    });

    $('#tbFleet ').on( 'click','tbody tr button.btnRemove',function(){
      tableFleet
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

    ////=============================
    // INISTIALISASI TABLE PART NUMBER
    $('#btnPN').on('click', function(e){
      $ata = $('#form-pn #ata').val();
      $part_number = $('#form-pn #part_number').val();
      $description = $('#form-pn #description').val();
      $ess = $('#form-pn #ess').val();
      $mel = $('#form-pn #mel').val();
      $sla = $('#form-pn #sla').val();
      $day_sla = $('#day_sla').val();
      $tur = $('#form-pn #tur').val();
      $slc = $('#form-pn #slc').val();
      $tsl = $('#form-pn #tsl').val();

      if($ata && $part_number && $description && $ess && $mel && $sla && $tur && $slc && $tsl){
        $data = {
          '0' : $ata,
          '1' : $part_number,
          '2' : $description,
          '3' : $ess,
          '4' : $mel,
          '5' : $sla+" "+$day_sla,
          '6' : $tur,
          '7' : $slc,
          '8' : $tsl,
          '9' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> '
        };
        addRow(tablePartNumber, $data);

        $("#form-pn :input").val("");
        $('#day_sla').val("hours");
        $('#ata').focus();
      }else if(!$ata){
        console.log("2");
        $('#ata').focus();
      }else if(!$description){
        console.log("3");
        $('#description').focus();
      }else if(!$ess){
        console.log("4");
        $('#ess').focus();
      }else if(!$mel){
        console.log("5");
        $('#mel').focus();
      }else if(!$sla){
        console.log("6");
        $('#sla').focus();
      }else if(!$tur){
        $('#tur').focus();
      }else if(!$slc){
        $('#slc').focus();
      }else if(!$tsl){
        $('#tsl').focus();
      }
    });

    $('#tbPartNumber ').on( 'click','tbody tr button.btnRemove',function(){
      tablePartNumber
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

    ////=============================
    // INISTIALISASI TABLE DELIVERY
    $('#btnDeliv').on('click', function(e){
      $('#form-deliv #code').focus();
      $code = $('#form-deliv #code').val();
      $address = $('#form-deliv #address').val();
      //reset form input after click add button
      $('#form-deliv #code').val("");
      $('#form-deliv #address').val("");

      if ($code && $address) {
          $data = {
            '0' : $code,
            '1' : $address,
            '2' : '<button class="btn btn-danger btn-sm btnRemove" type="button"><i class="fa fa-remove"></i></button> '
          };
          addRow(tableDeliv, $data);
      }else if (!$code) {
        $('#code').focus();
      }else if (!$address) {
        $('#address').focus();
      }
    });

    $('#tbDeliv ').on( 'click','tbody tr button.btnRemove',function(){
      tableDeliv
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

  });

  function removeRow(a){

  }
  function addRow($table, data){
    ($table).row.add(data ).draw();
  }
</script>
