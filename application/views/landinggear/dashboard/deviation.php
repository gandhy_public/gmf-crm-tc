  <section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <?php $this->load->view($header_menu); ?>
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
          <?php
          $this->load->view($title_menu);
          ?>
          <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
              <div class="box box-primary">
                <div class="box-body">
                  <div id="chartdiv3"  style="width: 100%; height: 400px;"></div>
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                      <div class="box-body table-responsive no-padding">
                         <table id="tbdevpoint" class="table table-striped table-bordered" cellspacing="0" width="100%">
                          <thead style="background-color: #009973; color:#ffffff;">
                            <tr>
                                <th rowspan="2">No.</th>
                                <th rowspan="2">Dev No</th>
                                <th colspan="3">TAT</th>
                                <th colspan="2">Gate</th>
                                <th colspan="3">Deviation Details</th>
                                <th rowspan="2">Root Cause</th>
                                <th rowspan="2">Corrective Action</th>
                                <!-- <th rowspan="2">Created By</th> -->
                                <th rowspan="3">Created Date</th>
                              </tr>
                              <tr>
                                <th>Eng TAT</th>
                                <th>Cus TAT</th>
                                <th>Prc TAT Increase</th>
                                <th>Current Gate</th>
                                <th>Next Gate Eff</th>
                                <th>Dev Reason</th>
                                <th>Prc Delayed</th>
                                <th>Department</th>

                              </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 0;
                            if (is_array($listDeviation)) {
                             foreach ($listDeviation as $row) {
                            $no++;
                             ?>
                             <tr>
                               <td><?= $no ?></td>
                               <td><?= $row->DEV_NUMBER ?></td>
                               <td><?= $row->DELAY_TAT ?></td>
                               <td><?= $row->DELAY_CUSTOMER ?></td>
                               <td><?= $row->CG_DELAY_PROCESS ?></td>
                               <td><?= $row->CURRENT_GATE ?></td>
                               <td><?= $row->NEXT_GATE_EFFCTD ?></td>
                               <td><?= $row->DEVIATION_REASON ?></td>
                               <td><?= $row->PROCESS_DELAYED ?></td>
                               <td><?= $row->DEPARTEMENT ?></td>
                               <td><?= $row->ROOT_CAUSE ?></td>
                               <td><?= $row->CORECTIVE_ACTION ?></td>
                               <!-- <td><?= $row->CREATED_BY ?></td> -->
                               <td><?php 
                                   if ($row->CREATED_DATE=='00000000') {
                                      echo '-';
                                   }else{
                                      echo date_create('d-m-Y', strtotime($row->CREATED_DATE));
                                   }
                                ?></td>
                             </tr>

                            <?php }} ?>
                          </tbody>         
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
</section>

<script src="<?php echo base_url(); ?>assets/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
   tinymce.init({
        selector: "textarea",
        theme: "modern",
        height: 400,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

    });
</script>
<!-- <script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/amcharts.js/serial.js"></script>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script type="text/javascript">
  var dataProvider = <?= $count_gate ?>;
  AmCharts.makeChart("chartdiv3", {
  "type": "serial",
  "theme": "light",
  "categoryField": "gate",
  "rotate": true,
  "startDuration": 1,
  "categoryAxis": {
    "gridPosition": "start",
    "position": "left"
  },
  "trendLines": [],
  "graphs": [
    {
      "balloonText": "Deviation Total [[category]]:[[value]]",
      "fillAlphas": 0.8,
      "id": "AmGraph-1",
      "lineAlpha": 0.2,
      "title": "Deviation Total",
      "type": "column",
      "valueField": "total"
    }
  ],
  "colors": [
            "#5a5a72",
          ],
  "guides": [],
  "valueAxes": [
    {
      "id": "ValueAxis-1",
      "position": "bottom",
      "axisAlpha": 0,
      "title": "Deviation Total"
    }
  ],
  "allLabels": [],
  "balloon": {},
  "titles": [],
  "dataProvider": dataProvider,
  "export": {
    "enabled": true
   }

});
  
  
</script>

