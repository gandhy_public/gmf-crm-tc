<?php foreach($posts as $post){ ?>
<div class="liya">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    <div class="stat">
      <div class="content-top-1">
        <div class="col-md-6 top-content">
          <h5><?php echo $post->EARTX ?></h5>
          <label><a href='<?php echo base_url('index.php/Landing_gear/project_list').'/'.$post->ACREG_REVTX ?>'><b><?php echo $post->ACREG_REVTX ?> </b></a></label>
        </div>
        <div class="col-md-6 top-content">
          <div class="progress-group">
            <span class="progress-text">Project Completion</span>
            <span class="progress-number"><b><?php echo (!$post->JUMLAH)? 0 : $post->JUMLAH ; ?></b>/<?php echo ($post->JUMLAH + $post->JUMLAH_PROGRESS) ?></span>
            <div class="progress sm">
              <div class="progress-bar progress-bar-yellow" style="width: <?php echo (($post->JUMLAH / ($post->JUMLAH + $post->JUMLAH_PROGRESS)))*100?>%"></div>
            </div>
          </div>
          <span class="label label-success" style="font-size: 11px;">MSN : <?php echo $post->SERGE ?></span>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>