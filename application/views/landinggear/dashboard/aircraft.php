<style type="text/css">
  .ajax-load{
  /*background: #e1e1e1;*/
    padding: 10px 0px;
    width: 100%;
  }
 .stat {
    padding-right: 0;
}
 
  .stat {
    /*width: 50%;*/
    padding-left: 0;
    padding-right: 0;
  }
  /*.stat {
    width: 100%;
    padding-left: 0;
    padding-right: 0px;
  }*/
  .content-top-1 {
    background-color: #fff;
    padding: 1em;
    margin-bottom: 1em;
    border: 1px solid #ebeff6;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -o-border-radius: 0px;
    -moz-border-radius: 0px;
    -ms-border-radius: 0px;
    -webkit-box-shadow: 0px 0px 5px -2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 5px -2px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 5px -2px rgba(0,0,0,0.75);
    height: 110px;
}
.content-top-1:nth-child(3) {
    margin-bottom: 1em;
  }
.content-top-1:nth-child(3), .content-top:nth-child(3) {
  margin-bottom: 0;
}
.top-content a i.fa{
  color: #fff;
  font-size: 35px;
}
.top-content h5 {
    font-size: 1.1em;
    margin-top: 18px;
    color: #777;
}
.top-content label {
    font-size: 1.9em;
    color: #333;
}
.col-md-6.top-content {
    float: left;
  }
  .col-md-6.top-content {
    float: left;
    width:50%;
  }
</style>
<section class="content-header">
    <h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <section class="content">
      <div class="input-group input-group-sm col-sm-3 pull-right">
        <input type="text" class="form-control" name="search_data" id="search_data">
          <span class="input-group-btn">
            <button type="button" id="go_search" class="btn btn-info btn-flat">Search</button>
          </span>
      </div>
   <!--  <div class="box box-solid">
      <div class="box-header">
        <div class="input-group input-group-sm col-sm-3 pull-right">
        <input type="text" class="form-control" name="search_data" id="search_data">
         <span class="input-group-btn">
            <button type="button" id="go_search" class="btn btn-info btn-flat">Search</button>
          </span>
        </div>
      </div>
      <div class="box-body">
        <div class="col-lg-12 col-xs-12 col-md-12">
          <div class="col-md-12" id="post-data">
     
          </div>
        </div>
      </div>
    </div> -->
    <br><br><br>
    <div class="col-md-12" id="post-data">
    </div>
  </section>
  <!-- <div class="ajax-load" style="text-align: center">
      <div id="stop"> No more records found </div>
      <button class="btn" id="load_more" data-val = "0">Load more..<img style="display: none" id="loader" src="http://demo.itsolutionstuff.com/plugin/loader.gif"> </button></div> -->
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function() {
    loadMoreData('');
    $( "#search_data" ).on('keyup', function (e) {
    if (e.keyCode == 13) {
        var page = '';
      $('#load_more').data('val', 0);
      $( "div" ).remove( ".liya" );
      $( "div" ).remove( "#stop" );
      $('#load_more').show();
      var input_data = $('#search_data').val();
      loadMoreData(page, input_data);
    }
      
    });
    $("#load_more").click(function(e){
      e.preventDefault();
      var page = $(this).data('val');
      var input_data = $('#search_data').val();
      // getcountry(page);
      loadMoreData(page, input_data);
    });
  });
  // var page = 1;
  // $(window).scroll(function() {
  //     if($(window).scrollTop() + $(window).height() >= $(document).height()) {
  //         // var nextPage = parseInt($('#pageno').val());
  //         page++;
  //         loadMoreData(page);
  //     }
  // });
  



function loadMoreData(page='', input_data=''){
  // alert(page);
  var data = { 'page': page ,
              'input_data' : input_data }
  $.ajax(
        {
            url: '<?= site_url("Landing_gear/data_aircraft") ?>',
            type: 'POST',
            // dataType: 'json',
            data : data,
            beforeSend: function()
            {   $("#stop").hide();
                $('#loader').show();

            }
        })
        .done(function(data)
        {   
           // alert(data);
            if(data == "null"){
                // $('.ajax-load').html("No more records found");
                $('#stop').show();
                $("#loader").hide();
                $('#load_more').hide();
                return;
            }
            // $('.ajax-load').hide();
            $("#post-data").append(data);
            $("#stop").hide();
            $("#loader").hide();
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
            // var nextPage = parseInt($('#pageno').val())+1;
            // $('#pageno').val(nextPage);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
              alert('server not responding...');
        });
        
}
var scroll  = function(){
  $('html, body').animate({
  scrollTop: $('#load_more').offset().top
  }, 1000);
};
</script>

