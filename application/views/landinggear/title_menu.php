<h4 align="center">Project No. <?php echo $docno; ?></h4>
	<!-- <u><?php //echo $title; ?></u> -->
<div class="row">
	<div class="col-lg-12 col-xs-12 col-md-12">
	    <div class="box box-primary">
		    <table  class="stripe row-border" width="100%">
				<tbody>
					<col width="15%">
					<col width="5%">
					<col width="30%">
					<col width="15%">
					<col width="5%">
					<col width="30%">
					<tr>
						<td style="padding-left:20px;"><b>A/C Registration</b></td>
						<td>:</td>
						<td><?php echo $listProject['ACREG_REVTX'] ?></td>
						<td><b>Customer</b></td>
						<td>:</td>
						<td><?php echo $listProject['COMPANY_NAME'] ?></td>
					</tr>

					<tr>
						<td style="padding-left:20px;"><b>Workscope</b></td>
						<td>:</td>
						<td><?php echo $listProject['REVTY'] ?></td>
						<td><b>A/C Type</b></td>
						<td>:</td>
						<td><?php echo $listProject['EARTX'] ?></td>
					</tr>

					<tr>
						<td style="padding-left:20px;"><b>Project Start Date</b></td>
						<td>:</td>
							<td>
								<?php
								$source = $listProject['UDATE_SMR'];
								$date = new DateTime($source);
								echo $date->format('d-m-Y'); // 31-07-2012
								?>
							</td>
						<td><b>Contractual TAT</b></td>
						<td>:</td>
						<td><?php echo $listProject['AGREED_TAT'] ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
