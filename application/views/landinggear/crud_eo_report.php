<section class="content-header">
	<h1>
	<?php echo $project_name; ?>
	
	</h1>
	<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-edit"></i> TC Services Support</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/administrator/services/landinggear/index">Landing As</a></li>
		<li class="active"><?php echo $landing_gear; ?> </li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<!-- /.Tab Pane Overview -->
					<li><a class="fa fa-dashboard" href="<?php echo base_url(); ?>index.php/administrator/projects/landinggear_tabs/"> Overview</a></li>
					<!-- /.Tab Pane Profit Analysis -->
					<li><a class="fa fa-flag" href="<?php echo base_url(); ?>index.php/administrator/services/landinggear/crud_eo_report"> Profit Analysis</a></li>
					<!-- /.Tab Pane EO Report -->
					<li><a class="active fa fa-database" href="<?php echo base_url(); ?>index.php/administrator/services/landinggear/crud_eo_report"> EO Report</a></li>
					<!-- /.Tab Pane CSI Survey -->
					<li><a class="fa fa-space-shuttle" href="<?php echo base_url(); ?>index.php/administrator/projects/crud_mdr/"> data-toggle="tab"> CSI Survey</a></li>					
				</ul>
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<?php echo $output; ?>
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>
	<div class="modal fade mymodal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content"></div>
		</div>
	</div>
</section>