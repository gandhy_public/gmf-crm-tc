<section class="content-header">
  	<h1>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h1>
    <?php $this->load->view($link_directory); ?>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<!-- <?php //$this->load->view($nav_tabs); ?> -->
				<!-- /.Grocery CRUD -->
				<div class="tab-content">
					<section class="content">
						<div class="box-header">
              <!-- <h3 class="box-title"><?php //echo $title; ?></h3> -->
              <div class="box-tools">
              </div>
            </div>
            <div class="box-body no-padding">
			          <table id="tbCategory" class="table table-bordered table-hover table-striped" >
                  <thead style="background-color: #3c8dbc; color:#ffffff;">
                    <tr>
                      <th><center>ID CUSTOMER</center></th>
                      <th><center>COMPANY NAME</center></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div>
					</section>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
		<!-- /.col -->
	</div>

</section>
<script>

var table;

  $(function () {
    var tbContract = $('#tbCategory').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: "<?=base_url('index.php/api/customers/ListCustomer/')?>",
            type: "POST",
            complete: function(json, type) {
              queryListContract = json.responseJSON.query;
            }
        },
        columns: [       
            {data: 'id', name: 'id',orderable: false},
            {data: 'name', name: 'name',orderable: false}
        ],
        dom: 'Blfrtip',
        buttons: []
    });
  });
</script>
